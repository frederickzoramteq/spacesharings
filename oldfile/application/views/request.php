<?php include "header.php";?>
<div class="container">
    <div class="row">
      <div class="col-lg-4 col-md-4">
	  </br>
	  </br>
	  </br>
	  </br>
	  <img src="<?php echo base_url('assets/images/faq2.png');?>">
	  
	  </div>
 <div class="col-lg-8 col-md-4">
		  <div class="sidebar_wrapper">
			<div class="widget advance_search_wrapper">		
				<div class="widget-title"><h4>HOST & PROPERTY INFORMATION</h4></div>
				<div class="search_form">
					<form name="requestfrom" id="requestfrom">
	              	                        <div class="row">
							<div class="col-lg-6 col-md-6 form-group">
								<input type="text" class="" placeholder="Your Name*" id="rname" maxlength="50" name="name"> 
								<span class="error" style="color:red" id="error_rname"></span>
							</div>
						
						        <div class="col-lg-6 col-md-6 form-group">
								<input type="email" class="" placeholder="Email Address*" id="rmail" name="email">
								<span class="error" style="color:red" id="error_rmail"></span>
							    </div>
                               </div>

                				 <div class="row">
                 
                                             <div class="col-lg-4 col-md-4 form-group">
                 
                                                 <select id="countryId" name="country" class="orderby form-control select2 required countries">
                 
                                                 <option value="231" selected>United States</option>
                 
                                                 </select>
                 
                                                 <span class="error" style="color:red" id="error_message_afcountry"></span>
                 
                                             </div>
                 
                                             <div class="col-lg-4 col-md-4 form-group">
                 
                                                 <select name="state" class="orderby form-control select2 required states" id="stateId">
                 
                                                     <option value="">Select State</option>
                 
                                                 </select>
                 
                                                 <span class="error" style="color:red" id="error_message_afstate"></span>
                 
                                             </div>

                                         <div class="col-lg-4 col-md-4 form-group">
                 
                                             <select name="city" class="orderby form-control select2 required cities" id="cityId">
                 
                                             <option value="">Select City</option>
                 
                                             </select>
                 
                                             </div>
                 
                                     </div>
                 
                                   
                 
                                    
                                        
                                        
                                                <div class="row">
							<div class="col-lg-12 col-md-6 form-group">
						        <input type="text" class="" placeholder="Subject*" id="rsub" name="subject"> 
						        <span class="error" style="color:red" id="error_rsub"></span>
							</div>
						</div>

						


						<div class="row">
							<div class="col-lg-12 col-md-12 form-group">
							<textarea rows="7" class="" placeholder="Describe your rental requirements*" id="rmsg" name="message"></textarea>
							<span class="error" style="color:red" id="error_rmsg"></span>
							</div>
						</div>
					

						
							
				            <div id="msg-request" class="error"></div>
				            
							<div class="btn_wrapper1">
							<button type="submit" class="btn rs_btn" id="rqs_submit">Submit Request</button>
							</div>
						
	                        </form>

				</div>
			</div>
			
		  </div>
	  </div>
</div>
</div>
<?php include "footer.php";?>