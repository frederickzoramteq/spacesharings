<?php include "header.php";?>
<!--<div class="page_wrapper" data-stellar-background-ratio=".5">
	<div class="banner_overlay"> 
		<div class="container"> 
			<div class="col-lg-12"> 
				<div class="row"> 
					<div class="page_title">
					  <h3>contact page template</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="breacrumb_wrapper">
	<div class="container">
		<div class="row">
		<div class="col-lg-12">
			<ol class="breadcrumb">
			  <li><a href="index.html">Home</a></li>
			  <li class="active">contact us</li>
			</ol>
		</div>
		</div>
	</div>
</div>-->
<div class="contactus_wraper">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="rs_heading_wrapper">
					<div class="rs_heading">
						<h3>get in touch with us</h3>
						
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="contact_section">
				<div class="col-lg-8 col-md-8">
                <form name="contactform" method="post" action="">
					<div class="contactus_form_wraper">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="YOUR NAME" id="uname" name="username"> 
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding_left">
							<div class="form-group">
								<input type="email" class="form-control" placeholder="EMAIL ADDRESS" id="umail" name="useremail"> 
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding_right">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="SUBJECT" id="sub" name="useresubject"> 
							</div>
						</div>
						<div class="form-group"><textarea rows="11" class="form-control" placeholder="YOUR MESSAGE" id="msg" name="mesg"></textarea></div>
						<div class="btn_wrapper1">
							<button type="submit" class="btn rs_btn" id="rs_submit">submit</button>
						</div>
					</div>
                </form>
				</div>
				<div class="col-lg-4 col-md-4">
					<div class="contact_detail_wrapper">
						<h6>Quick Contact</h6>
						<ul>
							<li>
								<i class="flaticon-pin56"></i><p>502, Greenscape Technocity, Millenium Business Park, Mahape, Navi Mumbai - 400 701</p>
							</li>
							<li>
								<i class="flaticon-phone41"></i><p>(+1) 123 456 789</p>
							</li>
							<li>
								<i class="flaticon-print47"></i><p>(+1) 123 789 102</p>
							</li>
							<li>
								<i class="flaticon-mail80"></i><p><a href="#">info@popothemes.com</a></p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--<div class="contact_map">
	<div id="real_map"></div>
</div>-->
<?php include "footer.php";?>