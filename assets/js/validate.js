//created by gopal
$(document).ready(function(){
$('#error_message_fname').hide();
$('#error_message_lname').hide();
$('#error_message_st_add1').hide();
$('#error_message_st_add2').hide();
$('#error_message_city').hide();
$('#error_message_state').hide();
$('#error_message_zip').hide();
$('#error_message_country').hide();
$('#error_message_phone').hide();
$('#error_message_email').hide();
$('#error_message_cemail').hide();
$('#error_message_password').hide();
$('#error_message_cpassword').hide();
 var error_fname = false;
 var error_lname = false;
 var error_st_add1 = false;
 var error_st_add2 = false;
 var error_city = false;
 var error_state = false;
 var error_zip = false;
 var error_country = false;
 var error_phone = false;
 var error_email = false;
 var error_cemail = false;
 var error_password = false;
 var error_cpassword = false;
           
   $('#register_userfname').focusout(function () {
                  check_fname();
            }); 
   $('#register_userlname').focusout(function () {
                  check_lname();
            }); 
   $('#register_st_add1').focusout(function () {
                  check_st_add1();
            });
//   $('#register_st_add2').focusout(function () {
//                   check_st_add2();
//             });
   $('#register_city').focusout(function () {
                  check_city();
            });
   $('#register_state').focusout(function () {
                  check_state();
            });
   $('#register_zip').focusout(function () {
                  check_zip();
            });
   $('#register_country').focusout(function () {
                  check_country();
            }); 
   $('#register_phone').focusout(function () {
                  check_phone();
            });
   $('#register_email').focusout(function () {
                  check_email();
            }); 
   $('#register_cemail').focusout(function () {
                  check_cemail();
            });

   $('#register_password').focusout(function () {
                  check_password();
                  check_cpassword();
            });
   $('#register_password_confirm').focusout(function () {
                  check_cpassword();
                  check_password();
            });


      function check_fname() {
        var fname = $('#register_userfname').val();
                  var user_lenth = $('#register_userfname').val().length;
                  if (user_lenth < 1 || user_lenth > 20 ) {
                        $('#error_message_fname').html('*Should be between 1-20 character');
                        $('#error_message_fname').fadeIn();
                        error_fname = true;
                        console.log('error_fname='+error_fname);
                      }
                  else {
                        $('#error_message_fname').fadeOut();
                        error_fname = false;
                  }
            }   
      function check_lname() {
                  var user_lenth = $('#register_userlname').val().length;
                  if (user_lenth < 2 || user_lenth > 20) {
                        $('#error_message_lname').html('*Should be between 1-50 character');
                        $('#error_message_lname').fadeIn();
                        error_lname = true;
                  }
                  else {
                        $('#error_message_lname').fadeOut();
                          error_lname = false;
                  }
            }
  function check_st_add1() {
                  var st_add1 = $('#register_st_add1').val();
                  if (st_add1=="") {
                        $('#error_message_st_add1').html('*This is a required field');
                        $('#error_message_st_add1').fadeIn();
                        error_st_add1 = true;
                  }
                  else {
                        $('#error_message_st_add1').fadeOut();
                        error_st_add1= false;
                  }
            } 
  function check_st_add2() {
                  var st_add2 = $('#register_st_add2').val();
                  if (st_add2=="") {
                        $('#error_message_st_add2').html('*This is a required field');
                        $('#error_message_st_add2').fadeIn();
                        error_st_add2 = true;
                  }
                  else {
                        $('#error_message_st_add2').fadeOut();
                          error_st_add2 = false;
                  }
            }
  function check_city() {
                  var city = $('#register_city').val();
                  if (city=="") {
                        $('#error_message_city').html('*This is a required field');
                        $('#error_message_city').fadeIn();
                        error_city = true;
                  }
                  else {
                        $('#error_message_city').fadeOut();
                        error_city = false;
                  }
            }
  function check_state() {
                  var state = $('#register_state').val();
                  if (state=="") {
                        $('#error_message_state').html('*This is a required field');
                        $('#error_message_state').fadeIn();
                        error_state = true;
                  }
                  else {
                        $('#error_message_state').fadeOut();
                        error_state = false;
                  }
            }
  function check_zip() {
                  var zip = $('#register_zip').val();
                  if (zip=="") {
                        $('#error_message_zip').html('*This is a required field');
                        $('#error_message_zip').fadeIn();
                        error_zip = true;
                  }
                  else {
                        $('#error_message_zip').fadeOut();
                        error_zip = false;
                  }
            }
  function check_country() {
                  var country = $('#register_country').val();
                  if (country=="") {
                        $('#error_message_country').html('*This is a required field');
                        $('#error_message_country').fadeIn();
                        error_country = true;
                  }
                  else {
                        $('#error_message_country').fadeOut();
                         error_country = false;
                  }
            }
  function check_phone() {
                  var phone = $('#register_phone').val();
                  if (phone=="") {
                        $('#error_message_phone').html('*This is a required field');
                        $('#error_message_phone').fadeIn();
                        error_phone = true;
                  }
                  else {
                        $('#error_message_phone').fadeOut();
                        error_phone = false;
                  }
            }

  function check_email() {
                  var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);

                  if (pattern.test($('#register_email').val())) {
                        $('#error_message_email').hide();
                         error_email = false;
                  }
                  else {

                        $('#error_message_email').html('*Invalid email address');
                        $('#error_message_email').show();
                        error_email = true;
                        console.log('error_email='+error_email);
                  }
            }  
  function check_cemail() {
                  var chkcemail = $('#register_cemail').val();
                  var email = $('#register_email').val();
                  var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);

                  if (pattern.test($('#register_cemail').val()) && chkcemail == email) {
                        $('#error_message_cemail').hide();
                        error_cemail = false;
                  }
                  else if (chkcemail=="") {
                            $('#error_message_cemail').html("*This is a required field");
                            $('#error_message_cemail').show();
                            error_cemail = true;
                            console.log("error_cemail= blanck");
                      }
                     else if(chkcemail != email){

                        $('#error_message_cemail').html("Email doesn't match");
                        $('#error_message_cemail').show();
                        error_cemail = true;
                        console.log("error_cemail!=email");
                      }
                      else if(chkcemail =="undefined"){

                        $('#error_message_cemail').html("Email is undefined");
                        $('#error_message_cemail').show();
                        error_cemail = true;
                        console.log("error_cemail= undefine");
                      }
                      else {

                        $('#error_message_email').html('*Invalid email address');
                        $('#error_message_email').show();
                        error_email = true;
                        console.log("error_cemail= invalid");
                      }
                  }
                  
            


      function check_password() {
            var pass = $('#register_password').val();
            if(pass==""||pass=='undefined'){
              $('#error_message_password').html('*Password is a required field');
              $('#error_message_password').show();
              error_password = true;
              console.log('error_password='+error_password);
            }
            else{
              var chkout = checkStrength(pass);
               $('#error_message_password').html(chkout);
               $('#error_message_password').show();
               error_password = false;

              }
                    
            }
            function checkStrength(password){

                //strength at begining

                var strength = 0;

                //when password is less than 6 means

                if (password.length<4) {
                $('#error_message_password').removeClass();
                $('#error_message_password').addClass('short');
                return 'Too short';
                }
                // when password length is 8 or more

                if (password.length>7) strength += 1;

                //when password contain both uppercase and lowercase character means

                if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1;

                //when password contain both character and number means

                if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength += 1;

                //when password contain one special character means

                if (password.match(/([!,%,&amp;,@,#,$,^,*,?,_,~])/)) strength += 1;


                //when password two or more special character means

                if (password.match(/(.*[!,%,&amp;,@,#,$,^,*,?,_,~].*[!,%,&amp;,@,#,$,^,*,?,_,~])/)) strength += 1;


                //now we have calculated strength value, we can return messages


                //if value is less than 2

                if (strength<2 ) {
                $('#error_message_password').removeClass();
                $('#error_message_password').addClass('weak');
                return 'Weak';
                } 
                else if (strength == 2 ) {
                $('#error_message_password').removeClass();
                $('#error_message_password').addClass('good');
                return 'Good';
                } 
                else {
                $('#error_message_password').removeClass();
                $('#error_message_password').addClass('strong');
                return 'Strong';
                }
              
                }
        function check_cpassword() {
              var pass = $('#register_password').val();
              var cpass = $('#register_password_confirm').val();
              if(cpass==""||cpass=="undefined"){
                        $('#error_message_cpassword').html("Password needs to be re-entered");
                            $('#error_message_cpassword').show();
                            error_cpassword = true;
                             console.log('error_cpassword= blank');
                      }
                      else if (cpass!= pass) {
                            $('#error_message_cpassword').html("Password doesn't match");
                            $('#error_message_cpassword').show();
                            error_cpassword = true;
                             console.log('error_cpassword='+error_cpassword);

                      }

                      else {
                            $('#error_message_cpassword').hide();
                            error_cpassword = false;
                      }

                }

$('#register-submit').click(function() {
  var register_userfname = $('#register_userfname').val();
  var register_userlname = $('#register_userlname').val();
  var register_st_add1 = $('#register_st_add1').val();
  var register_st_add2 = $('#register_st_add2').val();
  var register_city = $('#register_city').val();
  var register_state = $('#register_state').val();
  var register_zip = $('#register_zip').val();
  var register_country = $('#register_country').val();
  var register_phone = $('#register_phone').val();
  var register_email = $('#register_email').val();
  var register_cemail = $('#register_cemail').val();
  var register_password = $('#register_password').val();
   var register_cpassword = $('#register_password_confirm').val();

  console.log('register_userfname='+register_userfname,
    '<br>register_email='+register_email,
    '<br>register_cemail='+register_cemail,
    '<br>register_cpassword='+register_password,
    '<br>register_cpassword='+register_cpassword

    );
  // return false;

   // var error_fname = false;
   // var error_lname = false;
   // var error_st_add1 = false;
   // var error_st_add2 = false;
   // var error_city = false;
   // var error_state = false;
   // var error_zip = false;
   // var error_country = false;
   // var error_phone = false;
   // var error_email = false;
   // var error_cemail = false;
   // var error_password = false;
   // var error_cpassword = false;

  check_fname();
  check_lname();
  check_st_add1();
  check_st_add2();
  check_city();
  check_state();
  check_zip();
  check_country();
  check_phone();
  check_email();
  check_cemail();
  check_password();
  check_cpassword();
// console.log(error_fname,error_email,error_password);
console.log(error_fname,error_lname,error_st_add1 ,error_st_add2 ,error_city  ,error_state  ,error_zip  ,error_country  ,error_phone  ,error_email  ,error_cemail  ,error_password,error_cpassword);
var dataString ='action='+'create_user'+
                '&register_userfname='+register_userfname+
                '&register_userlname='+register_userlname+
                '&register_st_add1='+register_st_add1+
                '&register_st_add2='+register_st_add2+
                '&register_city='+register_city+
                '&register_state='+register_state+
                '&register_zip='+register_zip+
                '&register_country='+register_country+
                '&register_phone='+register_phone+
                '&register_email='+register_email+
                '&register_password='+register_password;
                

if(error_fname==false && error_lname==false && error_st_add1==false && error_city==false&& error_state==false&& error_zip==false&& error_country==false&& error_phone==false&& error_email==false&& error_cemail==false&& error_password==false&& error_cpassword==false){
  
  // console.log(register_userfname,register_userlname,register_st_add1,register_st_add2,register_city,register_state,register_zip,register_country,register_phone,register_email,register_password);
   // if(error_fname==false && error_email==false && error_password==false){

   $('#loader').show();
    $.ajax({
    type:'POST',
    url: ajaxurl,
    data: dataString,
    catch: false,
    dataType : 'json',
    success: function(response){
      if(response[0]=='fail'){
      $('#msg').addClass('strong').html(response[1]);
    }
    else if(response[0]=='success'){
    $('#msg').addClass('strong').html(response[1]);
    $("#register-form")[0].reset();
     window.setTimeout(function(){
        window.location.href = "http://www.spacesharings.com/user-login/";
    }, 5000);
    }
    else{
      $('#msg').addClass('strong').html('Oops! Something went wrong');
    }
      $('#loader').hide();
    },
    error: function (response) {
      console.log(response);
                      $('#msg').addClass('error').html(response);
                    $("#register-form")[0].reset();
                    $('#loader').hide();
                }

  });
  return false;
}
else{
  console.log("input fields are required");
  return false;
}

  });
  
  $('#log_btn_sub').click(function(){

 login_username = $('#login_username').val();
 login_password = $('#login_password').val();
 remember_me_checkbox = $('#remember_me_checkbox').val();
 if(login_username==""||login_username=="undefined"){
   $('#msg-log').addClass('error').html("*Username is required.");
 
  return false;
 }
 else if(login_password==""||login_password=="undefined"){
     $('#msg-log').addClass('error').html("*Password is required.");
  return false;
 }
 else{
  $('#msg-log').hide();
  $('#loader-log').show();
  var datas =   'login_username='+login_username+
                '&login_password='+login_password+
                '&remember_me_checkbox='+remember_me_checkbox;
       
    $.ajax({
    type:'POST',
    url: 'http://www.spacesharings.com/login',
    data: datas,
    catch: false,
    success: function(response){
      
      $('#msg-log').html(response);
      $('#msg-log').show();
      console.log(response);
      $('#loader-log').hide();
      if(response=='<h4 class="greencolor">Login successfull. Redirecting...!</h4>'){
      window.location = "http://www.spacesharings.com/advanced-form/";
      }
    },
    error: function (response) {
                      $('#msg-log').addClass('error').html(response);
                    $("#login-form")[0].reset()
                    $('#loader-log').hide();
                }

  });
  return false;

 }

});

 $('#rental_btn_sub').click(function(){

 login_username = $('#login_username').val();
 login_password = $('#login_password').val();
 remember_me_checkbox = $('#remember_me_checkbox').val();
 if(login_username==""||login_username=="undefined"){
   $('#msg-log').addClass('error').html("*Username is required.");
 
  return false;
 }
 else if(login_password==""||login_password=="undefined"){
     $('#msg-log').addClass('error').html("*Password is required.");
  return false;
 }
 else{
  $('#msg-log').hide();
  $('#loader-log').show();
  var datas =   'login_username='+login_username+
                '&login_password='+login_password+
                '&remember_me_checkbox='+remember_me_checkbox;
       
    $.ajax({
    type:'POST',
    // url: 'http://wp.goigi.biz/sharesapcenew/login',
    url: 'http://www.spacesharings.com/login',
    data: datas,
    catch: false,
    success: function(response){
      
      $('#msg-log').html(response);
      $('#msg-log').show();
      console.log(response);
      $('#loader-log').hide();
      if(response=='<h4 class="greencolor">Login successfull. Redirecting...!</h4>'){
      window.location = "http://www.spacesharings.com/rent/";
      }
    },
    error: function (response) {
                      $('#msg-log').addClass('error').html(response);
                    $("#rental-form")[0].reset()
                    $('#loader-log').hide();
                }

  });
  return false;

 }

});


});
