<?php
ob_start();
class BasicHosting extends CI_Controller
{
//============Constructor to call Model====================
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->model('Auth_model');
		$this->load->model('Generalmodel');
		$this->load->model('HostingModel');

		$this->load->library('email');
//$this->load->model('Newsletter_model');
		$this->load->library('encrypt');
		$this->load->helper('string');
		/* Load form helper */
		$this->load->helper(array('form'));
		/* Load form validation library */
		$this->load->library('form_validation');
		$this->load->model('CountryModel','country');
//****************************backtrace prevent*** END HERE*************************
	}

//============Constructor to call Model====================



	/*function add_hosting(){

		$date = date('Y-m-d h:i:s');
		$ip_address = $this->input->ip_address();
		$my_date = date("Y-m-d", time()); 
		$config = array(
			'upload_path' => "uploads/blog/",
			'upload_url' => base_url() . "uploads/blog/",
			'allowed_types' => "gif|jpg|png|jpeg",
    		'overwrite'    => FALSE
		);
		$this->load->library('upload', $config);
		//=====================+++++++++++++++++++++++===================
		//$this->form_validation->set_rules('fname','News Title', 'required');
		$this->form_validation->set_error_delimiters('<span class="label label-danger" style="font-size:11px;">', '</span>');
			//=====================+++++++++++++++++++++++===================
		if ($this->form_validation->run('basic_hostings') == FALSE) {

			$this->session->set_flashdata("success_msg", "<div class='alert alert-danger text-center'><b>Please enter the required field. !!</b></div>"); 
			//$data['success_msg'] = '';
			$this->load->view('header',$data);
			$data ['country']= $this->country->fetch_Country();
			$this->load->view('free-form',$data);
			$this->load->view('footer');
		//redirect('banner/addbanner',$data);
		}else{
			if (!$this->upload->do_upload('uploadFile[]')){
			//$data['success_msg'] = '<div class="alert alert-success text-center">You Must Select An Image File!</div>';
				$this->session->set_flashdata("success_msg", "<div class='alert alert-success text-center'>You Must Select An Image File!</div>");
				//print_r($_FILES['multipleUpload']);
				//$error = array('error' => $this->upload->display_errors());
				//print_r($error);
				$this->load->view('header',$data);
				$data ['country']= $this->country->fetch_Country();
				$this->load->view('free-form',$data);
				$this->load->view('footer');
			}else{
				$data['uploadFile'] = $this->upload->data();
				$filename=$data['uploadFile']['file_name'];
				$table_name = 'hosting';
				$id = '';

				$post_data = array(
					'id' => '',
					'host_type'=>'1',
					'fname' => $this->input->post('fname'),
					'lname' => $this->input->post('lname'),
					'phone' => $this->input->post('phone'),
					'email' => $this->input->post('email'),
					'image' => $filename,
					'description' => $this->input->post('desc'),
					'title' => $this->input->post('title'),
					'country' => $this->input->post('country'),
					'state' => $this->input->post('state'),
					'city' => $this->input->post('city'),
					'postcode' => $this->input->post('postcode'),
					'email_verified' => 'Yes',
					'createdate' => $date,
					'ip_address' => $ip_address,
					'status' => 'Yes'
				);

				$query = $this->HostingModel->insert($table_name,$post_data);

				$upload_data = $this->upload->data();
				//$data['success_msg'] = '<div class="alert alert-success text-center"> Data successfully inserted!!!</div>';
				$this->session->set_flashdata("success_msg", "<div class='alert alert-success text-center'> Data successfully inserted!!!</div>");
				$this->load->view('header',$data);
				$data ['country']= $this->country->fetch_Country();
				$this->load->view('free-form',$data);
				$this->load->view('footer');




			}


		}


	}*/

	function add_hosting()
	{
		$data['page']='Hostings';
		$date = date('Y-m-d h:i:s');
		$ip_address = $this->input->ip_address();
		$my_date = date("Y-m-d", time()); 
		$this->form_validation->set_error_delimiters('<span class="label label-danger" style="font-size:11px;">', '</span>');

			//=====================+++++++++++++++++++++++===================
		if ($this->form_validation->run('basic_hostings') == FALSE)
		{

			$this->session->set_flashdata("success_msg", "<div class='alert alert-danger text-center'><b>Please enter the required field !!</b></div>"); 
			//$data['success_msg'] = '';
			$data['page']='Hostings';
			$this->load->view('header',$data);
			$data ['country']= $this->country->fetch_Country();
			$this->load->view('free-form',$data);
			$this->load->view('footer');
		//redirect('banner/addbanner',$data);
		}
		else
		{
		//if($_POST){
			//$this->form_validation->set_error_delimiters('<span class="label label-danger" style="font-size:11px;">', '</span>');
			//for file upload
			$table_name = 'hosting';
			$id     = '';
			$post_data = array('id' => '',
							   'host_type'=>'2',
							   'fname' => $this->input->post('fname'),
							   'lname' => $this->input->post('lname'),
							   'phone' => $this->input->post('phone'),
							   'email' => $this->input->post('email'),
							   'description' => $this->input->post('desc'),
							   'title' => $this->input->post('title'),
							   'country' => $this->input->post('country'),
							   'state' => $this->input->post('state'),
							   'city' => $this->input->post('city'),
							   'postcode' => $this->input->post('postcode'),
							   'email_verified' => 'Yes',
							   'createdate' => $date,
							   'ip_address' => $ip_address,
							   'status' => 'Yes'
							);

			$prop_id = $this->HostingModel->insert_get_id_of_prop($table_name,$post_data);
			//echo $this->db->last_query(); exit();

			if(!empty($_FILES))
			{
			    $j = 1;                 
			    foreach($_FILES as $filekey=>$fileattachments)
			    {
			        foreach($fileattachments as $key=>$val)
			        {
			            if($key == 'name')
			            {
			                $val = $this->renameFileNames($val);
			            }

			            if(is_array($val))
			            {
			                $i = 1;
			                foreach($val as $v)
			                {
			                    $field_name = "multiple_".$filekey."_".$i;
			                    $_FILES[$field_name][$key] = $v;
			                    $i++;
			                }
			            }
			            else
			            {
			                $field_name = "single_".$filekey."_".$j;
			                $_FILES[$field_name] = $fileattachments;
			                $j++;
			                break;
			            }
			        }
			        // Unset the useless one 
			        unset($_FILES[$filekey]);
			    }

			    foreach($_FILES as $field_name => $file)
			    {
			        if(isset($file['error']) && $file['error']==0)
			        {
			            $config['upload_path'] = 'uploads/blog/';
			            $config['allowed_types'] = "gif|jpg|png|jpeg";
			            $config['max_size'] = 10000000;
			            $this->load->library('upload', $config);
			            $this->upload->initialize($config);

			            if (!$this->upload->do_upload($field_name))
			            {
			                $error = array('error' => $this->upload->display_errors());
			                $this->session->set_flashdata("success_msg", "<div class='alert alert-success text-center'>You Must Select An Image File!</div>");
			            }
			            else
			            {
			                $result=$this->HostingModel->insert_img_name($_FILES[$field_name]['name'], $prop_id);
							$this->session->set_flashdata("success_msg", "<div class='alert alert-success text-center'> Hosting/Renting Information was successfully uploaded!!!</div>");
						}
			        }
			        else
			        {
                        $file_name='noimg.png';
        			    $result=$this->HostingModel->insert_img_name($file_name, $prop_id);
        			    $this->session->set_flashdata("success_msg", "<div class='alert alert-success text-center'> Hosting/Renting Information was successfully uploaded!!!</div>");
			        }
			    }
			    
			    redirect('Page/basic_hosting');
			}
			//end of file upload system
			$this->load->view('header',$data);
			$data ['country']= $this->country->fetch_Country();
			$this->load->view('free-form',$data);
			$this->load->view('footer');
		}
	}
	
	
	
	private function renameFileNames($fileNames)
	{
	    $returnValue = array();
	    
	    foreach($fileNames as $fileName)
	    {
	        $extension = $this->getExtension($fileName);
	        $randomFileName = uniqid();
	        $newFileName = $randomFileName . $extension;
	        
	        $returnValue[] = $newFileName;
	    }
	    
	    return $returnValue;
	}
	
	private function getExtension($filename)
	{
	    $x = explode('.', $filename);
	    
	    if (count($x) === 1)
	    {
	        return '';
	    }
	    
	    $ext = ($this->file_ext_tolower) ? strtolower(end($x)) : end($x);
	    return '.'.$ext;
	}
}



?>
