<?php
ob_start();
class Gallery extends CI_Controller {
	function __construct() {
		parent::__construct();
			// $this->load->library(array('form_validation','session'));
		if($this->session->userdata('is_logged_in')!=1){
			redirect('supercontrol/home', 'refresh');
		}
		$this->load->model('supercontrol/gallery_model');
		$this->load->library('image_lib');

		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
		$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
		$this->output->set_header('Pragma: no-cache');
	}
	public function index()
	{
		if($this->session->userdata('is_logged_in')){
			redirect('supercontrol/galleryadd_view');
		}else{
			$this->load->view('supercontrol/main/login');
		}
	}


	public function addcategoryform(){

		$data['title'] = 'Add Category';
		$this->load->view('supercontrol/header',$data);
		$this->load->view('supercontrol/categoryadd_view',$data);
		$this->load->view('supercontrol/footer');
	}
	/*
		functiona name: add_gallerycategory
		Author: (Koushik Dutta)
		Usage: Add Images category wise into database
	*/
	public function add_gallerycategory(){
		$config = array(
			'upload_path' => "uploads/Gallery_category/",
			'upload_url' => base_url() . "uploads/Gallery_category/",
			'allowed_types' => "gif|jpg|png|jpeg"
		);
		//load upload class library
		$this->load->library('upload', $config);
		// Set Validation Rules
		$this->form_validation->set_rules('category_name','Category Name', 'trim|required');
		$this->form_validation->set_error_delimiters('<span class="label label-danger">', '</span>');
		if( $this->form_validation->run() && $this->upload->do_upload('userfile') ) {
			$data = $this->upload->data();
			// print_r($data);exit;
			// $image_path = base_url("uploads/member/" . $data['raw_name'] . $data['file_ext']);
			$filename = $data['raw_name'] . $data['file_ext'];
			// print_r($filename);
			$post_data = array(
				'category_name' => $this->input->post('category_name'),
				'gallery_cat_image' => $filename,
				'status' => 1
			);
			$this->gallery_model->insert_category($post_data);
			$this->session->set_flashdata('feedback', 'Album Added successfully!!!!');
			redirect('supercontrol/gallery/gallerycatlist');
		}else{
			$upload_error = $this->upload->display_errors();
			$this->load->view('supercontrol/header');
			$this->load->view('supercontrol/categoryadd_view',compact('upload_error'));
			$this->load->view('supercontrol/footer');
		}
	}
    //=======================Insert Page Data============

	public function gallerycatlist(){
		$query = $this->gallery_model->show_category();
		$data['category'] = $query;
		$data['title'] = 'Category List';
		$this->load->view('supercontrol/header',$data);
		$this->load->view('supercontrol/gallerycategorylist',$data);
		$this->load->view('supercontrol/footer');
	}


	public function show_category_id($id) {
		$id = $this->uri->segment(4);
		//exit();
		$data['title'] = "Edit Category";
		//Loading Database
		$this->load->database();
		//Calling Model
		$this->load->model('supercontrol/gallery_model');
		//Transfering data to Model
		$query = $this->gallery_model->show_category_id($id);
		$data['cat'] = $query[0];


		$this->load->view('supercontrol/header',$data);
		$this->load->view('supercontrol/category_edit', $data);
		$this->load->view('supercontrol/footer');
	}


	public function edit_category(){
		$data= $this->input->post();
		$config = array(
			'upload_path' => "uploads/Gallery_category/",
			'upload_url' => base_url() . "uploads/Gallery_category/",
			'allowed_types' => "gif|jpg|png|jpeg"
		);
        	//load upload class library
		$this->load->library('upload', $config);
		if ($_FILES['userfile']['name'] != '') {
			if (!$this->upload->do_upload('userfile')) {

				redirect('supercontrol/gallery/show_category_id/'.$data['id']);

			} else {
				$data1['userfile'] = $this->upload->data();
				$filename = $data1['userfile']['file_name'];
				$data = $this->input->post();
				$data['gallery_cat_image'] = $filename;
				$id= $data['catid'];



				$result = $this->gallery_model->category_edit($id, $data);
				if ($result) {
					$this->session->set_flashdata('add_message', 'Album Edited successfully!!!!');
				} else {
					$this->session->set_flashdata('add_message', 'Failed to edit Album !!!!');
				}
				redirect('supercontrol/gallery/gallerycatlist');
			}
		} else {

			$datalist = $this->input->post();
			$id = $this->input->post('catid');
			$data['title'] = "Category Edit";

			$this->load->database();

			$this->load->model('supercontrol/gallery_model');
			$query = $this->gallery_model->category_edit($id, $datalist);
			redirect('supercontrol/gallery/gallerycatlist');
		}
	}



	public function delete_category(){
		$this->load->database();
		$this->load->model('supercontrol/gallery_model');
		$id = $this->uri->segment(4);
		$this->gallery_model->delete_category($id);
		$data1['message'] = 'Data Deleted Successfully';
		redirect('supercontrol/gallery/successdeletecat');
	}

	public function successdeletecat(){
		$query = $this->gallery_model->show_category();
		$data['category'] = $query;
		$data['success_msg'] = '<div class="alert alert-success text-center"> <button class="close" aria-hidden="true" data-dismiss="alert" type="button" style="right:0;"></button>Data Deleted Successfully.....</div>';
		$this->load->view('supercontrol/header',$data);
		$this->load->view('supercontrol/gallerycategorylist',$data);
		$this->load->view('supercontrol/footer');
	}


	public function addgallery(){
		$data['title'] = "Gallery add";
		$this->load->view('supercontrol/header',$data);
		$this->load->view('supercontrol/galleryadd_view');
		$this->load->view('supercontrol/footer');
	}

	public function add_gallery(){
		$my_date = date("Y-m-d", time());
		$config = array(
			'upload_path' => "uploads/gallery/",
			'upload_url' => base_url() . "uploads/gallery/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf"
		);
		$this->load->library('upload', $config);
		if ($this->upload->do_upload("userfile")) {
			$data['img'] = $this->upload->data();
			$data['img']['file_name'];
			$data = array(
				'catname' => $this->input->post('catname'),
				'gallery_name' => $this->input->post('gallery_name'),
				'gallery_image' => $data['img']['file_name'],
				'gallery_status' => 1,
				'date' =>  $my_date
			);
			$this->gallery_model->insert_gallery($data);
			$data1['message'] = 'Data Inserted Successfully';
			redirect('supercontrol/gallery/success');
		}
		else{
			$data = array(
				'catname' => $this->input->post('catname'),
				'gallery_name' => $this->input->post('gallery_name'),
				'gallery_image' => $data['img']['file_name'],
				'gallery_status' => 1,
				'date' =>  $my_date
			);
			$this->gallery_model->insert_gallery($data);
			$data1['message'] = 'Data Inserted Successfully';
			redirect('supercontrol/gallery/success');
		}
	}
	public function success(){
		$this->load->database();
		$this->load->model('supercontrol/gallery_model');
		$query = $this->gallery_model->show_gallery();
		$data['gallery'] = $query;
		$data['title'] = "Gallery List";
		$data['success_msg'] = '<div class="alert alert-success text-center"> <button class="close" aria-hidden="true" data-dismiss="alert" type="button" style="right:0;"></button>Operation Successfully Done.....</div>';
		$this->load->view('supercontrol/header',$data);
		$this->load->view('supercontrol/showgallerylist');
		$this->load->view('supercontrol/footer');
	}


	public function show_gallery_id($id) {
		$id = $this->uri->segment(4);
		$data['title'] = "Edit Gallery";
		$this->load->database();
		$this->load->model('supercontrol/gallery_model');
		$query = $this->gallery_model->show_gallery_id($id);
		$data['ecms'] = $query;
		$query = $this->gallery_model->show_category();
		$data['category'] = $query;
		$this->load->view('supercontrol/header',$data);
		$this->load->view('supercontrol/gallery_edit', $data);
		$this->load->view('supercontrol/footer');
	}

	public function edit_gallery(){
		$config = array(
			'upload_path' => "uploads/gallery/",
			'upload_url' => base_url() . "uploads/gallery/",
			'allowed_types' => "gif|jpg|png|jpeg"
		);
		$this->load->library('upload', $config);
		if ($this->upload->do_upload("userfile")) {
			$data['img'] = $this->upload->data();
			$datalist = array(
				'catname' => $this->input->post('catname'),
				'gallery_name' => $this->input->post('gallery_name'),
				'gallery_image' => $data['img']['file_name']
			);
			$old_file = $this->input->post('old_file');
			$id = $this->input->post('gallery_id');
			$data['title'] = "Gallery Edit";
			$this->load->database();
			$this->load->model('supercontrol/gallery_model');
			$query = $this->gallery_model->gallery_edit($id,$datalist,$old_file);
			redirect('supercontrol/gallery/show_gallery');
		}else{
			$datalist = array(
				'catname' => $this->input->post('catname'),
				'gallery_name' => $this->input->post('gallery_name')
			);
			$old_file = $this->input->post('gallery_image');
			$id = $this->input->post('gallery_id');
			$data['title'] = "Gallery Edit";
			$this->load->database();
			$this->load->model('supercontrol/gallery_model');
			$query = $this->gallery_model->gallery_edit($id,$datalist,$old_file);
			redirect('supercontrol/gallery/show_gallery');
		}
	}

	public function show_gallery(){
		$this->load->database();
		$this->load->model('supercontrol/gallery_model');
		$query = $this->gallery_model->show_gallery();
		$data['gallery'] = $query;
		$data['title'] = "Gallery List";
		$this->load->view('supercontrol/header',$data);
		$this->load->view('supercontrol/showgallerylist');
		$this->load->view('supercontrol/footer');
	}


	public function delete_gallery($id){
		$this->load->database();
		$this->load->model('supercontrol/gallery_model');
		$id = $this->uri->segment(4);
		$result=$this->gallery_model->gallery_view($id);
		$gallery_image = $result[0]->gallery_image;
		$this->gallery_model->delete_gallery($id,$gallery_image);
		$data1['message'] = 'Data Deleted Successfully';
		redirect('supercontrol/gallery/successdelete');
	}


	public function successdelete(){

		$this->load->database();
		$this->load->model('supercontrol/gallery_model');
		$query = $this->gallery_model->show_gallery();
		$data['gallery'] = $query;
		$data['title'] = "Gallery List";
		$data['success_msg'] = '<div class="alert alert-success text-center"> <button class="close" aria-hidden="true" data-dismiss="alert" type="button" style="right:0;"></button>Operation Successfully.....</div>';
		$this->load->view('supercontrol/header',$data);
		$this->load->view('supercontrol/showgallerylist');
		$this->load->view('supercontrol/footer');
	}


	public function addform(){
		$query = $this->gallery_model->show_category();
		$data['category'] = $query;
		$data['title'] = "Add Gallery";

		$this->load->view('supercontrol/header',$data);
		$this->load->view('supercontrol/galleryadd_view',$data);
		$this->load->view('supercontrol/footer');
	}

	public function statusgallery ()
	{
		$stat= $this->input->get('stat');
		$id= $this->input->get('id');
		$this->load->model('supercontrol/gallery_model');
		$this->gallery_model->updt($stat,$id);
	}

	public function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('supercontrol/home', 'refresh');
	}
}
?>



