<?php
   ob_start();
   class Main extends CI_Controller{
   
   	function __construct() {
   
   		parent::__construct();
   
   		$this->load->database();
   
   		$this->load->model('supercontrol/Model_users');
   
   //****************************backtrace prevent*** START HERE*************************
   
   		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
   		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
   
   		$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
   
   		$this->output->set_header('Pragma: no-cache');
   
   		$this->load->library('session');
   
   //$this->load->helper('location_helper');
   
   //****************************backtrace prevent*** END HERE*************************
   
   	}
   
   	public function index(){
   		$this->login();
   
   	}
   
   	public function login() {
      		$data['title'] = "Spacesharing";
      		$this->load->view('supercontrol/headerlogin');
      		$this->load->view('supercontrol/login_view',$data);
      		$this->load->view('supercontrol/footerlogin');
   
   	}
   
   
   
   //=======================Form validation========================================
   
   //--------------================-------------
   
   	public function login_validation() {
   
   		$this->form_validation->set_rules('username','Username', 'required');
   
   		$this->form_validation->set_rules('password','Password', 'required');
   
   		if ($this->form_validation->run() == FALSE){
   
   			$this->session->set_flashdata('loginerror', 'Username and password can not be blank');
   
   			redirect('supercontrol/main/login');	
   
   		} else {
   
   			$data = array(
    				'username' => $this->input->post('username'),
   				'password' => $this->input->post('password')
   
   			);
   
   			$result = $this->Model_users->login($data);
   
   			if($result == TRUE) {
   
   				$username = $this->input->post('username');
   
   				if($result != false) {
   
   					$session_data = array(
   
   						'username'=>$this->input->post('username'),
   
   						'is_logged_in'=>1
   
   					);
   
   //print_r($session_data);
   
   // Add user data in session
   
   					$this->session->set_userdata('logged_in', $session_data);
   
   					$this->session->set_userdata('is_logged_in', '1');
   
   					redirect('supercontrol/Home');
   
   				}
   
   			}else {
   
   				$this->session->set_flashdata('loginerror', 'Invalid Username or Password!!!!');
   
   				redirect('supercontrol/main/login');	
   
   			}
   
   		}
   
   	}
   
   //--------------================--------------
   
   
   
   	public function reset_password(){
   
   		$this->form_validation->set_rules('old_password', 'Password', 'required');
   
   		$this->form_validation->set_rules('newpassword', 'New Password', 'required');
   
   		$this->form_validation->set_rules('re_password', 'Retype Password', 'required');
   
   		if($this->form_validation->run() == FALSE){
   
   			$this->session->set_flashdata('error', 'All Credentials are Required!!!!');
   
   			redirect('supercontrol/user/show_reset_pass');
   
   		}else{
   
   			$id=$this->input->post('id'); 
   
   			$query = $this->Model_users->checkOldPass(md5($this->input->post('old_password')),$id);
   
   			$data['oldpass'] = $query;
   
   //echo $this->db->last_query(); exit();
   
   			$count = count($query);
   
   			if($count=='1'){
   
   				$id=$this->input->post('id');
   
   				$query = $this->Model_users->saveNewPass(md5($this->input->post('newpassword')),$id);
   
   //print $this->db->last_query();
   
   //exit();
   
   				$this->session->set_flashdata('success', 'Password Changed Successfully!!!!');
   
   				redirect('supercontrol/user/show_reset_pass');
   
   			}else{
   
   				$this->session->set_flashdata('error', 'password can not be changed. Provide correct Credentials!!!!');
   
   				redirect('supercontrol/user/show_reset_pass');
   
   			}
   
   		}
   
   	}
   
   
   
   
   
   //==============================supercontrol reset Password======================================
   
   
   
   //==============================supercontrol Logout==============================================
   
   	public function logout(){
   
   		$this->session->sess_destroy();
   
   		redirect('supercontrol/main/login');
   
   	}
   
   //==============================supercontrol Logout==============================================
   
   }
   
   ?>