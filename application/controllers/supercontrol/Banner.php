<?php error_reporting(0);
class Banner extends CI_Controller {
		//============Constructor to call Model====================

		function __construct() {
			parent::__construct();
			$this->load->library(array('form_validation','session'));
			if($this->session->userdata('is_logged_in')!=1){
			redirect('supercontrol/home', 'refresh');
			}
			$this->load->model('supercontrol/banner_model');
			$this->load->library('image_lib');
			$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
            $this->output->set_header('Pragma: no-cache');

			//****************************backtrace prevent*** END HERE*************************

		}
		//============Constructor to call Model====================
		function index(){
			if($this->session->userdata('is_logged_in')){
				redirect('supercontrol/banneradd_view');
			}else{
				$this->load->view('supercontrol/main/login');
			}
		}
		//=======================Insert Page Data============
		/*function add_banner(){
			$config = array(
				'upload_path' => "uploads/banner/",
				'upload_url' => base_url() . "uploads/banner/",
				'allowed_types' => "gif|jpg|png|jpeg",
				'encrypt_name' => 'TRUE'
			);
			//load upload class library
        	$this->load->library('upload', $config);
				//=====================+++++++++++++++++++++++===================
				$this->form_validation->set_rules('banner_heading','Banner Heading', 'required');
				$this->form_validation->set_rules('banner_sub_heading', 'Banner Sub Heading', 'required|min_length[1]|max_length[100]');
				$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
				//=====================+++++++++++++++++++++++===================
				if ($this->form_validation->run() == FALSE) {
					$this->load->view('supercontrol/header');
					$data['success_msg'] = '<div class="alert alert-success text-center">Some Fields Can Not Be Blank</div>';
					$this->load->view('supercontrol/header');
            		$this->load->view('supercontrol/banneradd_view',$data);
					$this->load->view('supercontrol/footer');
					//redirect('banner/addbanner',$data);
				}else{
					if (!$this->upload->do_upload('userfile')){
            			$data['success_msg'] = '<div class="alert alert-success text-center">You Must Select An Image File!</div>';
						$this->load->view('supercontrol/header');
            			$this->load->view('supercontrol/banneradd_view', $data);
						$this->load->view('supercontrol/footer');
        			}else{
						 $data['userfile'] = $this->upload->data();
						 $filename=$data['userfile']['file_name'];
						 $data = array(
							'banner_heading' => $this->input->post('banner_heading'),
							'banner_sub_heading' => $this->input->post('banner_sub_heading'),
							'banner_img' => $filename,
							'banner_description' => $this->input->post('banner_description'),
							'status' => 1
						);
						$this->banner_model->insert_banner($data);
            			$upload_data = $this->upload->data();
						$query = $this->banner_model->show_banner();
						$data['ecms'] = $query;
						$this->session->set_flashdata('add_message', 'Banner Added successfully!!!!');
						redirect('supercontrol/banner/showbanner');
					}
				}
		}*/

		/*
			Add Banner Function -  Koushik Dutta
		*/
		public function add_banner()
		{
			$config = array(
				'upload_path' => "uploads/banner/",
				'upload_url' => base_url() . "uploads/banner/",
				'allowed_types' => "gif|jpg|png|jpeg",
				'encrypt_name' => 'TRUE'
			);

			// print_r($config);exit;
			$this->load->library('upload', $config);
			$this->load->library('form_validation');
			// Set Rules
			$this->form_validation->set_rules('banner_heading','Banner Heading', 'required');
			$this->form_validation->set_rules('banner_sub_heading', 'Banner Sub Heading', 'required|min_length[1]|max_length[100]');

			// Here I have set Error Delimeters
			$this->form_validation->set_error_delimiters('<span class="label label-danger">', '</span>');
			if( $this->form_validation->run() && $this->upload->do_upload('bannerFile') ) {
				$data = $this->upload->data();
				// print_r($data);exit;
				// $image_path = base_url("uploads/member/" . $data['raw_name'] . $data['file_ext']);
				$filename = $data['raw_name'] . $data['file_ext'];
				$post_data = array(
					'banner_heading' => $this->input->post('banner_heading'),
					'banner_sub_heading' => $this->input->post('banner_sub_heading'),
					'banner_img' => $filename,
					'banner_description' => $this->input->post('banner_description'),
					'status' => 1
				);
				// print_r($post_data);exit;
				$this->banner_model->insert_banner($post_data);
				$this->session->set_flashdata('add_message', 'Banner Added successfully!!!!');
				redirect('supercontrol/banner/showbanner');
			}else{
				// print_r(validation_errors());exit;
				$upload_error = $this->upload->display_errors();
				$this->load->view('supercontrol/header');
				$this->load->view('supercontrol/banneradd_view', compact('upload_error'));
				$this->load->view('supercontrol/footer');
			}
		}
		//=======================Insert Page Data============
		//================Add banner form=============
		function addbanner(){

			$data['title'] = "Banner add";
			$this->load->view('supercontrol/header',$data);
			$this->load->view('supercontrol/banneradd_view');
			$this->load->view('supercontrol/footer');
		}
		//================View Individual Data List=============

		//================View Individual Data List=============
  		//================Show Individual by Id=================
		function show_banner_id($id) {
			 $id = $this->uri->segment(4);
			//exit();
			$data['title'] = "Edit Banner";
			//Loading Database
			$this->load->database();
			//Calling Model
			$this->load->model('supercontrol/banner_model');
			//Transfering data to Model
			$query = $this->banner_model->show_banner_id($id);

			$data['ecms'] = $query;
			$this->load->view('supercontrol/header',$data);
			$this->load->view('supercontrol/banner_edit', $data);
			$this->load->view('supercontrol/footer');
		}



  	 	//================Update Individual ====================
		function edit_banner(){
				//============================================
				$banner_img = $this->input->post('banner_img');
				$config = array(
					'upload_path' => "uploads/banner/",
					'upload_url' => base_url() . "uploads/banner/",
					'allowed_types' => "gif|jpg|png|jpeg"
				);
				$this->load->library('upload', $config);
				if ($this->upload->do_upload("userfile")) {
				//echo $path = base_url(). "banner/";exit();
				//echo $path1 = "banner/";
				@unlink("uploads/banner/".$banner_img);

				//echo $image_data = $this->upload->data();
				$data['img'] = $this->upload->data();
				//*********************************
				//============================================
				$datalist = array(
					'banner_heading' => $this->input->post('banner_heading'),
					'banner_img' => $data['img']['file_name'],
					'banner_sub_heading' => $this->input->post('banner_sub_heading'),
					'banner_description' => $this->input->post('description')
				);
				//print_r($datalist); exit();
				$banner_img = $this->input->post('banner_img');
				//====================Post Data===================
				$id = $this->input->post('banner_id');
				$data['title'] = "Banner Edit";
				//Calling Model
				$this->load->model('supercontrol/banner_model');
				//Transfering data to Model
				$query = $this->banner_model->banner_edit($id,$datalist,$banner_img);
				$data1['message'] = 'Data Update Successfully';
				$query = $this->banner_model->show_bannerlist();
				$data['ecms'] = $query;
				$data['title'] = "Banner Page List";
				$this->load->view('supercontrol/header',$data);
				$this->load->view('supercontrol/showbannerlist', $data1);
				$this->load->view('supercontrol/footer');
			//*********************************
			}else{
				$datalist = array(
					'banner_heading' => $this->input->post('banner_heading'),
					'banner_sub_heading' => $this->input->post('banner_sub_heading'),
					'banner_description' => $this->input->post('description')
				);
			//====================Post Data===================
				$banner_img = $this->input->post('banner_img');
				$id = $this->input->post('banner_id');
				$data['title'] = "Banner Edit";
				//loading database
				$this->load->database();
				//Calling Model
				$this->load->model('supercontrol/banner_model');
				//Transfering data to Model
				$query = $this->banner_model->banner_edit($id,$datalist,$banner_img);
				$data1['message'] = 'Data Update Successfully';
				$query = $this->banner_model->show_bannerlist();
				$data['ecms'] = $query;
				$data['title'] = "Banner Page List";
				$this->load->view('supercontrol/header',$data);
				$this->load->view('supercontrol/showbannerlist', $data1);
				$this->load->view('supercontrol/footer');
			}
		}
		//================Update Individual ====================

		//=======================Delete Individual==============
		function delete_cms($id){
			//Loading  Database
			$this->load->database();
			//Transfering data to Model
			$this->cms_model->delete_cms($id);
			$data1['message'] = 'Data Deleted Successfully';
			redirect('individual/successdelete');
		}
		//======================Delete Individual===============

		//======================Show CMS========================
		function showbanner(){

			//Loading Database
			$this->load->database();
			//Calling Model
			$this->load->model('supercontrol/banner_model');
			//Transfering data to Model
			$query = $this->banner_model->show_banner();
			$data['ecms'] = $query;
			$data['title'] = "BannerList";
			$this->load->view('supercontrol/header',$data);
			$this->load->view('supercontrol/showbannerlist');
			$this->load->view('supercontrol/footer');

	}
		//======================Show CMS========================

		//=====================DELETE BANNER====================


			function delete_banner() {
			$id = $this->uri->segment(4);
			$result=$this->banner_model->show_banner_id($id);
			//print_r($result);
			$banner_img = $result[0]->banner_img;
			//echo $banner_img;exit();
			//Loading Database
			$this->load->database();

			//Transfering data to Model
			$query = $this->banner_model->delete_banner($id,$banner_img);
			$data['ecms'] = $query;
			$this->load->view('supercontrol/header',$data);
			//$this->load->view('showbannerlist', $data);
			redirect('supercontrol/banner/showbanner');
			$this->load->view('supercontrol/footer');
		}

		//=====================DELETE BANNER====================


}

?>

