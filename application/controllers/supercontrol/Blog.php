<?php


class Blog extends CI_Controller {


		//============Constructor to call Model====================


		function __construct() {


			parent::__construct();


			$this->load->library(array('form_validation','session'));
              $this->load->model('supercontrol/Country_model');

			if($this->session->userdata('is_logged_in')!=1){


			redirect('supercontrol/home', 'refresh');


			}


			$this->load->model('supercontrol/blog_model');


			$this->load->library('image_lib');


			$this->load->database();


			


				//****************************backtrace prevent*** START HERE*************************


			$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');


            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');


            $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);


            $this->output->set_header('Pragma: no-cache');


			


			//****************************backtrace prevent*** END HERE*************************


		}


		//============Constructor to call Model====================


		function index(){


		if($this->session->userdata('is_logged_in')){


			redirect('supercontrol/blog/blog_add_form');


			///supercontrol/blog_add_form


        }else{


        	$this->load->view('supercontrol/login');	


        }


		


		


	}

function advhosting(){
			$query = $this->Country_model->select_country();
			$data['country'] = $query;
			$data['title'] = "Add hostings";
			$this->load->view('supercontrol/header',$data);
			$this->load->view('supercontrol/hosting_advview');
			$this->load->view('supercontrol/footer');
		}

		 public function add_adv_member()
		{
			$rent=$this->input->post('renter_type');
				$renter_type=implode("," ,(array) $rent);

				$food =$this->input->post('additional_services'); 
             	$food_service = implode("," ,(array) $food);
			$date = date('Y-m-d h:i:s');
			$day = $this->input->post('day');
			$month = $this->input->post('month');
			$year = $this->input->post('year');
			// $dob = new DateTime($year.'-'.$month.'-'.$day);
			$dateofbirth = date('Y-m-d H:i:s');
			$reg_no = mt_rand(100000000, 999999999);
			$ip_address = $this->input->ip_address();

			$config = array(
				'upload_path' => 'uploads/blog/',
				'upload_url'  => base_url() . "uploads/blog/",
				'allowed_types'	=>		'jpg|gif|png|jpeg',
			);
			$this->load->library('upload', $config);
			$this->load->library('form_validation');
			// Set Rules
			$this->form_validation->set_rules('first_name','First Name', 'required');
			// $this->form_validation->set_rules('last_name', 'Last Name', 'required|min_length[1]|max_length[100]');
			// $this->form_validation->set_rules('email', 'Email','required|valid_email|is_unique[users.email]');
			// $this->form_validation->set_rules('phone', 'Phone','required|numeric');
			// $this->form_validation->set_rules('password', 'Password','required');
			// $this->form_validation->set_rules('postcode', 'Postcode','required|numeric');
			// $this->form_validation->set_rules('day', 'Day','required|numeric');
			// $this->form_validation->set_rules('month', 'Month','required');
			// $this->form_validation->set_rules('year', 'Year','required|numeric');
			// Here I have set Error Delimeters
			$this->form_validation->set_error_delimiters('<span class="label label-danger">', '</span>');
			if( $this->form_validation->run() && $this->upload->do_upload('userfile') ) {
				$table_name = 'hosting';
				$id = '';
				$fieldname = '';
				$action = 'insert';
				$data = $this->upload->data();
				// print_r($data);exit;
				// $image_path = base_url("uploads/member/" . $data['raw_name'] . $data['file_ext']);`host_type`, ``, ``, ``, ``, ``, `status`, `street1`, `street2`, `state`, `city`, `country`, `postcode`, `description`, `title`, `image`, `ip_address`, `createdate`, `email_verified`, `nationality`, `mother_tongue`, `age_group`, `ethinicity`, `kitchen`, `avail_dur`, `avail_from`, `area`, `bathroom`, `bedroom`, `pantry`, `pool`, `living_room`, `backyard`, `laundry_service`, `renter_ethnicity`, `rent_gender`, `renter_age_group`, `renter_type`, `food_service`, `renter_type1`, `user_id`, `date`, `status1`
				$filename = $data['raw_name'] . $data['file_ext'];
				$post_data = array(
					'fname' => $this->input->post('first_name'),
					'lname' => $this->input->post('last_name'),
					'email' => $this->input->post('email'),
					'host_type' => 2,
					'image' => $filename,
					'hostgender'=>$this->input->post('gender'),
					'phone' => $this->input->post('phone'),
					'country' => $this->input->post('country'),
					'state' => $this->input->post('state'),
					'city' => $this->input->post('city'),
					'postcode' => $this->input->post('postcode'),
					'street1' => $this->input->post('street1'),
					'street2' => $this->input->post('street2'),
					'title' =>$this->input->post('title'),
					'description' =>$this->input->post('description'),
					'status' => '1',
					'nationality' => $this->input->post('nationality'),
					//'mother_tongue'=> $this->input->post('mother_tongue'),
					'age_group'=> $this->input->post('agegroup'),
					'renter_ethnicity'=> $this->input->post('renter_ethnicity'),
					'avail_dur'=> $this->input->post('avl_dur'),
					//'avail_from'=> $this->input->post('available_from'),
					'area'=> $this->input->post('area'),
					'bedroom'=> $this->input->post('beds'),
					'bathroom'=> $this->input->post('bathroom'),
					'kitchen'=> $this->input->post('kitchen'),
					'hostgender'=> $this->input->post('hostgender'),
					'living_room'=> $this->input->post('living_room'),
					'laundry_service'=> $this->input->post('laundry_service'),
					'pantry'=> $this->input->post('Pantry'),
					'pool'=> $this->input->post('Pool'),
					'backyard'=> $this->input->post('Backyard'),
					//'rent_gender'=> $this->input->post('rent_gender'),
					'renter_age_group'=> $this->input->post('renter_age_group'),
				    'food_service'=> $food_service,
				    'renter_type'=> $renter_type

				);
				 // echo "<pre>";
				 // print_r($post_data);exit;
				$query = $this->Generalmodel->common_function($table_name,$id,$fieldname,$action,$post_data);
				$this->session->set_flashdata('add_message', 'Hosting Added successfully!!!!');
				redirect('supercontrol/blog/show_blog');
			}else{
				$upload_error = $this->upload->display_errors();
				$this->load->view('supercontrol/header');
				$this->load->view('supercontrol/hosting_addview',compact('upload_error'));
				$this->load->view('supercontrol/footer');
			}
		}
//======================Show Add form for blog add **** START HERE========================	


function blog_add_form(){


				$data['title'] = "Add Blog";


				$this->load->view('supercontrol/header',$data);


				$this->load->view('supercontrol/blogadd_view');


				$this->load->view('supercontrol/footer');


     }


		


//====================== Show Add form for blog add ****END HERE========================


		//=======================Insert Page Data============


		function add_blog(){


			$my_date = date("Y-m-d", time()); 


			$config = array(


			'upload_path' => "uploads/blog/",


			'upload_url' => base_url() . "uploads/blog/",


			'allowed_types' => "gif|jpg|png|jpeg"


			);


        	$this->load->library('upload', $config);


				//=====================+++++++++++++++++++++++===================


				$this->form_validation->set_rules('blog_title','News Title', 'required');


				$this->form_validation->set_rules('posted_by', 'Posted By', 'required|min_length[1]|max_length[100]');


				$this->form_validation->set_rules('blog_desc', 'blog Description', 'required|min_length[1]|max_length[100000]');


				$this->form_validation->set_error_delimiters('<div class="error">', '</div>');


				//=====================+++++++++++++++++++++++===================


				if ($this->form_validation->run() == FALSE) {


					$this->load->view('supercontrol/header');


					$data['success_msg'] = '<div class="alert alert-success text-center">Some Fields Can Not Be Blank</div>';


					$this->load->view('supercontrol/header');


            		$this->load->view('supercontrol/blogadd_view',$data);


					$this->load->view('supercontrol/footer');


					//redirect('banner/addbanner',$data);


				}else{


					if (!$this->upload->do_upload('userfile')){


            			$data['success_msg'] = '<div class="alert alert-success text-center">You Must Select An Image File!</div>';


						$this->load->view('supercontrol/header');


            			$this->load->view('supercontrol/blogadd_view', $data);


						$this->load->view('supercontrol/footer');


        			}else{


						 $data['userfile'] = $this->upload->data();


						 $filename=$data['userfile']['file_name'];


						 $data = array(


							'blog_title' => $this->input->post('blog_title'),


							'posted_by' => $this->input->post('posted_by'),


							'blog_image' => $filename,


							'blog_desc' => $this->input->post('blog_desc'),


							'date' => $my_date,


							'blog_status' => 1


						);


						$this->blog_model->insert_blog($data);


            			$upload_data = $this->upload->data();


						$query = $this->blog_model->show_blog();


						$data['ecms'] = $query;
						$data['title'] = "Show blog";

            			$data['success_msg'] = '<div class="alert alert-success text-center"> Data successfully inserted!!!</div>';


						$this->load->view('supercontrol/header',$data);


						$this->load->view('supercontrol/showbloglist',$data);


						$this->load->view('supercontrol/footer');


				


					}


				}


		}


		//=======================Insert Page Data============


function view_blog($id) {


			 $id = $this->uri->segment(4); 
			//exit();
			$data['title'] = "Edit Hosting";
			//Loading Database
			$this->load->database();
			//Calling Model
			$this->load->model('supercontrol/blog_model');
			//Transfering data to Model
			$query = $this->blog_model->show_blog_id($id);           
			$data['ecms'] = $query;
			$country = $query[0]->country;
			$state = $query[0]->state;
			$city = $query[0]->city;
			$query = $this->Country_model->show_country_view($country);
			$data['country'] = $query;
			// echo $query[0]->count_id;
			$query = $this->Country_model->show_state_view($state);
			$data['state'] = $query;
			$query = $this->Country_model->show_city_view($city);
			$data['city'] = $query;

    			$this->load->view('supercontrol/header',$data);


			$this->load->view('supercontrol/blog_view', $data);


			$this->load->view('supercontrol/footer');


		}


//=======================Insertion Success message for News *** START HERE=========


		function success(){


			$data['h1title'] = 'Add Blog';


			


			$data['title'] = 'Add Blog';


			$this->load->view('supercontrol/header');


			$this->load->view('supercontrol/blogadd_view',$data);


			$this->load->view('supercontrol/footer');


		}


//=======================Insertion Success message *** END HERE=========	


//======================Show News List **** START HERE========================


		function show_blog(){


		//Loading Database


			$this->load->database();


			//Calling Model


			$this->load->model('supercontrol/blog_model');


			//Transfering data to Model


			$query = $this->blog_model->show_blog();


			$data['ecms'] = $query;


			$data['title'] = "Hosting List";


			$this->load->view('supercontrol/header',$data);


			$this->load->view('supercontrol/showbloglist');


			$this->load->view('supercontrol/footer');


		


	}


//======================Show News List **** END HERE========================


//======================Status change **** START HERE========================


	function statusblog ()


		{


			     $stat= $this->input->get('stat'); 


				 $id= $this->input->get('id');   


		$this->load->model('supercontrol/blog_model');


		$this->blog_model->updt($stat,$id);


		}





//=======================Status change **** END HERE========================	


//================Show Individual by Id for News *** START HERE=================


		function show_blog_id($id) {


			 $id = $this->uri->segment(4); 


			//exit();


			$data['title'] = "Edit blog";


			//Loading Database


			$this->load->database();


			//Calling Model


			$this->load->model('supercontrol/blog_model');


			//Transfering data to Model


			$query = $this->blog_model->show_blog_id($id);


			$data['ecms'] = $query;


			$this->load->view('supercontrol/header',$data);


			$this->load->view('supercontrol/blog_edit', $data);


			$this->load->view('supercontrol/footer');


		}


		


//================Show Individual by Id for News *** END HERE=================


//================Update Individual blog***** START HERE ====================


		function edit_blog(){


			 $blog_image = $this->input->post('blog_image');


			 $config = array(


				'upload_path' => "uploads/blog/",


				'upload_url' => base_url() . "uploads/blog/",


				'allowed_types' => "gif|jpg|png|jpeg"


			);


			$this->load->library('upload', $config);


			if ($this->upload->do_upload("userfile")) {


				@unlink("uploads/blog/".$blog_image);


				//echo $image_data = $this->upload->data();


				$data['img'] = $this->upload->data();


			 	//echo $data['img']['file_name'];


				//exit();


				//*********************************


				//============================================


				$datalist = array(			


				'blog_title' => $this->input->post('blog_title'),


				'posted_by' => $this->input->post('posted_by'),


				'blog_desc' => $this->input->post('blog_desc'),


				'blog_image' => $data['img']['file_name']


				);


				//echo $gallery_name=$_POST['gallery_name'];


				//====================Post Data===================


				//print_r($datalist);


				//exit();


				$id = $this->input->post('blog_id');


				$data['title'] = "Blog Edit";


				//loading database


				$this->load->database();


				//Calling Model


				$this->load->model('supercontrol/blog_model');


				//Transfering data to Model


				$query = $this->blog_model->blog_edit($id,$datalist);


				// echo $ddd=$this->db->last_query();


				


				$data1['message'] = '<div class="alert alert-success text-center">Data successfully uploaded!!!</div>';


				$query = $this->blog_model->show_bloglist();


				$data['ecms'] = $query;


				$data['title'] = "Blog Page List";


				$this->load->view('supercontrol/header',$data);


				$this->load->view('supercontrol/showbloglist', $data1);


				$this->load->view('supercontrol/footer');


				//*********************************


		


			}else{


				$datalist = array(			


					'blog_title' => $this->input->post('blog_title'),


				    'posted_by' => $this->input->post('posted_by'),


				    'blog_desc' => $this->input->post('blog_desc')


				


				);


				$id = $this->input->post('blog_id');


				$data['title'] = "blog Edit";


				//loading database


				$this->load->database();


				//Calling Model


				$this->load->model('supercontrol/blog_model');


				//Transfering data to Model


				$query = $this->blog_model->blog_edit($id,$datalist);


				//echo $ddd=$this->db->last_query();


				//exit();


				$data1['message'] = '<div class="alert alert-success text-center"> Data successfully uploaded!!!</div>';


				$query = $this->blog_model->show_bloglist();


				$data['ecms'] = $query;


				$data['title'] = "blog Page List";


				$this->load->view('supercontrol/header',$data);


				$this->load->view('supercontrol/showbloglist', $data1);


				$this->load->view('supercontrol/footer');


			}


			


		}


//================Update Individual  News ***** END HERE====================





		//=====================DELETE NEWS====================





		


			function delete_blog() {


			$id = $this->uri->segment(4);


			$result=$this->blog_model->show_blog_id($id);


			$blog_image=$result[0]->image; 


			$query = $this->blog_model->delete_blog($id,$blog_image);


			


	        $query = $this->blog_model->show_bloglist();


			$data['ecms'] = $query;


			$data['title'] = "Hosting Page List";


			$this->session->set_flashdata('success_delete', 'Hosting Deleted Successfully !!!');


			redirect('supercontrol/blog/show_blog',TRUE);


		}





		//=====================DELETE NEWS====================





		//====================MULTIPLE DELETE=================


		function delete_multiple(){


		$ids = ( explode( ',', $this->input->get_post('ids') ));


		$this->blog_model->delete_mul($ids);


		$data4['msg1'] = '<div class="alert alert-success text-center"> Data successfully delete!!!</div>';


		$this->load->view('supercontrol/header');


		redirect('supercontrol/blog/show_blog',$data4);


		$this->load->view('supercontrol/footer');


		}


		//====================MULTIPLE DELETE=================


//======================Logout==========================


		public function Logout(){


        	$this->session->sess_destroy();


        	redirect('supercontrol/login');


    	}


//======================Logout==========================


}





?>