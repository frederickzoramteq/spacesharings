<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
//session_start(); //we need to call PHP's session object to access it through CI
class Home extends CI_Controller {
  function __construct(){
    parent::__construct();
    $this->load->library('session');
  }

  function index(){
    if($this->session->userdata('is_logged_in')=='1'){

      $session_data = $this->session->userdata('is_logged_in');

      $this->session->userdata('is_logged_in');

      @$data['UserName'] = @$session_data['username'];

      $data2['title'] = "Space sharing Dashbord";

      $this->load->view('supercontrol/header',$data2);

      $this->load->view('supercontrol/home_view', $data);

      $this->load->view('supercontrol/footer',$data);
    }else {
      redirect('supercontrol/main', 'refresh');
    }
  }



  function logout() {
    $this->session->unset_userdata('logged_in');
    session_destroy();
    redirect('supercontrol/home', 'refresh');
  }
}
?>