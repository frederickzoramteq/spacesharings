<?php
ob_start();
class Advhosting extends CI_Controller {
		//============Constructor to call Model====================
	function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->model('Auth_model');
		$this->load->model('Generalmodel');
		$this->load->model('HostingModel');
		$this->load->library('email');
		$this->load->library('encrypt');
		$this->load->helper('string');
		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->load->model('CountryModel','country');
		//**************backtrace prevent*** END HERE*************************
	}

		//============Constructor to call Model====================

public function add_hosting()
{
	$data['page']='Hostings';
	$uid = $this->session->userdata('is_user_id');
	$date = date('Y-m-d h:i:s');
	$ip_address = $this->input->ip_address();
	$my_date = date("Y-m-d", time()); 
	$this->form_validation->set_error_delimiters('<span class="label label-danger" style="font-size:11px;">', '</span>');
	if ($this->form_validation->run('advance_hostings') == FALSE) {

			$this->session->set_flashdata("success_msg", "<div class='alert alert-danger text-center'><b>Please enter the required field !!</b></div>"); 
			//$data['success_msg'] = '';
			$this->load->view('header',$data);
			$data ['country']= $this->country->fetch_Country();
			$this->load->view('advancesearch',$data);
			$this->load->view('footer');
		//redirect('banner/addbanner',$data);
		}else{
	
	$table_name = 'hosting';
	$id = '';
	$rent=$this->input->post('renter_type');
	$renter_type=implode("," ,(array) $rent);
	$food =$this->input->post('additional_services'); 
    $food_service = implode("," ,(array) $food);
	$post_data = array(
		'id' =>'' ,
		'user_id' =>$uid,
		'host_type' =>'1',
		'fname' => $this->input->post('fname'),
		'lname' => $this->input->post('lname'),
		'phone' => $this->input->post('phone'),
		'email' => $this->input->post('email'),
		'description' => $this->input->post('description'),
		'title' => $this->input->post('title'),
		'country' => $this->input->post('country'),
		'state' => $this->input->post('state'),
		'city' => $this->input->post('city'),
		'postcode' => $this->input->post('postcode'),
		'email_verified' => 'Yes',
		'createdate' => $date,
		'nationality' => $this->input->post('nationality'),
		'mother_tongue'=> $this->input->post('mother_tongue'),
		'age_group'=> $this->input->post('agegroup'),
		'renter_ethnicity'=> $this->input->post('renter_ethnicity'),
		'avail_dur'=> $this->input->post('avl_dur'),
		'avail_from'=> $this->input->post('available_from'),
		'area'=> $this->input->post('area'),
		'bedroom'=> $this->input->post('beds'),
		'bathroom'=> $this->input->post('bathroom'),
		'kitchen'=> $this->input->post('kitchen'),
		'hostgender'=> $this->input->post('hostgender'),
		'living_room'=> $this->input->post('living_room'),
		'laundry_service'=> $this->input->post('laundry_service'),
		'pantry'=> $this->input->post('Pantry'),
		'pool'=> $this->input->post('Pool'),
		'backyard'=> $this->input->post('Backyard'),
		'rent_gender'=> $this->input->post('rent_gender'),
		'renter_age_group'=> $this->input->post('renter_age_group'),
	    'food_service'=> $food_service,
	    'renter_type'=> $renter_type,
		'ip_address' => $ip_address,
		'status' => 'Yes'
	);

	$prop_id = $this->HostingModel->insert_get_id_of_prop($table_name,$post_data);

	if(!empty($_FILES))
	{
	    $j = 1;                 
	    foreach($_FILES as $filekey=>$fileattachments)
	    {
	        foreach($fileattachments as $key=>$val)
	        {
	            if($key == 'name')
	            {
	                $val = $this->renameFileNames($val);
	            }

	            if(is_array($val))
	            {
	                $i = 1;
	                foreach($val as $v)
	                {
	                    $field_name = "multiple_".$filekey."_".$i;
	                    $_FILES[$field_name][$key] = $v;
	                    $i++;   
		            }
	            }
	            else
	            {
	                $field_name = uniqid("single_", true);
                    $_FILES[$field_name] = $fileattachments;
                    $j++;
                    break;
			    }
		    }                       
			        // Unset the useless one 
			unset($_FILES[$filekey]);
	   }

	   foreach($_FILES as $field_name => $file)
	   {
            if(isset($file['error']) && $file['error']==0)
            {
                $config['upload_path'] = 'uploads/blog/';
                $config['allowed_types'] = "gif|jpg|png|jpeg";
                $config['max_size'] = 10000000;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
    
                if ( ! $this->upload->do_upload($field_name))
                {
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata("success_msg", "<div class='alert alert-success text-center'>You Must Select An Image File!</div>");
                }
                else
                {
                    $result=$this->HostingModel->insert_img_name($_FILES[$field_name]['name'], $prop_id);
    				$this->session->set_flashdata("success_msg", "<div class='alert alert-success text-center'> Hosting/Renting Information was successfully uploaded!!!</div>");
    			}
            }
            else
            {
            	$file_name='noimg.png';
      		 	$result=$this->HostingModel->insert_img_name($file_name, $prop_id);
    			$this->session->set_flashdata("success_msg", "<div class='alert alert-success text-center'> Hosting/Renting Information was successfully uploaded!!!</div>");
            }
	   }
	   
	   redirect('Page/AdvanceSearch');
	}
	//end of file upload system
	$this->load->view('header',$data);
	$data ['country']= $this->country->fetch_Country();
	$this->load->view('advancesearch',$data);
	$this->load->view('footer');
}
	
}


private function renameFileNames($fileNames)
{
    $returnValue = array();

    foreach($fileNames as $fileName)
    {
        $extension = $this->getExtension($fileName);
        $randomFileName = uniqid();
        $newFileName = $randomFileName . $extension;
        
        $returnValue[] = $newFileName;
    }
    
    return $returnValue;
}

private function getExtension($filename)
{
    $x = explode('.', $filename);
    
    if (count($x) === 1)
    {
        return '';
    }
    
    $ext = ($this->file_ext_tolower) ? strtolower(end($x)) : end($x);
    return '.'.$ext;
}

	/*function add_hosting(){
		$uid = $this->session->userdata('is_user_id');
		$date = date('Y-m-d h:i:s');
		$ip_address = $this->input->ip_address();
		$my_date = date("Y-m-d", time()); 

		$config = array(
			'upload_path' => "uploads/blog/",
			'upload_url' => base_url() . "uploads/blog/",
			'allowed_types' => "gif|jpg|png|jpeg"
		);
		$this->load->library('upload', $config);
		$this->form_validation->set_error_delimiters('<span class="label label-danger" style="font-size:11px;">', '</span>');

		//=========+++++++++++++++++++++++===================
		//if ($this->form_validation->run('basic_hostings') == FALSE) {
		if ($this->form_validation->run('advance_hostings') == FALSE) {
			$this->session->set_flashdata("success_msg", "<div class='alert alert-danger text-center'><b>Please enter the required field. !!</b></div>"); 
					//$data['success_msg'] = '';
			$this->load->view('header',$data);
			$data ['country']= $this->country->fetch_Country();
			$this->load->view('advancesearch',$data);
			$this->load->view('footer');
			
		}else{
			if (!$this->upload->do_upload('userfile')){
				$this->session->set_flashdata("success_msg", "<div class='alert alert-success text-center'>You Must Select An Image File!</div>");
				$this->load->view('header',$data);
				$data ['country']= $this->country->fetch_Country();
				$this->load->view('advancesearch',$data);
				$this->load->view('footer');
			}else{
				$data['userfile'] = $this->upload->data();
				$filename=$data['userfile']['file_name'];
				$table_name = 'hosting';
				$id = '';

				$rent=$this->input->post('renter_type');
				$renter_type=implode("," ,(array) $rent);

				$food =$this->input->post('additional_services'); 
             	$food_service = implode("," ,(array) $food);

				$post_data = array(
					'id' =>'' ,
					'user_id' =>$uid,
					'host_type' =>'2',
					'fname' => $this->input->post('fname'),
					'lname' => $this->input->post('lname'),
					'phone' => $this->input->post('phone'),
					'email' => $this->input->post('email'),
					'image' => $filename,
					'description' => $this->input->post('description'),
					'title' => $this->input->post('title'),
					'country' => $this->input->post('country'),
					'state' => $this->input->post('state'),
					'city' => $this->input->post('city'),
					'postcode' => $this->input->post('postcode'),
					'email_verified' => 'Yes',
					'createdate' => $date,
					'nationality' => $this->input->post('nationality'),
					'mother_tongue'=> $this->input->post('mother_tongue'),
					'age_group'=> $this->input->post('agegroup'),
					'renter_ethnicity'=> $this->input->post('renter_ethnicity'),
					'avail_dur'=> $this->input->post('avl_dur'),
					'avail_from'=> $this->input->post('available_from'),
					'area'=> $this->input->post('area'),
					'bedroom'=> $this->input->post('beds'),
					'bathroom'=> $this->input->post('bathroom'),
					'kitchen'=> $this->input->post('kitchen'),
					'hostgender'=> $this->input->post('hostgender'),
					'living_room'=> $this->input->post('living_room'),
					'laundry_service'=> $this->input->post('laundry_service'),
					'pantry'=> $this->input->post('Pantry'),
					'pool'=> $this->input->post('Pool'),
					'backyard'=> $this->input->post('Backyard'),
					'rent_gender'=> $this->input->post('rent_gender'),
					'renter_age_group'=> $this->input->post('renter_age_group'),
				    'food_service'=> $food_service,
				    'renter_type'=> $renter_type,
					'ip_address' => $ip_address,
					'status' => 'Yes'
					

				);

				$query = $this->HostingModel->insert($table_name,$post_data);

				$upload_data = $this->upload->data();
				
				$this->session->set_flashdata("success_msg", "<div class='alert alert-success text-center'> Data successfully inserted!!!</div>");
				$this->load->view('header',$data);
				$data ['country']= $this->country->fetch_Country();
				$this->load->view('advancesearch',$data);
				$this->load->view('footer');

				


			}


		}


	}*/

	
}



?>



