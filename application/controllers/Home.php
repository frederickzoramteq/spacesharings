<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct() {
		parent::__construct();
		 $this->load->model('Home_model');
		 $this->load->model('Generalmodel');
	}
	public  function index()
	{
		$data['page']='home';
		$this->load->view('header',$data);
		$data['testimonial'] = $this->Home_model->fetch_testimonials();
		$data['about']=$this->Home_model->fetch_about();
		$this->load->view('index', $data);
		$this->load->view('footer', $data);
	}
	public  function header()
	{
		$data['a']='a';
		$this->load->view('header', $data);
	}
	public  function footer()
	{
		//$cont = $this->Generalmodel->fetch_row('settings', "id=1");
		$data['page'] = "footer";
		$this->load->view('footer', $data);
	}
	
}
