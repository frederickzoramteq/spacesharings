<?php
ob_start();
class Request extends CI_Controller {
		//============Constructor to call Model====================
	function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->model('Auth_model');
		$this->load->model('Generalmodel');
		$this->load->model('HostingModel');
		$this->load->library('email');
		$this->load->library('encrypt');
		$this->load->helper('string');
		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->load->model('CountryModel','country');
			//**********backtrace prevent*** END HERE*******************
	}

		//============Constructor to call Model====================


	public function contactus_save() {
		$data['page']='Request';
		//$uid = $this->session->userdata('is_user_id');
		$queryuser = $this->Generalmodel->show_data_id('settings','1','id','get','');
		$from_email = $queryuser[0]->email;

		$date1 = date('Y-m-d h:i:s');   
		$to_email = $this->input->post('email');
		$this->form_validation->set_error_delimiters('<span class="label label-danger" style="font-size:11px;">', '</span>');
		if ($this->form_validation->run('post_request') == FALSE) {

                     $this->session->set_flashdata("success_msg", "<div class='alert alert-danger text-center'>These Fields can not be Blank</div>"); 
					//$data['success_msg'] = '';
					
					$data['page']='Request';
					$this->load->view('header',$data);
					$data ['country']= $this->country->fetch_Country();
					$this->load->view('request',$data);
					$this->load->view('footer');
					
					//redirect('banner/addbanner',$data);
				}else{
						$data = array('ContactName' => $this->input->post('name'),
            						  'state' => $this->input->post('state'),
            						  'city' =>  $this->input->post('city'),
            						  'country' =>  $this->input->post('country'),
            						  'Email' => $to_email,
            						  'Subject'=>$this->input->post('subject'),
            						  'Message' => $this->input->post('message'),
            						  'ContactDate' =>$date1
            						 );

		$inserevent = $this->Generalmodel->do_action('user_contact','','','insert',$data,'');
		
		//$msg = $this->load->view('contactadmin',$data,TRUE);
		$msg2 = $this->load->view('contactuser',$data,TRUE);
		$config['mailtype'] = 'html';
		$this->load->library('email');
		$this->email->initialize($config);
		$msg2 = $msg2;
		//$msg2 = $msg2;
		$subject2 = "Thank you for contacting Space share.";   
		$this->email->from($from_email, "Space Share"); 
		$this->email->to($to_email);
		//$this->email->bcc($from_email);
		$this->email->subject($subject2); 
		$this->email->message($msg2);
		
			
		if($this->email->send()){
			$this->session->set_flashdata('success', 'Email Sent successfully!!!!');
			redirect('Page/post_request', $data);
		}else{  
			$this->session->set_flashdata('fail', 'Email can not be sent!!!!');
			redirect('Page/post_request', $data);
		}
	}
}

 
	
}



?>



