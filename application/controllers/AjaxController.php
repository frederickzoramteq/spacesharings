<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AjaxController extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('AjaxModel','Ajax');
		
		
	}

	public function fetch_state()
	{
		$countryid = $this->input->post('data',TRUE);
		$state = $this->Ajax->fetch_state($countryid);
		$output = "<option value='0' selected='selected'>Select State</option>";
		foreach ($state as $row)
		{
      		//here we build a dropdown item line for each query result
			$output .= "<option value='".$row->state_id."'>".$row->state_name."</option>";
		}
		echo $output;
	}
	public function fetch_city()
	{
		$stateid = $this->input->post('data',TRUE);
		$city = $this->Ajax->fetch_city($stateid);
		$output = "<option value='0' selected='selected'>Select City</option>";
		foreach ($city as $row)
		{
      		//here we build a dropdown item line for each query result
			$output .= "<option value='".$row->city_id."'>".$row->city_name."</option>";
		}
		echo $output;
	}

	public function search()
	{
		$data = array(
			'key' => $this->input->post('key'),
			'country'=>$this->input->post('country'),
			'state'=>$this->input->post('state'),
			'city'=>$this->input->post('city'),
			'zip'=>$this->input->post('zip'),
			'lang'=>$this->input->post('lang'),
			'catg_l'=>$this->input->post('catg_l'),
		);
		echo json_encode($data);
        //$details = json_decode($data);

       // $output = json_validate($json);
        //print_r($output);
		exit;

       // $distance=$details[0]->key;
        //echo $distance;
	    //$searchresult = $this->Ajax->serch_result($data);
		/*echo $test = $this->input->post('keyword1');
		echo $test2 = $this->input->post('country1');
		echo $test3 = $this->input->post('state1');
		echo $test4 = $this->input->post('city1');
		exit;*/

		
	}


}