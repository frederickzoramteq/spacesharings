<?php
ob_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Auth extends CI_Controller{
	function __construct() {
		@parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->model('Auth_model');
		$this->load->model('Generalmodel');
		$this->load->library('email');
		//$this->load->model('Newsletter_model');
		$this->load->library('encrypt');
		$this->load->helper('string');
		/* Load form helper */
		$this->load->helper(array('form'));
		/* Load form validation library */
		$this->load->library('form_validation');
	}
	function Signup(){
		$data['title'] = "Registration";
		$data['page']='Register';
		$this->load->view('Header',$data);
		$this->load->view('Registration');
		$this->load->view('Footer');
	}
	function userregistration(){
		$data['page']='Register';
		$reg_date = date('Y-m-d H:i a');
		$reg_no = mt_rand(100000000, 999999999);
		$ip_address = $this->input->ip_address();
		//$this->form_validation->set_rules('email', 'Email', 'trim|required|is_unique[users.email]');
		//$this->form_validation->set_rules('password', 'Password', 'required');
       // $this->form_validation->set_rules('con_pass', 'Confirm Password', 'required|matches[password]');
       // $this->form_validation->set_rules('phone', 'Mobile Number ', 'required|regex_match[/^[0-9]{10}$/]');
		//<p class="text-danger">Donec ullamcorper nulla non metus auctor fringilla.</p>
		$this->form_validation->set_error_delimiters('<span class="label label-danger" style="font-size:11px;">', '</span>');
		if($this->form_validation->run('registerr')==FALSE){
			$this->session->set_flashdata("error"," <b>Something Went Wrong ..</b>");
			$querycnt = $this->Generalmodel->show_data_id('countries','','','get','');
			$data['country'] = $querycnt;
			$data['title'] = "Registration";
			$this->load->view('header',$data);
			$this->load->view('register',$data);
			$this->load->view('footer');
		}else{
			$datareg = array(
				'reg_no' => $reg_no,
				'ip_address' => $ip_address,
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'state' => $this->input->post('state'),
				'email' => $this->input->post('email'),
				'phone' => $this->input->post('phone'),
				'password' => md5($this->input->post('password')),
				'country' => $this->input->post('country'),
				'postcode' => $this->input->post('postcode'),
				'city' => $this->input->post('city'),
				'address_1' => $this->input->post('address_1'),
				'address_2' => $this->input->post('address_2'),
				'createdate' => $reg_date,
				'status' => 'Yes',
				'email_verified' => 'No'
			);
			// print_r($datareg);
			//exit();
			$query = $this->Generalmodel->show_data_id('users','','','insert',$datareg,'');
			//$this->session->set_flashdata('success', 'Chanel added successfully');
			//redirect($_SERVER['HTTP_REFERER']);
			$lastid = $this->db->insert_id();
			$from_email = "teamphp@goigi.in";
			$to_email = $this->input->post('email');
			$url = base_url()."Auth/verification/".$lastid;
			$data1 = array(
				'reg_no' => $reg_no,
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'email' => $this->input->post('email'),
				'password' => $this->input->post('password'),
				'createdate' => $reg_date,
				'url' => $url
			);
			$msg = $this->load->view('userregistrationtemplate',$data1,TRUE);
			
			$config['mailtype'] = 'html';
			$this->load->library('email');
			$this->email->initialize($config);
			$msg = $msg;
			$subject = 'Registration Success Message From Space Share.';
			$this->email->from($from_email, 'Space Share');
			$this->email->to($to_email);
			$this->email->subject($subject);
			$this->email->message(@$msg);
			
			if(@$this->email->send()){
				$this->session->set_flashdata("success", "<b >You Have Registered Successfully !!! Please Check Your Email For The Verify Link</b>");
				$querycnt = $this->Generalmodel->show_data_id('countries','','','get','');
				$data['country'] = $querycnt;
				$data['title'] = "Registration";
				$this->load->view('header',$data);
			$this->load->view('register',$data);
			$this->load->view('footer');
			}
			else{
			//echo "Not Send Mail"; exit();
				$this->session->set_flashdata("success", "<b>You Have Registered Successfully!!! </b>");
				$querycnt = $this->Generalmodel->show_data_id('countries','','','get','');
				$data['country'] = $querycnt;
				$data['title'] = "Registration";
				$this->load->view('header',$data);
			$this->load->view('register',$data);
			$this->load->view('footer');
			}
		}
	}
	function verification() {
		$last = $this->uri->total_segments();
		$allias = $this->uri->segment($last);
		$data = array(
			'status' => 'Yes',
			'email_verified' => 'Yes'
		);
		$query = $this->Auth_model->userverification($allias,$data);
		$query = $this->Auth_model->userverificationupdt();
		@$userstat = $query[0]->status;
		@$emailveri = $query[0]->email_verified;
		if(($userstat == 'Yes') && ($emailveri == 'Yes')){
			$this->session->set_flashdata('success', 'Email successfully verified.!!!!');
				redirect('Home', $data);
		}
		else{
			$this->session->set_flashdata('error', 'Please try after sometimes.!!!!');
				redirect('Home', $data);
		}
	}

		//====================Login=====================
	public function login_view(){
		$data['page']='Login';
		$data['title'] = "Login";
		$this->load->view('header',$data);
		$this->load->view('user-login',$data);
		$this->load->view('footer',$data);
	}
	public function login_validation(){
		// Retrieve session data
		$session_set_value = $this->session->all_userdata();

		// Check for remember_me data in retrieved session data
		if (isset($session_set_value['remember_me']) && $session_set_value['remember_me'] == "1") {
		$this->load->view('admin_page');
		} else {

		//echo $a=$this->input->post('email');
		//echo $b=$this->input->post('password');exit;
		$query = $this->Auth_model->user_login($this->input->post('email'),md5($this->input->post('password')));
		@$status = $query[0]->status;
		@$email_verified = $query[0]->email_verified;
		@$email = $query[0]->email;
		@$uid = $query[0]->id;
		@$profile_pic = $query[0]->profile_pic;
		$remember = $this->input->post('remember_me_checkbox');
		if($remember){
		$this->session->set_userdata('remember_me_checkbox', true);
		}
		if($query==true && $status=='Yes'){
			$this->session->set_userdata('logged',@$session_data);
			@$session_data = array(
				'email'=>$email,
				'uid'=>$id,
				'is_userlogged_in' => $uid,
				'profile_pic' => $profile_pic,
				'password' => $this->input->post('password')
			);
			$record = array(
				'online_status' => '1'
			);
			
			$query = $this->Auth_model->online_status($uid, $record);
			$this->session->set_userdata('logged_in', $session_data);
			$this->session->set_userdata('is_userlogged_in', $email);
			$this->session->set_userdata('is_user_id', $uid);
			
			//$this->session->set_userdata('is_user_id',$uid);
			redirect('Home',$session_data);
			//exit;
		}else{
			$this->session->set_flashdata("error", "Invalid Username or Password !!!");
				redirect($_SERVER['HTTP_REFERER']."#errorlog");
			
		}
	}
	}

	public function forget_password(){
		$data['title'] = "Forget Password";
		$this->load->view('header',$data);
		$this->load->view('forgotpassword',$data);
		$this->load->view('footer',$data);
	}

	public function get_password(){
		$useremail=$this->input->post('email');
		$query = $this->Auth_model->get_user_details($useremail);
		if(count($query)>0){
			$userid = $query[0]->id;
				//===========================------------------=========================
			$from_email = "info@spacesharings.com";
			$to_email = $this->input->post('email');
			$url = base_url()."Auth/createnewpassword/".$userid;
				//***********************================******************
			$data1 = array(
				'membername' =>$query[0]->first_name." ".$query[0]->last_name,
				'email' => $this->input->post('email'),
				'url' => $url
			);
				//***********************================******************
			$msg = $this->load->view('userpasswordtemplate',$data1,TRUE);
			$config['mailtype'] = 'html';
			$this->load->library('email');
			$this->email->initialize($config);
			$msg = $msg;
			$subject = 'Password  Recovery For Your Space Share Account.';
			$this->email->from($from_email, 'Space Share ADMINISTRATOR');
			$this->email->to($to_email);
			$this->email->subject($subject);
			$this->email->message($msg);
				//===========================------------------=========================
			if($this->email->send()){
				//echo 1;
				$this->session->set_flashdata('success', 'Email Sent successfully Check Your Email!!!!');
				redirect('Page/user_login', $data);
			}else{
				$this->session->set_flashdata('error', 'Email Not Sent Please try after sometimes!!!!');
				redirect('Auth/forget_password', $data);
			}
		}else{
			$this->session->set_flashdata('error', 'This email address you requested is not registered in our database!!!!');
				redirect('Auth/forget_password', $data);
		}

	}
		//=========================Create New Password=========================
	function createnewpassword(){
			//==============Get Settings Process===============
			//$userid=end($this->uri->segment_array());
			//$data['email']=$userid;
		$data['title']='Change Password';
		$this->load->view('header',$data);
		$this->load->view('newpassword');
		$this->load->view('footer');
	}
	function getnewpassword(){
		@$id=$this->input->post('idd');
		$data = array(
			'password' =>md5($this->input->post('new_pass'))
		);
		$this->form_validation->set_error_delimiters('<span class="label label-danger" style="font-size:11px;">', '</span>');
		if($this->form_validation->run('change_pass')==FALSE){
			
			$this->session->set_flashdata("error"," <b>Something Went Wrong ..</b>");
				redirect($_SERVER['HTTP_REFERER']);
		}
		else {
				$query = $this->Auth_model->update_password($id, $data);
				//echo $this->db->last_query(); exit();
				
					$this->session->set_flashdata('success', 'Successfully Updated your password!!!!');
					redirect('Page/user_login', $data);
				
			 
		
		}
		
	}
		//=========================Create New Password=========================
	public function logout(){
		$uid = $this->session->userdata('is_user_id');
		$record = array(
			'online_status' => '0'
		);
		$query = $this->Auth_model->ofline_status($uid,$record);
		session_destroy();
   		redirect(base_url());
	}
		//====================Login=====================
}
?>
