<?php

class HostingModel extends CI_Model {

	function __construct() {
		parent::__construct();
	}



	public function insert($table_name,$post_data)
	{

		$return = $this->db->insert($table_name, $post_data);
		return $return;

	}
	public function fetch_hostings()

	{
		$id="2";
		$this->db->select ('*');
		$this->db->from('hosting');
		$this->db->where('host_type',$id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	public function browse_list()
	{
		$query = $this->db->get('user_contact');
		$result = $query->result();
		return $result;
	}
	
	public function fetch_adv_hostings()

	{
		$id="1";
		$this->db->select ('*');
		$this->db->from('hosting');
		$this->db->where('host_type',$id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

public function fetch_details($id)

	{
		
		$this->db->select ('*');
		$this->db->from('hosting');
		$this->db->where('id',$id);
		$query = $this->db->get();
		$result = $query->result();
		
		return $result;
	}

}