<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Accountactivation extends CI_Controller {

function __construct() {
			@parent::__construct();
			$this->load->library(array('form_validation','session'));
			//$this->load->model('supercontrol/member_model');
			$this->load->model('supercontrol/Generalmodel');
			//****************************backtrace prevent*** END HERE*************************
		}
	
	public function user()
	{
		$data = array(
					'email_verified' => 'Yes',
					'phone_verified' => 'Yes',
					'status' => 'Yes'
					);
		$id = $this->uri->segment(3);
		$uid = base64_decode($id);
		$tablename='yacht_users';
		$action='update';
		$id = $uid;
		$fieldname = 'id';
		$query = $this->Generalmodel->show_data_id($tablename,$id,$fieldname,$action,$data);
		$this->load->view('success_message');
	}
}
