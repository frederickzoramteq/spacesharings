<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('CountryModel','country');
		$this->load->model('HostingModel','Hostinglist');
		$this->load->model('Generalmodel');
		$this->load->model('supercontrol/Consignment_model','cm');
		/* Load form helper */
		$this->load->helper(array('form'));
		/* Load form validation library */
		$this->load->library('form_validation');

	}

	public function AdvanceSearch() {
		// $pdf = new Dompdf();
		$data['page']='Hostings';
		$this->load->view('header',$data);
		$data ['country']= $this->country->fetch_Country();
		$data ['languages']= $this->cm->getLang();
		$this->load->view('advancesearch',$data);
		$this->load->view('footer');
	}

	public function Show_detail($id) {
		$data['page']='Details';
		$this->load->view('header',$data);
		$data ['details']= $this->Hostinglist->fetch_details($id);
		$data ['image']= $this->Hostinglist->fetch_image($id);
		$data['similar_prop']=$this->Hostinglist->fetch_similar_prop($id);
		$this->load->view('detail',$data);
		$this->load->view('footer');
	}
	public function basic_hosting() {
		$data['page']='Hostings';
		$this->load->view('header',$data);
		$data ['country']= $this->country->fetch_Country();
		$this->load->view('free-form',$data);
		$this->load->view('footer');
	}
	public function adv_hosting() {
		$data['page']='Hostings';
		$this->load->view('header',$data);
		$data ['country']= $this->country->fetch_Country();
		$this->load->view('user-login');
		$this->load->view('footer');
	}
	public function user_login() {
		$data['page']='Login';
		$this->load->view('header',$data);
		$data ['country']= $this->country->fetch_Country();
		$this->load->view('user-login');
		$this->load->view('footer');
	}
	public function basic_search() {
		$data['page']='Search';
		$this->load->view('header', $data);
		$offset = ($this->uri->segment(3) != '' ? $this->uri->segment(3) : 0);
		$num_rows=$this->Generalmodel->host_bas_num_rows();
		$config['base_url'] = base_url().'Page/basic_search';
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 6;
		$config['num_links'] = $num_rows;
		$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_link'] = '&raquo;';
		$config['uri_segment'] = 3;
		$this->pagination->initialize($config);
		$data['hosting_list']=$this->Generalmodel->basic_host_list($config['per_page'],$offset);
		$data['total']=$this->Generalmodel->host_bas_num_rows();
		$data ['country']= $this->country->fetch_Country();
		$data['states']=$this->Generalmodel->states_view('231');
		$this->load->view('grid_view',$data);
		$this->load->view('footer');
	}
	public function adv_search() {
		$data['page']='Search';
		$this->load->view('header',$data);
		$offset = ($this->uri->segment(3) != '' ? $this->uri->segment(3) : 0);
		$num_rows=$this->Generalmodel->host_adv_num_rows();
		$config['base_url'] = base_url().'Page/adv_search';
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 6;
		$config['num_links'] = $num_rows;
		$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_link'] = '&raquo;';
		$config['uri_segment'] = 3;
		$this->pagination->initialize($config);
		$data['hosting_list']=$this->Generalmodel->adv_host_list($config['per_page'],$offset);
		$data['total']=$this->Generalmodel->host_adv_num_rows();
		$data ['country']= $this->country->fetch_Country();
		$this->load->view('adv_search',$data);
		$this->load->view('footer');
	}
	public function post_request() {
		$data['page']='Request';
		$this->load->view('header',$data);
		$data ['country']= $this->country->fetch_Country();
		$this->load->view('request',$data);
		$this->load->view('footer');
	}
	public function browse_request() {
		$this->load->model('Search_model');

		if($_POST){

			$search_data= array(
				'country' => $_POST['country'],
				'state' => $_POST['state'],
				'city' => $_POST['city'],
			);
			$data['page']='Request';
			$this->load->view('header',$data);
			$data['country']= $this->country->fetch_Country();
			//$data['browse']=$this->Hostinglist->filter_browse_list($search_data);
			//echo $this->db->last_query(); exit();

			$offset = ($this->uri->segment(3) != '' ? $this->uri->segment(3) : 0);
			$num_rows=$this->Hostinglist->filter_browse_num_rows($search_data);
			$config['base_url'] = base_url().'Page/browse_request';
			$config['total_rows'] = $num_rows;
			$config['per_page'] = 10;
			$config['num_links'] = $num_rows;
			$config['use_page_numbers'] = TRUE;
			$config['full_tag_open'] = '<ul class="pagination">';
			$config['full_tag_close'] = '</ul>';
			$config['prev_link'] = '&laquo;';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['next_link'] = '&raquo;';
			$config['uri_segment'] = 3;
			$this->pagination->initialize($config);
			
			$data['browse']=$this->Hostinglist->filter_browse_list($search_data, $config['per_page'], $offset);
			
			$data['country']=$this->input->post('country');
			$data['state']=$this->input->post('state');
			$data['city']=$this->input->post('city');
			$this->load->view('browse_request', $data);
			$this->load->view('footer');
		}else{
			$data['page']='Request';
			$this->load->view('header',$data);
			$data['country']= $this->country->fetch_Country();

			$offset = ($this->uri->segment(3) != '' ? $this->uri->segment(3) : 0);
			$num_rows=$this->Hostinglist->browse_num_rows();
			$config['base_url'] = base_url().'Page/browse_request';
			$config['total_rows'] = $num_rows;
			$config['per_page'] = 10;
			$config['num_links'] = $num_rows;
			$config['use_page_numbers'] = TRUE;
			$config['full_tag_open'] = '<ul class="pagination">';
			$config['full_tag_close'] = '</ul>';
			$config['prev_link'] = '&laquo;';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';

			$config['next_link'] = '&raquo;';
			$config['uri_segment'] = 3;
			$this->pagination->initialize($config);
			$data['browse']=$this->Hostinglist->browse_list($config['per_page'],$offset);

			//$data['browse']=$this->Hostinglist->browse_list();

			//print_r($data);exit;
			$this->load->view('browse_request', $data);
			$this->load->view('footer');
		}
	}
	public function contact() {
		$data['page']='Contact';
		$this->load->view('header',$data);
		$data ['country']= $this->country->fetch_Country();
		$data['contch'] = $this->Generalmodel->fetch_row('settings', "id=1");
		$this->load->view('contactus',$data);
		$this->load->view('footer');
	}
	public function term() {
		$data['page']='Term';
		$this->load->view('header',$data);

		$data['cms'] = $this->Generalmodel->fetch_row('cms', "id=2");
		$this->load->view('term',$data);
		$this->load->view('footer');
	}
	public function policy()
	{
		$data['page']='Policy';
		$this->load->view('header',$data);

		$data['cms'] = $this->Generalmodel->fetch_row('cms', "id=3");
		$this->load->view('policy',$data);
		$this->load->view('footer');
	}
	public function register_page() {

		$data['page']='Register';
		$this->load->view('header',$data);
		$data ['country']= $this->country->fetch_Country();
		$this->load->view('register',$data);
		$this->load->view('footer');
	}
	public function contactus_save() {
		//$uid = $this->session->userdata('is_user_id');
		$data['contch'] = $this->Generalmodel->fetch_row('settings', "id=1");
		$data['Contact']='Contact';
		$queryuser = $this->Generalmodel->show_data_id('settings','1','id','get','');
		$from_email = $queryuser[0]->email;
		$date1 = date('Y-m-d h:i:s');
		$to_email = $this->input->post('email');
		$this->form_validation->set_error_delimiters('<span class="label label-danger" style="font-size:11px;">', '</span>');
		if ($this->form_validation->run('contact_rules') == FALSE) {

			$this->session->set_flashdata("error", "These Fields can not be Blank!!"); 
			$data['page']='Contact';
			$this->load->view('header',$data);
			$data ['country']= $this->country->fetch_Country();
			$this->load->view('contactus');
			$this->load->view('footer');

		}else{
			$data = array(
				'ContactName' => $this->input->post('name'),
				'Email' => $to_email,
				'Message' => $this->input->post('msg'),
				'Subject' => $this->input->post('sub'),
				'ContactDate' =>$date1
			);

			$inserevent = $this->Generalmodel->do_action('contactus','','','insert',$data,'');


			//$msg = $this->load->view('contactadmin',$data,TRUE);
			$msg2 = $this->load->view('contactuser',$data,TRUE);
			$config['mailtype'] = 'html';
			$this->load->library('email');
			$this->email->initialize($config);
			$msg2 = $msg2;
			//$msg2 = $msg2;
			$subject2 = "Thank you for contacting Space share.";
			$this->email->from($from_email, "Space Share");
			$this->email->to($to_email);
			//$this->email->bcc($from_email);
			$this->email->subject($subject2);
			$this->email->message($msg2);
			//$this->email->message($msg2);


			if($this->email->send()){
				$this->session->set_flashdata('success', 'Email Sent successfully!!!!');
				redirect('Page/contact', $data);
			}else{
				$this->session->set_flashdata('success', 'Email can not be sent!!!!');
				redirect('Page/contact', $data);
			//$this->session->$this->session->flashdata('success');
			}
		}
	}
	public function adminmail() {
		//$uid = $this->session->userdata('is_user_id');
		$queryuser = $this->Generalmodel->show_data_id('settings','1','id','get','');
		$from_email = $queryuser[0]->email;
		$queryadmin = $this->Generalmodel->user_contact_limit();
		//echo $this->db->last_query(); exit();
		$date1 = date('Y-m-d h:i:s');
		$to_email = $queryadmin[0]->Email;
		$data = array(
			'ContactName' => $queryadmin[0]->ContactName,
			'Phone' =>  $queryadmin[0]->Phone,
			'Email' => $to_email,
			'Message' => $queryadmin[0]->Message,
			'ContactDate' =>$date1
		);

		//$msg = $this->load->view('contactadmin',$data,TRUE);
		$msg2 = $this->load->view('contactadmin',$data,TRUE);
		$config['mailtype'] = 'html';
		$this->load->library('email');
		$this->email->initialize($config);
		$msg2 = $msg2;
		//$msg2 = $msg2;
		$subject2 = "A New User has been contacted from spaceshare.";
		$this->email->from($to_email, "Space Share");
		$this->email->to($from_email);
		//$this->email->bcc($from_email);
		$this->email->subject($subject2);
		$this->email->message($msg2);
		//$this->email->message($msg2);

		//////////////////////

		if($this->email->send()){
			$this->session->set_flashdata('success', 'Email Sent successfully!!!!');
			redirect('Page/contactus');
		}else{
			$this->session->set_flashdata('success', 'Email can not be sent!!!!');
			redirect('Page/contactus');
		}
	}
	public function services(){
		$query_cms = $this->Generalmodel->show_data_id('cms','5','id','get','');
		$data['cmsdata'] = $query_cms;
		$data['title'] = "Services";
		$this->load->view('header',$data);
		$this->load->view('services');
		$this->load->view('footer');
	}
	public function faq(){
		$query_faq = $this->Generalmodel->show_data_id('faq','1','faq_status','get','');
		$data['faqdata'] = $query_faq;
		$data['title'] = "FAQ";
		$this->load->view('header',$data);
		$this->load->view('faq');
		$this->load->view('footer');
	}
	public function basic_search_results(){
		$data['page']='Search';

		$data ['country']= $this->country->fetch_Country();
		$this->load->model('Search_model');

		if($_POST)
		{
    		$search_data=array('country' => $_POST['country'],
                			   'zip' => $_POST['zipcode']
                			  );
    		isset($_POST['state']) ? $search_data['state'] = $_POST['state'] : $search_data['state'] = 0;
    		isset($_POST['city']) ? $search_data['city'] = $_POST['city'] : $search_data['city'] = 0;

    		$data['search']=$this->Search_model->basic_filter_data($search_data);
			//echo $this->db->last_query(); exit();
		}
		
		$data['countryCode']=$this->input->post('country');
		$data['stateCode']=$this->input->post('state');
		$data['cityCode']=$this->input->post('city');
		$data['zipcode']=$this->input->post('zipcode');
		//echo $this->db->last_query(); exit();
		$this->load->view('header',$data);
		$this->load->view('search_results');
		$this->load->view('footer', $data);
	}
	public function adv_search_results(){
		$data['page']='Search';
		$data ['country']= $this->country->fetch_Country();
		$this->load->model('Search_model');
		$data['search']=$this->Search_model->search_data();
		//print_r($data);exit;
		if($_POST)
		{
			$search_data=array('country' => $_POST['country'],
                			   'zip' => $_POST['zipcode'],
                			   'languages' => $_POST['languages'],
                			   'renterCriteriaGender' => $_POST['gender'],
                			   'renterCriteriaAgeGroup' => $_POST['age_group'],
                			   'renterCriteriaNationality' => $_POST['nationality'],
                			   'renterCriteriaEthnicity' => $_POST['ethinicity'],
                			   'renterCriteriaCategory' => $_POST['category_list'],
                			   'renterCriteriaMotherTongue' => $_POST['languages']
                			  );
			isset($_POST['state']) ? $search_data['state'] = $_POST['state'] : $search_data['state'] = 0;
			isset($_POST['city']) ? $search_data['city'] = $_POST['city'] : $search_data['city'] = 0;

			$data['search']=$this->Search_model->adv_filter_data($search_data);
		}
		$data['languages']=$this->input->post('languages');
		$data['countryCode']=$this->input->post('country');
		$data['stateCode']=$this->input->post('state');
		$data['cityCode']=$this->input->post('city');
		$data['gender']=$this->input->post('gender');
		$data['age_group']=$this->input->post('age_group');
		$data['nationality']=$this->input->post('nationality');
		$data['ethinicity']=$this->input->post('ethinicity');
		$data['category_list']=$this->input->post('category_list');
		$data['languages']=$this->input->post('languages');
		$data['zipcode']=$this->input->post('zipcode');
		$this->load->view('header',$data);
		$this->load->view('adv_search_results');
		$this->load->view('footer', $data);
	}
	public	function subcuser()
	{
		if($this->input->post('submit')=='submit'){
			$email=$this->input->post('email');
			$fetch_row = $this->Generalmodel->fetch_row('subscribed', "email='$email'");
			if (count($fetch_row)> 0) {
				$this->session->set_flashdata('warn_sub', 'Already Subscribed');
				redirect($this->input->post('redirect'));
			}else {

				$mydate=date('Y-m-d');
				$subdata = array(
					'email' => $email,
					'status' =>'Active',
					'sub_date' => $mydate
				);
				$result=$this->Generalmodel->add_details('subscribed',$subdata);
				if($result)
				{
					$this->session->set_flashdata('succ_sub', 'Succeessful Subscribed Email');
					redirect($this->input->post('redirect'));
				}else{
					$this->session->set_flashdata('err_sub', 'Something Wrong');
					redirect($this->input->post('redirect'));
				}

			}


		}


	}
	function updatedata()
	{
		
		$dataupdate = array(
			'mother_tongue' => "Any"
		);
		$query = $this->Generalmodel->eidt_single_row('hosting', $dataupdate, "mother_tongue='selected'");
		if($query==TRUE)
		{
			echo "updated";
		}

	}
}
