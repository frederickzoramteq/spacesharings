<?php //include "header.php";?>
<div class="property_view_wrapper">
	<div class="container">
		<style>
         .error
         {
         font-size: 16px;
         color: red;
         }
      </style>
		<div class="row">
			<div class="col-lg-12 col-md-12">

				<div class="row">
					
					<div class="col-lg-4 col-md-4">
					</br>
				</br>
			</br>
		</br>
		<img src="<?php echo base_url('assets/images/faq2.png');?>" >

	</div>
	<div class="col-lg-8 col-md-4">
		<p class="topcaption">Use this form to specify your rental requirements - the location, from when and the duration of the stay. In the description section, please mention your requirements of the type of property, services needed and any other criteria. Based on the information, a prospective host may get in touch. Or we in Space Share can match with you with the prospective hosts.</p>
		<div class="sidebar_wrapper">
			<div class="widget advance_search_wrapper">		
				<div class="widget-title"><h4>Rental Request</h4></div>
				<div class="search_form">
					<form name="requestfrom" id="requestfrom"  action="<?php echo base_url();?>Request/contactus_save" method="POST">
						<?php
							
							echo $this->session->flashdata('success');
							echo $this->session->flashdata('fail');
				            echo $this->session->flashdata('add_message');
                            echo $this->session->flashdata('success_msg');
                        ?>
						<div class="row">
							<div class="col-lg-6 col-md-6 form-group">
								<input type="text" class="" placeholder="Your Name*" id="rname" maxlength="50" name="name" value="<?php echo set_value('name'); ?>"> 
								  <?php echo form_error('name'); ?>
							</div>

							<div class="col-lg-6 col-md-6 form-group">
								<input type="email" class="" placeholder="Email Address*" id="rmail" name="email" value="<?php echo set_value('email'); ?>">
								<?php echo form_error('email'); ?>
							</div>
						</div>
<div class="row">
							<div class="col-lg-12 col-md-6 form-group">
								<input type="text" class="" placeholder="Rental Location*" id="rsub" name="location" value="<?php echo set_value('location'); ?>"> 
								<?php echo form_error('location'); ?>
							</div>
						</div>
						<div class="row">

							<div class="col-lg-4 col-md-4 form-group">
								<select name="country" id="country" class="form-group">
									<option value="menu_order" selected="selected">Country</option>
									<!-- <?php foreach ($country as $value) {
										?>
										<option value='<?= $value->count_id;?>' >
											<?= $value->count_name;?>
										</option>
										<?php
									}
									?> -->
									 <option value="231">United States</option>
             <option value="38">Canada</option>
             <option value="44">China</option>
             <option value="101">India</option>
              <option value="109">Japan</option>
             <option value="230">United Kingdom </option>
								</select>
								<span class="error" style="color:red" id="error_message_afcountry"></span>
							</div>

							<div class="col-lg-4 col-md-4 form-group">

								<select name="state" id="state" class="form-group">
									<option value="" selected="selected">City</option>
								</select>
								<span class="error" style="color:red" id="error_message_afstate"></span>
							</div>
							<div class="col-lg-4 col-md-4 form-group">
								<select name="city" id="city" class="form-group">
									<option value="" selected="selected">State</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-6 form-group">
								<input type="text" class="" placeholder="Subject*" id="rsub" name="subject" value="<?php echo set_value('subject'); ?>"> 
								<?php echo form_error('subject'); ?>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 form-group">
								<textarea rows="7" class="" placeholder="Describe your rental requirements*" id="rmsg" name="message" value="<?php echo set_value('message'); ?>"></textarea>
								<?php echo form_error('message'); ?>
							</div>
						</div>

						<div id="msg-request" class="error"></div>

						<div class="btn_wrapper1">
							<input  type="submit" class="btn rs_btn" id="rqs_submit" value="Submit Request">
						</div>
					</form>

				</div>
			</div>

		</div>
	</div>
</div>
</div>
</div>
</div>
</div>
<?php //include "footer.php";?>