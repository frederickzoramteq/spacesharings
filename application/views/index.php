<?php //include "header.php";?>
<!-- Banner Start -->
<div class="clearfix"></div>
<div class="container">
<style>
.carousel-inner .first_revoslide, .carousel-inner .third_revoslide, .carousel-inner .second_revoslide{
	min-height:471px;
	}
</style>

<div class="row clearfix">
    <div class="bannerbg">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="row clearfix">
            	<div class="col-sm-6 col-xs-6">
                	<div class="bannertext">
                   <a href="<?php if ($this->session->userdata('is_user_id')!=""):  echo base_url('Page/AdvanceSearch'); else: echo base_url('Page/basic_hosting');  endif ?>">
                    	<h2>Going abroad for studies? Find the perfect host.</h2>
                        <p>Find your comfort zone by finding the perfect place and family to stay with while getting a sense of security and feeling like home.</p>
                        </a>
                    </div>
                </div>
            	<div class="col-sm-6 col-xs-6">
                	<div class="bannertext">
                     <a href="<?php if ($this->session->userdata('is_user_id')!=""):  echo base_url('Page/AdvanceSearch'); else: echo base_url('Page/basic_hosting');  endif ?>">
                    	<h2>Want an affordable space to stay without rental hassles and ready to move in?</h2>
                        <p>Find your ideal space for just the amount of time you need. Get additional services of your choice.</p></a>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
            	<div class="col-sm-6 col-xs-6">
                	<div class="bannertext">
                    <a href="<?php if ($this->session->userdata('is_user_id')!=""):  echo base_url('Page/adv_search'); else: echo base_url('Page/basic_search');  endif ?>">
                    	<h2>Lonely Empty Nester with rooms to spare? Find an ideal renter.</h2>
                        <p>Rent out your empty rooms and earn extra cash. No need to downsize or move but stay where you are most comfortable – your own home</p>
                        </a>
                    </div>
                </div>
            	<div class="col-sm-6 col-xs-6">
                	<div class="bannertext">
                    <a href="<?php if ($this->session->userdata('is_user_id')!=""):  echo base_url('Page/adv_search'); else: echo base_url('Page/basic_search');  endif ?>">
                    	<h2>Your home is an asset and resource. Use it to earn money.</h2>
                        <p>Rent out your place while you are away or rent out a room or two while staying at the property. No extra effort or investment needed while you earn extra cash </p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 " style="background: #2b2525;
          margin-left: -15px;
          padding-top: 52px;">
          <!--<div class="testimonial_wrapper padding_right50">-->
          <div class="col-lg-12 col-md-12">
             <div class="testimonial_slider">
                <div class="owl-carousel owl-theme">
                  <?php foreach($testimonial as $data){?>
                  <?php //print_r($data);?>
                   <div class="item">
                      <div class="icon_wrapper1">
                        <?php if(!empty($data['testimonial_image'])){?>
                         <img class="img-circle" src="<?php echo base_url();?>assets/images/<?php echo $data['testimonial_image'];?>" alt="">
                         <?php } else{?>
                          <img class="img-circle" src="<?php echo base_url();?>assets/images/avtar.png" alt="">
                          <?php }?>
                      </div>
                      <div class="testimonial_content">
                         
                         <p><?php echo $data['testimonial_desc']?></p>
                         <span><?php echo $data['posted_by']?></span>
                      </div>
                   </div>
                   <?php }?><!-- end of for each loop-->

                   <!-- <div class="item">
                      <div class="icon_wrapper1" style=".icon_wrapper1 {
                         float: left;
                         display: inline-block;
                         margin: 0 0 0 30%;
                         text-align: center;
                         border-radius: 60px;
                         position: relative;
                         }">
                         <img class="img-circle" src="<?php echo base_url('assets/images/testimonial_3.jpg');?>" alt="">
                      </div>
                      <div class="testimonial_content">
                         
                         <p>Preferred this stay much more than staying at a hotel. Made arrangements to get breakfast and dinner from the host. So had lovely made to order home made meals. The hosts also took care of any special needs I had.</p>
                         <span>John Spencer</span>
                      </div>
                   </div> -->
                   <!-- <div class="item">
                      <div class="icon_wrapper1">
                         <img class="img-circle" src="<?php echo base_url('assets/images/testimonial_4.png');?>" alt="">
                      </div>
                      <div class="testimonial_content">
                         
                         <p>I really got a good deal and a more personalized stay. Would highly recommend this option instead of a hotel or a rental stay.</p>
                         <span>Lei Zhang</span>
                      </div>
                   </div> -->
                   <!-- <div class="item">
                      <div class="icon_wrapper1">
                         <img class="img-circle" src="<?php echo base_url('assets/images/testimonial_2.png');?>" alt="">
                      </div>
                      <div class="testimonial_content">
                         
                         <p>My hosts proved to be a wonderful couple. I got a furnished room with all the facilities I could ask for. Got to use the internet connections and laundry from the very first day. Also got good advices and help on how to understand the local lingos.</p>
                         <span>Lohn Anderson</span>
                      </div>
                   </div> -->
                   <!-- <div class="item">
                      <div class="icon_wrapper1">
                        <img class="img-circle" src="<?php echo base_url('assets/images/testimonial_2.png');?>" alt="">
                      </div>
                      <div class="testimonial_content">
                         
                         <p>Had a wonderful stay by hooking up with the right people through this site. Would highly recommend this website.</p>
                         <span></span>
                      </div>
                   </div> -->
                </div>
                <!--</div>--> 
             </div>
          </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
</div>
<!-- Banner End -->
<!-- Top Rated Wrapper Start -->
<div class="top_rated_wraper">
   <div class="container">
      <div class="row">
         <div class="col-lg-12">
            <div class="rs_heading_wrapper">
               <div class="rs_heading">
                      <h3><?php echo $about[0]['cms_sub_heading']?></h3>
               </div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 rpw1">
            <?php echo $about[0]['description']?>
            <!-- <h5>About Us - We are the "UBER" of Space-Share service. What Uber did in ride-share space, Space Share does the same in property sharing space.</h5>
            <p>Aim is to bring the right match between the property owner or host and the renter or paying guest. Not all the properties shown here are available for rental at this time. Please go to the Rental section to search for available properties.</p> -->
            <!-- <div class="btn_wrapper"><a href="javascript:void(0);" class="btn rs_btn">read more</a></div> -->
         </div>
         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 rpw1">
            <div class="img_wrapper">
               <img src="<?php echo base_url('assets/images/blog/blog.png');?>" alt="">
            </div>
         </div>
      </div>
   </div>
</div>
<?php //include "footer.php"?>