        <!-- Footer Wrapper Start -->
         <div class="modal requestmodal fade bd-example-modal-lg" id="propertyCarouselModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top: 22px;">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
              </div>
              <div class="modal-body">
               <h4>This is the Property's Carousel Modal (WORK IN PROGRESS)</h4>
              </div>
              <div class="modal-footer">
                <div class="btn btn-primary btn-sm" data-dismiss="modal">Close</div>
              </div>
            </div>
          </div>
    	</div>
        
    	<?php 
    	   $this->load->model('Generalmodel'); 
           $cont = $this->Generalmodel->fetch_row('settings', "id=1");
             //print_r($cont);
        ?>
        <div class="suscribe_wrapper" data-stellar-background-ratio=".5">
        	<div class="banner_overlay">
        		<div class="container">
        			<div class="row">
        				<div class="suscribe_content">
        					<div class="col-lg-8 col-md-8">
        						 <?php 
        						      if($this->session->flashdata('succ_sub'))
        						      {
        						 ?>
                                        <div class="alert alert-success">
                                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                                            <strong>Success!</strong> 
                                            <?php 
                                                echo $this->session->flashdata('succ_sub'); 
                                            ?>
                                        </div>
                                <?php 
        						      }
        						      else if($this->session->flashdata('err_sub'))
        						      {  
        						?>
                                        <div class="alert alert-danger">
                                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                                            <strong>Error!</strong> <?php echo $this->session->flashdata('err_sub'); ?>
                                        </div>
                                <?php 
        						      }
        						      else if($this->session->flashdata('warn_sub'))
        						      {  
        						?>
                                        <div class="alert alert-warning">
                                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                                            <strong>Warning!</strong> <?php echo $this->session->flashdata('warn_sub'); ?>
                                        </div>
                                <?php 
        						      }
        						 ?>
        						<div class="suscribe_content_wrapper">
        							<div class="news_detail"><h4>Subscribe Newsletter</h4><p>Get weekly updates of hot properties</p></div>
        							<div class="news_search_wrapper">
        								<form action="<?php echo base_url();?>Page/subcuser" method="post">
        									<input type="hidden" name="redirect" value="<?php echo current_url(); ?>">
        								<input type="email" name="email" required class="form-control search_input" placeholder="Enter your email address">
        								 <button type="submit" name="submit" class="btn search_btn" value="submit">subscribe</button>
        								
        							</form>
        							</div>
        						</div>
        					</div>
        					<div class="col-lg-4 col-md-4">
        						<div class="social_wrapper">
        							<ul>
        								<li><a href="<?=$cont->facebook_link?>" target="_blank"><i class="flaticon-facebook55"></i></a></li>
        								<li><a href="<?=$cont->twitter_link?>" target="_blank"><i class="flaticon-twitter1"></i></a></li>
        								<li><a href="<?=$cont->googleplus_link?>" target="_blank"><i class="flaticon-google116"></i></a></li>
        								<li><a href="<?=$cont->instagram_link?>" target="_blank"><i class="flaticon-pinterest34"></i></a></li>
        							</ul>
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>
        <div class="footer_wrapper">
        	<div class="container">
        		<div class="row">
        			<div class="col-lg-3 col-md-3">
        				<div class="widget widget_text">
        					<h4 class="widget-title">about us</h4>
        					<p>We are a group seeking to bring people together who wants to host paying guests and those looking for a homely touch away from home. In the process creating bonds and friendships while making good use of already existing resources and earning extra income .</p>
        					
        				</div>
        			</div>
        			<div class="col-lg-3 col-md-3">
        				<div class="widget widget_twitter_feed">
        					<h4 class="widget-title">Contact Us</h4>
        					<ul>
        
        						<li><i class="flaticon-phone41"></i><span><?=$cont->phone?></span></li>
        						<li><i class="flaticon-mail80"></i><span><?=$cont->email?>
        </span></li>
        						<li><i class="flaticon-pin56"></i><span>
        <?=$cont->address?></span></li>
        					</ul>
        				</div>
        			</div>
        			<div class="col-lg-3 col-md-3">
        				<div class="widget widget_flickr">
        					<h4 class="widget-title">Recent properties</h4>
        					<ul>
        						<li><a href="javascript:void(0)"><img src="<?php echo base_url('assets/images/rp1.jpg');?>" alt=""></a></li>
        						<li><a href="javascript:void(0)"><img src="<?php echo base_url('assets/images/rp2.jpg');?>" alt=""></a></li>
        						<li><a href="javascript:void(0)"><img src="<?php echo base_url('assets/images/rp3.jpg');?>" alt=""></a></li>
        						<li><a href="javascript:void(0)"><img src="<?php echo base_url('assets/images/rp4.jpg');?>" alt=""></a></li>
        						<li><a href="javascript:void(0)"><img src="<?php echo base_url('assets/images/rp5.jpg');?>" alt=""></a></li>
        						<li><a href="javascript:void(0)"><img src="<?php echo base_url('assets/images/rp6.jpg');?>" alt=""></a></li>
        						<li><a href="javascript:void(0)"><img src="<?php echo base_url('assets/images/rp7.jpg');?>" alt=""></a></li>
        						<li><a href="javascript:void(0)"><img src="<?php echo base_url('assets/images/rp8.jpg');?>" alt=""></a></li>
        					</ul>
        				</div>
        			</div>
        			<div class="col-lg-3 col-md-3">
        				<div class="widget widget_recent_property">
        					<h4 class="widget-title">recent properties</h4>
        					<div class="tagcloud"><a href="javascript:void(0)">Popo Homes</a><a href="javascript:void(0)">interior</a><a href="javascript:void(0)">property</a><a href="javascript:void(0)">hot properties</a><a href="javascript:void(0)">rental homes</a><a href="javascript:void(0)">office for sale</a><a href="javascript:void(0)">properties in uk</a><a href="javascript:void(0)">buy home</a><a href="javascript:void(0)">private villa with garden</a>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>
        <!-- Footer Wrapper End -->
        
        <!-- BottomFooter Wrapper Start -->
        <footer class="bottom_footer_wrapper">
        	<div class="container">
        		<div class="row">
        			<div class="col-lg-12 col-md-12">
        				<div class="bottom_footer">
        					<ul>
        						<li><a href="javascript:void(0)">© Copyright 2016 spacesharings.com</a></li>
        						<li><a href="<?=base_url('Page/term');?>">Terms &amp; Conditions</a></li>
        						<li><a href="<?=base_url('Page/policy');?>">Privacy Policy</a></li>
        						</li>
        					</ul>
        				</div>
        			</div>
        		</div>
        	</div>
        </footer>
        <!-- BottomFooter Wrapper End -->
        
        <!--main js file start--> 
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-2.2.4.min.js');?>"></script>
        
        <script>
            $(document).ready(function() {
                $("#submit").on('click', function() {
                	    var keyword=$("#search_key").val();
                	    var country=$("#country").val();
                	    var state=$("#state").val();
                	    var city=$("#city").val();
                	    var zip_code=$("#zip_code").val();
                	    var languages =$("#languages").val();
                	    var category_list=$("#category_list").val();
                	    /*var dataString = 'keyword1='+ keyword + '&country1='+ country + '&state1='+ state + '&city1='+ city + '&zip_code1='+ zip_code+ '&languages1='+ languages + '&category_list='+ category_list ;
        		        alert(dataString);*/
        		     	//  exit;
                        $.ajax({
                        url: '<?= base_url("AjaxController/search")?>',
                        type: 'POST',
                        dataType: 'json',
                        // data: dataString,
                       	data: {key: keyword, country: country, state:state, city:city, zip:zip_code,lang:languages,catg_l: category_list},
                        success: function(data){
                          //alert(data);
                          console.log(data);
                          //$("#state").html(data);
                      }
                  })
                    
                });
            });
        </script>
        <script>
            $(document).ready(function()
            	    		  {
                                $("#country").on('change',
                                				 function()
                                				 {
                                					fetchStates($(this).val());
                   				 				 }
                                			    );
        
                                $("#state").on('change',
                                        		function()
                                        		{
                									fetchCities($(this).val());
                                    			}
            						  		  );
        
            					var countryCode = "<?php if(isset($countryCode) && !is_array($countryCode)) echo $countryCode; ?>";
        						var stateCode = "<?php if(isset($stateCode)) echo $stateCode; ?>";
                                var cityCode = "<?php if(isset($cityCode)) echo $cityCode; ?>";
        
        	  				  	if( parseInt(countryCode) >= 0 && typeof countryCode != 'undefined' )
        	  				  	{
        		  				  $("#country").val(countryCode);
        	  				      fetchStates(countryCode, stateCode);
        		  			  	}
        
        		  			  	if( parseInt(stateCode) >= 0 && typeof stateCode != 'undefined')
        		  			    {
        		  				  fetchCities(stateCode, cityCode);
        			  		  	}
            				  }
        	  				);
        
        
        	function fetchStates(countryCode, stateCode)
        	{
        		var localCopyCountryCode = countryCode;
        		var localCopyStateCode = stateCode;
        
             	$.ajax({url: '<?= base_url("AjaxController/fetch_state")?>',
                        type: 'POST',
                        data: {data: countryCode},
                        success: function(data)
                        		 {
                                  	$("#state").html(data);
          		  				  	$("#state").val(localCopyStateCode);
        
          		  				  	if(localCopyStateCode == 0 || localCopyStateCode == '' || typeof localCopyStateCode == 'undefined')
          		  				  	{
          		  				      $("#state").val(0);
          	  		  				}
                  				 }
          		   	   }
        	          );
        	}
        
        	function fetchCities(stateCode, cityCode)
        	{
        		var localCopyStateCode = stateCode;
        		var localCopyCityCode = cityCode;
        
        		$.ajax({url:'<?= base_url("AjaxController/fetch_city")?>',
        				type:'POST',
        				data:{data: stateCode},
        				success: function(data)
        						 {
        							$("#city").html(data);
        							$("#city").val(localCopyCityCode);
        
          		  				  	if(localCopyCityCode == 0 || localCopyCityCode == '' || typeof localCopyCityCode == 'undefined')
          		  				  	{
          		  				  	  $("#city").val(0);
          	  		  				}
        						 }
        			   }
        	          );
        	}
        </script>
        
        <!--Plugin-->
        <script type="text/javascript" src="<?php echo base_url('assets/js/modernizr.custom.js');?>"></script> 
        <script type="text/javascript" src="<?php echo base_url('assets/js/plugin/smoothscroll/smoothscroll.js');?>"></script> 
        <script type="text/javascript" src="<?php echo base_url('assets/js/plugin/animate/wow.js');?>"></script> 
        <script type="text/javascript" src="<?php echo base_url('assets/js/plugin/parallax/stellar.js');?>"></script> 
        <script type="text/javascript" src="<?php echo base_url('assets/js/plugin/owl/owl.carousel.js');?>"></script> 
        <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-multiselect.js');?>"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#example-getting-started').multiselect();
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#example-getting-started1').multiselect();
            });
        </script>
        <script type="text/javascript">
          $("#uploadFile").change(function(){
             $('#image_preview').html("");
             var total_file=document.getElementById("uploadFile").files.length;
            for(var i=0;i<total_file;i++)
             {
              $('#image_preview').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");
             }
          });
          $('form').ajaxForm(function() 
           {
            alert("Uploaded SuccessFully");
           }); 
        </script>
            
        
        <script src="<?php echo base_url('assets/js/slick.min.js');?>"></script>
        <script src="<?php echo base_url('assets/js/owl.carousel.min.js');?>"></script>
    	<script src="<?php echo base_url('assets/js/owl.animate.js');?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.prettyPhoto.js');?>"></script>
        <script src="<?php echo base_url('assets/js/custom2.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/plugin/priceslider/bootstrap-slider.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/custom.js');?>"></script> 
        <script src="<?php echo base_url('plugins/select2/select2.full.min.js');?>"></script>
    </body>
</html>