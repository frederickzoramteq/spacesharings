<div class="contactus_wraper">
   <div class="container">
      <div class="row">
         <div class="col-lg-12">
            <div class="rs_heading_wrapper">
               <div class="rs_heading">
                  <h3><?php echo $cms->cms_pagetitle; ?> </h3>
               </div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="contact_section">
           
            <div class="col-lg-12 col-md-12">
              <?php echo $cms->description; ?> 
            </div>
           
         </div>
      </div>
   </div>
</div>
