<!DOCTYPE html>

<html lang="en">
<?php 
if($this->session->userdata('is_user_id')!='') 
{
//$this->session->set_userdata('is_user_id',$uid);
	$userid=$this->session->userdata('is_user_id');
    // echo $userid;
}
?>
<head>
	<meta charset="utf-8" />
	<title><?=PROJECT_NAME?> </title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta name="description"  content="space share"/>
	<meta name="keywords" content="space share">
	<meta name="author"  content=""/>
	<meta name="MobileOptimized" content="320">
	<link href="https://fonts.googleapis.com/css?family=Montserrat%7cRaleway:400,500%7cPoppins%7cSource+Sans+Pro%7cPoppins%7cSource+Sans+Pro" rel="stylesheet" />

	<!-- Font Awesome -->
	<link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css"/>
	<!--CSS Start-->
	<link href="<?php echo base_url('assets/css/main.css'); ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url('assets/css/style5.css');?>" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="plugins/select2/select2.min.css" />
	<link href="<?php echo base_url('assets/css/slick.css'); ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url('assets/css/slick-theme.css'); ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url('assets/css/prettyPhoto.css'); ?>" rel="stylesheet" type="text/css"/>
	
	 <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-multiselect.css'); ?>" type="text/css">
        
	<!--CSS End-->
	<!-- favicon links-->
	<link rel="icon" type="image/icon" href="<?php echo base_url('assets/images/favicon.png');?>" />
</head>
<!--Head End-->
<!--Body Start-->
<body>


	<!-- Main menu Start -->
	<div class="menu_wrapper">
		<div class="container">
			<div class="">
				<div class="navbar_header">
					<div class="logo"><a class="navbar_brand" href=""><img src="<?php echo base_url('assets/images/Space-Sharings-01.png');?>" alt="">
					</a></div>
				</div>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="">
				<div class="menu_toggle"><span></span><span></span><span></span></div>
				<div class="real_menu">
					<div class="close_btn"></div>
					<div class="menu_overlay"></div>
					<ul class="nav navbar-nav navbar-right">
						<li class="rightBorderDarkYellow 
						           <?php 
						               if($page=="home")
						               { 
						           ?>
						             		active
						           <?php
                                       }
						           ?>
						          "
						>
							<a href="<?php echo base_url('Home');?>">home</a>
						</li>
						<li class="rightBorderDarkYellow 
						            <?php 
						               if($page=="Register")
						               { 
						            ?>
						            	 active
						            <?php 
                                       }
						            ?>
						          "
						>
						    <?php 
							    if($this->session->userdata('is_user_id')!='') 
							    {
							        $userid=$this->session->userdata('is_user_id');
						        }
						        else
								{
							?>
							<a href="<?=base_url('Page/register_page');?>">Register</a>
							<?php
								}
							?>	
						</li>
						<li class="rightBorderDarkYellow 
						           <?php 
						              if($page=="Hostings")
						              { 
						           ?>
						           	active 
						           <?php 
                                      } 
                                   ?>"
                        >
                        	<a href="javascript:void(0)">
                        		Host &amp; Earn <i class="flaticon-arrow483"></i>
                        	</a>
							<ul class="sub-menu">
								<li><a href="<?php echo base_url('Page/basic_hosting');?>"> Basic Hosting</a></li>
								<li>
									<?php 
									if($this->session->userdata('is_user_id')!='') 
									{
										$userid=$this->session->userdata('is_user_id');

										?>
										<!-- <a href="<?php echo base_url('Page/AdvanceSearch');?>">Advanced Hosting</a> -->
										<?= anchor('Page/AdvanceSearch', 'Advanced Hosting'); ?>
									
									<?php
								}
								else
								{
									?>
									<a href="<?php echo base_url('Page/user_login');?>">Advanced Hosting</a>
									<?php
								}
								?>
								</li>			
							</ul>
						</li>
						<li class="rightBorderDarkYellow 
						           <?php 
						             if($page=="Search")
						             { 
						           ?> 
						           	active 
						           <?php 
                                     } 
                                   ?>"
                        >
                        	<a href="javascript:void(0)">
                        		Search Rental <i class="flaticon-arrow483"></i>
                        	</a>
							<ul class="sub-menu">
								<li><a href="<?php echo base_url('Page/basic_search');?>">Basic Search View</a></li>
								<li>
									<?php 
									   if($this->session->userdata('is_user_id')!='') 
									   {
									?>
										<a href="<?php echo base_url('Page/adv_search');?>">Advanced Search </a>
									<?php 
									   } 
									   else
									   { 
									?>
										<a href="<?php echo base_url('Page/user_login');?>">Advanced Search</a>
									<?php 
									   }
									?>
								</li>
							</ul>
						</li>
						<li class="rightBorderDarkYellow 
						           <?php 
						              if($page=="Request")
						              { 
						           ?> 
						           	active 
						           <?php 
                                      } 
                                   ?>"
                        >
                        	<a href="javascript:void(0)">
                        		Rental Requests <i class="flaticon-arrow483"></i>
                        	</a>
							<ul class="sub-menu">
								<li><a href="<?php echo base_url('Page/post_request');?>">Post Your Requests</a></li>
								<li><a href="<?php echo base_url('Page/browse_request');?>">Browse Requests</a></li>
							</ul>
						</li>


						<li class="rightBorderDarkYellow 
						           <?php 
						              if($page=="Contact")
						              {
						           ?>
						               active
						           <?php 
                                      }
                                   ?>"
                        >
                        	<a href="<?php echo base_url('Page/contact');?>">contact us</a>
                       	</li>

						<li class="<?php 
						              if($page=="Login")
						              { 
						           ?> 
						               active 
						           <?php 
                                      } 
                                   ?>"
                        >

							<?php 
							    if($this->session->userdata('is_user_id')!='') 
							    {
								  $userid=$this->session->userdata('is_user_id');
							?>
							<?= anchor('Auth/logout', 'Logout'); ?>

						</li>
							<?php
						        }
						        else
						        {
							?>

									<?= anchor('Page/user_login', 'Login'); ?>
						</li>
						<?php
					            }
					    ?>	
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- Main menu End -->