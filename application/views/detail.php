<!--<div class="page_wrapper" data-stellar-background-ratio=".5">
   <div class="banner_overlay">
      <div class="container">
         <div class="col-lg-12">
            <div class="row">
               <div class="page_title">
                  <h3>Property Listings </h3>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>-->
<!--<div class="breacrumb_wrapper">
   <div class="container">
      <div class="row">
         <div class="col-lg-12">
            <ol class="breadcrumb">
               <li><a href="<?php echo base_url('Home');?>">Home</a></li>
               <li class="active">Property Listing</li>
            </ol>
         </div>
      </div>
   </div>
</div>-->


<div class="property_view_wrapper">
   <div class="container">
      <div class="row">
        <div class="col-md-12">
        <div class="row">
           <div class="col-lg-12">
              <div class="rs_heading_wrapper">
                 <div class="rs_heading">
                    <h3 class="property_view_h center"><?php echo $details[0]['title'];?></h3>
                    <!--<h2><span>price - </span>$24000</h2>-->
                   
                 </div>
              </div>
           </div>
        </div>
        <div class="row">
           <div class="col-lg-12 col-md-12">
             <div class="slideshow">
             	<span class="post-author">Posted by - <?php echo $details[0]['fname'];?></span>
				<div class="slideshow-main">
                    <div class="slide">
                        <?php
                            if(count($image)>0)
                            {
                                foreach($image as $img)
                                {
                        ?>
                                <div>
                                    <img src="<?php echo base_url();?>uploads/blog/<?php echo $img['image_name']?>"  alt="Slide show">
                                </div>
                        <?php
                                } 
                            }
                            else
                            {
                        ?>
                          		<div>
                                    <img src="<?php echo base_url();?>uploads/blog/noimg.png"  alt="Slide show">
                                </div>
                        <?php 
                            }
                        ?>
                    </div>
                 </div>
                 <div class="slideshow-nav-main">
                    <div class="slideshow-nav">
                      <?php 
                          if(count($image)>0)
                          {
                            foreach($image as $img)
                            {
                      ?>
                                <div>
                                    <img src="<?php echo base_url();?>uploads/blog/<?php echo $img['image_name'];?>" width="100" height="70" alt="Slide show thumb">
                                </div>
                      <?php
                            }
                          }
                          else
                          {
                      ?>
                              <div>
                                <img src="<?php echo base_url();?>uploads/blog/noimg.png"  alt="Slide show" width="100" height="70">
                            </div>
                      <?php
                          }
                      ?>
                    </div>
                 </div>
              </div>
              <div class="property_detail_wrapper">
                 <div class="property_post_detail">
                    <div class="row">
                    	<div class="property_post_detail_top">
                      		<div class="col-md-6">
                      			<i class="fa fa-envelope"></i> : <?php echo $details[0]['email'];?>&nbsp;&nbsp; <i class="fa fa-calendar"></i> : <?php $mydate=$details[0]['createdate'];    echo date("jS F, Y", strtotime($mydate));?>
                      		</div>
                          	<div class="col-md-6" style="text-align:right">
                            	<b>Country</b>:
                             	<?php 
                                 	$cid=$details[0]['country'];
                                 	$sid=$details[0]['state'];
                                 	$cityd=$details[0]['city'];
                                 	$this->load->model('supercontrol/Country_model');
                                 	$this->db->from('countries');
                                 	$this->db->where('count_id',$cid);
                                 	$query = $this->db->get();
                                 	
                                 	foreach ($query->result() as $rows)
                                 	{
                                 	    echo $rows->count_name;
                                 	}
                                ?>

                               <b>State</b>:
                               <?php 
                                   $sid=$details[0]['state'];
                                   $cityd=$details[0]['city'];
                                   $this->load->model('supercontrol/Country_model');
                                   $this->db->from('states');
                                   $this->db->where('state_id',$sid);
                                   $query1 = $this->db->get();
    
                                   foreach ($query1->result() as $rows1)
                                   {
                                     echo $rows1->state_name;
                                   }
                               ?>

                               <b>City</b>:
                               <?php 
                                   $cityd=$details[0]['city'];
                                   $this->load->model('supercontrol/Country_model');
                                   $this->db->from('cities');
                                   $this->db->where('city_id',$cityd);
                                   $query1 = $this->db->get();

                                   foreach ($query1->result() as $rows2)
                                   {
                                     echo $rows2->city_name;
                                   }
                               ?>
                            </div>
                      	</div>
                    </div>
                    <p class="property_post_detail_cont"><?php echo $details[0]['description'];?></p>
                    <div class="row">
                    	<div class="spacification">
                      		<div class="col-md-4">
                                <?php 
                                    if($details[0]['kitchen']=='yes')
                                    {
                                ?>
                                  		<i class="fa fa-check"></i> Kitchen
                                <?php
                                    }
                                    else
                                    {
                                ?>
                                		<i class="fa fa-times-circle"></i> Kitchen
                                <?php
                                    }
                                ?>
                        	</div>
                      	<div class="col-md-4">
                        	<?php 
                        	   if($details[0]['laundry_service']=='yes')
                               {
                            ?>
                          	   	  <i class="fa fa-check"></i> Laundry Service
                            <?php
                               }
                               else
                               {
                            ?>
                        	   	  <i class="fa fa-times-circle"></i> Laundry Service
                            <?php 
                               }
                            ?>
                        </div>
                      	<div class="col-md-4">
                            <?php 
                                if($details[0]['living_room']=='yes')
                                { 
                            ?>
                          			<i class="fa fa-check"></i> Living Room
                            <?php 
                                } 
                                else 
                                { 
                            ?>
                        			<i class="fa fa-times-circle"></i> Living Room
                          	<?php 
                                } 
                            ?>
                        </div>
                      </div>
                    </div>
                   	<div class="row">
                    	<div class="spacification">
                      		<div class="col-md-4">
                        		<?php 
                        		  if($details[0]['pantry']=='yes')
                                  { 
                                ?>
                          		  	<i class="fa fa-check"></i> Pantry
                          		<?php 
                                  } 
                                  else 
                                  { 
                                ?>
                        			<i class="fa fa-times-circle"></i> Pantry
                          		<?php 
                                  } 
                                ?>
                        	</div>
                      		<div class="col-md-4">
                          		<?php 
                          		    if($details[0]['pool']=='yes')
                                    { 
                                ?>
                          				<i class="fa fa-check"></i> Pool
                          		<?php 
                                    } 
                                    else 
                                    { 
                                ?>
                        				<i class="fa fa-times-circle"></i> Pool
                          		<?php 
                                    } 
                                ?>
                      		</div>
                      		<div class="col-md-4">
                        		<?php 
                        		  if($details[0]['backyard']=='yes')
                                  {
                                ?>
                          			<i class="fa fa-check"></i> Backyard
                          		<?php 
                                  } 
                                  else 
                                  { 
                                ?>
                        			<i class="fa fa-times-circle"></i> Backyard
                          		<?php 
                                  } 
                                ?>
                      		</div>
                      </div>
                    </div>
                    <div class="row">
                    	<div class="spacification2">
                          <div class="col-md-4">
                          	<i class="fa fa-area-chart" aria-hidden="true"></i>
                          	<?php echo $details[0]['area'];?> 
                          	SQFT
                          </div>
                          <div class="col-md-4">
                          	<i class="fa fa-bath"></i>
                          	<?php echo $details[0]['bathroom'];?> 
                          	Baths
                          </div>
                          <div class="col-md-4">
                          	<i class="fa fa-bed" aria-hidden="true"></i>
                          	<?php echo $details[0]['bedroom'];?> 
                          	Beds
                          </div>
                      	</div>
                    </div>
                   <!--  
                   <div class="share_warpper">
                      <h5>share property</h5>
                         <?php
                            $fullurl="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                         ?>
                         Social Icons
                       <ul class="list-inline social-icons">
                           <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?=$fullurl?>"><i class="flaticon-facebook55"></i></a></li>
                           <li><a href="https://twitter.com/home?status=<?=$fullurl?>"><i class="flaticon-twitter1"></i></a></li>
                           <li><a href="https://plus.google.com/share?url=<?=$fullurl?>"><i class="flaticon-google116"></i></a></li>
                           <li><a href="http://pinterest.com/pin/create/button/?url=<?=$fullurl?>"><i class="flaticon-pinterest34"></i></a></li>
                           <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=&title=&summary=&source=<?=$fullurl?>"><i class="fa fa-linkedin-in"></i></a></li>
                       </ul>
                   </div>
                   -->
                 </div>
              </div>
           </div>
        </div>
        <div class="row">
           <div class="simillar_project_wrapper">
            <div class="col-lg-12">
                <div class="rs_heading_wrapper">
                    <div class="rs_heading">
                        <h3>Similer Properties</h3>
                        <p>You may be interseted in the following properties.</p>
                    </div>
                </div>
            </div>
            <?php foreach($similar_prop as $sp){ ?>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                <div class="property_wrapper">
                    <div class="property_img_wrapper">
                        <a href="<?php echo base_url();?>Page/Show_detail/<?php echo $sp['id'];?>"><img src="<?php echo base_url();?>uploads/blog/<?php echo $sp['image_name'];?>" alt=""></a>
                        <!-- <div class="tag_sale">sale</div>
                        <div class="tag_price">$24000</div> -->
                    </div>
                    <div class="property_detail">
                        <div class="property_content">
                            <h5><a href="<?php echo base_url();?>Page/Show_detail/<?php echo $sp['id'];?>"><?php echo $sp['title']?></a></h5>
                           
                            <p><?php echo substr(stripslashes(strip_tags($sp['description'])),0,100);?>....</p>
                        </div>
                        <ul>
                          <?php if(!empty($sp['area'])){?>
                            <li><i class="flaticon-drawing18"></i><?php echo $sp['area']?> SQFT</li>
                            <?php }else{?>
                            <li><i class="flaticon-drawing18"></i>No records found</li>
                            <?php }?>
                            <?php if(!empty($sp['bathroom'])){?>
                            <li><i class="flaticon-bathtub3"></i><?php echo $sp['bathroom']?> bath</li>
                            <?php }else{?>
                            <li><i class="flaticon-bathtub3"></i>No records found</li>
                            <?php }?>
                            <?php if(!empty($sp['bedroom'])){?>
                            <li><i class="flaticon-bed40"></i><?php echo $sp['bedroom']?> beds</li>
                            <?php }else{?>
                            <li><i class="flaticon-bed40"></i>No records found</li>
                            <?php }?>
                        </ul>
                    </div>
                </div>
            </div>
            <?php } ?>
            <!-- <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                <div class="property_wrapper">
                    <div class="property_img_wrapper">
                        <img src="<?php echo base_url('assets/images/08.jpg');?>" alt=""> -->
                        <!-- <div class="tag_sale">sale</div>
                        <div class="tag_price">$24000</div> -->
                    <!-- </div>
                    <div class="property_detail">
                        <div class="property_content">
                            <h5><a href="javascript:void(0)">Property Title Here</a></h5>
                            <p>Phasellus in egestas libero, congue lacus. Cras vel lacus nisiduis.</p>
                        </div>
                        <ul>
                            <li><i class="flaticon-drawing18"></i>750 SQFT</li>
                            <li><i class="flaticon-bathtub3"></i>2 bath</li>
                            <li><i class="flaticon-bed40"></i>2 beds</li>
                        </ul>
                    </div>
                </div>
            </div> -->
            <!-- <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                <div class="property_wrapper">
                    <div class="property_img_wrapper">
                        <img src="<?php echo base_url('assets/images/06.jpg');?>" alt=""> -->
                       <!--  <div class="tag_sale">sale</div>
                       <div class="tag_price">$24000</div> -->
                    <!-- </div>
                    <div class="property_detail">
                        <div class="property_content">
                            <h5><a href="javascript:void(0)">Property Title Here</a></h5>
                            <p>Phasellus in egestas libero, congue lacus. Cras vel lacus nisiduis.</p>
                        </div>
                        <ul>
                            <li><i class="flaticon-drawing18"></i>750 SQFT</li>
                            <li><i class="flaticon-bathtub3"></i>2 bath</li>
                            <li><i class="flaticon-bed40"></i>2 beds</li>
                        </ul>
                    </div>
                </div>
            </div> -->
           </div>
           </div>
        </div>
      </div>
      <!--row-->
   </div>
</div>