<?php //include "header.php";?>
<div class="contactus_wraper">
   <div class="container">
      <div class="row">
         <div class="col-lg-12">
            <div class="rs_heading_wrapper">
               <div class="rs_heading">
                  <h3>Get in touch with us</h3>
               </div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="contact_section">
           
            <div class="col-lg-8 col-md-8">
               <form name="contactform" method="post" action="<?php echo base_url();?>Page/contactus_save">
               	 <?php
               if($this->session->flashdata('success')!=''){
               	?>
            	<div class="alert  alert-success text-center">
               <button class="close" data-close="alert"></button>
               <span> <?php echo $this->session->flashdata('success'); ?>  </span>
            </div>
            <?php
               }
               ?> 
                <?php
               if($this->session->flashdata('error')!=''){
               	?>
            	<div class="alert  alert-danger text-center">
               <button class="close" data-close="alert"></button>
               <span> <?php echo $this->session->flashdata('error'); ?>  </span>
            </div>
            <?php
               }
               ?>
                  <div class="contactus_form_wraper">
                     <div class="form-group">
                        <input type="text" class="form-control" placeholder="*YOUR NAME" id="uname" name="name" value="<?php echo set_value('name'); ?>">
                        <?php echo form_error('name'); ?> 
                     </div>
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding_left">
                        <div class="form-group">
                           <input type="email" class="form-control" placeholder="*EMAIL ADDRESS" id="umail" name="email" value="<?php echo set_value('email'); ?>">
                           <?php echo form_error('email'); ?>  
                        </div>
                     </div>
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding_right">
                        <div class="form-group">
                           <input type="text" class="form-control" placeholder="SUBJECT" id="sub" name="sub" value="<?php echo set_value('sub'); ?>"> 
                        </div>
                     </div>
                     <div class="form-group"><textarea rows="11" class="form-control" placeholder="*YOUR MESSAGE" id="msg" name="msg" value="<?php echo set_value('msg'); ?>"></textarea>
					<?php echo form_error('msg'); ?> 
                     </div>
                     <div class="btn_wrapper1">
                        <button type="submit" class="btn rs_btn" id="rs_submit">submit</button>
                     </div>
                  </div>
               </form>
            </div>
            <div class="col-lg-4 col-md-4">
               <div class="contact_detail_wrapper">
                  <h6>Quick Contact</h6>
                  <ul>
                     <li>
                        <i class="flaticon-pin56"></i>
                        <p><?=$contch->address?>
                           </span>
                     </li>
                     </p>
                     </li>
                     <li>
                        <i class="flaticon-phone41"></i>
                        <p><?=$contch->phone?></p>
                     </li>
                     <li>
                        <i class="flaticon-print47"></i>
                        <p>(+1) 123 789 102</p>
                     </li>
                     <li>
                        <i class="flaticon-mail80"></i>
                        <p><a href="javascript:void(0)"><?=$contch->email?>
                           </span>
                     </li>
                     </a></p>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!--<div class="contact_map">
   <div id="real_map"></div>
   </div>-->
<?php //include "footer.php";?>