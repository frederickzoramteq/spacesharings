<?php 
error_reporting(0);
//include "header.php";?>
<div class="property_view_wrapper">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-4">
			</div>
			<div class="col-lg-9 col-md-8">
				<p class="topcont">Use the search form on the left to look for available rentals by location. Advanced user may also specify your criteria of the ideal host (the one who you will be most comfortable with). The right or the detail area shows the outcome of the search.  Browse the results and click on a result for more detail.</p>
			</div>
			<div class="col-lg-3 col-md-4">
				<div class="sidebar_wrapper">
					<div class="widget advance_search_wrapper">		
						<div class="widget-title"><h4>Basic Search </h4></div>
							<div class="search_form">
								<form method="post" action='<?php echo base_url();?>Page/basic_search_results'>
									<div class="form-group">
										<label>Country</label>
										<select name="country" id="country" class="orderby form-control select2">
											<option value="231" <?php if($country=="231") { echo 'selected'; } ?>>United States</option>
											<option value="38" <?php if($country=="38") { echo 'selected'; }?>>Canada</option>
											<option value="44" <?php if($country=="44") { echo 'selected'; }?>>China</option>
											<option value="101" <?php if($country=="101") { echo 'selected'; }?>>India</option>
											<option value="109" <?php if($country=="109") { echo 'selected'; }?>>Japan</option>
											<option value="230" <?php if($country=="230") { echo 'selected'; }?>>United Kingdom </option>
										</select>
									</div>
									<div class="form-group">
										<label>State</label>
											<select name="state" id="state" class="orderby form-control select2">
												<?php 
													if($state!="")
													{
														$fetch_state = $this->Generalmodel->fetch_row('states', "state_id='$state'");
												?>
														<option value="<?=$state?>" selected="selected"><?= $fetch_state->state_name;?></option>
												<?php 
													} 
													else
													{ 
												?>
														<option value="0" selected="selected">Select State</option>
												<?php 
													}	
												?>
												<?php 
													foreach ($states as $st) 
													{
														?>
														<option value='<?= $st->state_id;?>'>
															<?= $st->state_name;?>

														</option>
												<?php   
													}
												?>
											</select>
									</div>
									<div class="form-group">
										<label>City</label>
										<select name="city" id="city" class="orderby form-control select2">
											<?php 
												if($city!="")
												{
													$fetch_city = $this->Generalmodel->fetch_row('cities', "city_id='$city'");
											?>
													<option value="<?=$city?>" selected="selected"><?=$fetch_city->city_name?></option>
											<?php 
												} 
												else
												{ 
											?>
													<option value="0" selected="selected">Select City</option>
											<?php 
												}	
											?>
										</select>
									</div>
									<div class="form-group">
										<label>Zipcode</label>
										<input type="zipcode" name="zipcode" class="form-control" value="<?php echo $zipcode;?>" >
									</div>
									
									<div class="btn_wrapper" style="margin-top:10px;">
										<input type="submit" id="submit" class="btn rs_btn2" value="search">
									</div>
								</form>
							</div>
					</div>
				</div>
			</div>
				
			<div class="col-lg-9 col-md-8">
					<div class="row">
						<div class="col-lg-12">
							<div class="rs_heading_wrapper">
								<div class="rs_heading">
									<div class="col-lg-10 col-md-10 col-sm-10 padding">
										<h3 class="property_view_h">Property Listings </h3>
										<p class="property_view_p">Total <?php echo count($search);?> results found.</p>
									</div>
									<div class="col-lg-2 col-md-2s col-sm-2 padding">
										<ul class="view_button">
											<li><a href="javascript:void(0)" id="grid" class="active"><i class="flaticon-keyboard50"></i></a></li>
											<li><a href="javascript:void(0)" id="list"><i class="flaticon-list95"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="grid_view animated fadeIn">
							<?php
								if(!empty($search)) 
								{
									foreach ($search as $row)
									{
									    $hostid = $row->id;
									    $imgdet=$this->Generalmodel->fetch_single('hosting_image',$hostid);
							?>
										<div class="col-lg-4 col-md-6 col-sm-4 col-xs-6">
											<div class="property_wrapper">
												<div class="property_img_wrapper"> 
													<div class="property1">
														<?php 
															if(isset($imgdet) && $imgdet->image_name != '') 
															{
														?>
																<img  src="<?php echo base_url();?>uploads/blog/<?php echo $row->image_name;?>" alt="" >
														<?php 
															}
															else 
															{
														?>
																<img  src="<?php echo base_url();?>uploads/blog/noimg.png" alt="">
														<?php 
															}
														?>
													</div>
												
												</div>
												<div class="property_detail">
													<div class="property_content">
														<!--<a href="<?php echo base_url();?>Page/Show_detail/<?php echo $row->id;?>">-->
															<h5>
																<a href="<?php echo base_url();?>Page/Show_detail/<?php echo $row->hosting_id;?>"><?php echo $row->title;?></a></h5>
																<p><?php echo substr(stripslashes(strip_tags( $row->description)),0,100);?>....</p>
													</div>
													<ul>
														<li><i class="flaticon-drawing18"></i><?php echo $row->area;?>SQFT</li>
														<li><i class="flaticon-bathtub3"></i><?php echo $row->bathroom ;?></li>
														<li><i class="flaticon-bed40"></i><?php echo $row->bedroom ;?></li>
													</ul>
												</div>
											</div>
										</div>

							<?php 
									}
								}
								else
								{
							?>
									<div class="col-md-12">
										<div class="title-section title-height46">
											<h1 class="title">No Result Found</h1>
											<div class="sub-title">
												<!-- this is sub title -->
											</div>
										</div>
									</div><!-- col.md-12 -->
							<?php 
								}
							?>
						</div>
						<div class="list_view animated fadeIn">
										<?php
											if(!empty($search))
											{
												foreach ($search as $row)
												{
													?>
													<div class="property_wrapper">
														<div class="col-lg-5 col-md-5 col-sm-5"> 
															<div class="property_img_wrapper ">
																<div class="property1">
																<?php 
																	if($row->image_name!='') 
																	{ 
																?>
																		<img  src="<?php echo base_url();?>uploads/blog/<?php echo $row->image_name;?>" alt="">
																<?php 
																	} 
																	else 
																	{ 
																?>
																		<img  src="<?php echo base_url();?>uploads/blog/noimg.png" alt="" >
																<?php 
																	}
																?>
																</div>
															</div>
														</div>
													<div class="col-lg-7 col-md-7 col-sm-7">
														<div class="property_detail">
															<div class="property_content">
																<h5><a href="property_detail2.html"><?php echo $row->title;?></a></h5>
																<p><?php echo substr(stripslashes(strip_tags( $row->description)),0,150);?>....</p>
															</div>
															<ul>
																<li><i class="flaticon-drawing18"></i><?php echo $row->area;?>SQFT</li>
																<li><i class="flaticon-bathtub3"></i><?php echo $row->bathroom ;?></li>
																<li><i class="flaticon-bed40"></i><?php echo $row->bedroom ;?></li>
																<li><i class="flaticon-car211"></i><?php echo $row->backyard ;?></li>
															</ul>
														</div>
													</div>
												</div>
										<?php 
												}
											}
											else
											{ 
										?>
												<div class="col-md-12">
													<div class="title-section title-height46">
														<h1 class="title">No Result Found</h1>
														<div class="sub-title">
															<!-- this is sub title -->
														</div>
													</div>
												</div><!-- col.md-12 -->
										<?php 
											} 
										?>
						</div>
					</div>
			</div>
		</div><!--row-->
	</div>
</div>
