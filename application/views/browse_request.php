<?php //include "header.php";?>
 <style>
         .property_detail ul li {
         color: white;
         }
      </style>
<div class="property_view_wrapper">
   <div class="container">
      <div class="row">
      	<div class="col-lg-3 col-md-4">
            </div>
        <div class="col-lg-9 col-md-8">
         <p class="topcont">Hosts, use this form to find prospective renters. See if you have the property and services which matches renter's criteria.</p>
        </div>
         <div class="col-lg-12">
            <div class="rs_heading_wrapper">
              
               <div class="rs_heading">
                  <div class="search_form">
                     <div class="col-md-6 col-md-offset-3">
                        <div class="row">
                           <div class="col-md-12">

                              <div>
                                 <h3>Browse Rental Requests</h3>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
     
      <div class="row">
         <form method="POST">
            <div class="col-md-10 col-md-offset-1" style="margin-bottom:20px;">
               <div class="col-md-3">
                  <div class="form-group">
                     <select id="country" name="country" id="" class="orderby form-control select2 countries select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                        <!--  <option value="">Select Country</option> -->
                        <!-- <?php foreach ($country as $value) {
                           ?>
                           <option value='<?= $value->count_id;?>' >
                             <?= $value->count_name;?>
                           
                           </option>
                           <?php   
                              }
                              ?> --><!-- United States, Canada, China, India, Japan and United Kingdom  -->
                        <option value="231">United States</option>
                        <option value="38">Canada</option>
                        <option value="44">China</option>
                        <option value="101">India</option>
                        <option value="109">Japan</option>
                        <option value="230">United Kingdom </option>
                     </select>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="form-group">
                     <select name="state" id="state" class="orderby form-control select2 required states select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                     <option value="0">Select State</option>
                     </select>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="form-group">
                     <select name="city" id="city" class="orderby form-control select2 required cities select2-hidden-accessible"   tabindex="-1" aria-hidden="true">
                     <option value="0">Select City</option>
                     </select>
                  </div>
               </div>
               <!-- --------------------------/city---------------------------- -->    
               <div class="col-md-3">
                  <div class="form-group">
                     <div class="btn_wrapper">
                        <input name="reqs_btn_list" class="btn rs_btn" id="reqs_btn_list" value="Search" type="submit">
                     </div>
                  </div>
               </div>
               <!-- --------------------------/city---------------------------- -->
            </div>
         </form>
      </div>
      <div class="row">
         <div class="col-md-10 col-md-offset-1">
            <div class="grid_view1 animated fadeIn" style="display: block;">
               <div class="property_wrapper">
                  
                  <?php if(empty($browse)){ ?>
                  <h3>No such records found!</h3>

                     <?php } else{ ?>

                  <?php foreach ($browse as $row) { ?>
                  <div class="property_detail">
                     <ul style="background:#0363a7;">
                        <li>Posted By – <?php echo $row->ContactName;?>
                        </li>
                        <li><i class="fa fa-calendar"></i><?php $mydate=$row->ContactDate;    echo date("jS F, Y", strtotime($mydate));?></li>
                        <li> <b> Country</b> :   <?php 
                           $cid=$row->country;
                           $sid=$row->state;
                           $cityd=$row->city;
                           $this->load->model('supercontrol/Country_model');
                           $this->db->from('countries');
                           $this->db->where('count_id',$cid);
                           $query = $this->db->get();
                           foreach ($query->result() as $rows) {
                             echo $rows->count_name;
                           }
                           
                           ?></li>
                        <li> <b> State</b> :   <?php 
                           $sid=$row->state;
                           $cityd=$row->city;
                           $this->load->model('supercontrol/Country_model');
                           $this->db->from('states');
                           $this->db->where('state_id',$sid);
                           $query1 = $this->db->get();
                          foreach ($query1->result() as $rows1) {
                             echo $rows1->state_name;
                           
                           }
                           
                           
                           ?></li>
                        <li> <b> City</b> :   <?php 
                           $cityd=$row->city;
                           $this->load->model('supercontrol/Country_model');
                           $this->db->from('cities');
                           $this->db->where('city_id',$cityd);
                           $query1 = $this->db->get();
                           
                           foreach ($query1->result() as $rows2) {
                             echo $rows2->city_name;
                           }
                           
                           
                           ?></li>
                     </ul>
                     <div class="property_content">
                        <h4><?php echo $row->Subject;?></h4>
                        <!-- <a href="javascript:void(0);"><h5><?php echo $row->Message;?></h5></a> -->
                        <p> <?php $msg=$row->Message;?>
                           <?php  $count=str_word_count("$msg"); ?>
                        </p>
                        <p><?php $valDesc = strip_tags($msg); ?>
                           <?php echo substr( $valDesc , 0, 280);?>...
                        </p>
                        
                           <?php if($count>35)
                              {  ?>
                     <a href="#exampleModal<?php echo $row->ContactId;?>" style="color: #0363a7;" data-toggle="modal" data-target="#exampleModal<?php echo $row->ContactId;?>"> Read More &gt;&gt;</a>
                           
                           <?php  } ?>
       <div class="modal requestmodal fade bd-example-modal-lg" id="exampleModal<?php echo $row->ContactId;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top: 22px;">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  
               <b>Posted By</b> – <?php echo $row->ContactName;?> &nbsp; &nbsp; <i class="fa fa-calendar"></i> <?php $mydate=$row->ContactDate;    echo date("jS F, Y", strtotime($mydate));?> 
               <b> Country</b> :   <?php 
                           $cid=$row->country;
                           $sid=$row->state;
                           $cityd=$row->city;
                           $this->load->model('supercontrol/Country_model');
                           $this->db->from('countries');
                           $this->db->where('count_id',$cid);
                           $query = $this->db->get();
                           foreach ($query->result() as $rows) {
                             echo $rows->count_name;
                           }
                           ?>
                           <b> State</b> :   <?php 
                           $sid=$row->state;
                           $cityd=$row->city;
                           $this->load->model('supercontrol/Country_model');
                           $this->db->from('states');
                           $this->db->where('state_id',$sid);
                           $query1 = $this->db->get();
                           foreach ($query1->result() as $rows1) {
                             echo $rows1->state_name;
                           }
                           ?>
                           <b> City</b> :   <?php 
                           $cityd=$row->city;
                           $this->load->model('supercontrol/Country_model');
                           $this->db->from('cities');
                           $this->db->where('city_id',$cityd);
                           $query1 = $this->db->get();
                           foreach ($query1->result() as $rows2) {
                             echo $rows2->city_name;
                           }
                           ?>

              </div>
              <div class="modal-body">
               <h4><?php echo $row->Subject;?></h4>
                <p><?php echo $row->Message;?></p>
              </div>
              <div class="modal-footer">
                <div class="btn btn-primary btn-sm" data-dismiss="modal">Close</div>
              </div>
            </div>
          </div>
        </div>
                    
                     </div>
                  </div>
                  <?php } } ?>
                  <!--End of foreachLoop-->
                  
                  <hr>
               </div>
               <div class="col-lg-12">
  <nav class="pagger_wrapper">
    <?php echo $this->pagination->create_links(); ?>
  </nav>
</div> 
            </div>
         </div>
      </div>

   </div>
   
</div>


