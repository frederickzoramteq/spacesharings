<?php //$this->load->view ('header');?>

<div class="page-container">
  <div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
      <?php $this->load->view ('supercontrol/leftbar');?>
    </div>
  </div>
  <div class="page-content-wrapper">
    <div class="page-content">
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <li> <a href="<?php echo base_url(); ?>supercontrol/home">Home</a> <i class="fa fa-circle"></i> </li>
          <li> <span>supercontrol Panel</span> </li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tabbable-line boxless tabbable-reversed">
            <div class="tab-content">
              <div class="tab-pane active" id="tab_0">
                <div class="portlet box blue-hoki">
                  <div class="portlet-title">
                    <div class="caption"> <i class="fa fa-gift"></i>Member edit</div>
                    <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
                  </div>
                  <div class="portlet-body form"> 
                    <!-- BEGIN FORM-->
                    <?php foreach($ecms as $i){?>
                    <form action="<?php echo base_url().'supercontrol/Member/edit_member' ?>" class="form-horizontal form-bordered" method="post" enctype="multipart/form-data">
                      <input type="hidden" name="id" value="<?=$i->id;?>">
                      <input type="hidden" name="oldpass" value="<?=$i->password;?>">
                      <input type="hidden" name="profile_pic" value="<?php echo $i->profile_pic;?>">
                      <div class="form-body">
                        <!--<div class="form-group">
                          <label class="control-label col-md-3">Registration Type</label>
                          <div class="col-md-8">
                            <select name="reg_type" id="reg_type" class="form-control">
                              <option value="personal" <?php if($i->reg_type=='personal'){?> selected="selected" <?php }?>>personal</option>
                              <option value="company" <?php if($i->reg_type=='company'){?> selected="selected" <?php }?>>company</option>
                            </select>
                          </div>
                        </div>-->
                       
                        <div class="form-group">
                          <label class="control-label col-md-3">First Name</label>
                          <div class="col-md-8"> <?php echo form_input(array('id' => 'first_name', 'value' => $i->first_name, 'name' => 'first_name','class'=>'form-control' )); ?> <?php echo form_error('first_name'); ?> </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Last Name</label>
                          <div class="col-md-8"> <?php echo form_input(array('id' => 'last_name', 'value' => $i->last_name, 'name' => 'last_name','class'=>'form-control' )); ?> <?php echo form_error('last_name'); ?> </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Email</label>
                          <div class="col-md-8"> <?php echo form_input(array('id' => 'email', 'value' => $i->email,  'name' => 'email','class'=>'form-control' )); ?> <?php echo form_error('email'); ?> </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Phone</label>
                          <div class="col-md-8"> <?php echo form_input(array('id' => 'phone', 'value' => $i->phone, 'name' => 'phone','class'=>'form-control' )); ?> <?php echo form_error('phone'); ?> </div>
                        </div>
                        <!--<div class="form-group">
                          <label class="control-label col-md-3">Username</label>
                          <div class="col-md-8"> <?php echo form_input(array('id' => 'username', 'value' => $i->username, 'name' => 'username','class'=>'form-control' )); ?> <?php echo form_error('username'); ?> </div>
                        </div>-->
                        <div class="form-group">
                          <label class="control-label col-md-3">Password</label>
                          <div class="col-md-8">
                            <input type="password" value="<?=$i->password?>" name="password" class="form-control" id="password" />
                            <?php echo form_error('password'); ?></div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Gender</label>
                          <div class="col-md-8">
                            <select name="gender" id="gender" class="form-control">
                              <option value="male" <?php if($i->gender=='male'){?> selected="selected" <?php }?>>male</option>
                              <option value="female" <?php if($i->gender=='female'){?> selected="selected" <?php }?>>female</option>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Country </label>
                          <div class="col-md-8">
                            <select class="form-control"  name="country" id="country" onChange="getstatedetails(this.value)">
                              <option value="">Select Country </option>
                              <?php foreach($country as $c){?>
                              <option value="<?=$c->count_id;?>" <?php if($i->country==$c->count_id){?> selected="selected" <?php }?>>
                              <?=$c->count_name;?>
                              </option>
                              <?php }?>
                            </select>
                          </div>
                        </div>
                        <div id="sub">
                          <div class="form-group">
                            <label class="control-label col-md-3">State</label>
                            <div class="col-md-8">
                              <select class="form-control"  name="state" id="state" onChange="getcitydetails(this.value)">
                                <option value="">Select State</option>
                                <?php foreach($state as $c){?>
                                <option value="<?=$c->state_id;?>" <?php if($i->state==$c->state_id){?> selected="selected" <?php }?>>
                                <?=$c->state_name;?>
                                </option>
                                <?php }?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div id="sub1">
                          <div class="form-group">
                            <label class="control-label col-md-3">City</label>
                            <div class="col-md-8">
                              <select class="form-control"  name="city" id="city">
                                <option value="">Select City</option>
                                <?php foreach($city as $c){?>
                                <option value="<?=$c->city_id;?>" <?php if($i->city==$c->city_id){?> selected="selected" <?php }?>>
                                <?=$c->city_name;?>
                                </option>
                                <?php }?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Postcode</label>
                          <div class="col-md-8"> <?php echo form_input(array('id' => 'postcode', 'value' => $i->postcode, 'name' => 'postcode','class'=>'form-control' )); ?> <?php echo form_error('postcode'); ?> </div>
                        </div>
                        <?php 
							@$dob = $i->dob;
							@$dateofbirth = explode('-', $dob);
							@$year = $dateofbirth[0];
							@$month   = $dateofbirth[1];
							@$day  = $dateofbirth[2];
						?>
                        <div class="form-group">
                          <label class="control-label col-md-3">Date of Birth</label>
                          <div class="col-md-8" style="margin-left:-15px;">
                            <div class="col-md-4">
                              <select class="form-control" id="day" name="day" onchange="date_check()" required>
                                <option>Date</option>
                                <?php for($j=1; $j<=31; $j++){ ?>
                                <option value="<?php echo $j  ?>" <?php if($day==$j){ ?> selected="selected" <?php }?>><?php echo $j   ?></option>
                                <?php } ?>
                              </select>
                            </div>
                            <div class="col-md-4">
                              <?php
                           $options = array(
                               ''    =>'Month',
                               '01'         => 'January',
                               '02'           => 'February',
                               '03'           => 'March',
                               '04'           => 'April',
                               '05'           => 'May',
                               '06'           => 'June',
                               '07'           => 'July',
                               '08'           => 'August',
                               '09'           => 'September',
                               '10'           => 'October',
                               '11'           => 'November',
                               '12'           => 'December',
                           );
						   
                           echo form_dropdown('month', $options,$month, 'class="form-control" id="month" selected="selected" name="month" onchange="date_check()"');
						   
                           ?>
                            </div>
                            <div class="col-md-4">
                              <select class="form-control" id="yeara" name="year" onchange="date_check()">
                                <option value="">Year</option>
                                <?php for ($k=1920; $k<2018; $k++){  ?>
                                <option value="<?php echo $k  ?>" <?php if($year==$k){ ?> selected="selected" <?php }?>><?php echo $k ?></option>
                                <?php } ?>
                              </select>
                            </div>
                            <p id="date_error" style="color: red;" ></p>
                            <?php echo form_error('month'); ?><?php echo form_error('day'); ?><?php echo form_error('year'); ?> </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Status</label>
                          <div class="col-md-8">
                            <select name="status" id="status" class="form-control">
                              <option value="Yes" <?php if($i->status=='Yes'){?> selected="selected" <?php }?>>Active</option>
                              <option value="No" <?php if($i->status=='No'){?> selected="selected" <?php }?>>InActive</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="form-actions">
                        <div class="row">
                          <div class="col-md-offset-3 col-md-9"> <?php echo form_submit(array('id' => 'submit', 'name' => 'Submit' , 'value' => 'Submit' ,'class'=>'btn red')); ?>
                            <button class="btn default" type="button">Cancel</button>
                          </div>
                        </div>
                      </div>
                    </form>
                    <?php }?>
                    <script>
						function date_check() {
						var month= $('#month').val();
						var date= $('#day').val();
						var yeara= $('#yeara').val();
						var html='';
						$('#date_error').empty();
						if((month==2)|(month==4)|(month==6)|(month==9)|(month==11)&& (date==31)  )
						{
						$('#date_error').empty();
						html='';
						html="Error: Selected Month Does not have this Date. Please Correct it.";
						$('#date_error').append(html);
						}
						if(month==2 && (date == 30 | date==31) ){
						
						$('#date_error').empty();
						html='';
						html="Error: February month do not have 30th and 31st date. Please Correct it."
						$('#date_error').append(html);
						}
						if(yeara !=null) {
						if ((month == 2 & date > 28) && yeara % 4 != 0) {
						$('#date_error').empty();
						html='';
						html="Error: 29th day of February is only available in Leap years. Please Correct it."
						$('#date_error').append(html);
						}
						}
						}
                    </script> 
                    <script>
 function getstatedetails(id)
            {
                //alert('this id value :'+id);
                $.ajax({
                    type: "POST",
                    url: '<?php echo base_url().'supercontrol/Member/ajax_state_list'.'/';?>'+id,
                    data: id='cat_id',
                    success: function(data){
                        //alert(data);
                        $('#sub').html(data);
                },
});
            }
			
 function getcitydetails(id)
            {
                //alert('this id value :'+id);
                $.ajax({
                    type: "POST",
                    url: '<?php echo base_url().'supercontrol/Member/ajax_city_list'.'/';?>'+id,
                    data: id='cat_id',
                    success: function(data){
                        //alert(data);
                        $('#sub1').html(data);
                },
});
            }

</script> 
                    <!-- END FORM--> 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END CONTENT BODY --> 
  </div>
  <!-- END CONTENT --> 
  <!-- BEGIN QUICK SIDEBAR --> 
  <!-- END QUICK SIDEBAR --> 
</div>
<!-- END CONTAINER -->
<?php //$this->load->view ('footer');?>
