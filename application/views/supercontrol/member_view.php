<?php //$this->load->view ('header');?>

<div class="page-container">
  <div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
      <?php $this->load->view ('supercontrol/leftbar');?>
    </div>
  </div>
  <div class="page-content-wrapper">
    <div class="page-content">
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <li> <a href="<?php echo base_url(); ?>supercontrol/home">Home</a> <i class="fa fa-circle"></i> </li>
          <li> <span>supercontrol Panel</span> </li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tabbable-line boxless tabbable-reversed">
            <div class="tab-content">
              <div class="tab-pane active" id="tab_0">
                <div class="portlet box blue-hoki">
                  <div class="portlet-title">
                    <div class="caption"> <i class="fa fa-gift"></i>Member edit</div>
                    <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
                  </div>
                  <div class="portlet-body form"> 
                    <!-- BEGIN FORM-->
                    <?php foreach($ecms as $i){?>
                    <form action="<?php echo base_url().'supercontrol/Member/edit_member' ?>" class="form-horizontal form-bordered" method="post" enctype="multipart/form-data">
                      <div class="form-body">
                        <!--<div class="form-group">
                          <label class="control-label col-md-3">Registration Type</label>
                          <div class="col-md-8">
                            <?= $i->reg_type; ?>
                          </div>
                        </div>-->
                      
                        <div class="form-group">
                          <label class="control-label col-md-3">First Name</label>
                          <div class="col-md-8">
                            <?=$i->first_name?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Last Name</label>
                          <div class="col-md-8"> <?php echo $i->last_name ?> </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Reg No.</label>
                          <div class="col-md-8">
                            <?=$i->reg_no?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">ip Address</label>
                          <div class="col-md-8">
                            <?=$i->ip_address?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Email</label>
                          <div class="col-md-8"> <?php echo  $i->email ?> </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Phone</label>
                          <div class="col-md-8"> <?php echo $i->phone?> </div>
                        </div>
                        <!--<div class="form-group">
                          <label class="control-label col-md-3">Username</label>
                          <div class="col-md-8"> <?php echo  $i->username ?> </div>
                        </div>-->
                        <div class="form-group">
                          <label class="control-label col-md-3">Gender</label>
                          <div class="col-md-8">
                            <?=$i->gender?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Country </label>
                          <div class="col-md-8">
                            <?php foreach($country as $c){?>
                            <?=$c->count_name;?>
                            <?php }?>
                          </div>
                        </div>
                        <div id="sub">
                          <div class="form-group">
                            <label class="control-label col-md-3">State</label>
                            <div class="col-md-8">
                              <?php foreach($state as $c){?>
                              <?=$c->state_name;?>
                              <?php }?>
                            </div>
                          </div>
                        </div>
                        <div id="sub1">
                          <div class="form-group">
                            <label class="control-label col-md-3">City</label>
                            <div class="col-md-8">
                              <?php foreach($city as $c){?>
                              <?=$c->city_name;?>
                              <?php }?>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Postcode</label>
                          <div class="col-md-8"> <?php echo $i->postcode; ?> </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Date of Birth</label>
                          <div class="col-md-8" style="margin-left:-15px;">
                            <?=$i->dob;?>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3">Status</label>
                            <div class="col-md-8">
                              <?php if($i->status=='Yes'){ ?>
                              <b style="color:#060;">Active</b>
                              <?php }else{?>
                              <b style="color:#F00;">InActive</b>
                              <?php }?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                    <?php }?>
                    <!-- END FORM--> 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END CONTENT BODY --> 
  </div>
  <!-- END CONTENT --> 
  <!-- BEGIN QUICK SIDEBAR --> 
  <!-- END QUICK SIDEBAR --> 
</div>
<!-- END CONTAINER -->
<?php //$this->load->view ('footer');?>
