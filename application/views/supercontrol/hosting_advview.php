
<script type="text/javascript" src="https://code.jquery.com/jquery-1.8.2.js"></script>
<script type="text/javascript">
  $(function() {
    setTimeout(function() { $("#testdiv").fadeOut(1500); }, 5000)
    $('#btnclick').click(function() {
      $('#testdiv').show();
      setTimeout(function() { $("#testdiv").fadeOut(1500); }, 5000)
    })
  })
</script>
<!-- BEGIN CONTAINER -->

<div class="page-container"> 
  <!-- BEGIN SIDEBAR -->
  <div class="page-sidebar-wrapper"> 
    <!-- BEGIN SIDEBAR --> 
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse"> 
      <!-- BEGIN SIDEBAR MENU --> 
      <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
      <?php $this->load->view ('supercontrol/leftbar');?>
      <!-- END SIDEBAR MENU --> 
      <!-- END SIDEBAR MENU --> 
    </div>
    <!-- END SIDEBAR --> 
  </div>
  <!-- END SIDEBAR --> 
  <!-- BEGIN CONTENT -->
  <div class="page-content-wrapper"> 
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content"> 
      <!-- BEGIN PAGE BAR -->
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <li> <a href="">Home</a> <i class="fa fa-circle"></i> </li>
          <li> <span>supercontrol panel</span> </li>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <!--<button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions <i class="fa fa-angle-down"></i> </button>-->
            <ul class="dropdown-menu pull-right" role="menu">
              <li> <a href="#"> <i class="icon-bell"></i> Action</a> </li>
              <li> <a href="#"> <i class="icon-shield"></i> Another action</a> </li>
              <li> <a href="#"> <i class="icon-user"></i> Something else here</a> </li>
              <li class="divider"> </li>
              <li> <a href="#"> <i class="icon-bag"></i> Separated link</a> </li>
            </ul>
          </div>
        </div>
      </div>
      <!-- END PAGE BAR --> 
      <!-- END PAGE HEADER-->
      <?php if (isset($success_msg)) { echo $success_msg; } ?>
      <div class="row">
        <div class="col-md-12">
          <div class="tabbable-line boxless tabbable-reversed">
            <div class="tab-content">
              <div class="tab-pane active" id="tab_0">
                <div class="portlet box blue-hoki">
                  <div class="portlet-title">
                    <div class="caption">
                      <i class="fa fa-gift"></i>Add Hosting
                    </div>
                    <div class="tools">
                      <a href="javascript:;" class="collapse"> </a>
                      <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                      <a href="javascript:;" class="reload"> </a>
                      <a href="javascript:;" class="remove"> </a>
                    </div>
                  </div>
                  <div class="portlet-body form">
                    <?php
                    echo form_open_multipart('supercontrol/Member/add_adv_member','class="form-horizontal form-bordered"');
                    ?>
                    <div class="form-body">
                      <div class="form-group last">
                        <label class="control-label col-md-3">Hosting Image</label>
                        <div class="col-md-9">
                          <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> 
                              <img src="https://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                            </div>
                            <div>
                              <span class="btn default btn-file">
                                <span class="fileinput-new"> Select image </span>
                                <span class="fileinput-exists"> Change </span>
                                <?php
                                $file = array('id' => 'banner', 'type' => 'file','name' => 'userfile', 'onchange' => 'readURL(this);' );
                                echo form_input($file);
                                ?>
                              </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                            </div>
                          </div>
                          <div class="clearfix margin-top-10"> <span class="label label-danger">Format</span> jpg, jpeg, png, gif </div>
                        </div>
                        <div class="col-md-3">
                          <?php if(isset($upload_error)) echo $upload_error ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3">First Name</label>
                        <div class="col-md-5"> 
                          <?php echo form_input(array('id' => 'first_name', 'name' => 'first_name','class'=>'form-control', 'value' => set_value('first_name') )); ?>
                        </div>
                        <div class="col-md-4">
                          <?php echo form_error('first_name'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3">Last Name</label>
                        <div class="col-md-5"> 
                          <?php echo form_input(array('id' => 'last_name', 'name' => 'last_name','class'=>'form-control', 'value' => set_value('last_name') )); ?>
                        </div>
                        <div class="col-md-4">
                          <?php echo form_error('last_name'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3">Email</label>
                        <div class="col-md-5">
                          <?php echo form_input(array('id' => 'email', 'name' => 'email','class'=>'form-control', 'value' => set_value('email') )); ?>
                        </div>
                        <div class="col-md-4">
                          <?php echo form_error('email'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3">Phone</label>
                        <div class="col-md-5">
                          <?php echo form_input(array('id' => 'phone', 'name' => 'phone','class'=>'form-control', 'value' => set_value('phone') )); ?>
                        </div>
                        <div class="col-md-4">
                          <?php echo form_error('phone'); ?>
                        </div>
                      </div>
                       <!--  <div class="form-group">
                          <label class="control-label col-md-3">Password</label>
                          <div class="col-md-5">
                            <input type="password" name="password" class="form-control" id="password" />
                          </div>
                          <div class="col-md-4">
                            <?php echo form_error('password'); ?>
                          </div>
                        </div> -->
                        <div class="form-group">
                          <label class="control-label col-md-3">Gender</label>
                          <div class="col-md-5">
                            <select name="gender" id="gender" class="form-control">
                              <option value="Male">male</option>
                              <option value="Female">female</option>
                            </select>
                          </div>
                          <div class="col-md-4">
                            <?php echo form_error('gender'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Country</label>
                          <div class="col-md-5">
                            <select class="form-control"  name="country" id="country" onChange="getstatedetails(this.value)">
                              <option value="">Select Country</option>
                              <?php foreach($country as $c){?>
                              <option value="<?=$c->count_id;?>"><?=$c->count_name;?></option>
                              <?php }?>
                            </select>
                          </div>
                          <!-- <div class="col-md-4">
                            <?php echo form_error('country'); ?>
                          </div> -->
                        </div>
                        <div id="sub"></div>
                        <div id="sub1"></div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Postcode</label>
                          <div class="col-md-5">
                            <?php echo form_input(array('id' => 'postcode', 'name' => 'postcode','class'=>'form-control', 'value' => set_value('postcode') )); ?>
                          </div>
                          <div class="col-md-4">
                            <?php echo form_error('postcode'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Street 1</label>
                          <div class="col-md-5">
                            <?php echo form_input(array('id' => 'street1', 'name' => 'street1','class'=>'form-control', 'value' => set_value('street1') )); ?>
                          </div>
                          <div class="col-md-4">
                            <?php echo form_error('postcode'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Street 2</label>
                          <div class="col-md-5">
                            <?php echo form_input(array('id' => 'postcode', 'name' => 'street2','class'=>'form-control', 'value' => set_value('street2') )); ?>
                          </div>
                          <div class="col-md-4">
                            <?php echo form_error('postcode'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Rental Title</label>
                          <div class="col-md-5">
                            <?php echo form_input(array('id' => 'title', 'name' => 'title','class'=>'form-control', 'value' => set_value('title') )); ?>
                          </div>
                          <div class="col-md-4">
                            <?php echo form_error('postcode'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Description</label>
                          <div class="col-md-5">
                            <?php echo form_input(array('id' => 'description', 'name' => 'description','class'=>'form-control', 'value' => set_value('description') )); ?>
                          </div>
                          <div class="col-md-4">
                            <?php echo form_error('description'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Nationality</label>
                          <div class="col-md-5">
                            <select name="nationality" class="orderby form-control select2 select2-hidden-accessible" id="afnationality" tabindex="-1" aria-hidden="true">
                <option value="" selected="selected">-Select Host’s Nationality-</option>
                <option value="El Salvador">El Salvador </option>
                <option value="Honduras">Honduras</option>
                <option value="Guatemala">Guatemala</option>
                <option value="India">India </option>
                <option value="Ecuador">Ecuador</option>
              </select>
                          </div>
                          <div class="col-md-4">
                            <?php echo form_error('gender'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Age Group</label>
                          <div class="col-md-5">
                           <select name="agegroup" id="afagegroup" class="orderby form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                            <option value="" selected="selected">-Select Host’s Age group-</option>
                            <option value="16–20">16–20</option>
                            <option value="21-30">21-30</option>
                            <option value="31-40">31-40</option>
                            <option value="41–60">41–60</option>
                            <option value="60+">60+</option>
                            <option value="Any of the above">Any of the above</option>
                          </select>
                        </div>
                        <div class="col-md-4">
                          <?php echo form_error('gender'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3">Host’s Ethnicity</label>
                        <div class="col-md-5">
                         <select name="renter_ethnicity" id="afethnity" class="orderby form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                          <option value="" selected="selected">-Select Host’s Ethnicity-</option>
                          <option value="Caucasian or White">Caucasian or White</option>
                          <option value="African or Black">African or Black</option>
                          <option value="East Asian">East Asian</option>
                          <option value="Middle Eastern">Middle Eastern</option>
                          <option value="Latino">Latino</option>
                          <option value="Chinese">Chinese</option>
                          <option value="Spanish ">Spanish </option>
                          <option value="Indian ">Indian </option>
                        </select>
                      </div>
                      <div class="col-md-4">
                        <?php echo form_error('gender'); ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3">Host’s is a:</label>
                      <div class="col-md-5">
                        
                        <input type="radio" name="hostgender" value="Male" class="radio"> Male
                        <input type="radio" name="hostgender" value="Female" class="radio"> Female
                        <input type="radio" name="hostgender" value="Couple" class="radio"> Couple
                        <!-- <br/> -->
                        <!-- <input type="radio" name="afhostgender" value="Family" class="radio" style="    margin-left: 82px;"> Family -->
                        <input type="radio" name="hostgender" value="Family" class="radio"> Family
                        <input type="radio" name="hostgender" value="Partners" class="radio"> Partners
                        <input type="radio" name="hostgender" value="Others" class="radio"> Other
                      </div>
                      <div class="col-md-4">
                        <?php echo form_error('gender'); ?>
                      </div>
                    </div>

                    <div class="form-group">
                     <label class="control-label col-md-3">Duration Available:</label>
                      <div class="col-md-5">
                        
                        <select name="avl_dur" class="orderby form-control select2 select2-hidden-accessible" id="afarea" tabindex="-1" aria-hidden="true">
                          <option value="0 – 100">1+ days</option>
                          <option value="101 – 200">1+ week</option>
                          <option value="201 – 300">1/2 to 1 month</option>
                          <option value="301-500">1+ months</option>
                          <option value="501 – 800">1/2 year</option>
                          <option value="801 – 1000">1+ years</option>
                        </select>
                      </div>
                      <div class="col-md-4">
                        <?php echo form_error('gender'); ?>
                      </div>
                    </div>
                     
                    
                    <div class="form-group">
                      <label class="control-label col-md-3">Area</label>
                      <div class="col-md-5">
                       
                        <select name="area" class="orderby form-control select2 select2-hidden-accessible" id="afarea" tabindex="-1" aria-hidden="true">
                          <option value="0 – 100">0 – 100</option>
                          <option value="101 – 200">101 – 200</option>
                          <option value="201 – 300">201 – 300</option>
                          <option value="301-500">301-500</option>
                          <option value="501 – 800">501 – 800</option>
                          <option value="801 – 1000">801 – 1000</option>
                          <option value="1001 – 1200">1001 – 1200</option>
                          <option value="1201 – 1600">1201 – 1600</option>
                          <option value="1601 – 2400">1601 – 2400</option>
                          <option value="2401+">2401+</option>
                        </select>
                      </div>
                      <div class="col-md-4">
                        <?php echo form_error('gender'); ?>
                      </div>
                    </div>

                    

                    <div class="form-group">
                      <label class="control-label col-md-3">bathroom</label>
                      <div class="col-md-5">
                        
                        <select name="bathroom" class="orderby form-control select2 select2-hidden-accessible" id="afbath" tabindex="-1" aria-hidden="true">
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                          <option value="6+">6+</option>
                        </select>
                      </div>
                      <div class="col-md-4">
                        <?php echo form_error('gender'); ?>
                      </div>
                       </div>
   <div class="form-group">
                      <label class="control-label col-md-3">Bedroom</label>
                      <div class="col-md-5">                 

<select name="beds" class="orderby form-control select2 select2-hidden-accessible" id="afbeds" tabindex="-1" aria-hidden="true">
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6+">6+</option>
        </select>
   </div>
                      <div class="col-md-4">
                        <?php echo form_error('gender'); ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3">Kitchen</label>
                      <div class="col-md-5">
                       
                        <input type="radio" name="kitchen" value="yes" class="radio" checked=""> Yes
                        <input type="radio" name="kitchen" value="no" class="radio">
                        No
                      </div>
                      <div class="col-md-4">
                        <?php echo form_error('gender'); ?>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3">Laundry</label>
                      <div class="col-md-5">
                       
                        <input type="radio" name="laundry_service" value="yes" class="radio" checked=""> Yes
                        <input type="radio" name="laundry_service" value="no" class="radio">
                        No
                      </div>
                      <div class="col-md-4">
                        <?php echo form_error('gender'); ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3">Living Room</label>
                      <div class="col-md-5">
                      
                       <input type="radio" name="living_room" value="yes" class="radio" checked=""> Yes
                       <input type="radio" name="living_room" value="no" class="radio">     No
                     </div>
                     <div class="col-md-4">
                      <?php echo form_error('gender'); ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3">Pantry</label>
                    <div class="col-md-5">
                      
                      <input type="radio" name="Pantry" value="yes" class="radio" checked=""> Yes
                      <input type="radio" name="Pantry" value="no" class="radio"> 
                      No
                    </div>
                    <div class="col-md-4">
                      <?php echo form_error('gender'); ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3">Pool</label>
                    <div class="col-md-5">
                    
                     <input type="radio" name="Pool" value="yes" class="radio" checked=""> Yes
                     <input type="radio" name="Pool" value="no" class="radio"> 
                     No
                   </div>  
                 </div>
                 <div class="form-group">
                  <label class="control-label col-md-3">Backyard</label>
                  <div class="col-md-5">
                   <input type="radio" name="Backyard" value="yes" class="radio" checked=""> Yes
                   <input type="radio" name="Backyard" value="no" class="radio"> 
                   No
                 </div>  
               </div>
               <div class="form-group">
                <label class="control-label col-md-3">Renter ethnicity</label>
                <div class="col-md-5">
                
        <!--<select name="renter_ethnicity" class="orderby form-control ">
         <option value="" selected="selected">-Select Renter Ethnicity-</option>-->
         <select name="renter_ethnicity" class="orderby form-control select2 select2-hidden-accessible" id="afbeds" tabindex="-1" aria-hidden="true">
           <option value="Caucasian or White">Caucasian or White</option>
           <option value="African or Black">African or Black</option>
           <option value="East Asian">East Asian</option>
           <option value="Middle Eastern">Middle Eastern</option>
           <option value="Latino">Latino</option>
           <option value="Chinese">Chinese</option>
           <option value="Spanish ">Spanish </option>
           <option value="Indian ">Indian </option>
         </select>
       </div>  
     </div>

     <div class="form-group">
      <label class="control-label col-md-3">Renter Age Group</label>
      <div class="col-md-5">
      
       <select name="renter_age_group" class="orderby form-control select2 select2-hidden-accessible" id="afbeds" tabindex="-1" aria-hidden="true">
         <option value="" selected="selected">-Select Renter Age group-</option>
         <option value="16–20">16–20</option>
         <option value="21-30">21-30</option>
         <option value="31-40">31-40</option>
         <option value="41–60">41–60</option>
         <option value="60+">60+</option>
         <option value="Any of the above">Any of the above</option>
       </select>
     </div>  
   </div>
   <div class="form-group">
    <label class="control-label col-md-3">Food Service</label>
    <div class="col-md-5">
    
     <input type="checkbox" name="additional_services[]" value="Breakfast" class="checkbox-inline"> Breakfast
     <input type="checkbox" name="additional_services[]" value="Dinner" class="checkbox-inline"> Dinner
     <input type="checkbox" name="additional_services[]" value="Snacks" class="checkbox-inline"> Snacks
     <input type="checkbox" name="additional_services[]" value="Cooking Facility" class="checkbox-inline"> Cooking Facility
     <input type="checkbox" name="additional_services[]" value="Other Meals" class="checkbox-inline"> Other Meals
   </div>  
 </div>
 <div class="form-group">
  <label class="control-label col-md-3">Renter’s Category </label>
  <div class="col-md-5">
   
   <input type="checkbox" name="renter_type[]" value="Student" class="checkbox-inline"> Student
   <input type="checkbox" name="renter_type[]" value="Working Single" class="checkbox-inline"> Working Single
   <input type="checkbox" name="renter_type[]" value="Couple" class="checkbox-inline"> Couple
   <input type="checkbox" name="renter_type[]" value="Family with Children" class="checkbox-inline"> Family with Children
   <input type="checkbox" name="renter_type[]" value="Single Parents with Kids" class="checkbox-inline"> Single Parents with Kids
   <input type="checkbox" name="renter_type[]" value="Retiree" class="checkbox-inline"> Retiree
   <input type="checkbox" name="renter_type[]" value="Others" class="checkbox-inline"> Others
 </div>  
</div>
<div class="col-md-4">
  <?php echo form_error('gender'); ?>
</div>
</div>

</div>
<div class="form-actions">
  <div class="row">
    <div class="col-md-offset-3 col-md-9"> 
      <?php echo form_submit(array('id' => 'submit', 'name' => 'Submit' , 'value' => 'Submit' ,'class'=>'btn red')); ?>
      <button type="button" class="btn default">Cancel</button>
    </div>
  </div>
</div>
<?= form_close(); ?>
<script>
  function date_check() {
    var month= $('#month').val();
    var date= $('#day').val();
    var yeara= $('#yeara').val();
    var html='';
    $('#date_error').empty();
    if((month==2)|(month==4)|(month==6)|(month==9)|(month==11)&& (date==31)  )
    {
      $('#date_error').empty();
      html='';
      html="Error: Selected Month Does not have this Date. Please Correct it.";
      $('#date_error').append(html);
    }
    if(month==2 && (date == 30 | date==31) ){

      $('#date_error').empty();
      html='';
      html="Error: February month do not have 30th and 31st date. Please Correct it."
      $('#date_error').append(html);
    }
    if(yeara !=null) {
      if ((month == 2 & date > 28) && yeara % 4 != 0) {
        $('#date_error').empty();
        html='';
        html="Error: 29th day of February is only available in Leap years. Please Correct it."
        $('#date_error').append(html);
      }
    }
  }
</script>
<script>
  $('INPUT[type="file"]').change(function () {
    var ext = this.value.match(/\.(.+)$/)[1];
    switch (ext) {
      case 'jpg':
      case 'jpeg':
      case 'JPG':
      case 'png':
      case 'gif':
      $('#banner').attr('disabled', false);
      break;
      default:
      alert('This is not an allowed file type.');
      this.value = '';
    }
  });
</script>
<script>
  function getstatedetails(id)
  {
    $.ajax({
      type: "POST",
      url: '<?php echo base_url().'supercontrol/Member/ajax_state_list'.'/';?>'+id,
      data: id='cat_id',
      success: function(data){
        $('#sub').html(data);
      },
    });
  }

  function getcitydetails(id)
  {
    $.ajax({
      type: "POST",
      url: '<?php echo base_url().'supercontrol/Member/ajax_city_list'.'/';?>'+id,
      data: id='cat_id',
      success: function(data){
        $('#sub1').html(data);
      },
    });
  }
</script>
<!-- END FORM-->
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- END CONTENT BODY --> 
</div>
<!-- END CONTENT --> 
<!-- BEGIN QUICK SIDEBAR --> 
<!-- END QUICK SIDEBAR --> 
</div>
<!-- END CONTAINER -->
<?php //$this->load->view ('footer');?>
