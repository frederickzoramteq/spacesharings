<style type="text/css">
.dashboard-stat .visual > i {
    margin-left: 0;
}
</style>

<div class="page-container">
  <!-- BEGIN SIDEBAR -->
  <div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <?php $this->load->view('supercontrol/leftbar');//include"lib/leftbar.php" ?>
    <!-- END SIDEBAR -->
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
  <div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->
      <!-- BEGIN PAGE BAR -->
       <h3 class="page-title"> Dashboard
        </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li><a style="pointer-events:none;" href="<?php base_url();?>supercontrol/home">Home</a><i class="fa fa-angle-right"></i></li>
                <li><span>Dashboard</span></li>
            </ul>
        </div>
      <!-- END PAGE BAR -->
      <!-- BEGIN PAGE TITLE-->
      <!-- BEGIN DASHBOARD STATS 1-->
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="dashboard-stat blue">
            <div class="visual"> <i class="fa fa-sliders"></i> </div>
            <div class="details">
              <!--<div class="number"> <span data-counter="counterup" data-value="1349">0</span> </div>-->
              <div class="desc">Registered Members</div>
            </div>
            <a class="more" href="<?php echo base_url(); ?>supercontrol/member/showmember"> View more <i class="m-icon-swapright m-icon-white"></i> </a> </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="dashboard-stat red">
            <div class="visual"> <i class="fa fa-picture-o"></i> </div>
            <div class="details">
              <!--<div class="number"> <span data-counter="counterup" data-value="12,5">0</span>M$ </div>-->
              <div class="desc">CMS</div>
            </div>
            <a class="more" href="<?php echo base_url();?>supercontrol/cms/show_cms"> View more <i class="m-icon-swapright m-icon-white"></i> </a> </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="dashboard-stat green">
            <div class="visual"> <i class="fa fa-newspaper-o"></i> </div>
            <div class="details">
              <!--<div class="number"> <span data-counter="counterup" data-value="549">0</span> </div>-->
              <div class="desc">Testimonial</div>
            </div>
            <a class="more" href="<?php echo base_url();?>supercontrol/testimonial/show_testimonial"> View more <i class="m-icon-swapright m-icon-white"></i> </a> </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="dashboard-stat purple">
            <div class="visual"> <i class="fa fa-envelope"></i> </div>
            <div class="details">
              <!--<div class="number"> + <span data-counter="counterup" data-value="89"></span>% </div>-->
              <div class="desc">Contact Users</div>
            </div>
            <a class="more" href="<?php echo base_url();?>supercontrol/contact/show_contact"> View more <i class="m-icon-swapright m-icon-white"></i> </a> </div>
        </div>
        <div class="col-md-12" style="text-align:center; padding-top:50px;"><b style="font-size:30px;">Welcome To <a style="text-decoration:none; pointer-events:none; cursor:pointer;" target="_blank" href="<?php echo base_url();?>"><?php echo $title;?></a> </b></div>
      </div>
      <div class="clearfix"></div>
      <!-- END DASHBOARD STATS 1-->
     </div>
    <!-- END CONTENT BODY -->
  </div>
  <!-- END CONTENT -->
  <!-- BEGIN QUICK SIDEBAR -->
  <a href="javascript:;" class="page-quick-sidebar-toggler"> <i class="icon-login"></i> </a>
  <div class="page-quick-sidebar-wrapper" data-close-on-body-click="false">
    