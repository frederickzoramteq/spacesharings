<?php //$this->load->view ('header');?>

<!-- BEGIN CONTAINER -->

<style>

#sample_1_filter{

 padding: 8px;

 float: right;

}

#sample_1_length{

 padding: 8px;

}

#sample_1_info{

 padding: 8px;	

}

#sample_1_paginate{

 float: right;

 padding: 8px;

}	

</style>

<div class="page-container">

  <!-- BEGIN SIDEBAR -->

  <div class="page-sidebar-wrapper">

    <!-- BEGIN SIDEBAR -->

    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->

    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->

    <div class="page-sidebar navbar-collapse collapse">

      <!-- BEGIN SIDEBAR MENU -->

      <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->

      <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->

      <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->

      <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->

      <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->

      <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->

      <?php $this->load->view ('supercontrol/leftbar');?>

      <!-- END SIDEBAR MENU -->

      <!-- END SIDEBAR MENU -->

    </div>

    <!-- END SIDEBAR -->

  </div>

  <!-- END SIDEBAR -->

  <!-- BEGIN CONTENT -->

  <style>

  .dataTables_info{padding:7px;}

</style>

<div class="page-content-wrapper">

  <!-- BEGIN CONTENT BODY -->

  <div class="page-content">

    <!-- BEGIN PAGE HEADER-->

    <!-- BEGIN THEME PANEL -->

    <!-- END THEME PANEL -->

    <!-- BEGIN PAGE BAR -->

    <div class="page-bar">

      <ul class="page-breadcrumb">

        <li> <a href="<?php echo base_url(); ?>supercontrol/home">Home</a> <i class="fa fa-circle"></i> </li>

        <li> <span>Supercontrol Panel</span> <i class="fa fa-circle"></i> </li>

        <li> <span>Show Category List </span> </li>

      </ul>

    </div>

    <!-- END PAGE BAR -->

    <!-- BEGIN PAGE TITLE-->

    <!-- END PAGE TITLE-->

    <!-- END PAGE HEADER-->

    <?php if(@$add_message){echo @$add_message;}?>

    <?php if(@$message){echo @$message;}?>

    <?php if(@$msg){echo @$msg;}?>

    <?php if(@$msg1){echo @$msg1;}?> 

    <?php if($this->session->flashdata('success_delete')!=''){?>
    <div class="alert alert-success text-center">
      <?php echo $this->session->flashdata('success_delete');?>
    </div>
      <?php
      }
    ?>
    <!-- Code start from Here (Koushik Dutta)-->
    <?php if( $error = $this->session->flashdata('feedback')) : ?>
      <div class="row">
        <div class="col-lg-12">
          <div class="alert alert-dismissible alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Well done!</strong> <?= $error ?>.
          </div>
        </div>
      </div>
    <?php endif;?>
    <!-- Code end Here -->

    <div class="row">

      <div class="col-md-12">

        <div class="tabbable-line boxless tabbable-reversed">

          <div class="tab-content">

            <div class="tab-pane active" id="tab_0">

              <div class="portlet box blue-hoki">

                <div class="portlet-title">

                  <div class="caption"> <i class="fa fa-gift"></i>Show Category</div>

                  <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>

                </div>



                <div class="portlet-body form">



                  <!-- BEGIN FORM-->

                  <table class="table table-striped table-bordered table-hover table-checkable order-column dt-responsive" id="sample_1">

                    <div id="mydiv">

                      <thead>

                        <tr>
                          <th width="30">Image</th>
                          <th width="30">Category</th>
                          <th width="27">Action</th>



                        </tr>

                      </thead>

                      <tbody>

                        <?php if(is_array($category)): ?>

                         <?php

                         $ctn=1;

                         foreach($category as $i){

                          ?>

                          <tr class="table table-striped table-bordered table-hover table-checkable order-column dt-responsive" id="sample_1">


                            <td  style="max-width:400px;"><?php if($i->gallery_cat_image==""){?>No Image<?php }else{?><img src="<?php echo base_url()?>uploads/Gallery_category/<?php echo $i->gallery_cat_image;?>" width="150" height="100" /><?php }?></td>
                            <td  style="max-width:400px;"><?php echo $i->category_name;?></td>



                            <td style="max-width:50px;"><a class="btn green btn-sm btn-outline sbold uppercase" href="<?php echo base_url()?>supercontrol/gallery/show_category_id/<?php echo $i->catid; ?>">Edit</a> | <a class="btn red btn-sm btn-outline sbold uppercase" onclick="return confirm('Are you sure about this delete?');" href="<?php echo base_url()?>supercontrol/gallery/delete_category/<?php echo $i->catid; ?>">Delete</a>
                            </td>





                          </tr>



                          <?php $ctn++;}?>

                        <?php endif; ?>

                      </tbody>

                    </div>

                  </table>

                  <!-- END FORM-->

                </div>

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

  <!-- END CONTENT BODY -->

</div>

<!-- END CONTENT -->

<!-- BEGIN QUICK SIDEBAR -->

<!-- END QUICK SIDEBAR -->

</div>

