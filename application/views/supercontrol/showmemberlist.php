<style>
#sample_1_filter {
	padding: 8px;
	float: right;
}
#sample_1_length {
	padding: 8px;
}
#sample_1_info {
	padding: 8px;
}
#sample_1_paginate {
	float: right;
	padding: 8px;
}
.dataTables_length {
	padding:10px;
}
.dataTables_filter {
	padding: 10px;
	float: right;
}
.dataTables_info {
	padding:10px;
}
.pagination-panel {
	padding: 10px;
	float: right;
}
</style>
<?php //$this->load->view ('header');?>

<!-- BEGIN CONTAINER -->

<div class="page-container"> 
  
  <!-- BEGIN SIDEBAR -->
  
  <div class="page-sidebar-wrapper"> 
    
    <!-- BEGIN SIDEBAR --> 
    
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
    
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    
    <div class="page-sidebar navbar-collapse collapse"> 
      
      <!-- BEGIN SIDEBAR MENU -->
      <?php $this->load->view ('supercontrol/leftbar');?>
      <!-- END SIDEBAR MENU --> 
      <!-- END SIDEBAR MENU --> 
    </div>
    <!-- END SIDEBAR --> 
  </div>
  
  <!-- END SIDEBAR --> 
  
  <!-- BEGIN CONTENT -->
  
  <div class="page-content-wrapper"> 
    
    <!-- BEGIN CONTENT BODY -->
    
    <div class="page-content"> 
      
      <!-- BEGIN PAGE HEADER--> 
      
      <!-- BEGIN THEME PANEL --> 
      
      <!-- END THEME PANEL --> 
      
      <!-- BEGIN PAGE BAR -->
      
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <li> <a href="<?php echo base_url(); ?>supercontrol/home">Home</a> <i class="fa fa-circle"></i> </li>
          <li> <span>supercontrol Panel</span> <i class="fa fa-circle"></i> </li>
          <li> <span>Show Memer List </span> </li>
        </ul>
      </div>
      
      <!-- END PAGE BAR --> 
      
      <!-- BEGIN PAGE TITLE--> 
      
      <!-- END PAGE TITLE--> 
      
      <!-- END PAGE HEADER-->
      
      <?php if (isset($success_msg)) { echo $success_msg; } ?>
      <?php if (isset($msg)) { echo $msg; } ?>
      <div class="row">
		<?php if (isset($success_msg)) { echo $success_msg; } ?>
        <?php if($this->session->flashdata('add_message')!=''){?>
        <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">&#10006;</button>
        <strong><?php echo @$this->session->flashdata('add_message');?></strong> 
        </div>
        <?php }?>
        <?php if($this->session->flashdata('edit_message')!=''){?>
        <div class="alert alert-success1" style="background-color:#98E0D5;">
        <button type="button" class="close" data-dismiss="alert">&#10006;</button>
        <strong style="color:#063;"><?php echo @$this->session->flashdata('edit_message');?></strong> 
        </div>
        <?php }?>
        <?php if($this->session->flashdata('delete_message')!=''){?>
        <div class="alert alert-error" style="background-color:#F0959B;">
        <button type="button" class="close" data-dismiss="alert">&#10006;</button>
        <strong style="color:#900;"><?php echo @$this->session->flashdata('delete_message');?></strong> 
        </div>
        <?php }?>
        <div class="col-md-12">
          <div class="tabbable-line boxless tabbable-reversed">
            <div class="tab-content">
              <div class="tab-pane active" id="tab_0">
                <div class="portlet box blue-hoki">
                  <div class="portlet-title">
                    <div class="caption"> <i class="fa fa-gift"></i><?php echo $title;?></div>
                    <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
                  </div>
                  <div class="portlet-body form"> 
                    
                    <!-- BEGIN FORM-->
                    
                    <table class="table table-striped table-bordered table-hover table-checkable order-column dt-responsive" id="sample_1">
                      <thead>
                        <tr>
                          <th width="39" style="max-width:200px;">Name</th>
                          <th width="39" style="max-width:200px;">Email</th>
                          <th width="39" style="max-width:200px;" >Registration Number</th>
                          <!--<th width="39">Type</th>-->
                          <th width="39">Status</th>
                          <th width="66">View</th>
                          <th width="66">Edit</th>
                          <th width="66">Delete</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                			foreach($edata as $i){
                		?>
                        <tr class="table table-striped table-bordered table-hover table-checkable order-column dt-responsive" id="sample_1">
                          <td  style="max-width:200px;"><?php echo $i->first_name ." ". $i->last_name?></td>
                          <td  style="max-width:200px;"><?php echo $i->email;?></td>
                          <td  style="max-width:200px;"><?php echo $i->reg_no;?></td>
                          
                          <!--<td  style="max-width:200px;"><b <?php if($i->reg_type=='personal'){?> style="color:blue;" <?php }else{?> style="color:green;" <?php }?>><?php echo $i->reg_type;?></b></td>-->
                          <td  style="max-width:200px;"><?php if($i->status=='Yes'){ ?><b style="color:#060;">Active</b><?php }else{?><b style="color:#F00;">InActive</b><?php }?></td> 
                          <td style="max-width:50px;"><a class="btn yellow btn-sm btn-outline sbold uppercase" href="<?php echo base_url()?>supercontrol/Member/view_member/<?php echo $i->id; ?>">View</a></td>
                          <td style="max-width:250px;"><a class="btn green btn-sm btn-outline sbold uppercase" href="<?php echo base_url()?>supercontrol/member/show_member_id/<?php echo $i->id; ?>">Edit</a></td>
                          <td style="max-width:250px;"><a onclick="return confirm('Are you sure you want to delete ?');" class="btn red btn-sm btn-outline sbold uppercase" href="<?php echo base_url()?>supercontrol/member/delete_member/<?php echo $i->id; ?>">Delete</a></td>
                        </tr>
                        <?php }?>
                      </tbody>
                    </table>
                    
                    <!-- END FORM--> 
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!-- END CONTENT BODY --> 
    
  </div>
  
  <!-- END CONTENT --> 
  
  <!-- BEGIN QUICK SIDEBAR --> 
  
  <!-- END QUICK SIDEBAR --> 
  
</div>
<script>

			function f1(stat,id)

	{

	//alert(stat);
		//alert(id);
		$.ajax({
                    type:"get",
                    url:"<?php echo base_url(); ?>supercontrol/testimonial/statustestimonial",
                     data:{stat : stat, id :id}
					  });

		//$.get('<?php echo base_url(); ?>banner/statusbanner',{ stat : stat , id : id });

	}

</script> 

<!-- END CONTAINER -->

<?php //$this->load->view ('footer');?>
