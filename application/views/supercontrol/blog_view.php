<?php //$this->load->view ('header');?>
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">
        <!-- BEGIN SIDEBAR -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <div class="page-sidebar navbar-collapse collapse">
            <!-- BEGIN SIDEBAR MENU -->
            <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
            <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
            <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
            <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
            <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
            <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
            <?php $this->load->view ('supercontrol/leftbar');?>
            <!-- END SIDEBAR MENU -->
            <!-- END SIDEBAR MENU -->
        </div>
        <!-- END SIDEBAR -->
    </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN THEME PANEL -->
            
            <!-- END THEME PANEL -->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="<?php echo base_url(); ?>supercontrol/home">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>supercontrol Panel</span>
                    </li>
                </ul>
                
            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <div class="tabbable-line boxless tabbable-reversed">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_0">
                                <div class="portlet box blue-hoki">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Hosting View</div>
                                            <div class="tools">
                                             
                                                <a href="javascript:;" class="reload"> </a>
                                                <a href="javascript:;" class="remove"> </a>
                                            </div>
                                        </div>
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <?php foreach($ecms as $i){?>
                                            <table class="table table-striped table-bordered table-hover table-checkable order-column dt-responsive" id="sample_1"><br />
                                              
                                             <tr><td style="width:30%;" > Image:</td>  <td height="130px;" width="500px"><?php if($i->image==""){?>
                                                No Image
                                                <?php }else{?>
                                                <img style="width:500px; height:300px;" src="<?php echo base_url()?>uploads/blog/<?php echo $i->image;?>" width="150" height="100" style="border: 2px solid #ddd;"/>
                                                <?php }?></td></tr>
                                                <!-- <tr><td >Blog Title :</td>  <td><?php echo $i->description;?></td></tr> -->
                                                <tr><td >Posted By :</td>  <td><?php echo $i->fname;?></td></tr> 
                                                
                                                <tr><td >Hosting Description:</td>  <td height="130px;" width="500px"><?php echo $i->description;?></td></tr>
                                                <tr><td >phone:</td>  <td height="130px;" width="500px"><?php echo $i->phone;?></td></tr>
                                                <tr><td >Hosting email:</td>  <td height="130px;" width="500px"><?php echo $i->email;?></td></tr>
                                                <tr><td >country:</td>  <td height="130px;" width="500px">
                                                    <?php foreach($country as $c){?>
                                                    <?=$c->count_name;?>
                                                    <?php }?>
                                                </td></tr>
                                                <tr><td >state:</td>  <td height="130px;" width="500px"><?php foreach($state as $c){?>
                                                  <?=$c->state_name;?>
                                                  <?php }?></td></tr>
                                                  <tr><td >city:</td>  <td height="130px;" width="500px"> <?php foreach($city as $c){?>
                                                      <?=$c->city_name;?>
                                                      <?php }?></td></tr>
                                                      <tr><td >postcode:</td>  <td height="130px;" width="500px"><?php echo $i->postcode;?></td></tr>
                                                      <tr><td >Gender:</td>  <td height="130px;" width="500px"><?php echo $i->hostgender;?></td></tr>
                                                      <tr><td >Available From:</td>  <td height="130px;" width="500px"><?php echo $i->avail_from;?></td></tr>
                                                      <tr><td >Area:</td>  <td height="130px;" width="500px"><?php echo $i->area;?></td></tr>
                                                      <tr><td >Bathroom:</td>  <td height="130px;" width="500px"><?php echo $i->bathroom;?></td></tr>
                                                      <tr><td >Bedroom:</td>  <td height="130px;" width="500px"><?php echo $i->bedroom;?></td></tr>
                                                      <tr><td >Pool:</td>  <td height="130px;" width="500px"><?php echo $i->pool;?></td></tr>
                                                      <tr><td >pantry:</td>  <td height="130px;" width="500px"><?php echo $i->pantry;?></td></tr>
                                                      <tr><td >Living Room:</td>  <td height="130px;" width="500px"><?php echo $i->living_room;?></td></tr>
                                                      <tr><td >Backyard:</td>  <td height="130px;" width="500px"><?php echo $i->backyard;?></td></tr>
                                                      <tr><td >Loundry service:</td>  <td height="130px;" width="500px"><?php echo $i->laundry_service;?></td></tr>
                                                      <tr><td >Renter Ethinicity:</td>  <td height="130px;" width="500px"><?php echo $i->renter_ethnicity;?></td></tr>  
                                                      <tr><td >Renter Gender:</td>  <td height="130px;" width="500px"><?php echo $i->rent_gender;?></td></tr>
                                                      <tr><td >Renter Age Group:</td>  <td height="130px;" width="500px"><?php echo $i->renter_age_group;?></td></tr>

                                                  </table>
                                                  
                                                  
                                                  
                                                  
                                                  <?php }?> 
                                                  <div style="margin-left:537px;">
                                                    <td style="max-width:70px;"><a class="btn green btn-sm btn-outline sbold uppercase" href="<?php echo base_url()?>supercontrol/blog/show_blog" style="width:70px;">Back</a></td></div>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                <!-- BEGIN QUICK SIDEBAR -->
                
                <!-- END QUICK SIDEBAR -->
            </div>
            <!-- END CONTAINER -->
            <?php //$this->load->view ('footer');?>
