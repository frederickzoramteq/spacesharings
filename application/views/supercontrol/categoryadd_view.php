<div class="page-container">
  <div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
      <?php $this->load->view ('supercontrol/leftbar');?>
    </div>
  </div>
  <div class="page-content-wrapper">
    <div class="page-content">
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <li> <a href="<?php echo base_url(); ?>supercontrol/home">Home</a> <i class="fa fa-circle"></i> </li>
          <li> <span>Admin panel</span> </li>
        </ul>
      </div>
      <!-- <div class="alert alert-success alert-dismissable" style="padding:10px;">
        <button class="close" aria-hidden="true" data-dismiss="alert" type="button" style="right:0;"></button>
        <strong>
        <?php 
				$last = end($this->uri->segments); 
				if($last=="success"){echo "Data Added Successfully ......";}
				if($last=="successdelete"){echo "Data Deleted Successfully ......";}
            ?>
        </strong> </div> -->
      <div class="row">
        <div class="col-md-12">
          <div class="tabbable-line boxless tabbable-reversed">
            <div class="tab-content">
              <div class="tab-pane active" id="tab_0">
                <div class="portlet box blue-hoki">
                  <div class="portlet-title">
                    <div class="caption"> <i class="fa fa-gift"></i>Add Category type</div>
                  </div>
                  <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <?php
                    echo form_open_multipart('supercontrol/gallery/add_gallerycategory','class="form-horizontal form-bordered"');
                    ?>
                    <div class="form-body">
                      <div class="form-group">
                        <label class="control-label col-md-3"> Subject type Title</label>
                        <div class="col-md-5">
                          <?php echo form_input(array('id' => 'category_name', 'name' => 'category_name','class'=>'form-control', 'value' => set_value('category_name')));
                          ?>
                        </div>
                        <div class="col-md-3" style="border-left: none !important;">
                          <?php echo form_error('category_name'); ?>
                        </div>
                      </div>
                      <div class="form-group last">
                        <label class="control-label col-md-3">Image</label>
                        <div class="col-md-6">
                          <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">

                              <img src="https://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />

                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> 
                            </div>
                            <div>
                              <span class="btn default btn-file">
                                <span class="fileinput-new"> Select image </span>
                                <span class="fileinput-exists"> Change </span>
                                <?php
                                $file = array('id' => 'banner', 'type' => 'file','name' => 'userfile', 'onchange' => 'readURL(this);' );
                                echo form_input($file);
                                ?>
                              </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                            </div>
                          </div>
                          <div class="clearfix margin-top-10">
                            <span class="label label-danger">Format</span> jpg, jpeg, png, gif 
                          </div>
                        </div>
                        <div class="col-md-3" style="border-left: none !important;">
                          <?php if(isset($upload_error)) echo $upload_error ?>
                        </div>
                      </div>
                    </div>
                    <div class="form-actions">
                      <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                          <?php
                          echo form_submit(array('id' => 'submit', 'value' => 'Submit' ,'class'=>'btn red'));
                          ?>
                          <button type="button" class="btn default">Cancel</button>
                        </div>
                      </div>
                    </div>
                    <?= form_close();?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
