

<html>
<head>
<title>Image manipulation using codeigniter</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style.css">
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.0/jquery-ui.min.js"></script>
<script>
// Show select image using file input.
function readURL(input) {
$('#default_img').show();
if (input.files && input.files[0]) {
var reader = new FileReader();

reader.onload = function(e) {
$('#select')
.attr('src', e.target.result)
.width(300)
.height(200);

};

reader.readAsDataURL(input.files[0]);
}
}
</script>
</head>
<body>
<div class="main">
<div class="data">
<div class="page">
<div id="content">
<h2 id="form_head">Codelgniter Image Manipulation</h2><br/>
<hr>
<div id="form_input">
<?php
$data = array(
'enctype' => 'multipart/form-data'
);
// Form open
echo form_open('manipulation_controller/value', $data);

// File input field.
$file = array('type' => 'file','name' => 'userfile','onchange' => 'readURL(this);');
echo form_input($file);
echo "<br>";
echo "<br>";
?>
<?php // show image which we choose in file input ?>
<div id='default_img'>
<img id="select" src="#" alt="your image" />
</div>


<div id="form_button">
<?php
// Submit Button.
echo form_submit('submit', 'Upload', "class='submit'");
?>
</div>
<?php echo form_close(); ?>
</div>


</div>
</div>
</div>

<?php // Result image will show on here. ?>
<div id='img_resize'>



</div>

<?php // Input fields for watermark option. ?>

</div>

<?php // Input fields for crop option. ?>

</div>
<?php // Input fields for rotate option. ?>

<?php echo form_close(); ?>
</div>



</body>
</html>

