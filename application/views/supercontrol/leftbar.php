<?php 
$nl=$this->uri->segment(2); 
$nl2=$this->uri->segment(3); 
?>
<div class="page-sidebar navbar-collapse collapse"> 
  <!-- BEGIN SIDEBAR MENU -->
  <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
    <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
    <li class="sidebar-toggler-wrapper hide"> 
      <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
      <div class="sidebar-toggler"> </div>
      <!-- END SIDEBAR TOGGLER BUTTON --> 
    </li>
    <li class="sidebar-search-wrapper"> </li>
    <li class="nav-item start "> <a href="<?php echo base_url()?>supercontrol/home" class="nav-link "> <span class="title">Dashboard</span> </a>
      <ul class="sub-menu">
        <li class="nav-item start "> <a href="<?php echo base_url()?>supercontrol/home" class="nav-link "> <i class="icon-list"></i> <span class="title">View Dashboard</span> </a> </li>
      </ul>
    </li>
    <li class="heading">
      <h3 class="uppercase">Features</h3>
    </li>
    <!--Supratim Sen (CMS Section Start- 20/04/2016)-->
    <li class="nav-item <?php if($nl=="cms"){ ?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-list"></i> <span class="title">CMS Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
      <ul class="sub-menu">
        <li class="nav-item  <?php if($nl=="cms"){ ?>active open<?php }?>"> <a href="<?php echo base_url()?>supercontrol/cms/show_cms" class="nav-link "> <span class="title">Manage CMS Page</span> </a> </li>
        <!-- <li class="nav-item  <?php if($nl=="show_desc"){ ?>active open<?php }?>"> <a href="<?php echo base_url()?>supercontrol/cms/show_desc" class="nav-link "> <span class="title">Manage Page Description</span> </a> </li> -->
       
      </ul>
    </li>
   <!--  <li class="nav-item  <?php if($nl=="Banner"){?>active open<?php  }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-diamond"></i> <span class="title">Banner Management</span> <span class="arrow"></span> </a>
   <ul class="sub-menu">
     <li class="nav-item <?php if($nl2 == "Add Banner"){?>active open<?php }?>"> <a href="<?php echo base_url(); ?>supercontrol/Banner/addbanner" class="nav-link "> <span class="title">Add Banner</span> </a> </li>
     <li class="nav-item <?php if($nl2 == "Show Banner"){?>active open<?php }?>"> <a href="<?php echo base_url(); ?>supercontrol/Banner/showbanner" class="nav-link "> <span class="title">Show Banner</span> </a> </li>
   </ul>
     </li> -->
  
  
    
    <li class="nav-item  <?php if($nl=="member"){?>active open<?php  }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-users"></i> <span class="title">Member Management</span> <span class="arrow"></span> </a>
      <ul class="sub-menu">
        <li class="nav-item <?php if($nl2 == "addmember"){?>active open<?php }?>"> <a href="<?php echo base_url(); ?>supercontrol/member/addmember" class="nav-link "> <span class="title">Add Member</span> </a> </li>
        
         <li class="nav-item <?php if($nl2 == "showmember" || $nl2=="show_member_id"){?>active open<?php }?>"> <a href="<?php echo base_url(); ?>supercontrol/member/showmember" class="nav-link "> <span class="title">Show Member</span> </a> </li>

      </ul>
    </li>
    
    <!-- <li class="nav-item <?php if($nl=="gallery"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">Image Gallery Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
      <ul class="sub-menu">
        <li class="nav-item  <?php if($nl2=="addcategoryform"){?>active open<?php }?>"> <a href="<?php echo base_url();?>supercontrol/gallery/addcategoryform" class="nav-link "> <span class="title">Add Category[Gallery]</span> </a> </li>
        <li class="nav-item  <?php if($nl2=="gallerycatlist" || $nl2=="show_category_id" ){?>active open<?php }?>"> <a href="<?php echo base_url();?>supercontrol/gallery/gallerycatlist" class="nav-link "> <span class="title">View Category</span> </a> </li>
        <li class="nav-item  <?php if($nl2=="addform"){?>active open<?php }?>"> <a href="<?php echo base_url();?>supercontrol/gallery/addform" class="nav-link "> <span class="title">Add Gallery</span> </a> </li>
        <li class="nav-item  <?php if($nl2=="show_gallery" || $nl2=="show_gallery_id"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>supercontrol/gallery/show_gallery" class="nav-link "> <span class="title">View Gallery</span> </a> </li>
      </ul>
    </li> -->
    
    <li class="nav-item <?php if($nl=="blog"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">Hosting Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item <?php if($nl2 == "advhosting"){?>active open<?php }?>"> <a href="<?php echo base_url(); ?>supercontrol/blog/advhosting" class="nav-link "> <span class="title">Add Advance  Hosting</span> </a> </li>
	<!-- <li class="nav-item  <?php if($nl2=="blog_add_form"){?>active open<?php }?>"> <a href="<?php echo base_url();?>supercontrol/blog/blog_add_form" class="nav-link "> <span class="title">Add hosting</span> </a> </li> -->
      <li class="nav-item  <?php if($nl2=="show_blog" || $nl2=="show_blog_id" || $nl2=="view_blog"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>supercontrol/blog/show_blog" class="nav-link "> <span class="title">Manage Hosting</span> </a> </li>
    </ul>
  </li>

<!-- <li class="nav-item <?php if($nl=="faq"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-list"></i> <span class="title">FAQ Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
      <ul class="sub-menu">
        <li class="nav-item  <?php if($nl2=="faq_add_form"){?>active open<?php }?>"> <a href="<?php echo base_url();?>supercontrol/faq/faq_add_form" class="nav-link "> <span class="title">Add FAQ</span> </a> </li>
        <li class="nav-item  <?php if($nl2=="show_faq"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>supercontrol/faq/show_faq" class="nav-link "> <span class="title">Manage FAQ</span> </a> </li>
      </ul>
    </li> -->
<!--     
    <li class="nav-item <?php if($nl=="news"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">News Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
      <ul class="sub-menu">
        <li class="nav-item  <?php if($nl2=="news_add_form"){?>active open<?php }?>"> <a href="<?php echo base_url();?>supercontrol/news/news_add_form" class="nav-link "> <span class="title">Add News</span> </a> </li>
        <li class="nav-item  <?php if($nl2=="show_news" || $nl2=="show_news_id" || $nl2=="view_news"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>supercontrol/news/show_news" class="nav-link "> <span class="title">Manage News</span> </a> </li>
      </ul>
    </li> -->
   
    
   <li class="nav-item <?php if($nl=="testimonial"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">Testimonial Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
      <ul class="sub-menu">
        <li class="nav-item  <?php if($nl2=="testmonial_add_form"){?>active open<?php }?>"> <a href="<?php echo base_url();?>supercontrol/testimonial/testmonial_add_form" class="nav-link "> <span class="title">Add Testimonial</span> </a> </li>
        <li class="nav-item  <?php if($nl2=="show_testimonial"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>supercontrol/testimonial/show_testimonial" class="nav-link "> <span class="title">Manage Testimonial</span> </a> </li>
      </ul>
    </li>
    
    <li class="nav-item <?php if($nl=="contact"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-list"></i> <span class="title">Contact Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
      <ul class="sub-menu">
        <li class="nav-item  <?php if($nl2=="show_contact"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>supercontrol/contact/show_contact" class="nav-link "> <span class="title">Manage Contact</span> </a> </li>
        <li class="nav-item  <?php if($nl2=="show_subscr"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>supercontrol/contact/show_subscr" class="nav-link "> <span class="title">Manage Subscribe Mail </span> </a> </li>
      </ul>
    </li>
    
    <!--<li class="nav-item <?php if($nl=="subscription"){?>active open<?php }?>"> 
    	<a href="javascript:;" class="nav-link nav-toggle"> 
        	<i class="icon-docs"></i> 
            <span class="title">Subscription Mgmt</span> 
            <span class="addindividual"></span> 
            <span class="arrow addindividual"></span> 
        </a>
      	<ul class="sub-menu">
        <li class="nav-item  <?php if($nl2=="subscription_add_view"){?>active open<?php }?>"> <a href="<?php echo base_url();?>supercontrol/subscription/subscription_add_view" class="nav-link "> <span class="title">Add Subscription</span> </a> </li>
        <li class="nav-item  <?php if($nl2=="show_subscription" || $nl2=="show_news_id" || $nl2=="view_news"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>supercontrol/subscription/show_subscription" class="nav-link "> <span class="title">Subscription List</span> </a> </li>
      </ul>
    </li>-->
    
    <!--<li class="nav-item <?php if($nl=="news"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">News Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
      <ul class="sub-menu">
        <li class="nav-item  <?php if($nl2=="news_add_form"){?>active open<?php }?>"> <a href="<?php echo base_url();?>supercontrol/news/news_add_form" class="nav-link "> <span class="title">Add News</span> </a> </li>
        <li class="nav-item  <?php if($nl2=="show_news" || $nl2=="show_news_id" || $nl2=="view_news"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>supercontrol/news/show_news" class="nav-link "> <span class="title">Manage News</span> </a> </li>
      </ul>
    </li>-->
    
    <!--<li class="nav-item <?php if($nl=="newsletter"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">News Letter Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
      <ul class="sub-menu">
        <li class="nav-item  <?php if($nl2=="show_newsletter"){?>active open<?php }?>"> <a href="<?php echo base_url();?>supercontrol/newsletter/show_newsletter" class="nav-link "> <span class="title">Manage News Letter</span> </a> </li>
      </ul>
    </li>-->
    
    <li class="nav-item <?php if($nl=="adminprofile" ||$nl=="settings" ||$nl=="user" ||$nl=="main" ){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-list"></i> <span class="title">Tools</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
      <ul class="sub-menu">
        <li class="nav-item  <?php if($nl=="adminprofile"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>supercontrol/adminprofile/show_adminprofile_id/1" class="nav-link "> <span class="title">Admin profile</span> </a> </li>
        <li class="nav-item  <?php if($nl=="settings"){?>active open<?php }?>"> <a href="<?php echo base_url();?>supercontrol/settings/show_settings_id/1" class="nav-link "> <span class="title">Settings</span> </a> </li>
        <li class="nav-item  <?php if($nl=="user"){?>active open<?php }?>"> <a href="<?php echo base_url();?>supercontrol/user/show_reset_pass" class="nav-link "> <span class="title">Change Password</span> </a> </li>
        <li class="nav-item  <?php if($nl=="logout"){?>active open<?php }?>"> <a href="<?php echo base_url()?>supercontrol/main/logout" class="nav-link "> <span class="title">Log out</span> </a> </li>
      </ul>
    </li>
  </ul>
  
  <!-- END SIDEBAR MENU --> 
  
  <!-- END SIDEBAR MENU --> 
  
</div>
