<?php include "header.php";?>
<!--<section class="page-title ptb-50" style="padding-top: :140px;">
<div class="container">
<div class="row">
<div class="col-md-12">
<h2>Portfolio Card Style</h2>
<ol class="breadcrumb">
<li><a href="javascript:void(0)">Home</a></li>
<li><a href="javascript:void(0)">Portfolio</a></li>
<li class="active">Portfolio 4</li>
</ol>
</div>
</div>
</div>
</section>-->

<section class="section-padding">
	<div class="container">
		<div class="text-center mb-50">
			<h2 class="section-title">Plans</h2>
			<p class="section-sub"></p>
		</div>
		<div class="portfolio-container">
<!-- <ul class="portfolio-filter brand-filter text-center">
<li class="active waves-effect waves-light" data-group="all">All</li>
<li class="waves-effect waves-light" data-group="websites">Websites</li>
<li class="waves-effect waves-light" data-group="branding">Branding</li>
<li class="waves-effect waves-light" data-group="marketing">Marketing</li>
<li class="waves-effect waves-light" data-group="photography">Photography</li>
</ul> -->
<div class="portfolio portfolio-with-title col-4 gutter mtb-50">
	<div class="portfolio-item" data-groups='["all", "websites", "branding"]'>
		<div class="card">
			<div class="card-image waves-effect waves-block waves-light">
				<img class="activator" src="<?php  echo base_url('assets/img/portfolio/portfolio-1.jpg');?>" alt="image">
			</div>
			<div class="card-content">
				<span class="card-title activator">Plan Title <i class="fa fa-ellipsis-v right"></i> <i class="fa fa-heart-o right"></i></span>
				<p><a href="javascript:void(0)">Fasting</a></p>
			</div>
			<div class="card-reveal">
				<span class="card-title">Plan Title <i class="material-icons right">&#xE5CD;</i></span>
				<p><a href="javascript:void(0)">Fasting</a></p>
				<p>Authoritatively grow quality technologies for strategic sources.</p>
				<a href="javascript:void(0)" class="readmore">View full Plan</a>
			</div>
		</div>
	</div>
	<div class="portfolio-item" data-groups='["all", "websites", "branding"]'>
		<div class="card">
			<div class="card-image waves-effect waves-block waves-light">
				<img class="activator" src="<?php  echo base_url('assets/img/portfolio/portfolio-1.jpg');?>" alt="image">
			</div>
			<div class="card-content">
				<span class="card-title activator">Plan Title <i class="fa fa-ellipsis-v right"></i> <i class="fa fa-heart-o right"></i></span>
				<p><a href="javascript:void(0)">Fasting</a></p>
			</div>
			<div class="card-reveal">
				<span class="card-title">Plan Title <i class="material-icons right">&#xE5CD;</i></span>
				<p><a href="javascript:void(0)">Fasting</a></p>
				<p>Authoritatively grow quality technologies for strategic sources.</p>
				<a href="javascript:void(0)" class="readmore">View full Plan</a>
			</div>
		</div>
	</div>
	<div class="portfolio-item" data-groups='["all", "websites", "branding"]'>
		<div class="card">
			<div class="card-image waves-effect waves-block waves-light">
				<img class="activator" src="<?php  echo base_url('assets/img/portfolio/portfolio-1.jpg');?>" alt="image">
			</div>
			<div class="card-content">
				<span class="card-title activator">Plan Title <i class="fa fa-ellipsis-v right"></i> <i class="fa fa-heart-o right"></i></span>
				<p><a href="javascript:void(0)">Fasting</a></p>
			</div>
			<div class="card-reveal">
				<span class="card-title">Plan Title <i class="material-icons right">&#xE5CD;</i></span>
				<p><a href="javascript:void(0)">Fasting</a></p>
				<p>Authoritatively grow quality technologies for strategic sources.</p>
				<a href="javascript:void(0)" class="readmore">View full Plan</a>
			</div>
		</div>
	</div>
	<div class="portfolio-item" data-groups='["all", "branding", "marketing"]'>
		<div class="card">
			<div class="card-image waves-effect waves-block waves-light">
				<img class="activator" src="<?php  echo base_url('assets/img/portfolio/portfolio-2.jpg');?>" alt="image">
			</div>
			<div class="card-content">
				<span class="card-title activator">Plan Title <i class="fa fa-ellipsis-v right"></i> <i class="fa fa-heart-o right"></i></span>
				<p><a href="javascript:void(0)">New to Faith</a></p>
			</div>
			<div class="card-reveal">
				<span class="card-title">Plan Title <i class="material-icons right">&#xE5CD;</i></span>
				<p><a href="javascript:void(0)">New to Faith</a></p>
				<p>Authoritatively grow quality technologies for strategic sources.</p>
				<a href="javascript:void(0)" class="readmore">View full Plan</a>
			</div>
		</div>
	</div>
	<div class="portfolio-item" data-groups='["all", "websites", "branding", "photography"]'>
		<div class="card">
			<div class="card-image waves-effect waves-block waves-light">
				<img class="activator" src="<?php  echo base_url('assets/img/portfolio/portfolio-3.jpg');?>" alt="image">
			</div>
			<div class="card-content">
				<span class="card-title activator">Plan Title <i class="fa fa-ellipsis-v right"></i> <i class="fa fa-heart-o right"></i></span>
				<p><a href="javascript:void(0)">Through the Bible</a></p>
			</div>
			<div class="card-reveal">
				<span class="card-title">Plan Title <i class="material-icons right">&#xE5CD;</i></span>
				<p><a href="javascript:void(0)">Through the Bible</a></p>
				<p>Authoritatively grow quality technologies for strategic sources.</p>
				<a href="javascript:void(0)" class="readmore">View full Plan</a>
			</div>
		</div>
	</div>
	<div class="portfolio-item" data-groups='["all", "websites", "photography", "marketing"]'>
		<div class="card">
			<div class="card-image waves-effect waves-block waves-light">
				<img class="activator" src="<?php  echo base_url('assets/img/portfolio/portfolio-4.jpg');?>
				" alt="image">
			</div>
			<div class="card-content">
				<span class="card-title activator">Portfolio Title <i class="fa fa-ellipsis-v right"></i> <i class="fa fa-heart-o right"></i></span>
				<p><a href="javascript:void(0)">Work
</a></p>
			</div>
			<div class="card-reveal">
				<span class="card-title">Portfolio Title <i class="material-icons right">&#xE5CD;</i></span>
				<p><a href="javascript:void(0)">Work
</a></p>
				<p>Authoritatively grow quality technologies for strategic sources.</p>
				<a href="javascript:void(0)" class="readmore">View full Plan</a>
			</div>
		</div>
	</div>
	<div class="portfolio-item" data-groups='["all", "branding", "photography"]'>
		<div class="card">
			<div class="card-image waves-effect waves-block waves-light">
				<img class="activator" src="<?php  echo base_url('assets/img/portfolio/portfolio-5.jpg');?>" alt="image">
			</div>
			<div class="card-content">
				<span class="card-title activator">Plan Title <i class="fa fa-ellipsis-v right"></i> <i class="fa fa-heart-o right"></i></span>
				<p><a href="javascript:void(0)">Leadership
</a></p>
			</div>
			<div class="card-reveal">
				<span class="card-title">Plan Title <i class="material-icons right">&#xE5CD;</i></span>
				<p><a href="javascript:void(0)">Leadership
</a></p>
				<p>Authoritatively grow quality technologies for strategic sources.</p>
				<a href="javascript:void(0)" class="readmore">View full Plan</a>
			</div>
		</div>
	</div>
	<div class="portfolio-item" data-groups='["all", "marketing"]'>
		<div class="card">
			<div class="card-image waves-effect waves-block waves-light">
				<img class="activator" src="<?php  echo base_url('assets/img/portfolio/portfolio-6.jpg');?>" alt="image">
			</div>
			<div class="card-content">
				<span class="card-title activator">Plan Title <i class="fa fa-ellipsis-v right"></i> <i class="fa fa-heart-o right"></i></span>
				<p><a href="javascript:void(0)">Marriage
</a></p>
			</div>
			<div class="card-reveal">
				<span class="card-title">Plan Title <i class="material-icons right">&#xE5CD;</i></span>
				<p><a href="javascript:void(0)">Marriage
</a></p>
				<p>Authoritatively grow quality technologies for strategic sources.</p>
				<a href="javascript:void(0)" class="readmore">View full Plan</a>
			</div>
		</div>
	</div>
	<div class="portfolio-item" data-groups='["all", "marketing", "photography"]'>
		<div class="card">
			<div class="card-image waves-effect waves-block waves-light">
				<img class="activator" src="<?php  echo base_url('assets/img/portfolio/portfolio-7.jpg');?>" alt="image">
			</div>
			<div class="card-content">
				<span class="card-title activator">Plan Title <i class="fa fa-ellipsis-v right"></i> <i class="fa fa-heart-o right"></i></span>
				<p><a href="javascript:void(0)">Prayer
</a></p>
			</div>
			<div class="card-reveal">
				<span class="card-title">Plan Title <i class="material-icons right">&#xE5CD;</i></span>
				<p><a href="javascript:void(0)">Prayer
</a></p>
				<p>Authoritatively grow quality technologies for strategic sources.</p>
				<a href="javascript:void(0)" class="readmore">View full Plan</a>
			</div>
		</div>
	</div>
	<div class="portfolio-item" data-groups='["all", "websites"]'>
		<div class="card">
			<div class="card-image waves-effect waves-block waves-light">
				<img class="activator" src="<?php  echo base_url('assets/img/portfolio/portfolio-8.jpg');?>" alt="image">
			</div>
			<div class="card-content">
				<span class="card-title activator">Plan Title <i class="fa fa-ellipsis-v right"></i> <i class="fa fa-heart-o right"></i></span>
				<p><a href="javascript:void(0)">Forgiveness</a></p>
			</div>
			<div class="card-reveal">
				<span class="card-title">Plan Title <i class="material-icons right">&#xE5CD;</i></span>
				<p><a href="javascript:void(0)">Forgiveness</a></p>
				<p>Authoritatively grow quality technologies for strategic sources.</p>
				<a href="javascript:void(0)" class="readmore">View full Plan</a>
			</div>
		</div>
		
	</div>
</div>
<div class="load-more-button text-center">
	<a class="waves-effect waves-light btn btn-large pink"> <i class="fa fa-spinner left"></i> Load More</a>
</div>
</div>
</div>
</section>

<?php include "footer.php";?>