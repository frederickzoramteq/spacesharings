<?php //include "header.php";?>
<style type="text/css">
input[type=file]{
      display: inline;
    }
    #image_preview{
      
      padding: 10px;
    }
    #image_preview img{
      width: 200px;
      padding: 5px;
    }
</style>
<div class="property_view_wrapper">
   <div class="container">
      <style>
         .error
         {
         font-size: 16px;
         color: red;
         }
        
      </style>
      <div class="row">
         <div class="col-lg-12 col-md-12">
            <div class="row">
               <div class="col-lg-4 col-md-4">
                  </br>
                  </br>
                  </br>
                  </br>
                  <img src="<?php echo base_url('assets/images/faq2.png');?>" >
               </div>
               <div class="col-lg-8 col-md-6">
                <p class="topcaption">Use this form to provide details about the rental you have to offer. Provide some information about yourself. Also specify the type of renter or guest you are looking for.  This information will help us to match you up with the ideal renter.</p>
                  <div class="sidebar_wrapper">
                     <div id="amsg">
                     </div>
                     <div class="widget advance_search_wrapper">
                        <div class="widget-title">
                           <h4>Rental Advanced-Form</h4>
                        </div>
                        <div class="search_form">
                           <form id="advfrom" action="<?php echo base_url();?>Advhosting/add_hosting" method="POST" enctype="multipart/form-data">
                              <?php
                                 echo $this->session->flashdata('add_message');
                                 echo $this->session->flashdata('success_msg');
                                 ?>
                              <h3>Contact Information</h3>
                              <!-- Cionatct Information starts -->
                              <div class="row">
                                 <div class="col-lg-6 col-md-6 form-group">
                                    <!--  <label>First Name* </label> -->
                                    <input type="text" name="fname"  maxlength="50" placeholder="First Name*" value="<?php echo set_value('fname'); ?>">
                                    <?php echo form_error('fname'); ?>
                                 </div>
                                 <div class="col-lg-6 col-md-6 form-group">
                                    <!-- <label>Last Name* </label> -->
                                    <input type="text" name="lname"  maxlength="50" placeholder="Last Name*" value="<?php echo set_value('lname'); ?>">
                                    <?php echo form_error('lname'); ?>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-6 col-md-6 form-group">
                                    <!-- <label>Phone* </label> -->
                                    <input type="text" name="phone" placeholder="Phone*" value="<?php echo set_value('phone'); ?>" >
                                     <?php echo form_error('phone'); ?>
                                 </div>
                                 <div class="col-lg-6 col-md-6 form-group">
                                    <!-- <label>Email* </label> -->
                                    <input type="email" name="email" placeholder="Email*" id="afemail" value="<?php echo set_value('email'); ?>">
                                    <?php echo form_error('email'); ?>
                                 </div>
                              </div>
                              <h3>Host's Information</h3>
                              <div class="row">
                                 <div class="col-lg-6 col-md-6 form-group">
                                    <select name="nationality" class="orderby form-control select2 select2-hidden-accessible" id="afnationality" tabindex="-1" aria-hidden="true">
                                       <option value="" selected="selected">-Select Host's Nationality-</option>
                                       <?php foreach ($country as $value) {
                                 ?>
                                 <option value='<?= $value->count_name;?>' >
                                    <?= $value->count_name;?>

                                 </option>
                                 <?php   
                              }
                              ?>
                                       <!-- <option value="El Salvador">El Salvador </option>
                                       <option value="Honduras">Honduras</option>
                                       <option value="Guatemala">Guatemala</option>
                                       <option value="India">India </option>
                                       <option value="Ecuador">Ecuador</option> -->
                                    </select>
                                 </div>
                                 <div class="col-md-6">
                                    <select id="afmothertongue" name="mother_tongue" class="orderby form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                       <option value="Any" selected="selected" value="">-Select Hosts Mother Tongue-</option>
                                       <option value="English">English</option>
                                       <option value="Spanish">Spanish</option>
                                       <option value="Portuguese">Portuguese</option>
                                       <option value="French">French</option>
                                       <option value="Chinese(Mandarin)">Chinese(Mandarin)</option>
                                       <option value="Chinese(Traditional)">Chinese(Traditional)</option>
                                       <option value="Japanese">Japanese</option>
                                       <option value="Hindi">Hindi</option>
                                       <option value="Bengali">Bengali</option>
                                       <option value="Telegu">Telegu</option>
                                       <option value="Tamil">Tamil</option>
                                       <option value="Marathi">Marathi</option>
                                       <option value="Punjabi">Punjabi</option>
                                       <option value="Russian">Russian</option>
                                       <option value="Arabic">Arabic</option>
                                       <option value="German">German</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-6 col-md-6 form-group">
                                    <select name="agegroup" id="afagegroup" class="orderby form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                       <option value="Any" selected="selected">-Select Host's Age group-</option>
                                       <option value="16-20">16-20</option>
                                       <option value="21-30">21-30</option>
                                       <option value="31-40">31-40</option>
                                       <option value="41-60">41-60</option>
                                       <option value="60+">60+</option>
                                       <option value="Any of the above">Any of the above</option>
                                    </select>
                                 </div>
                                 <div class="col-lg-6 col-md-6 form-group">
                                    <select name="renter_ethnicity" id="afethnity" class="orderby form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                       <option value="Any" selected="selected">-Select Host's Ethnicity-</option>
                                       <option value="Caucasian or White">Caucasian or White</option>
                                       <option value="African or Black">African or Black</option>
                                       <option value="East Asian">East Asian</option>
                                       <option value="Middle Eastern">Middle Eastern</option>
                                       <option value="Latino">Latino</option>
                                       <option value="Chinese">Chinese</option>
                                       <option value="Spanish">Spanish </option>
                                       <option value="Indian">Indian </option>
                                    </select>
                                    <span class="error" style="color:red" id="error_message_afethnity"></span>
                                 </div>
                              </div>
                              <div class="">
                                 <div class="">
                                    <label style="font-weight: bold">Host's Gender or Type :</label>
                                    </div>
                                    <div>
                                    
                                    	<label  class="radio-inline">
                                        <input type="radio" name="hostgender" checked="" value="Male" class="radio"> Male
                                        </label>
                                    
                                   
                                    	<label  class="radio-inline">
                                        <input type="radio" name="hostgender" value="Female" class="radio"> Female
                                    	</label>
                                    
                                    	<label  class="radio-inline">
                                        <input type="radio" name="hostgender" value="Couple" class="radio"> Couple
                                    	</label>
                                    
                                    <!-- <br/> -->
                                    <!-- <input type="radio" name="afhostgender" value="Family" class="radio" style="    margin-left: 82px;"> Family -->
                                   
                                    	<label  class="radio-inline">
                                        <input type="radio" name="hostgender" value="Family" class="radio"> Family
                                    </label>
                                    
                                    	<label  class="radio-inline">
                                        <input type="radio" name="hostgender" value="Partners" class="radio"> Partners
                                        </label>
                                    
                                    	<label  class="radio-inline">
                                        <input type="radio" name="hostgender" value="Others" class="radio"> Other
                                    	</label>
                                   
                                    <span class="error" style="color:red" id="error_message_afhostgender"></span>
                                 </div>
                              </div>
                              <h3>Listing Information</h3>
                              <label>Property Location</label>
                              <!-- Property Location sub heading starts -->
                              
                              <div class="row">
                                 <div class="col-lg-6 col-md-6 form-group">
                                    <label>Country </label>
                                    <select name="country" id="country" class="form-group">
                                       <!-- <option value="menu_order" selected="selected">Country</option> -->
                                       <!-- <?php foreach ($country as $value) {
                                          ?>
                                          <option value='<?= $value->count_id;?>' >
                                            <?= $value->count_name;?>
                                          
                                          </option>
                                          <?php
                                             }
                                             ?> -->
                                       <option value="231">United States</option>
                                       <option value="38">Canada</option>
                                       <option value="44">China</option>
                                       <option value="101">India</option>
                                       <option value="109">Japan</option>
                                       <option value="230">United Kingdom </option>
                                    </select>
                                 </div>
                                 <div class="col-lg-6 col-md-6 form-group">
                                    <label>State </label>
                                    <select name="state" id="state" class="form-group">
                                    <option value="0" selected="selected">Select State</option>
                                    </select>
                                 </div>
                                 <div class="col-lg-6 col-md-6 form-group">
                                    <label>City </label>
                                    <select name="city" id="city" class="form-group">
                                    <option value="0" selected="selected">Select City</option>
                                    </select>
                                 </div>
                                 <div class="col-lg-6 col-md-6 form-group">
                                    <label>Zipcode*</label>
                                    <input type="text" class="form-group" placeholder="Zipcode" id="postcode" name="postcode" value="<?php echo set_value('postcode'); ?>" >
                                    <?php echo form_error('postcode'); ?>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12 col-md-12 form-group">
                                    <label>Address* </label>
                                    <input type="text" name="address" id="afaddress" placeholder="Enter the Street Address" value="<?php echo set_value('address'); ?>" >
                                     <?php echo form_error('address'); ?>
                                 </div>
                              </div>
                             <!--  <h3>Personal Information</h3> -->
                              
                              <!-- Property Location sub heading starts -->
                             <!--  <div class="row">
                                <div class="col-lg-6 col-md-6 form-group">
                                   <label>Laguage</label>
                                   <select name="lang" id="lang" class="form-group">
                                      <option value="" name="lang">Language</option>
                                      <?php
                                         foreach ($languages as $lang) {
                                           ?>
                                      <option value="<?= $lang->id ?>" name="<?= $lang->name ?>"><?= $lang->name; ?></option>
                                      <?php
                                         }
                                         ?>
                                   </select>
                                </div>
                                <div class="col-lg-6 col-md-6 form-group">
                                    <label>Category</label>
                                   <select name="state" id="state" class="form-group">
                                      <option value="" name="category">Category</option>
                                      <option value="single" name="single">Single</option>
                                      <option value="couple" name="couple">Couple</option>
                                      <option value="family" name="family">Family</option>
                                      <option value="single parent" name="single parent">Single Parent</option>
                                      <option value="others" name="others">Others</option>
                                   </select>
                                </div>
                             </div> -->
                              <div class="subheading">
                                 <h3>Rental Details (Provide information about the rental you are hosting)</h3>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12 col-md-12 form-group">
                                    <label>Rental Title* </label>
                                    <input type="text" name="title" placeholder="*Rental Title" id="aftitle"  value="<?php echo set_value('title'); ?>">
                                    <?php echo form_error('title'); ?>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12 col-md-12 form-group">
                                    <label>Rental Description* </label>
                                    <textarea name="description" id="afdesc"  placeholder="*Rental Description"><?php echo set_value('description'); ?></textarea>
                                    <?php echo form_error('description'); ?>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="exampleInputFile">Upload Images</label>
                                 <!-- <input type="file" name="userfile" id="featured1" size="50" multiple="multiple">
                                 <?php echo form_error('userfile'); ?> -->
                                 <input type="file" id="uploadFile" name="uploadFile[]" multiple/>

      <input type="submit" class="btn btn-success" name='submitImage' value="Upload Image" style="opacity: 0; display: none;"/>
      <div class="clearfix"></div>

  <div id="image_preview"></div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="exampleInputFile">Duration Available </label>
                                       <select name="avl_dur" class="orderby form-control select2 select2-hidden-accessible" id="afarea" tabindex="-1" aria-hidden="true">
                                          <option value="0-100">1+ days</option>
                                          <option value="101-200">1+ week</option>
                                          <option value="201-300">1/2 to 1 month</option>
                                          <option value="301-500">1+ months</option>
                                          <option value="501-800">1/2 year</option>
                                          <option value="801-1000">1+ years</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="exampleInputFile">Available From</label>
                                       <input type="date" name="available_from">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-4 col-md-4 form-group">
                                    <label>Area (in Sq Foot) </label>
                                    <select name="area" class="orderby form-control select2 select2-hidden-accessible" id="afarea" tabindex="-1" aria-hidden="true">
                                       <option value="0 - 100">0-100</option>
                                       <option value="101 - 200">101-200</option>
                                       <option value="201 - 300">201-300</option>
                                       <option value="301-500">301-500</option>
                                       <option value="501 - 800">501-800</option>
                                       <option value="801 - 1000">801-1000</option>
                                       <option value="1001 - 1200">1001-1200</option>
                                       <option value="1201 - 1600">1201-1600</option>
                                       <option value="1601 - 2400">1601-2400</option>
                                       <option value="2401+">2401+</option>
                                    </select>
                                    <span class="error" style="color:red" id="error_message_afarea"></span>
                                 </div>
                                 <div class="col-lg-4 col-md-4 form-group">
                                    <label>Bedrooms</label>
                                    <select name="beds" class="orderby form-control select2 select2-hidden-accessible" id="afbeds" tabindex="-1" aria-hidden="true">
                                       <option value="1">1</option>
                                       <option value="2">2</option>
                                       <option value="3">3</option>
                                       <option value="4">4</option>
                                       <option value="5">5</option>
                                       <option value="6+">6+</option>
                                    </select>
                                 </div>
                                 <div class="col-lg-4 col-md-4 form-group">
                                    <label>Bathrooms</label>
                                    <select name="bathroom" class="orderby form-control select2 select2-hidden-accessible" id="afbath" tabindex="-1" aria-hidden="true">
                                       <option value="1">1</option>
                                       <option value="2">2</option>
                                       <option value="3">3</option>
                                       <option value="4">4</option>
                                       <option value="5">5</option>
                                       <option value="6+">6+</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="">
                                 <div class="" style="font-size: 12px;">
                                    <label style="font-weight: bold">Food Services Available :</label>
                                 </div>
                                 <div class="food_munna">
                                 	
                                    <label class="checkbox-inline" >
                                    <input type="checkbox" name="additional_services[]" value="Breakfast" class="checkbox-inline"> Breakfast
                                    </label>
                                   
                                    <label class="checkbox-inline" >
                                    <input type="checkbox" name="additional_services[]" value="Dinner" class="checkbox-inline"> Dinner
                                    </label>
                                    
                                    <label class="checkbox-inline" >
                                    <input type="checkbox" name="additional_services[]" value="Snacks" class="checkbox-inline"> Snacks
                                    </label>
                                    
                                    <label class="checkbox-inline" >
                                    <input type="checkbox" name="additional_services[]" value="Cooking Facility" class="checkbox-inline"> Cooking Facility
                                    </label>
                                   
                                    <label class="checkbox-inline" >
                                    <input type="checkbox" name="additional_services[]" value="Other Meals" class="checkbox-inline"> Other Meals
                                    </label>
                                    
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-12"><label style="font-weight: bold">Access To :</label></div>
                                 <div class="col-lg-4 col-md-4 form-group">
                                    
                                    <div class="radio">
                                    <label class="name" style="font-weight: bold">Kitchen :</label>
                                    <label >
                                    <input type="radio" name="kitchen" value="yes" class="radio" checked=""> Yes
                                    </label>
                                    
                                    <label >
                                    <input type="radio" name="kitchen" value="no" class="radio"> No
                                    </label>
                                    </div>
                                 </div>
                                 <div class="col-lg-4 col-md-4 form-group">
                                    <div class="radio">
                                    <label class="name" style="font-weight: bold">Laundry :</label>
                                    <label >
                                    <input type="radio" name="laundry_service" value="yes" class="radio" checked=""> Yes
                                    </label>
                                    
                                    <label >
                                    <input type="radio" name="laundry_service" value="no" class="radio"> No
                                    </label>
                                    </div>
                                 </div>
                                 <div class="col-lg-4 col-md-4 form-group">
                                    <div class="radio">
                                    <label class="name" style="font-weight: bold">Living Room :</label>
                                    <label >
                                    <input type="radio" name="living_room" value="yes" class="radio" checked=""> Yes
                                    </label>
                                    
                                    <label>
                                    <input type="radio" name="living_room" value="no" class="radio"> No
                                    </label>
                                    </div>
                                 </div>
                                 <div class="col-lg-4 col-md-4 form-group">
                                    <div class="radio">
                                    <label class="name" style="font-weight: bold">Pantry :</label>
                                    <label >
                                    <input type="radio" name="Pantry" value="yes" class="radio" checked=""> Yes
                                    </label>
                                    
                                    <label >
                                    <input type="radio" name="Pantry" value="no" class="radio"> No
                                    </label>
                                    </div>
                                 </div>
                                 <div class="col-lg-4 col-md-4 form-group">
                                    <div class="radio">
                                    <label class="name" style="font-weight: bold">Pool :</label>
                                    <label>
                                    <input type="radio" name="Pool" value="yes" class="radio" checked=""> Yes
                                    </label>
                                   
                                    <label>
                                    <input type="radio" name="Pool" value="no" class="radio">
                                    No
                                    </label>
                                    </div>
                                 </div>
                                 <div class="col-lg-4 col-md-4 form-group">
                                    <div class="radio">
                                    <label class="name" style="font-weight: bold">Backyard :</label>
                                    <label >
                                    <input type="radio" name="Backyard" value="yes" class="radio" checked=""> Yes
                                    </label>
                                   
                                    <label >
                                    <input type="radio" name="Backyard" value="no" class="radio">
                                    No
                                    </label>
                                    </div>
                                 </div>
                              </div>
                              <!-- Preferred Renter Criteria sub heading starts -->
                              <div class="subheading">
                                 <h3>Preferred Renter Criteria  (The type of guest you would like to host)</h3>
                              </div>
                              <div class="row">
                                 <div class="col-lg-6 col-md-6 form-group">
                                    <label for="exampleInputFile" style="width:100%; display:block;">Renter Ethnicity </label>
                                    <!--<select name="renter_ethnicity" class="orderby form-control ">
                                       <option value="" selected="selected">-Select Renter Ethnicity-</option>-->
                                     <select id="example-getting-started" multiple="multiple">
                                       <option value="Caucasian or White">Caucasian or White</option>
                                       <option value="African or Black">African or Black</option>
                                       <option value="East Asian">East Asian</option>
                                       <option value="Middle Eastern">Middle Eastern</option>
                                       <option value="Latino">Latino</option>
                                       <option value="Chinese">Chinese</option>
                                       <option value="Spanish ">Spanish </option>
                                       <option value="Indian ">Indian </option>
                                    </select> 

                                 </div>
                                 <div class="col-lg-6 col-md-6 form-group">
                                    <label for="exampleInputFile" style="width:100%; display:block;">
                                  Renter Age group </label>
                                    <select id="example-getting-started1" multiple="multiple" name="renter_age_group">
                                      
                                       <option value="16-20" selected="">16-20</option>
                                       <option value="21-30">21-30</option>
                                       <option value="31-40">31-40</option>
                                       <option value="41-60">41-60</option>
                                       <option value="60+">60+</option>
                                       <option value="Any">Any</option>
                                    </select>
                                    <span class="error" style="color:red" id="error_message_afpref_adgegroup"></span>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12 col-md-12 form-group renters">
                                    <label class="clearfix" style="font-weight: bold">Renters Gender :</label>
                                    
                                    <label class="radio-inline">
                                    <input type="radio" checked="" name="rent_gender" value="Male" class="radio"> Male
                                    </label>
                                    
                                    <label class="radio-inline">
                                    <input type="radio" name="rent_gender" value="Female" class="radio"> Female
                                    </label>
                                    
                                    <label class="radio-inline">
                                    <input type="radio" name="rent_gender" value="Others" class="radio"> Other
                                    </label>

                                    <label class="radio-inline">
                                    <input type="radio" name="rent_gender" value="Any" class="radio"> Any (Male or Female)
                                    </label>
                                    
                                    
                                    
                                    <br>
                                    <span class="error" style="color:red" id="error_message_afpref_gender"></span>
                                 </div>
                              </div>
                              <div class="row">
                                 <!--     <div class="col-lg-12 col-md-12 form-group" style="font-size: 12px;">
                                    <label style="font-weight: bold">Food Services Available :</label><br>
                                    <input type="checkbox" name="additional_services[]" value="Breakfast" class="checkbox-inline"> Breakfast
                                    <input type="checkbox" name="additional_services[]" value="Dinner" class="checkbox-inline"> Dinner
                                    <input type="checkbox" name="additional_services[]" value="Snacks" class="checkbox-inline"> Snacks
                                    <input type="checkbox" name="additional_services[]" value="Cooking Facility" class="checkbox-inline"> Cooking Facility
                                    <input type="checkbox" name="additional_services[]" value="Other Meals" class="checkbox-inline"> Other Meals
                                    </div> -->
                                 <div class="col-lg-12 col-md-12 form-group" style="font-size: 12px;">
                                    <label class="clearfix" style="font-weight: bold">Renter's Category :</label>
                                    
                                    <label class="checkbox-inline">
                                    <input type="checkbox" name="renter_type[]" value="Student" class="checkbox-inline"> Student
                                    </label>
                                   
                                    <label class="checkbox-inline">
                                    <input type="checkbox" name="renter_type[]" value="Working Single" class="checkbox-inline"> Working Single
                                    </label>
                                    
                                    <label class="checkbox-inline">
                                    <input type="checkbox" name="renter_type[]" value="Couple" class="checkbox-inline"> Couple
                                    </label>
                                   
                                    <label class="checkbox-inline">
                                    <input type="checkbox" name="renter_type[]" value="Family with Children" class="checkbox-inline"> Family with Children
                                    </label>
                                   
                                    <label class="checkbox-inline">
                                    <input type="checkbox" name="renter_type[]" value="Single Parents with Kids" class="checkbox-inline1"> Single Parents with Kids
                                    </label>
                                   
                                    <label class="checkbox-inline">
                                    <input type="checkbox" name="renter_type[]" value="Retiree" class="checkbox-inline1"> Retiree
                                    </label>
                                    
                                    <label class="checkbox-inline">
                                    <input type="checkbox" name="renter_type[]" value="Others" class="checkbox-inline1"> Others
                                    </label>
                                   
                                 </div>
                              </div>
                              <input type="hidden" id="post_nonce_field" name="post_nonce_field" value="a58a590abb"><input type="hidden" name="_wp_http_referer" value="/advanced-form/">
                              <input type="hidden" name="advance_submitted" id="afsubmitted" value="true">
                              <div class="btn_wrapper">
                                 <input type="submit" id="advanced-form" value="Submit" class="btn rs_btn">
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php //include "footer.php";?>
