<?php
error_reporting(0);
$this->load->model('Generalmodel'); ?>
<div class="property_view_wrapper">
	<div class="container">
		<div class="row">
        	<div class="col-lg-3 col-md-4">
            </div>
			<div class="col-lg-9 col-md-8">
				<p class="topcont">Use the search form on the left to look for available rentals by location. Advanced user may also specify your criteria of the ideal host (the one who you will be most comfortable with). The right or the detail area shows the outcome of the search.  Browse the results and click on a result for more detail.</p>
			</div>
        	<div class="col-lg-3 col-md-4">
				<div class="sidebar_wrapper">
					<div class="widget advance_search_wrapper">		
						<div class="widget-title"><h4>Advance Search</h4></div>
						<div class="search_form">
							<form id="" method="POST" action='<?php echo base_url();?>Page/adv_search_results'>
								<!-- <div class="form-group">
									<label>search by keyword</label>
									<input type="text" name="search_key" id='search_key'class="form-control" placeholder="enter your keywords">
								</div> -->
								<p style="font-size:16px;text-decoration: underline; font-weight: bold;">Specify Preferred Location</p>
								<div class="form-group">
									<label>Country</label>

									<select name="country" id="country" class="orderby form-control select2">
										<option value="menu_order" selected="selected">Country</option>
										<!-- <?php foreach ($country as $value) {
											?>
											<option value='<?= $value->count_id;?>' >
												<?= $value->count_name;?>

											</option>
											<?php   
										}
										?> -->
										<option value="231">United States</option>
										<option value="38">Canada</option>
										<option value="44">China</option>
										<option value="101">India</option>
										<option value="109">Japan</option>
										<option value="230">United Kingdom </option>

									</select>
								</div>
								<div class="form-group">
									<label>State</label>
									<select name="state" id="state" class="orderby form-control select2">
										<option value="0" selected="selected">Select State</option>
									</select>
								</div>
								<div class="form-group">
									<label>City</label>
									<select name="city" id="city" class="orderby form-control select2">
										<option value="0" selected="selected">Select City</option>
									</select>
								</div>
								<div class="form-group">
									<label>Zipcode</label>
									<input type="zipcode" name="zipcode" class="form-control" >
								</div>
								<p style="font-size:16px;text-decoration: underline; font-weight: bold;">Specify Host’s Criteria</p>
								<div class="form-group">
									<label>Gender </label>
									<select name="gender" id="category_list" class="orderby form-control select2">
										<option value="Any" selected="selected">Any</option>
										<option value="Male">Male</option>
										<option value="Female">Female</option>
										<option value="Others">Others</option>
									</select>
								</div>
								<div class="form-group">
									<label>Age Group </label>
									<select name="age_group" id="category_list" class="orderby form-control select2">
										<option value="Any" selected="selected">Any</option>
										<option value="16–20">16–20</option>
										<option value="21-30">21-30</option>
										<option value="31-40">31-40</option>
										<option value="41–60">41–60</option>
										<option value="60+">60+</option>
										<option value="Any">Any</option>
									</select>
								</div>
								<div class="form-group">
									<label>Nationality </label>
									<select name="nationality" id="category_list" class="orderby form-control select2">
										<option value="Any" selected="selected">Any</option>
										 <?php foreach ($country as $value) {
											?>
											<option value='<?= $value->count_name;?>' >
												<?= $value->count_name;?>

											</option>
											<?php   
										}
										?>
										<!-- <option value="El Salvador">El Salvador </option>
										<option value="Honduras">Honduras</option>
										<option value="Guatemala">Guatemala</option>
										<option value="India">India </option>
										<option value="Ecuador">Ecuador</option> -->
									</select>
								</div>
								<div class="form-group">
									<label>Ethnicity </label>
									<select name="ethinicity" id="category_list" class="orderby form-control select2">
										<option value="Any" selected="selected">Any</option>
										<option value="Caucasian or White">Caucasian or White</option>
										<option value="African or Black">African or Black</option>
										<option value="East Asian">East Asian</option>
										<option value="Middle Eastern">Middle Eastern</option>
										<option value="Latino">Latino</option>
										<option value="Chinese">Chinese</option>
										<option value="Spanish">Spanish </option>
										<option value="Indian">Indian </option>
										<option value="Japanese">Japanese</option>
									</select>
								</div>
								<div class="form-group">
									<label>Status Or Category </label>
									<select name="category_list" id="category_list" class="orderby form-control select2">
										<option value="Any" selected="selected">Any</option>
										<option value="student">Student</option>
										<option value="workingsingle">Working Single</option>
										<option value="familywithchildren">Family with Children</option>
										<option value="retiree">Retiree</option>
										<option value="single">Single</option>
										<option value="Couple">Couple</option>
										<option value="family">family</option>
										<option value="single Parent">Single Parents with Kids</option>
										<option value="Others">others</option>
									</select>
								</div>

								<div class="form-group">
									<label>Speaks Languages</label>
									<select name="languages" class="orderby form-control select2" id="languages">
										<option value="Any" selected="selected">Any</option>
										<option value="English">English</option>
										<option value="Spanish">Spanish</option>
										<option value="Portuguese">Portuguese</option>
										<option value="French">French</option>
										<option value="Chinese(Mandarin)">Chinese(Mandarin)</option>
										<option value="Chinese(Traditional)">Chinese(Traditional)</option>
										<option value="Japanese">Japanese</option>
										<option value="Hindi">Hindi</option>
										<option value="Bengali">Bengali</option>
										<option value="Telegu">Telegu</option>
										<option value="Tamil">Tamil</option>
										<option value="Marathi">Marathi</option>
										<option value="Punjabi">Punjabi</option>
										<option value="Russian">Russian</option>
										<option value="Arabic">Arabic</option>
										<option value="German">German</option>
									</select>
								</div>
										<!-- <div class="form-group">
											<label>Status Or Category </label>
											<select name="category_list" id="category_list" class="orderby form-control select2">
												<option value="menu_order" selected="selected">Any</option>
												<option value="single">Single</option>
												<option value="Couple">Couple</option>
												<option value="family">family</option>
												<option value="single Parent">Single parents</option>
												<option value="Others">others</option>
											</select>
										</div> -->

										<div class="btn_wrapper" style="margin-top:10px;">
											<input type="submit" id="submit" class="btn rs_btn2" value="search"></div>
										</form>
									</div>
								</div>
				</div>
			</div>
			<div class="col-lg-9 col-md-8">
				<div class="row">
					<div class="col-lg-12">
						<div class="rs_heading_wrapper">
							<div class="rs_heading">
								<div class="col-lg-10 col-md-10 col-sm-10 padding">
									<h3 class="property_view_h">PROPERTY LISTINGS</h3>
									<p class="property_view_p">Total <?php echo $total;?> results found.</p>

								</div>
								<div class="col-lg-2 col-md-2s col-sm-2 padding">
									<ul class="view_button">
										<li><a href="javascript:void(0)" id="grid" class="active"><i class="flaticon-keyboard50"></i></a></li>
										<li><a href="javascript:void(0)" id="list"><i class="flaticon-list95"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="grid_view animated fadeIn">
    					<?php
    						foreach ($hosting_list as $row)
    						{
    						  $hostid= $row->id;
    				          $imgdet=$this->Generalmodel->fetch_single('hosting_image',$hostid);
    					?>
    						  <div class="col-lg-4 col-md-6 col-sm-4 col-xs-6">
    							<div class="property_wrapper" style="min-height:481px;">
    								<div class="property_img_wrapper"> 
        								<div class="property1">
        									<a href="<?php echo base_url();?>Page/Show_detail/<?php echo $row->id;?>">
                    							<?php
                    						      if(isset($imgdet) && $imgdet->image_name!='')
                    							  {
                    							?>
                    							  	<img  src="<?php echo base_url();?>uploads/blog/<?php echo $imgdet->image_name;?>" alt="">
                    							<?php 
                    							  }
                    							  else
                    							  {
                    							?>
                    							   <img  src="<?php echo base_url();?>uploads/blog/noimg.png" alt="" >
                    							<?php 
                    							  }
                    							?>
        									</a>
        								</div>
    								</div>
    								<div class="property_detail">
    									<div class="property_content">
    										<h5><a href="<?php echo base_url();?>Page/Show_detail/<?php echo $row->id;?>"><?php echo $row->title;?></a></h5>
    										<p><?php echo substr(stripslashes(strip_tags( $row->description)),0,100);?>....</p>
    									</div>
    									<ul style="min-height:90px;">
    										<li><i class="flaticon-drawing18"></i><?php echo $row->area;?>SQFT</li>
    										<li><i class="flaticon-bathtub3"></i><?php echo $row->bathroom ;?></li>
    										<li><i class="flaticon-bed40"></i><?php echo $row->bedroom ;?></li>
    										<li><i class="flaticon-car211"></i><?php echo $row->backyard ;?></li>
    									</ul>
    								</div>
    							</div>
    						</div>
						<?php } ?> 
					</div>
					<div class="list_view animated fadeIn">
						<?php
						  foreach ($hosting_list as $row)
						  {
							$hostid= $row->id;
		                    $imgdet=$this->Generalmodel->fetch_single('hosting_image',$hostid);
					    ?>
							<div class="property_wrapper">
								<div class="col-lg-5 col-md-5 col-sm-5"> 
									<div class="property_img_wrapper"> 
    									<div class="property1">
                							<a href="<?php echo base_url();?>Page/Show_detail/<?php echo $row->id;?>">
                            					<?php
                								   if(isset($imgdet) && $imgdet->image_name!='')
                								   {
                                				?>
                                				   	 <img  src="<?php echo base_url();?>uploads/blog/<?php echo $imgdet->image_name;?>" alt="">
                                				<?php 
                								   }
                								   else 
                								   {
                                				?>
                                					 <img  src="<?php echo base_url();?>uploads/blog/noimg.png" alt="" >
                                				<?php 
                                				   }
                                				?>
                							</a>
                						</div>
            						</div>
								</div>
    							<div class="col-lg-7 col-md-7 col-sm-7">
    								<div class="property_detail">
    									<div class="property_content">
    										<h5><a href="<?php echo base_url();?>Page/Show_detail/<?php echo $row->id;?>"><?php echo $row->title;?></a></h5><span></span>
    										<p><?php echo substr(stripslashes(strip_tags( $row->description)),0,150);?>....</p>
    									</div>
    									<ul>
    										<li><i class="flaticon-drawing18"></i><?php echo $row->area;?>SQFT</li>
    										<li><i class="flaticon-bathtub3"></i><?php echo $row->bathroom ;?></li>
    										<li><i class="flaticon-bed40"></i><?php echo $row->bedroom ;?></li>
    										<li><i class="flaticon-car211"></i><?php echo $row->backyard ;?></li>
    									</ul>
    								</div>
    							</div>
							</div>
						<?php  } ?>
					</div>
					<div class="col-lg-12">
	<nav class="pagger_wrapper">
		<?php echo $this->pagination->create_links(); ?>
	</nav>
</div> 
				</div>
			</div>
		</div><!--row-->
	</div>
</div>
			
<?php //include "footer.php";?>