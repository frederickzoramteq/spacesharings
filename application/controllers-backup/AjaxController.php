<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AjaxController extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('AjaxModel','Ajax');
		
		
	}

	public function fetch_state()
	{
		$countryid = $this->input->post('data',TRUE);
		$state = $this->Ajax->fetch_state($countryid);
		$output = null;
		foreach ($state as $row)
		{
      		//here we build a dropdown item line for each query result
			$output .= "<option value='".$row->state_id."'>".$row->state_name."</option>";
		}
		echo $output;
	}
	public function fetch_city()
	{
		$stateid = $this->input->post('data',TRUE);
		$city = $this->Ajax->fetch_city($stateid);
		$output = null;
		foreach ($city as $row)
		{
      		//here we build a dropdown item line for each query result
			$output .= "<option value='".$row->city_id."'>".$row->city_name."</option>";
		}
		echo $output;
	}
}