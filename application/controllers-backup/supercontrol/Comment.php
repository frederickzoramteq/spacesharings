<?php
class Comment extends CI_Controller {
		//============Constructor to call Model====================
		function __construct() {
			parent::__construct();
			$this->load->library(array('form_validation','session'));
			if($this->session->userdata('is_logged_in')!=1){
			redirect('supercontrol/home', 'refresh');
			}
			$this->load->model('supercontrol/Comment_model');
			$this->load->library('image_lib');
			
				//****************************backtrace prevent*** START HERE*************************
			$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
            $this->output->set_header('Pragma: no-cache');
			
			//****************************backtrace prevent*** END HERE*************************
		}
		//============Constructor to call Model====================
		function index()
	{
		if($this->session->userdata('is_logged_in')){
			redirect('supercontrol/commentadd_view');
        }else{
        	$this->load->view('supercontrol/login');	
        }
		
		
	}

//======================Show Comment List **** START HERE========================
		function show_comment(){
		//Loading Database
			$this->load->database();
			//Calling Model
			$this->load->model('supercontrol/Comment_model');
			//Transfering data to Model
			$query = $this->Comment_model->show_comment();
			$data['ecms'] = $query;
			$data['title'] = "Comment List";
			$this->load->view('supercontrol/header',$data);
			$this->load->view('supercontrol/showcommentlist');
			$this->load->view('supercontrol/footer');
		
	}
//======================Show Comment List **** END HERE========================
//======================Status change **** START HERE========================
	function statuscomment ()
		{
			     $stat= $this->input->get('stat'); 
				 $id= $this->input->get('id');   
		$this->load->model('supercontrol/Comment_model');
		$this->Comment_model->updt($stat,$id);
		}

		//=====================DELETE NEWS====================

		
			function delete_comment() {
			$id = $this->uri->segment(4);
			$query = $this->Comment_model->delete_comment($id,$comment_image);
			$data['ecms'] = $query;
			$data['title'] = "Comment List";
			$this->session->set_flashdata('delete_message', 'Comment Deleted successfully!!!!');
			redirect('supercontrol/Comment/show_comment');
		}

		//=====================DELETE NEWS====================

		//====================MULTIPLE DELETE=================
		function delete_multiple(){
		$ids = ( explode( ',', $this->input->get_post('ids') ));
		$this->Comment_model->delete_mul($ids);
		$data['title'] = "Comment List";
		$this->session->set_flashdata('delete_message', 'Comment Deleted successfully!!!!');
		redirect('supercontrol/comment/show_comment');
		}
		//====================MULTIPLE DELETE=================
//======================Logout==========================
		public function Logout(){
        	$this->session->sess_destroy();
        	redirect('supercontrol/login');
    	}
//======================Logout==========================
}

?>