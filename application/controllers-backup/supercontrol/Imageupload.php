<?php
ob_start();
error_reporting(0);
class Imageupload extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
		$this->load->model('supercontrol/Product_model');
    }
    function index()
    {   
	    $this->load->helper('form');
    $data = array();
    $data['title'] = 'Multiple file upload';
        $this->load->view('supercontrol/product_img',$data);
    }
    function doupload() {


		$config['upload_path'] = "./uploads/product_img";
		$config['allowed_types'] = '*';
		$config['max_size'] = 0;
		$this->load->library('upload', $config);

		if (!$this->upload->do_upload("file"))
		{
			$response = $this->upload->display_errors();
		}
		else
		{
			$response = $this->upload->data();
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($response));


         $this->load->database();
		 $this->load->model('supercontrol/Product_model');
		 $this->load->helper('date');
		 

		
            $db_data = array(
							 'pid'=>$this->input->post('pid'),
							 'product_image'=> str_replace(" ","_", $_FILES['file']['name']),
							 'date' => date('Y-m-d H:i:s')
							 );
							 

        $this->db->insert('product_images',$db_data);  
           // print_r($names);
			//redirect('supercontrol/imageupload/product_img/'.$this->input->post('product_id').'/imgup');
			//redirect('index.php/imageupload/product_img/'.$this->input->post('product_id').'');
    }
	
	function product_img($id) {
			$this->load->model('supercontrol/Product_model');
			$query = $this->Product_model->show_product_multi_image($id); 
			$data['mulimg'] = $query;
			//echo $this->db->last_query();
			//exit();
			$data['title'] = "Edit product Image";
			$this->load->view('supercontrol/header',$data);
			$this->load->view('supercontrol/product_img',$data);
			$this->load->view('supercontrol/footer_img');
			
					
	}
	
		function product_img_ajax($id) {
			$query = $this->Product_model->show_product_multi_image($id); 
			$data['mulimg'] = $query;
			//echo $this->db->last_query();
		//exit();
			$this->load->view('supercontrol/product_img_ajax',$data);
			$data['title'] = "Edit product Image";
			
					
	}
	
	function delete_product_img($id){
			//Loading  Database
			$this->load->database();
			$this->load->model('supercontrol/Product_model'); 
			$id = $this->uri->segment(4);
			//echo $id; exit();
			$result=$this->Product_model->show_product_multi_image($id); 
			//print_r($result); exit();
			$product_image = $result[0]->product_image;
			$this->Product_model->delete_product_img($id,$product_image);
			//$data['emember'] = $query; 
			$this->session->set_flashdata('delete_message', 'Image Deleted successfully!!!!');
			redirect('supercontrol/imageupload/product_img/'.$this->uri->segment(5).'/imgdlt');
		}
	
	function show_images($id){
		$this->load->database();
			/* Calling Model */
			$$this->load->model('supercontrol/Product_model');
			/* Transfering data to Model */
			$query = $this->Product_model->show_multi_image();
			$data['eimg'] = $query;
			$this->load->view('supercontrol/header',$data);
			$this->load->view('supercontrol/show_membership_plan', $data);
			$data['title'] = "Membership Plan List";
			$this->load->view('supercontrol/footer_img');	
	}
	
	
	
	
}
?>