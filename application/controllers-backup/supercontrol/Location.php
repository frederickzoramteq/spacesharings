<?php
class Location extends CI_Controller {
		//============Constructor to call Model====================
		function __construct() {
			parent::__construct();
			//$this->load->model('categorymodel', 'cat');
			$this->load->helper('form');
			$this->load->library(array('form_validation','session'));
			if($this->session->userdata('is_logged_in')!=1){
			redirect('supercontrol/home', 'refresh');
			}
			$this->load->model('supercontrol/location_model','cat');
			$this->load->library('image_lib');
			/*function __construct() {
			parent::__construct();
			$this->load->model('categorymodel', 'cat');
			$this->load->helper('form');
			}*/

			//****************************backtrace prevent*** START HERE*************************
			$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
            $this->output->set_header('Pragma: no-cache');
			
			//****************************backtrace prevent*** END HERE*************************
		}
		//============Constructor to call Model====================
		function index()
	{
		if($this->session->userdata('is_logged_in')){
			$data['categories'] = $this->cat->category_menu();
			$this->load->view('supercontrol/header',$data);
			$this->load->view('supercontrol/locationadd_view', $data);
			$this->load->view('supercontrol/footer');
			//redirect('locationadd_view');
        }else{
        	$this->load->view('supercontrol/login');	
        }
		
		//$data['categories'] = $this->cat->category_menu();
			//$this->load->view('locationadd_view', $data);
	}
//======================Show Add form for location add **** START HERE========================	
function location_add_form(){
				$data['title'] = "Add Location";
				$this->load->view('supercontrol/header',$data);
				$this->load->view('supercontrol/locationadd_view');
				$this->load->view('supercontrol/footer');
     }
		
//====================== Show Add form for location add ****END HERE========================
		//=======================Insert Page Data============
		function add_location(){
					$data = array(
					'category_name' => $this->input->post('name'),
					'parent_id' => $this->input->post('pid'),
					);
					//print_r($data); exit();
					$this->load->model('supercontrol/location_model');
					$this->location_model->insert_location($data);
					$this->load->view('supercontrol/header',$data);
					//$this->load->view('location/success',$data);
					$data['categories'] = $this->cat->category_menu();
					$data['success_msg'] = '<div class="alert alert-success text-center">Data Added Successfully!</div>';
					$this->load->view('supercontrol/locationadd_view',$data);
					//redirect('location',$data);
					$this->load->view('supercontrol/footer');
			}
		//=======================Insert Page Data============
		
		

//=======================Insertion Success message for News *** START HERE=========
		function success(){
			$data['h1title'] = 'Add Location';
			
			$data['title'] = 'Add Location';
			$this->load->view('supercontrol/header');
			$this->load->view('supercontrol/locationadd_view',$data);
			$this->load->view('supercontrol/footer');
		}
//=======================Insertion Success message *** END HERE=========	
//======================Show News List **** START HERE========================
		function show_location(){
		//Loading Database
			$this->load->database();
			//Calling Model
			$this->load->model('supercontrol/location_model');
			$query = $this->location_model->show_location();
			 //echo $ddd=$this->db->last_query();
			 //exit();
			//========================================
			//****************************************
			/*$data['categorieslisting'] = $this->cat->category_listing();*/
			//========================================
			$data['eloca'] = $query;
			$data['title'] = "Location List";
			$this->load->view('supercontrol/header',$data);
			$this->load->view('supercontrol/showlocationlist',$data);
			$this->load->view('supercontrol/footer');
		
	}
//======================Show News List **** END HERE========================
//======================Status change **** START HERE========================
	function statusnews ()
		{
			     $stat= $this->input->get('stat'); 
				 $id= $this->input->get('id');   
		$this->load->model('news_model');
		$this->news_model->updt($stat,$id);
		}

//=======================Status change **** END HERE========================	
//================Show Individual by Id for Location *** START HERE=================
		function show_location_id($id) {
			 $id = $this->uri->segment(4); 
			//exit();
			$data['title'] = "Edit Location";
			//Loading Database
			$this->load->database();
			//Calling Model
			$this->load->model('supercontrol/location_model');
			//Transfering data to Model
			$query = $this->location_model->show_location_id($id);
			$data['elocation'] = $query;
			$this->load->view('supercontrol/header',$data);
			$this->load->view('supercontrol/location_edit', $data);
			$this->load->view('supercontrol/footer');
		}
		
//================Show Individual by Id for Location *** END HERE=================
//================Update Individual Location***** START HERE ====================
		function edit_location(){
				$datalist = array(			
					'category_name' => $this->input->post('category_name'),
				);
				$id = $this->input->post('category_id');
				$data['title'] = "Location Edit";
				//loading database
				$this->load->database();
				//Calling Model
				$this->load->model('supercontrol/location_model');
				//Transfering data to Model
				$query = $this->location_model->location_edit($id,$datalist);
				//echo $ddd=$this->db->last_query();
				//exit();
				$data1['message'] = 'Updated Successfully';
				$query = $this->location_model->show_locationlist();
				$data['eloca'] = $query;
				$data['title'] = "Location Page List";
				$this->load->view('supercontrol/header',$data);
				$this->load->view('supercontrol/showlocationlist', $data1);
				$this->load->view('supercontrol/footer');
		}
//================Update Individual  Location ***** END HERE====================

		//=====================DELETE LOCATION====================
			function delete_location() {
			$this->load->model('supercontrol/location_model');
			$id = $this->uri->segment(4);
			$result=$this->location_model->show_location_id($id);
			//Loading Database
			$this->load->database();
			if($query = $this->location_model->delete_location($id)){
				$data2['message'] = 'Deleted Successfully';
				$data['title'] = "Location Page List";
				$data['eloca'] = $query;
				$data['title'] = "Location Page List";
				$this->session->set_flashdata('success', 'Data Deleted !!!!');
				redirect('supercontrol/location/show_location',TRUE);
			}else{
       			$this->session->set_flashdata('success','You Can Not Delete A Parent Category');
				redirect('supercontrol/location/show_location',TRUE);
    		}
			
		}

		//=====================DELETE LOCATION====================

		//====================MULTIPLE DELETE=================
		function delete_multiple(){
		$ids = ( explode( ',', $this->input->get_post('ids') ));
		$this->news_model->delete_mul($ids);
		$this->load->view('header');
		redirect('news/show_news');
		$this->load->view('footer');
		}
		//====================MULTIPLE DELETE=================
//======================Logout==========================
		public function Logout(){
        	$this->session->sess_destroy();
        	redirect('supercontrol/login');
    	}
//======================Logout==========================
}

?>