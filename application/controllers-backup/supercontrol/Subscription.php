<?php
ob_start();
class Subscription extends CI_Controller {
		//============Constructor to call Model====================
		function __construct() {
			parent::__construct();
			$this->load->model('supercontrol/Generalmodel');
			$this->load->library('image_lib');
					//****************************backtrace prevent*** START HERE*************************
			$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
            $this->output->set_header('Pragma: no-cache');
			//****************************backtrace prevent*** END HERE*************************
		}
		//============Constructor to call Model====================
		function index(){
			if($this->session->userdata('is_logged_in')){
				redirect('supercontrol/subscription_add_view');
        	}else{
        		$this->load->view('supercontrol/login');	
        	}
		}
//======================Show Add form for blog add **** START HERE========================	
		function subscription_add_view(){
			$data['title'] = "Add subscription";
			$this->load->view('supercontrol/header',$data);
			$this->load->view('supercontrol/subscription_add_view');
			$this->load->view('supercontrol/footer');
			}
		//====================== Show Add form for blog add ****END HERE========================
		//=======================Insert Blog Data *** START HERE============
		function add_subscription(){
			//======================Modified========================
			$my_date = date("Y-m-d", time());
			//=====================+++++++++++++++++++++++===================
			$this->load->library('form_validation');
		    $this->form_validation->set_rules('subscription_title', 'Subscription Title','required');
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			//=====================+++++++++++++++++++++++===================
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('supercontrol/header');
				$data['success_msg'] = '<div class="alert alert-success text-center">Some Fields Can Not Be Blank</div>';
				$this->load->view('supercontrol/header');
				$this->load->view('supercontrol/subscription_add_view',$data);
				$this->load->view('supercontrol/footer');
			}else{
				$data = array(
					 'subscription_title' => $this->input->post('subscription_title'),
					 'amount' => $this->input->post('amount'),
					 'payment_type' => $this->input->post('payment_type'),
					 'status' =>  'Y'
				); 
				//print_r($data);
				//exit();
				$this->Generalmodel->show_data_id('subscription_plan','','','insert',$data);
				$this->session->set_flashdata('success', 'subscription Added successfully!!!!');
				redirect('supercontrol/subscription/show_subscription');
			}
		}
		//=======================Insert Blod Data **** END HERE============
		//======================Show Blog List **** START HERE========================
		function show_subscription(){
			$query = $this->Generalmodel->show_data_id('subscription_plan','','','get','');
			$data['ecms'] = $query;
			$data['title'] = "subscription List";
			$this->load->view('supercontrol/header',$data);
			$this->load->view('supercontrol/showsubscriptionlist');
			$this->load->view('supercontrol/footer');
		}
		//======================Show Blog List **** END HERE========================
		//================Show Individual by Id for BLOG *** START HERE=================
		function show_subscription_id($id) {
			$id = $this->uri->segment(4); 
			$data['title'] = "Edit Subscription";
			//Transfering data to Model
			$query = $this->Generalmodel->show_data_id('subscription_plan',$id,'id','get','');
			$data['id']=$id;
			//echo $this->db->last_query($query);
			//print_r($query);
			//exit();
			$data['ecms'] = $query;
			$this->load->view('supercontrol/header',$data);
			$this->load->view('supercontrol/subscription_edit', $data);
			$this->load->view('supercontrol/footer');
		}
		//================Show Individual by Id for BLOG *** END HERE=================
		//================Update Individual blog***** START HERE ====================
		function edit_subscription(){
				//=======================MODIFIED======================
				$data = array(
					 'subscription_title' => $this->input->post('subscription_title'),
					 'amount' => $this->input->post('amount'),
					 'payment_type' => $this->input->post('payment_type'),
					 'status' =>  $this->input->post('status')
				); 
				//print_r($data);
				//exit();
				$id = $this->input->post('id');
				
				$query = $this->Generalmodel->show_data_id('subscription_plan',$id,'id','update',$data);
				$this->session->set_flashdata('success', 'Subscription Updated successfully!!!!');
				redirect('supercontrol/subscription/show_subscription');
		}
		//=======================MODIFIED======================
		//=====================Delete Subscription==============
		function delete_subscription(){
			 $id = $this->uri->segment(4);
			$query = $this->Generalmodel->show_data_id('subscription_plan',$id,'id','delete','');
	        $this->session->set_flashdata('success', 'Subscription Plan Deleted successfully!!!!');
			redirect('supercontrol/subscription/show_subscription');
		}
		//=====================Delete Subscription==============
		
		//================Update Individual  Blog ***** END HERE====================
		
		function add_plan_view(){
			@$data['planid']= end($this->uri->segment_array());
			$data['title']="Add Plan Details";
			$this->load->view('supercontrol/header',$data);
			$this->load->view('supercontrol/subscription_plan_add_view');
			$this->load->view('supercontrol/footer');
		}
		
		function add_subscription_plan_details(){
			//======================Modified========================
			$my_date = date("Y-m-d", time());
			//=====================+++++++++++++++++++++++===================
			$this->load->library('form_validation');
		    $this->form_validation->set_rules('features', 'Features','required');
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			//=====================+++++++++++++++++++++++===================
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('supercontrol/header');
				$data['success_msg'] = '<div class="alert alert-success text-center">Some Fields Can Not Be Blank</div>';
				$this->load->view('supercontrol/header');
				$this->load->view('supercontrol/subscription_plan_add_view',$data);
				$this->load->view('supercontrol/footer');
			}else{
				$data = array(
					 'plan_id' => $this->input->post('plan_id'),
					 'features' => $this->input->post('features'),
					 'status' =>  'Y'
				); 
				$this->Generalmodel->show_data_id('subscription_details','','','insert',$data);
				//echo $this->db->last_query();
				//exit();
				$this->session->set_flashdata('success', 'Subscription Plan Details Added successfully!!!!');
				redirect('supercontrol/subscription/show_subscription');
			}
		
		}
		
		function show_plan_details(){
			@$id = end($this->uri->segment_array());
			$data['title'] = "Subscription Plan Details";
			$query = $this->Generalmodel->show_data_id('subscription_details',$id,'plan_id','get','');
			
			//=======================Get Package Name======================
			$data['ecms'] = $query;
			
			$queryname = $this->Generalmodel->show_data_id('subscription_plan',$id,'id','get','');
			$data['planname']=$queryname[0]->subscription_title;
			$this->load->view('supercontrol/header',$data);
			$this->load->view('supercontrol/subscription_plan_detail_list', $data);
			$this->load->view('supercontrol/footer');
		}
}

?>