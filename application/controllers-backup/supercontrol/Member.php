<?php
ob_start();
class Member extends CI_Controller {
		//============Constructor to call Model====================
		function __construct() {
			parent::__construct();
			$this->load->library(array('form_validation','session'));
			if($this->session->userdata('is_logged_in')!=1){
			redirect('supercontrol/home', 'refresh');
			}

			//$this->load->model('supercontrol/member_model');
			$this->load->model('supercontrol/Generalmodel');
			$this->load->model('supercontrol/Country_model');
			$this->load->library('image_lib');
			$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
            $this->output->set_header('Pragma: no-cache');
			//****************************backtrace prevent*** END HERE*************************
		}

		//============Constructor to call Model====================

		function index(){
		}

		//=======================Insert Page Data============
		/*function add_member(){
			$date = date('Y-m-d h:i:s');
			$day = $this->input->post('day');
			$month = $this->input->post('month');
			$year = $this->input->post('year');
			// $dob = new DateTime($year.'-'.$month.'-'.$day);
			// $dateofbirth = $dob->format('Y-m-d');
			$dateofbirth = date('Y-m-d');
			$reg_no = mt_rand(100000000, 999999999);
			$ip_address = $this->input->ip_address();
			$config = array(
			'upload_path' => "uploads/member/",
			'upload_url' => base_url() . "uploads/member/",
			'allowed_types' => "gif|jpg|png|jpeg"
			);
        	$this->load->library('upload', $config);
				//=====================+++++++++++++++++++++++===================
				$this->form_validation->set_rules('first_name','First Name', 'required');
				$this->form_validation->set_rules('last_name', 'Last Name', 'required|min_length[1]|max_length[100]');
				$this->form_validation->set_rules('email', 'Email','required|valid_email|is_unique[users.email]');
				//$this->form_validation->set_rules('paypal_email', 'Paypal Email','required|valid_email');
				$this->form_validation->set_rules('phone', 'Phone','required');
				$this->form_validation->set_rules('password', 'Password','required');
				$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
				//=====================+++++++++++++++++++++++===================
				if ($this->form_validation->run() == FALSE) {
					$this->load->view('supercontrol/header');
					$data['success_msg'] = '<div class="alert alert-success text-center">Some Fields Can Not Be Blank</div>';
					$this->load->view('supercontrol/header');
            		$this->load->view('supercontrol/memberadd_view',$data);
					$this->load->view('supercontrol/footer');
					//redirect('member/addmember',$data);
				}else{
					if (!$this->upload->do_upload('userfile')){
            			$data['success_msg'] = '<div class="alert alert-success text-center">You Must Select An Image File!</div>';
						$query = $this->Country_model->select_country();
						$data['country'] = $query;
						$this->load->view('supercontrol/header');
            			$this->load->view('supercontrol/memberadd_view', $data);
						$this->load->view('supercontrol/footer');
        			}else{
						 $data['userfile'] = $this->upload->data();
						 $filename=$data['userfile']['file_name'];
						//=================Insert=================
						$table_name = 'users';
						$id = '';
						$fieldname = '';
						$action = 'insert';
						$data = array(
							'first_name' => $this->input->post('first_name'),
							'last_name' => $this->input->post('last_name'),
							'email' => $this->input->post('email'),
							'reg_type' => $this->input->post('reg_type'),
							'profile_pic' => $filename,
							'phone' => $this->input->post('phone'),
							'password' => md5($this->input->post('password')),
							'country' => $this->input->post('country'),
							'state' => $this->input->post('state'),
							'city' => $this->input->post('city'),
							'postcode' => $this->input->post('postcode'),
							'email_verified' => 'Yes',
							'createdate' => $date,
							'reg_no' => $reg_no,
							'ip_address' => $ip_address,
							'dob' => $dateofbirth,
							'status' => 'Yes'
						);
						//=====================
						$query = $this->Generalmodel->common_function($table_name,$id,$fieldname,$action,$data);
						$upload_data = $this->upload->data();
						//=================Insert=================
						$this->session->set_flashdata('add_message', 'member Added successfully!!!!');
						redirect('supercontrol/member/showmember');	
					}
				}
		}*/
		/*
			functiona name: add_member
			Author: (Koushik Dutta)
			Usage: Add Member into database
		*/
		public function add_member()
		{
			$date = date('Y-m-d h:i:s');
			$day = $this->input->post('day');
			$month = $this->input->post('month');
			$year = $this->input->post('year');
			// $dob = new DateTime($year.'-'.$month.'-'.$day);
			$dateofbirth = date('Y-m-d H:i:s');
			$reg_no = mt_rand(100000000, 999999999);
			$ip_address = $this->input->ip_address();

			$config = array(
				'upload_path' => 'uploads/member/',
				'upload_url'  => base_url() . "uploads/member/",
				'allowed_types'	=>		'jpg|gif|png|jpeg',
			);
			$this->load->library('upload', $config);
			$this->load->library('form_validation');
			// Set Rules
			$this->form_validation->set_rules('first_name','First Name', 'required');
			$this->form_validation->set_rules('last_name', 'Last Name', 'required|min_length[1]|max_length[100]');
			$this->form_validation->set_rules('email', 'Email','required|valid_email|is_unique[users.email]');
			$this->form_validation->set_rules('phone', 'Phone','required|numeric');
			$this->form_validation->set_rules('password', 'Password','required');
			$this->form_validation->set_rules('postcode', 'Postcode','required|numeric');
			$this->form_validation->set_rules('day', 'Day','required|numeric');
			$this->form_validation->set_rules('month', 'Month','required');
			$this->form_validation->set_rules('year', 'Year','required|numeric');
			// Here I have set Error Delimeters
			$this->form_validation->set_error_delimiters('<span class="label label-danger">', '</span>');
			if( $this->form_validation->run() && $this->upload->do_upload('userfile') ) {
				$table_name = 'users';
				$id = '';
				$fieldname = '';
				$action = 'insert';
				$data = $this->upload->data();
				// print_r($data);exit;
				// $image_path = base_url("uploads/member/" . $data['raw_name'] . $data['file_ext']);
				$filename = $data['raw_name'] . $data['file_ext'];
				$post_data = array(
					'first_name' => $this->input->post('first_name'),
					'last_name' => $this->input->post('last_name'),
					'email' => $this->input->post('email'),
					'reg_type' => $this->input->post('reg_type'),
					'profile_pic' => $filename,
					'phone' => $this->input->post('phone'),
					'password' => md5($this->input->post('password')),
					'country' => $this->input->post('country'),
					'state' => $this->input->post('state'),
					'city' => $this->input->post('city'),
					'postcode' => $this->input->post('postcode'),
					'email_verified' => 'Yes',
					'createdate' => $date,
					'reg_no' => $reg_no,
					'ip_address' => $ip_address,
					'dob' => $dateofbirth,
					'status' => 'Yes'
				);
				// print_r($post_data);
				$query = $this->Generalmodel->common_function($table_name,$id,$fieldname,$action,$post_data);
				$this->session->set_flashdata('add_message', 'member Added successfully!!!!');
				redirect('supercontrol/member/showmember');
			}else{
				$upload_error = $this->upload->display_errors();
				$this->load->view('supercontrol/header');
				$this->load->view('supercontrol/memberadd_view',compact('upload_error'));
				$this->load->view('supercontrol/footer');
			}
		}

		//=======================Insert Page Data============

		//================Add member form=============

		function addmember(){
			$query = $this->Country_model->select_country();
			$data['country'] = $query;
			$data['title'] = "Add Member";
			$this->load->view('supercontrol/header',$data);
			$this->load->view('supercontrol/memberadd_view');
			$this->load->view('supercontrol/footer');
		}
	   public function ajax_state_list($id)
			{
			$query = $this->Country_model->select_state($id);
			$data['state'] = $query;
			$this->load->view('supercontrol/getstateview',$data);
			}
	  public function ajax_city_list($id)
			{
			$query = $this->Country_model->select_city($id);
			$data['city'] = $query;
			$this->load->view('supercontrol/getcityview',$data);
			}

		//================View Individual Data List=============
		//================View Individual Data List=============

  		//================Show Individual by Id=================

		function show_member_id($id) {
			$mid = $this->uri->segment(4);
			$action3='get';
			$data3='';
			$tablename3='users';
			$fieldname3 = 'id';
			$id3 = $mid;
			$query1 = $this->Generalmodel->show_data_id($tablename3,$id3,$fieldname3,$action3,$data3);
			//echo "<pre/>"; print_r($query1);
			$data['ecms'] = $query1;
			$country = $query1[0]->country;
			$state = $query1[0]->state;
			$city = $query1[0]->city;
			$query = $this->Country_model->select_country();
			$data['country'] = $query;
			//echo $this->db->last_query(); exit();
			$query = $this->Country_model->select_state($country);
			$data['state'] = $query;
			//echo $this->db->last_query(); exit();
			$query = $this->Country_model->select_city($state);
			$data['city'] = $query;
			//echo $this->db->last_query(); exit();
			$data['title'] = "Member Edit";
			$this->load->view('supercontrol/header',$data);
			$this->load->view('supercontrol/member_edit');
			$this->load->view('supercontrol/footer');
		}
		
		function view_member($id) {
			$mid = $this->uri->segment(4);
			$action3='get';
			$data3='';
			$tablename3='users';
			$fieldname3 = 'id';				
			$id3 = $mid;
			$query1 = $this->Generalmodel->show_data_id($tablename3,$id3,$fieldname3,$action3,$data3);
			//echo "<pre/>"; print_r($query1);
			$data['ecms'] = $query1;
			$country = $query1[0]->country;
			$state = $query1[0]->state;
			$city = $query1[0]->city;
			
			$query = $this->Country_model->show_country_view($country);
			$data['country'] = $query;
			$query = $this->Country_model->show_state_view($state);
			$data['state'] = $query;
			$query = $this->Country_model->show_city_view($city);
			$data['city'] = $query;
			$data['title'] = "Member View";
			$this->load->view('supercontrol/header',$data);
			$this->load->view('supercontrol/member_view');
			$this->load->view('supercontrol/footer');
		}

		function statusmember (){
			$stat= $this->input->get('stat'); 
			$id= $this->input->get('id');   
			$this->member_model->updt($stat,$id);
		}

   		//================Show Individual by Id=================

  	 	//================Update Individual ====================

		function edit_member(){
			 //============================================
		$date = date('Y-m-d h:i:s');
			@$day = $this->input->post('day');
			@$month = $this->input->post('month');
			@$year = $this->input->post('year');
			if($day!='' && $month!='' && $year!=''){
			@$dob = new DateTime($year.'-'.$month.'-'.$day);
			@$dateofbirth = $dob->format('Y-m-d');
			}
			 $pass = $this->input->post('oldpass'); 
			 $newpass = $this->input->post('password'); 
			if($pass==$newpass){
				$password = $pass;
			}else{
				$password = md5($newpass);
			}
			
		 	 $profile_pic = $this->input->post('profile_pic');
			 $config = array(
				'upload_path' => "uploads/member/",
				'upload_url' => base_url() . "uploads/member/",
				'allowed_types' => "gif|jpg|png|jpeg"
			);
			$this->load->library('upload', $config);
			if ($this->upload->do_upload("userfile")) {
				@unlink("uploads/member/".$profile_pic);
				$data['img'] = $this->upload->data();
				 $data = array(
							'first_name' => $this->input->post('first_name'),
							'last_name' => $this->input->post('last_name'),
							'email' => $this->input->post('email'),
							'reg_type' => $this->input->post('reg_type'),
							'phone' => $this->input->post('phone'),
							'password' => $password,
							'country' => $this->input->post('country'),
							'state' => $this->input->post('state'),
							'city' => $this->input->post('city'),
							'postcode' => $this->input->post('postcode'),
							'dob' => $dateofbirth,
							'status' => $this->input->post('status'),
							'profile_pic' => $data['img']['file_name']
						);
			}else{
				$data = array(
							'first_name' => $this->input->post('first_name'),
							'last_name' => $this->input->post('last_name'),
							'email' => $this->input->post('email'),
							'reg_type' => $this->input->post('reg_type'),
							'phone' => $this->input->post('phone'),
							'password' => $password,
							'country' => $this->input->post('country'),
							'state' => $this->input->post('state'),
							'city' => $this->input->post('city'),
							'postcode' => $this->input->post('postcode'),
							'dob' => $dateofbirth,
							'status' => $this->input->post('status')
						);
					}
				$tablename='users';
				$action='update';
				$id = $this->input->post('id');
				$fieldname = 'id';
				$query = $this->Generalmodel->show_data_id($tablename,$id,$fieldname,$action,$data);
				$data['title'] = "Member Edit";
				$this->session->set_flashdata('edit_message', 'Member Updated Successfully !!!!');			
				redirect('supercontrol/member/showmember');
		}


		function showmember(){
			//=====================
			$table_name = 'users';
			$id = '';
			$fieldname = '';
			$action = 'get';
			$data = '';
			//=====================
			$query = $this->Generalmodel->common_function($table_name,$id,$fieldname,$action,$data);
			/*echo "<pre>";
			print_r($query);
			exit();*/
			$data['edata'] = $query;
			$data['title'] = "Member List";
			$this->load->view('supercontrol/header',$data);
			$this->load->view('supercontrol/showmemberlist');
			$this->load->view('supercontrol/footer');
		}

		//======================Show CMS========================
		//=====================DELETE member====================
			function delete_member()
			{
			$tablename='users';
			$data = '';
			$action='delete';
			$id = $this->uri->segment(4);
			$fieldname = array(
					'id' => $this->uri->segment(4)
				);		
			$query = $this->Generalmodel->show_data_id($tablename,$id,$fieldname,$action,$data);
			$this->session->set_flashdata('delete_message', 'Member Deleted Successfully !!!!');		
			redirect('supercontrol/member/showmember');
      }
		//=====================DELETE member====================
		
}



?>



