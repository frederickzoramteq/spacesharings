<?php
ob_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Auth extends CI_Controller{
	function __construct() {
		@parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->model('Auth_model');
		$this->load->model('Generalmodel');
		$this->load->library('email');
		//$this->load->model('Newsletter_model');
		$this->load->library('encrypt');
		$this->load->helper('string');
		/* Load form helper */
		$this->load->helper(array('form'));
		/* Load form validation library */
		$this->load->library('form_validation');
	}
	function Signup(){
		$data['title'] = "Registration";
		$this->load->view('Header',$data);
		$this->load->view('Registration');
		$this->load->view('Footer');
	}
	function userregistration(){
		$reg_date = date('Y-m-d H:i a');
		$reg_no = mt_rand(100000000, 999999999);
		$ip_address = $this->input->ip_address();
		//$this->form_validation->set_rules('email', 'Email', 'trim|required|is_unique[users.email]');
		//$this->form_validation->set_rules('password', 'Password', 'required');
       // $this->form_validation->set_rules('con_pass', 'Confirm Password', 'required|matches[password]');
       // $this->form_validation->set_rules('phone', 'Mobile Number ', 'required|regex_match[/^[0-9]{10}$/]');
		//<p class="text-danger">Donec ullamcorper nulla non metus auctor fringilla.</p>
		$this->form_validation->set_error_delimiters('<span class="label label-danger" style="font-size:11px;">', '</span>');
		if($this->form_validation->run('registerr')==FALSE){
			$this->session->set_flashdata("error"," <b>Something Went Wrong ..</b>");
			$querycnt = $this->Generalmodel->show_data_id('countries','','','get','');
			$data['country'] = $querycnt;
			$data['title'] = "Registration";
			$this->load->view('header');
			$this->load->view('register',$data);
			$this->load->view('footer');
		}else{
			$datareg = array(
				'reg_no' => $reg_no,
				'ip_address' => $ip_address,
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'state' => $this->input->post('state'),
				'email' => $this->input->post('email'),
				'phone' => $this->input->post('phone'),
				'password' => md5($this->input->post('password')),
				'country' => $this->input->post('country'),
				'postcode' => $this->input->post('postcode'),
				'city' => $this->input->post('city'),
				'address_1' => $this->input->post('address_1'),
				'address_2' => $this->input->post('address_2'),
				'createdate' => $reg_date,
				'status' => 'Yes',
				'email_verified' => 'No'
			);
			// print_r($datareg);
			//exit();
			$query = $this->Generalmodel->show_data_id('users','','','insert',$datareg,'');
			//$this->session->set_flashdata('success', 'Chanel added successfully');
			//redirect($_SERVER['HTTP_REFERER']);
			$lastid = $this->db->insert_id();
			$from_email = "teamphp@goigi.in";
			$to_email = $this->input->post('email');
			$url = base_url()."Accountverification/".$lastid;
			$data1 = array(
				'reg_no' => $reg_no,
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'email' => $this->input->post('email'),
				'password' => $this->input->post('password'),
				'createdate' => $reg_date,
				'url' => $url
			);
			$msg = $this->load->view('Userregistrationtemplate',$data1,TRUE);
			$config['mailtype'] = 'html';
			$this->load->library('email');
			$this->email->initialize($config);
			$msg = $msg;
			$subject = 'Registration Success Message From Yes I Do.';
			$this->email->from($from_email, 'Yes I Do');
			$this->email->to($to_email);
			$this->email->subject($subject);
			$this->email->message(@$msg);
			if(@$this->email->send()){
				$this->session->set_flashdata("success", "<b >You Have Registered Successfully !!! Please Check Your Email For The Verify Link</b>");
				$querycnt = $this->Generalmodel->show_data_id('countries','','','get','');
				$data['country'] = $querycnt;
				$data['title'] = "Registration";
				$this->load->view('header');
			$this->load->view('register',$data);
			$this->load->view('footer');
			}
			else{
			//echo "Not Send Mail"; exit();
				$this->session->set_flashdata("success", "<b>You Have Registered Successfully!!! </b>");
				$querycnt = $this->Generalmodel->show_data_id('countries','','','get','');
				$data['country'] = $querycnt;
				$data['title'] = "Registration";
				$this->load->view('header');
			$this->load->view('register',$data);
			$this->load->view('footer');
			}
		}
	}
	function verification() {
		$last = $this->uri->total_segments();
		$allias = $this->uri->segment($last);
		$data = array(
			'status' => 'Yes',
			'email_verified' => 'Yes'
		);
		$query = $this->Auth_model->userverification($allias,$data);
		$query = $this->Auth_model->userverificationupdt();
		@$userstat = $query[0]->status;
		@$emailveri = $query[0]->email_verified;
		if(($userstat == 'Yes') && ($emailveri == 'Yes')){
			$data['title'] = "Verification";
			$this->load->view('header',$data);
			$this->load->view('Successreg',$data);
			$this->load->view('footer');
		}
		else{
			$data['title'] = "False";
			$this->load->view('header',$data);
			$this->load->view('Unsuccessreg',$data);
			$this->load->view('footer');
		}
	}

		//====================Login=====================
	public function login_view(){
		$data['title'] = "Login";
		$this->load->view('header',$data);
		$this->load->view('user-login',$data);
		$this->load->view('footer',$data);
	}
	public function login_validation(){


		//echo $a=$this->input->post('email');
		//echo $b=$this->input->post('password');exit;
		$query = $this->Auth_model->user_login($this->input->post('email'),md5($this->input->post('password')));
		@$status = $query[0]->status;
		@$email_verified = $query[0]->email_verified;
		@$email = $query[0]->email;
		@$uid = $query[0]->id;
		@$profile_pic = $query[0]->profile_pic;
		if($query==true && $status=='Yes'){
			$this->session->set_userdata('logged',@$session_data);
			@$session_data = array(
				'email'=>$email,
				'uid'=>$id,
				'is_userlogged_in' => $uid,
				'profile_pic' => $profile_pic
			);
			$record = array(
				'online_status' => '1'
			);
			$query = $this->Auth_model->online_status($uid,$record);
			$this->session->set_userdata('logged_in', $session_data);
			$this->session->set_userdata('is_userlogged_in',$email);
			$this->session->set_userdata('is_user_id',$uid);
			//$this->session->set_userdata('is_user_id',$uid);
			redirect('Page/basic_search',$session_data);
			//exit;
		}else{
			$this->session->set_flashdata("logerror", "<b>Invalid Username or Password !!!");
				redirect($_SERVER['HTTP_REFERER']."#errorlog");
			//echo 0;
		}

	}

	public function forget_password(){
		$data['title'] = "Forget Password";
		$this->load->view('header',$data);
		$this->load->view('forgotpassword',$data);
		$this->load->view('footer',$data);
	}

	public function get_password(){
		$useremail=$this->input->post('email');
		$query = $this->Auth_model->get_user_details($useremail);
		if(count($query)>0){
			$userid = $query[0]->id;
				//===========================------------------=========================
			$from_email = "info@yesido.com";
			$to_email = $this->input->post('email');
			$url = base_url()."Auth/createnewpassword/".$userid;
				//***********************================******************
			$data1 = array(
				'membername' =>$query[0]->first_name." ".$query[0]->last_name,
				'email' => $this->input->post('email'),
				'url' => $url
			);
				//***********************================******************
			$msg = $this->load->view('userpasswordtemplate',$data1,TRUE);
			$config['mailtype'] = 'html';
			$this->load->library('email');
			$this->email->initialize($config);
			$msg = $msg;
			$subject = 'Change Password For Your Yes I Do  Account.';
			$this->email->from($from_email, 'Yes I Do ADMINISTRATOR');
			$this->email->to($to_email);
			$this->email->subject($subject);
			$this->email->message($msg);
				//===========================------------------=========================
			if($this->email->send()){

				echo 1;
			}else{
				echo 1;
			}
		}else{
			echo 0;
		}

	}
		//=========================Create New Password=========================
	function createnewpassword(){
			//==============Get Settings Process===============
			//$userid=end($this->uri->segment_array());
			//$data['email']=$userid;
		$data['title']='Change Password';
		$this->load->view('header',$data);
		$this->load->view('Newpassword');
		$this->load->view('footer');
	}
	function getnewpassword(){
		@$id=$this->input->post('idd');
		$data = array(
			'password' =>md5($this->input->post('new_pass'))
		);
		$query = $this->Auth_model->update_password($id,$data);
			 //echo $this->db->last_query(); exit();
		echo 1;
	}
		//=========================Create New Password=========================
	public function logout(){
		$uid = $this->session->userdata('is_user_id');
		$record = array(
			'online_status' => '0'
		);
		$query = $this->Auth_model->ofline_status($uid,$record);
		$this->session->sess_destroy();
		$data['page']='';
		$this->load->view('header');
		$this->load->view('user-login');
		$this->load->view('footer');
	}
		//====================Login=====================
}
?>
