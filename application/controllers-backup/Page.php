<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {
   
function __construct() {
		parent::__construct();
		$this->load->model('CountryModel','country');
		
	}
   
   public function AdvanceSearch() {
		$data['page']='';
		$this->load->view('header');	
		$this->load->view('advancesearch');
		$this->load->view('footer');
	}

	public function basic_hosting() {
		$data['page']='';
		$this->load->view('header');
		$data ['country']= $this->country->fetch_Country();
		$this->load->view('free-form',$data);
		$this->load->view('footer');
	}
	public function adv_hosting() {
		$data['page']='';
		$this->load->view('header');
		$this->load->view('user-login');
		$this->load->view('footer');
	}
	public function user_login() {
		$data['page']='';
		$this->load->view('header');
		$this->load->view('user-login');
		$this->load->view('footer');
	}
	public function basic_search() {
		$data['page']='';
		$this->load->view('header');
		$this->load->view('grid_view');
		$this->load->view('footer');
	}
	public function adv_search() {
		$data['page']='';
		$this->load->view('header');
		$this->load->view('adv_search');
		$this->load->view('footer');
	}
	public function post_request() {
		$data['page']='';
		$this->load->view('header');
		$this->load->view('request');
		$this->load->view('footer');
	}
	public function browse_request() {
		$data['page']='';
		$this->load->view('header');
		$this->load->view('browse_request');
		$this->load->view('footer');
	}
	public function contact() {
		$data['page']='';
		$this->load->view('header');
		$this->load->view('contactus');
		$this->load->view('footer');
	}
	public function register_page() {
		$this->load->view('header');
		$data ['country']= $this->country->fetch_Country();
		$this->load->view('register',$data);
		$this->load->view('footer');
	}


}
