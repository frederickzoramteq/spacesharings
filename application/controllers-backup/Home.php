<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct() {
		parent::__construct();
	}
	public  function home()
	{
		$data['page'] = "home";
		$this->load->view('index',$data);
	}
	public  function header()
	{
		$data['page'] = "Header";
		$this->load->view('header',$data);
	}
	public  function footer()
	{
		$data['page'] = "footer";
		$this->load->view('footer',$data);
	}
	public function index() {
		$this->header();
		$this->home();
		$this->footer();
	}
}
