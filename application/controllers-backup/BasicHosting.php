<?php
ob_start();
class BasicHosting extends CI_Controller {
		//============Constructor to call Model====================
	function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->model('Auth_model');
		$this->load->model('Generalmodel');
		$this->load->model('HostingModel');
		
		$this->load->library('email');
		//$this->load->model('Newsletter_model');
		$this->load->library('encrypt');
		$this->load->helper('string');
		/* Load form helper */
		$this->load->helper(array('form'));
		/* Load form validation library */
		$this->load->library('form_validation');
		$this->load->model('CountryModel','country');
			//****************************backtrace prevent*** END HERE*************************
	}

		//============Constructor to call Model====================



	function add_hosting(){

            $date = date('Y-m-d h:i:s');
		    $ip_address = $this->input->ip_address();
			$my_date = date("Y-m-d", time()); 
			$config = array(
			'upload_path' => "uploads/blog/",
		'upload_url' => base_url() . "uploads/blog/",
			'allowed_types' => "gif|jpg|png|jpeg"
			);
        	$this->load->library('upload', $config);
				//=====================+++++++++++++++++++++++===================
				//$this->form_validation->set_rules('fname','News Title', 'required');
				$this->form_validation->set_error_delimiters('<span class="label label-danger" style="font-size:11px;">', '</span>');
				//=====================+++++++++++++++++++++++===================
				if ($this->form_validation->run('basic_hostings') == FALSE) {

                     $this->session->set_flashdata("success_msg", "<div class='alert alert-success text-center'>Some Fields Can Not Be Blank</div>"); 
					//$data['success_msg'] = '';
					$this->load->view('header');
			$data ['country']= $this->country->fetch_Country();
			$this->load->view('free-form',$data);
			$this->load->view('footer');
					//redirect('banner/addbanner',$data);
				}else{
					if (!$this->upload->do_upload('userfile')){
            			//$data['success_msg'] = '<div class="alert alert-success text-center">You Must Select An Image File!</div>';
            			$this->session->set_flashdata("success_msg", "<div class='alert alert-success text-center'>You Must Select An Image File!</div>");
						$this->load->view('header');
			$data ['country']= $this->country->fetch_Country();
			$this->load->view('free-form',$data);
			$this->load->view('footer');
        			}else{
						 $data['userfile'] = $this->upload->data();
						 $filename=$data['userfile']['file_name'];
						 $table_name = 'hosting';
			$id = '';

			$post_data = array(
				'id' => '',
				'fname' => $this->input->post('fname'),
				'lname' => $this->input->post('lname'),
				'image' => $filename,
				'description' => $this->input->post('desc'),
				'title' => $this->input->post('title'),
				'country' => $this->input->post('country'),
				'state' => $this->input->post('state'),
				'city' => $this->input->post('city'),
				'postcode' => $this->input->post('postcode'),
				'email_verified' => 'Yes',
				'createdate' => $date,
				'ip_address' => $ip_address,
				'status' => 'Yes'
			);

			$query = $this->HostingModel->insert($table_name,$post_data);

            	$upload_data = $this->upload->data();
            			//$data['success_msg'] = '<div class="alert alert-success text-center"> Data successfully inserted!!!</div>';
            	$this->session->set_flashdata("success_msg", "<div class='alert alert-success text-center'> Data successfully inserted!!!</div>");
			$this->load->view('header');
			$data ['country']= $this->country->fetch_Country();
			$this->load->view('free-form',$data);
			$this->load->view('footer');

				


					}


				}


		}

	
}



?>



