<?php
class CountryModel extends CI_Model{
	function __construct() {
		parent::__construct();
	}

	public function fetch_country()
	{
		$query = $this->db->get('countries');
		$result = $query->result();
		return $result;
	}

}