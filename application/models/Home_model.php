<?php
class Home_model extends CI_Model{
	function __construct() {
		parent::__construct();
	}

	public function fetch_testimonials()
	{
		$query = $this->db->get('testimonial');
		$result = $query->result_array();
		return $result;
	}
	public function fetch_about()
	{
		$query="select * from cms where cms_heading='Home' and status='1'";
		$result = $this->db->query($query);
		return $result->result_array();
	}
	public function fetch_row($tbl, $where)
    {

    	$this->db->select('*');
    	$this->db->where($where);
    	$query=$this->db->get($tbl);
    	return $query->row();

    }

}