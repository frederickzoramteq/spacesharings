<?php
ob_start();

class Generalmodel extends CI_Model{
	function __construct() {
		parent::__construct();
	}
	//=============== Select , Select by id , insert , update , delete ====================================================
	public function record_count($tablename,$primary_key,$wheredata) {
		if($wheredata!=''){
			$this->db->where(array($primary_key => $wheredata));
		}
		$result=$this->db->count_all_results($tablename);
		//$result= $this->db->from($tablename);
		return $result;
		/*$this->db->where($primary_key,$wheredata);
		$query = $this->db->get();
		foreach($query->result() as $r){
			return $r->rows;
		}*/
	}

	public function record_count_search($tablename,$primary_key,$likefield,$wheredata,$allias) {
		//echo ">>>".$primary_key;
		//echo "  =========.".$wheredata;
		if($primary_key==''){
			$this->db->like($likefield, $allias, 'after');
			$result=$this->db->count_all_results($tablename);
			return $result;
		}else{
			$this->db->where(array($primary_key => $wheredata));
			$this->db->like($likefield, $allias, 'after');
			$result=$this->db->count_all_results($tablename);
			return $result;
		}


	}




	public function record_count_searchnew($tablename,$primary_key1,$primary_key2,$likefield,$wheredata1,$wheredata2,$allias) {
		$this->db->where(array($primary_key1 => $wheredata1));
		$this->db->where(array($primary_key2 => $wheredata2));
		if($likefield!=''){
			$this->db->like($likefield, $allias, 'after');
		}
		$result=$this->db->count_all_results($tablename);
		return $result;
	}

	public function getAllData($table_name,$primary_key,$wheredata,$limit,$start){
		if(@$limit!='' || @$start!=''){
			$this->db->limit($limit, $start);
		}
		$this->db->select ('*');
		$this->db->from($table_name);
		if($primary_key=='' && $wheredata==''){
			$where='1=1';
		}else{
			$this->db->where($primary_key,$wheredata);
		}
		$query = $this->db->get();
		$result = $query->result();
		return $result;

	}

	public function getAllDatadist($table_name,$primary_key,$wheredata,$limit,$start){
		if(@$limit!='' || @$start!=''){
			$this->db->limit($limit, $start);
		}
		$this->db->distinct();
		$this->db->select ($primary_key);
		$this->db->from($table_name);

		$query = $this->db->get();
		$result = $query->result();
		return $result;

	}


	function getSearchData($table_name,$primary_key,$likefield,$wheredata,$limit,$start,$allias){
		//================*********************=====================
		if(@$limit!='' || @$start!=''){
			$this->db->limit($limit, $start);
		}
		$this->db->select ('*');
		$this->db->from($table_name);
		if($primary_key=='' && $wheredata==''){
			$where='1=1';
		}else if($primary_key=='' || $wheredata==''){
			$where='1=1';
		}else{
			$this->db->where($primary_key,$wheredata);
		}
		$this->db->like($likefield, $allias, 'after');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
		//================*********************=====================
	}

	//function getSearchDataWine1($table_name,$primary_key1,primary_key2,$wheredata1,$wheredata1,$limit,$start){
	function getSearchDataWine($table_name,$primary_key1,$primary_key2,$wheredata1,$wheredata2,$limit,$start,$allias){
		//================*********************=====================
		$this->db->select ('*');
		$this->db->from($table_name);
		$this->db->where($primary_key1,$wheredata1);
		$this->db->where($primary_key2,$wheredata2);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
		//================*********************=====================
	}

	public function fetch_videos($limit, $start) {
		$this->db->limit($limit, $start);
		$query = $this->db->get("video");

		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}

	/*public function fetch_videos($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("video");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }*/

    public function do_action($table_name,$id,$fieldname,$action,$data,$limit){

    	if($action=='get'){
    		if(($id !='') && ($fieldname!='')&& ($data=='')&& ($limit=='')){
    			$this->db->select ('*');
    			$this->db->from($table_name);
    			$this->db->where($fieldname,$id);
    			$this->db->limit($limit);
    		}else{
    			$this->db->select ('*');
    			$this->db->from($table_name);
    			$this->db->limit($limit);
    		}
    		$query = $this->db->get();
    		$result = $query->result();
    		return $result;
    	}
    	if($action=='insert'){
    		$return = $this->db->insert($table_name, $data);
    		if ((bool) $return === TRUE) {
    			return $this->db->insert_id();
    		}else {
    			return $return;
    		}
    	}
    	if($action=='update'){
    		$this->db->where($fieldname, $id);
    		$return=$this->db->update($table_name, $data);
    		return $return;
    	}
    	if($action=='delete'){
    		$this->db->where($fieldname, $id);
    		$this->db->delete($table_name);
    	}

    }
    public function show_data_id($table_name,$id,$fieldname,$action,$data){
    	if($action=='get'){
    		if(($id !='') && ($fieldname!='')&& ($data=='')){
    			$this->db->select ('*');
    			$this->db->from($table_name);
    			$this->db->where($fieldname,$id);
    		}else{
    			$this->db->select ('*');
    			$this->db->from($table_name);
    		}
    		$query = $this->db->get();
    		$result = $query->result();
    		return $result;
    	}
    	if($action=='insert'){
    		$return = $this->db->insert($table_name, $data);
    		if ((bool) $return === TRUE) {
    			return $this->db->insert_id();
    		}else {
    			return $return;
    		}
    	}
    	if($action=='update'){
    		$this->db->where($fieldname, $id);
    		$return=$this->db->update($table_name, $data);
    		return $return;
    	}
    	if($action=='delete'){
    		$this->db->where($fieldname, $id);
    		$this->db->delete($table_name);
    	}
    }
	//=============== Select , Select by id , insert , update , delete ====================================================
	//=============== Select , Select by id By Limit =======================================================================
    public function show_data_id_order($table_name,$id,$fieldname,$order,$orderfield,$action,$data){

    	if($action=='get'){
    		if(($id !='') && ($fieldname!='')&& ($data=='')){
    			$this->db->select ('*');
    			$this->db->from($table_name);
    			$this->db->where($fieldname,$id);
    			$this->db->order_by($orderfield,$order);
    		}else{
    			$this->db->select ('*');
    			$this->db->from($table_name);
    		}
    		$query = $this->db->get();
    		$result = $query->result();
    		return $result;
    	}
    	if($action=='insert'){
    		$return = $this->db->insert($table_name, $data);
    		if ((bool) $return === TRUE) {
    			return $this->db->insert_id();
    		}else {
    			return $return;
    		}
    	}
    	if($action=='update'){
    		$this->db->where($fieldname, $id);
    		$return=$this->db->update($table_name, $data);
    		return $return;
    	}
    	if($action=='delete'){
    		$this->db->where($fieldname, $id);
    		$this->db->delete($table_name);
    	}
    }

    public function user_limit($uid){
    	$this->db->select ( '*' );
    	$this->db->from('users');
                //$this->db->where()
    	$this->db->order_by('id', 'desc');
    	$this->db->limit('4,0');
    	$query = $this->db->get();
    	$result = $query->result();
    	return $result;
    }

    public function user_active($uid){
    	$this->db->select ( '*' );
    	$this->db->from('users');
    	$this->db->where('id!=',$uid);
    	$this->db->where('online_status','1');
    	$this->db->order_by('id', 'desc');
    	$this->db->limit('4,0');
    	$query = $this->db->get();
    	$result = $query->result();
    	return $result;
    }

	//=============== Select , Select by id By Limit =======================================================================

	//================ Join of 2 tables ======================================================================================

    public function dynamic_join($table_name,$id,$fieldname,$table_name2,$fieldname2,$fieldname3,$action,$data){
    	if($action=='join'){
    		if(($id !='') && ($fieldname!='')&& ($data=='')) {
    			$this->db->select ( '*' );
    			$this->db->from($table_name);
    			$this->db->join($table_name2, $table_name.".".$fieldname .'='. $table_name2.".".$fieldname2);
    			$this->db->where($table_name.".".$fieldname3, $id);
    		}else{
    			$this->db->select ( '*' );
    			$this->db->from($table_name);
    			$this->db->join($table_name2, $table_name .".". $fieldname .'='. $table_name2 .".". $fieldname2);
    		}
    		$query = $this->db->get();
    		$result = $query->result();
    		return $result;
    	}
    }
	//================ Join of 2 tables ======================================================================================
	//================ Join of 2 tables by limit======================================================================================
    public function dynamic_join_limt($table_name,$id,$fieldname,$table_name2,$fieldname2,$fieldname3,$order,$limit,$action,$data){
    	if($action=='join'){
    		if(($id !='') && ($fieldname!='')&& ($data=='')){
    			$this->db->select ('*');
    			$this->db->from($table_name);
    			$this->db->join($table_name2, $table_name.".".$fieldname .'='. $table_name2.".".$fieldname2);
    			$this->db->where($table_name.".".$fieldname3, $id);
    			$this->db->order_by($table_name.".".$fieldname3, $order);
    			$this->db->limit($limit,0);
    		}else{
    			$this->db->select ( '*' );
    			$this->db->from($table_name);
    			$this->db->join($table_name2, $table_name .".". $fieldname .'='. $table_name2 .".". $fieldname2);
    			$this->db->order_by($table_name.".".$fieldname3, $order);
    			$this->db->limit($limit,0);
    		}
    		$query = $this->db->get();
    		$result = $query->result();
    		return $result;
    	}
    }
	//================ Join of 2 tables by limit======================================================================================

    public function advance_search($age1,$age2,$height1,$height2,$relationship,$community,$country,$state,$city,$edication,$work_as,$body_type,$body_color,$diet,$gender) {
    	$where = '';
    	$final_where='';
    	if($relationship)
    		$where.=" relationship = '".$relationship."' ";
    	if($community){
    		$where.="AND community = '".$community."'  ";
    	}
    	if($country){
    		$where.="AND country = '".$country."'  ";
    	}
    	if($state){
    		$where.="AND state = '".$state."'  ";
    	}
    	if($city){
    		$where.="AND city = '".$city."'  ";
    	}
    	if($edication){
    		$where.="AND education != 'NULL'  ";
    	}
    	if($work_as){
    		$where.="AND work_as != 'NULL'  ";
    	}
    	if($body_type){
    		$where.="AND body_type = '".$body_type."'  ";
    	}
    	if($body_color){
    		$where.="AND body_color = '".$body_color."'  ";
    	}
    	if($diet){
    		$where.="AND diet = '".$diet."'  ";
    	}
    	if($gender){
    		$where.="AND gender = '".$gender."'  ";
    	}
    	if($age1 && $age2){
    		$where.="AND age BETWEEN '".$age1."' AND '".$age2."' ";
    	}
    	if($height1 && $height2){
    		$where.="AND height BETWEEN '".$height1."' AND '".$height2."' ";
    	}
    	if($where!='')
    		$final_where = 'WHERE '.$where;
    	$Q = $this->db->query("SELECT * FROM users ".$final_where." order by id desc");
    	$result = $Q->result();
    	return $result;
    }



    public function user_contact_limit(){
    	$this->db->select ( '*' );
    	$this->db->from('user_contact');
    	$this->db->order_by('ContactId', 'desc');
    	$this->db->limit('1,0');
    	$query = $this->db->get();
    	$result = $query->result();
    	return $result;
    }

    public function fetch_all_join($query)
    {
    	$query = $this->db->query($query);
    	return $query->result();
    }
    public function count_all($tbl) {

        $this->db->select('*');

        return $this->db->count_all($tbl);
    }

    public function add_details($tbl,$data)
    {
    	$query=$this->db->insert($tbl,$data);
    	return $query;

    }
    public function fetch_single($tbl, $id)
    {
    	$this->db->select('*');
    	$this->db->where('hosting_id', $id);
    	$query=$this->db->get($tbl);
    	return $query->row();

    }
    public function fetch_single_join($query)
    {
    	$query = $this->db->query($query);
    	return $query->row();
    }

    public function checkrequest($sender_id, $receiver_id)
    {


    	$this->db->where('sender_id', $sender_id);
    	$this->db->where('receiver_id', $receiver_id);
    	$query = $this->db->get('friend_request');
          // exit();
    	if($query->num_rows() == 1)
    	{
    		return $query->result();
    	}
    	else
    	{
    		return false;
    	}
    }
    public function checkgroup($uid,$group_id)
    {
    	$this->db->where('uid', $uid);
    	$this->db->where('group_id', $group_id);
    	$query = $this->db->get('group_member');
          // exit();
    	if($query->num_rows() == 1)
    	{
    		return $query->result();
    	}
    	else
    	{
    		return false;
    	}
    }
    public function eidt_single_row($tbl,$data, $where)
    {
    	$this->db->where($where);
    	$this->db->update($tbl,$data);
    	return true;

    }
    public function delete_single_con($tbl,$where)
    {
    	$this->db->where($where);
    	$delete = $this->db->delete($tbl);
    	return $delete;

    }
    public function fetch_rows($tbl, $where)
    {
    	$this->db->select('*');
        //$this->db->where('status', 'Yes');
    	$this->db->where($where);
    	$this->db->order_by('id', 'desc');
    	$this->db->limit('5,0');
    	$query=$this->db->get($tbl);
    	return $query->result();

    }
    public function fetch_row($tbl, $where)
    {

    	$this->db->select('*');
    	$this->db->where($where);
    	$query=$this->db->get($tbl);
    	return $query->row();

    }
    public function fetch_all($tbl)
    {
    	$this->db->select('*');
        //$this->db->where('status', 'Yes');
    	$this->db->order_by('id', 'desc');
    	$this->db->limit('4,0');
    	$query=$this->db->get($tbl);
    	return $query->result();
       // return $num = $query->num_rows();
    }
    public function fetch($tbl)
    {
    	$this->db->select('*');
        //$this->db->where('status', 'Yes');
    	$this->db->order_by('id', 'desc');
    	$query=$this->db->get($tbl);
    	return $query->result();
       // return $num = $query->num_rows();
    }

    public function amencornercategory_users()
    {
    	$query = $this->db->select('*')
    	->where('user_type','amen_corner')
    	->get('users');
    	return $query->result();
    }

    public function millionairecategory_users()
    {
    	$query = $this->db->select('*')
    	->where('user_type','millionaire')
    	->get('users');
    	return $query->result();
    }

    /*public function bothcategory_users()
    {
    	$query = $this->db->select('*')
    						->where('user_type','both')
    						->get('users');
    	return $query;
    }*/

    public function postData($data)
    {
    	$this->db->insert('post', $data);
    }
    public function postDataOwnWall($data)
    {
    	$this->db->insert('post', $data);
    }

    public function addPhoto($table_name,$id,$fieldname,$action,$data)
    {
    	if($action=='get'){
    		if(($id !='') && ($fieldname!='')&& ($data=='')){
    			$this->db->select ('*');
    			$this->db->from($table_name);
    			$this->db->where($fieldname,$id);
    		}else{
    			$this->db->select ('*');
    			$this->db->from($table_name);
    		}
    		$query = $this->db->get();
    		$result = $query->result();
    		return $result;
    	}
    	if($action=='insert'){
    		$return = $this->db->insert($table_name, $data);
    		if ((bool) $return === TRUE) {
    			return $this->db->insert_id();
    		}else {
    			return $return;
    		}
    	}
    	if($action=='update'){
    		$this->db->where($fieldname, $id);
    		$return=$this->db->update($table_name, $data);
    		return $return;
    	}
    	if($action=='delete'){
    		$this->db->where($fieldname, $id);
    		$this->db->delete($table_name);
    	}
    }

    public function wallPost($uid)
    {
    	$query = $this->db->select("*")
    	->where('onwall',$uid)
    	->get('post');
    	return $query->result();
    }

    public function requestStatus($uid,$status)
    {
    	$query = $this->db->select("*")
    	->from("friend_request")
    	->where(array('sender_id' => $uid, 'status' => 'Yes'))
    	->get();
    	return $query->result();
    }
    public function findGroupid()
    {
    	$query = $this->db->select('group_id')
        ->select('COUNT(group_id) AS Countedgroup_id')
        ->from('group_member')
        ->group_by('group_id')
        /*->where('uid',$uid)*/
        ->get();
        return $query->result();
    }
    public function groupMembers($group_id)
    {
        /*foreach ($group_id as $value) {
            // print_r($value);
            $id = $value->group_id;
        }*/
        $query = $this->db->select('COUNT(group_id)')
        ->from('group_member')
        ->where('group_id',$group_id)
        ->get();
        if($query->num_rows() > 0){
            return $query->row();
        }
    }
    public function count_group($id)
    {
       /* SELECT COUNT(uid) as total FROM group_member where group_id =1*/
        $query = $this->db->select('COUNT(uid)')
                            ->from('group_member')
                            ->where('group_id',$id)
                            ->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
    }
    public function states_view($id){

        $this->db->select('*');

        $this->db->from('states');

        $this->db->where('country_id', $id);

        $query = $this->db->get();

        $result = $query->result();

        return $result;

    }
    public function host_bas_num_rows()
        {
            $query = $this->db
            ->select('*')
            ->from('hosting')
            ->where('host_type', 2)
            ->get();
            return $query->num_rows();
        }
        public function basic_host_list($limit, $offset )
        {
            //$user_id = $this->session->userdata('user_id');
            $query = $this->db
            ->select('*')
            ->order_by('id','desc')
            ->from('hosting')
            ->where('host_type', 2)
            ->limit( $limit, $offset )
            ->get();
            return $query->result();
        }
         public function host_adv_num_rows()
        {
            $query = $this->db
            ->select('*')
            ->from('hosting')
            ->where('host_type', 1)
            ->get();
            return $query->num_rows();
        }
         public function adv_host_list( $limit, $offset )
        {
            //$user_id = $this->session->userdata('user_id');
            $query = $this->db
            ->select('*')
            ->order_by('id','desc')
            ->from('hosting')
            ->where('host_type', 1)
            ->limit( $limit, $offset)
            ->get();
            return $query->result();
        }
}

?>

