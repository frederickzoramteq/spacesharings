<?php

class HostingModel extends CI_Model {

	function __construct() {
		parent::__construct();
	}



	public function insert($table_name,$post_data)
	{

		$return = $this->db->insert($table_name, $post_data);
		return $return;

	}
	public function fetch_hostings()

	{
		$id="2";
		/*$this->db->select ('*');
		$this->db->from('hosting');
		$this->db->where('host_type',$id);*/
		$query="select h1.id, h1.host_type, h1.fname, h1.lname, h1.phone, h1.email, h1.hostgender, h1.status, h1.street1, h1.street2, h1.state, h1.city, h1.country, h1.postcode, h1.description, h1.title, h1.email_verified, h1.nationality, h1.mother_tongue, h1.age_group, h1.ethinicity, h1.kitchen, h1.avail_from, h1.area, h1.bathroom, h1.bedroom, h1.pantry, h1.pool, h1.living_room, h1.backyard, h1.laundry_service, h1.renter_ethnicity, h1.rent_gender, 	h1.renter_age_group, h1.renter_type, h1.food_service, h1.renter_type1, h1.user_id, h1.date, h1.status1, h2.id as image_tbl_id, h2.hosting_id, h2.image_name from hosting h1, hosting_image h2 where h1.host_type='$id' and h1.id=h2.hosting_id  group by h2.hosting_id";
		$query=$this->db->query($query);
		/*$query = $this->db->get();*/
		$result = $query->result();
		return $result;

	}
	public function browse_num_rows()
	{
			$query = $this->db
            ->select('*')
            ->from('user_contact')
            ->get();
            return $query->num_rows();
	}
	public function browse_list($limit, $offset)
	{
		$this->db->from('user_contact');
		$this->db->order_by("ContactId DESC");
		$this->db->limit( $limit, $offset);
		$query = $this->db->get(); 
		$result = $query->result();
		return $result;
	}
	public function filter_browse_num_rows($search_data)
	{
		$country=$search_data['country'];
		$state=$search_data['state'];
		$city=$search_data['city'];
		$where='';
		if($country!='')
		{
			$where='country = "'.$country.'"';
		}
		if($state!=0 && $where!='')
		{
			$where=$where.' AND state ="'.$state.'"';
		}
		elseif($state!=0 && $where=='')
		{
			$where=$where.' state = "'.$state.'"';
		}

		if($city!=0 && $where!='')
		{
			$where=$where.' AND city = "'.$city.'"';
		}
		elseif($city!=0 && $where=='')
		{
			$where=$where.' city = "'.$city.'"';
		}

		$query="select * from user_contact where $where ORDER BY ContactId DESC";
		$query=$this->db->query($query);
         return $query->num_rows();
	}
	public function filter_browse_list($search_data, $limit, $offset)
	{
		$country=$search_data['country'];
		$state=$search_data['state'];
		$city=$search_data['city'];
		$where='';
		if($country!='')
		{
			$where='country = "'.$country.'"';
		}
		if($state!=0 && $where!='')
		{
			$where=$where.' AND state ="'.$state.'"';
		}
		elseif($state!=0 && $where=='')
		{
			$where=$where.' state = "'.$state.'"';
		}

		if($city!=0 && $where!='')
		{
			$where=$where.' AND city = "'.$city.'"';
		}
		elseif($city!=0 && $where=='')
		{
			$where=$where.' city = "'.$city.'"';
		}

		$query="select * from user_contact where $where ORDER BY ContactId DESC";
		$this->db->limit($limit, $offset);
		$query=$this->db->query($query);
		$result = $query->result();
		return $result;
	}
	public function fetch_adv_hostings()
	{
		$id="1";
		/*$this->db->select ('*');
		$this->db->from('hosting');
		$this->db->where('host_type',$id);
		$query = $this->db->get();*/
		$query="select h1.id, h1.host_type, h1.fname, h1.lname, h1.phone, h1.email, h1.hostgender, h1.status, h1.street1, h1.street2, h1.state, h1.city, h1.country, h1.postcode, h1.description, h1.title, h1.email_verified, h1.nationality, h1.mother_tongue, h1.age_group, h1.ethinicity, h1.kitchen, h1.avail_from, h1.area, h1.bathroom, h1.bedroom, h1.pantry, h1.pool, h1.living_room, h1.backyard, h1.laundry_service, h1.renter_ethnicity, h1.rent_gender, 	h1.renter_age_group, h1.renter_type, h1.food_service, h1.renter_type1, h1.user_id, h1.date, h1.status1, h2.id as image_tbl_id, h2.hosting_id, h2.image_name from hosting h1, hosting_image h2 where h1.host_type='$id' and h1.id=h2.hosting_id group by h2.hosting_id";
		$result=$this->db->query($query);
		$result = $result->result();
		return $result;
	}

public function fetch_details($id)
	{
		$this->db->select ('*');
		$this->db->from('hosting');
		$this->db->where('id',$id);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
public function fetch_image($id)
{
	$this->db->select('*');
	$this->db->from('hosting_image');
	$this->db->where('hosting_id',$id);
	$query=$this->db->get();
	$result=$query->result_array();
	return $result;
}
public function fetch_similar_prop($id){
	$query1="select country from hosting where id='$id'";
	$query2=$this->db->query($query1);
	$area=$query2->result_array();
	$country=$area[0]['country'];
	$query="select h1.id, h1.host_type, h1.fname, h1.lname, h1.phone, h1.email, h1.hostgender, h1.status, h1.street1, h1.street2, h1.state, h1.city, h1.country, h1.postcode, h1.description, h1.title, h1.email_verified, h1.nationality, h1.mother_tongue, h1.age_group, h1.ethinicity, h1.kitchen, h1.avail_from, h1.area, h1.bathroom, h1.bedroom, h1.pantry, h1.pool, h1.living_room, h1.backyard, h1.laundry_service, h1.renter_ethnicity, h1.rent_gender, 	h1.renter_age_group, h1.renter_type, h1.food_service, h1.renter_type1, h1.user_id, h1.date, h1.status1, h2.id as image_tbl_id, h2.hosting_id, h2.image_name from hosting h1, hosting_image h2  where h1.country='$country' and h1.id=h2.hosting_id group by h2.hosting_id ORDER BY h1.id LIMIT 3";

	$query=$this->db->query($query);

	$result=$query->result_array();
	
	return $result;
}
public function insert_get_id_of_prop($table_name,$post_data)
{
	$this->db->insert($table_name, $post_data);
   	$insert_id = $this->db->insert_id();
   	return  $insert_id;
}
public function insert_img_name($image_name, $hosting_id)
{
	$mydate = date('Y-m-d h:i:s');
	$query="insert into hosting_image (hosting_id, image_name, date_time) values ('$hosting_id','$image_name', '$mydate')";
	$result=$this->db->query($query);
	return $result;
}


}