<?php
class Search_model extends CI_Model{

	
	public function search_data(){
		 /*$name=$this->input->post('search_key');
		 $state=$this->input->post('state');
		 $city=$this->input->post('city');
		 $zipcode=$this->input->post('zipcode');	 
		 $languages=$this->input->post('languages');
		 $country=$this->input->post('country'); 
         $category_list=$this->input->post('category_list');
		 $gender=$this->input->post('gender');
		 $age_group=$this->input->post('age_group');
		 $nationality=$this->input->post('nationality');	 
		 $ethinicity=$this->input->post('ethinicity');
		 $this->db->like(array('state'=>$state,'description'=>$name,'hostgender'=>$gender,'renter_age_group'=>$age_group,'nationality'=>$nationality,'renter_ethnicity'=>$ethinicity,'postcode'=>$zipcode,'city'=>$city,'country'=>$country,'mother_tongue'=>$languages));
    	$query=$this->db->get('hosting');*/
    	$query="SELECT h1.id, 
                       h1.host_type, 
                       h1.fname, 
                       h1.lname, 
                       h1.phone, 
                       h1.email, 
                       h1.hostgender, 
                       h1.status, 
                       h1.street1, 
                       h1.street2, 
                       h1.state, 
                       h1.city, 
                       h1.country, 
                       h1.postcode, 
                       h1.description, 
                       h1.title, 
                       h1.email_verified, 
                       h1.nationality, 
                       h1.mother_tongue, 
                       h1.age_group, 
                       h1.ethinicity, 
                       h1.kitchen, 
                       h1.avail_from, 
                       h1.area, 
                       h1.bathroom, 
                       h1.bedroom, 
                       h1.pantry, 
                       h1.pool, 
                       h1.living_room, 
                       h1.backyard, 
                       h1.laundry_service, 
                       h1.renter_ethnicity, 
                       h1.rent_gender, 
                       h1.renter_age_group, 
                       h1.renter_type, 
                       h1.food_service, 
                       h1.renter_type1, 
                       h1.user_id, 
                       h1.date, 
                       h1.status1, 
                       h2.id as image_tbl_id, 
                       h2.hosting_id, 
                       h2.image_name  
                FROM hosting h1, hosting_image h2 
                WHERE h1.host_type=2 AND h1.id=h2.hosting_id  
                GROUP BY h2.hosting_id";

    	$query=$this->db->query($query);

    	return $query->result();
	}
	public function adv_search_data(){
		 /*$name=$this->input->post('search_key');
		 $state=$this->input->post('state');
		 $city=$this->input->post('city');
		 $zipcode=$this->input->post('zipcode');	 
		 $languages=$this->input->post('languages');
		 $country=$this->input->post('country');
            

         $category_list=$this->input->post('category_list');
		 $gender=$this->input->post('gender');
		 $age_group=$this->input->post('age_group');
		 $nationality=$this->input->post('nationality');	 
		 $ethinicity=$this->input->post('ethinicity');
		 
		 
		 $this->db->like(array('state'=>$state,'city'=>$city,'country'=>$country,'mother_tongue'=>$languages));
    	$query=$this->db->get('hosting');
    	
    	
    	return $query->result();*/
    	$query="select h1.id, h1.host_type, h1.fname, h1.lname, h1.phone, h1.email, h1.hostgender, h1.status, h1.street1, h1.street2, h1.state, h1.city, h1.country, h1.postcode, h1.description, h1.title, h1.email_verified, h1.nationality, h1.mother_tongue, h1.age_group, h1.ethinicity, h1.kitchen, h1.avail_from, h1.area, h1.bathroom, h1.bedroom, h1.pantry, h1.pool, h1.living_room, h1.backyard, h1.laundry_service, h1.renter_ethnicity, h1.rent_gender, 	h1.renter_age_group, h1.renter_type, h1.food_service, h1.renter_type1, h1.user_id, h1.date, h1.status1, h2.id as image_tbl_id, h2.hosting_id, h2.image_name from hosting h1, hosting_image h2 where h1.host_type=1 and h1.id=h2.hosting_id  group by h2.hosting_id";
    	$query=$this->db->query($query);
    	return $query->result();
	}
	public function search_data_id(){
		$id=$this->input->post('searchID');
		$query=$this->db->where('user_id',$id)->get('registration');
		return $query->row();
	}
	public function search_user(){
		$gender=$this->input->post('gender');
		$city=$this->input->post('city');
		$caste=$this->input->post('caste');	 
		$from=$this->input->post('date_from');	 
		$to=$this->input->post('date_to');
		/*if(!empty($city)){
			$this->db->where('gender',$gender)->where("(dob BETWEEN $from-01-01 AND $to-01-01)")->like(array('city'=>$city,));
		}
		elseif(!empty($caste)){
			$this->db->where('gender',$gender)->where("(dob BETWEEN $from-01-01 AND $to-01-01)")->like(array('caste'=>$caste));
		}
		elseif(!empty($city) && !empty($caste)){
			$this->db->where('gender',$gender)->where("(dob BETWEEN $from-01-01 AND $to-01-01)")->like(array('city'=>$city,'caste'=>$caste));
		}
		else
		{
			$like=' ';
		}*/
		$this->db->where('gender',$gender)->like(array('city'=>$city,'caste'=>$caste));
    	$query=$this->db->get('registration');
    	return $query->result();
	}
	public function basic_filter_data($data)
	{
		/*$this->load->model('Admin_model');*/
		
		
		$country=$data['country'];
		
		$state=$data['state'];
		
		$city=$data['city'];
		
		$zip=$data['zip'];
		
		
		$where='';


		/*if($keywords!='')
		{
			$where='CONCAT_WS(h1.title, h1.description) LIKE "%'.$keywords.'%" ';
		}*/
		if($country!='' && $where!='')
		{
			$where=$where.' AND h1.country = "'.$country.'"';
		}
		elseif($country!='' && $where=='')
		{
			$where=$where.' h1.country = "'.$country.'"';
		}

		if($state!=0 && $where!='')
		{
			$where=$where.' AND h1.state = "'.$state.'"';
		}
		elseif($state!=0 && $where=='')
		{
			$where=$where.' h1.state = "'.$state.'"';
		}

		if($city!=0 && $where!='')
		{
			$where=$where.' AND h1.city = "'.$city.'"';
		}
		elseif($city!=0 && $where=='')
		{
			$where=$where.' h1.city = "'.$city.'"';
		}

		if($zip!='' && $where!='')
		{
			$where=$where.' AND h1.postcode = "'.$zip.'"';
		}
		elseif($zip!='' && $where=='')
		{
			$where=$where.' h1.postcode = "'.$zip.'"';
		}

		$query="Select * from hosting h1 LEFT JOIN hosting_image h2 on h1.id = h2.hosting_id WHERE ($where) and host_type=2";
		
		//$query="Select * from hosting h1 inner join hosting_image h2 on h1.id = h2.hosting_id WHERE ($where) and host_type=2";

		//select * from hosting h1, hosting_image h2 from hosting WHERE $where and host_type=2 and h1.id=h2.hosting_id group by h2.hosting_id"
		$result=$this->db->query($query);
		return $result->result();
		
	}
	public function adv_filter_data($data)
	{
		/*$this->load->model('Admin_model');*/
		
		//$keywords=$data['key_words'];

		$country = $data['country'];
		$state = $data['state'];
		$city = $data['city'];
		$zip = $data['zip'];
		$languages= $data['languages'];
		$gender = $data['renterCriteriaGender'];
		$ageGroup = $data['renterCriteriaAgeGroup'];
		$nationality = $data['renterCriteriaNationality'];
		$ethnicity = $data['renterCriteriaEthnicity'];
		$rentersCategory = $data['renterCriteriaCategory'];

		$where='';


		/*if($keywords!='')
		{
			$where='h1.street1 LIKE "%'.$keywords.'%" OR h1.street2 LIKE "%'.$keywords.'%"';
		}*/
		if($country !='' && $where!='')
		{
		    if($country != 'menu_order')
		    {
		      $where=$where.' AND h1.country="'.$country.'"';
		    }
		}
		elseif($country!='' && $where=='')
		{
		    if($country != 'menu_order')
		    {
		        $where=$where.' h1.country = "'.$country.'"';
		    }
		}

		if($state!=0 && $where!='')
		{
			$where=$where.' AND h1.state = "'.$state.'"';
		}
		elseif($state!=0 && $where=='')
		{
			$where=$where.' h1.state = "'.$state.'"';
		}

		if($city!=0 && $where!='')
		{
			$where=$where.' AND h1.city = "'.$city.'"';
		}
		elseif($city!=0 && $where=='')
		{
			$where=$where.' h1.city = "'.$city.'"';
		}

		if($zip!='' && $where!='')
		{
			$where=$where.' AND h1.postcode = "'.$zip.'"';
		}
		elseif($zip!='' && $where=='')
		{
			$where=$where.' h1.postcode = "'.$zip.'"';
		}

		if($gender != '' && $where == '')
		{
		    if($gender != 'Any')
		    {
		        $where .= ' h1.hostgender = "'.$gender.'"';
		    }
		}
		elseif($gender != '' && $where != '')
		{
		    if($gender != 'Any')
		    {
		        $where .= ' AND h1.hostgender = "'.$gender.'"';
		    }
		}

		if($ageGroup != '' && $where == '')
		{
		    if($ageGroup != 'Any')
		    {
		        $where .= ' h1.age_group = "'.$ageGroup.'"';
		    }
		}
		elseif($ageGroup != '' && $where != '')
		{
		    if($ageGroup != 'Any')
		    {
		      $where .= ' AND h1.age_group = "'.$ageGroup.'"';
		    }
		}

		if($nationality != '' && $where == '')
		{
		    if($nationality != 'Any')
		    {
		      $where .= ' h1.nationality = "'.$nationality.'"';
		    }
		}
		elseif($nationality != '' && $where != '')
		{
		    if($nationality != 'Any')
		    {
		        $where .= ' AND h1.nationality = "'.$nationality.'"';
		    }
		}

		if($ethnicity != '' && $where == '')
		{
		    if($ethnicity != 'Any')
		    {
		      $where .= ' h1.renter_ethnicity = "'.$ethnicity.'"';
		    }
		}
		elseif($ethnicity != '' && $where != '')
		{
		    if($ethnicity != 'Any')
		    {
		        $where .= ' AND h1.renter_ethnicity = "'.$ethnicity.'"';
		    }
		}
		
		if($rentersCategory != '' && $where == '')
		{
		    if($rentersCategory != 'Any')
		    {
		      $where .= ' h1.renter_type = "'.$rentersCategory.'"';
		    }
		}
		elseif($rentersCategory != '' && $where != '')
		{
		    if($rentersCategory != 'Any')
		    {
		      $where .= ' AND h1.renter_type = "'.$rentersCategory.'"';
		    }
		}
		
		if($languages!='' && $where!='')
		{
		    if($languages != 'Any')
		    {
		      $where=$where.' AND h1.mother_tongue = "'.$languages.'"';
		    }
		}
		elseif($languages!='' && $where=='')
		{
		    if($languages != 'Any')
		    {
		      $where=$where.' h1.mother_tongue = "'.$languages.'"';
		    }
		}

		if($where == '')
		{
		    $query="SELECT h1.id,
                           h1.title,
                           h1.description,
                           h1.area,
                           h1.bathroom,
                           h1.bedroom,
                           h1.backyard,
                           h2.hosting_id,
                           h2.image_name 
                    FROM hosting h1 
                    LEFT JOIN hosting_image h2 
                    ON h1.id = h2.hosting_id 
                    WHERE host_type=1";
		}
		else
		{
		    $query="SELECT h1.id,
                           h1.title,
                           h1.description,
                           h1.area,
                           h1.bathroom,
                           h1.bedroom,
                           h1.backyard,
                           h2.hosting_id,
                           h2.image_name
                    FROM hosting h1 
                    LEFT JOIN hosting_image h2 
                    ON h1.id = h2.hosting_id 
                    WHERE ($where) and host_type=1";
		}
		
		$query .= " GROUP BY h1.id";

		//select * from hosting h1, hosting_image h2 WHERE $where and host_type=1  and h1.id=h2.hosting_id group by h2.hosting_id";
		$result=$this->db->query($query);
		return $result->result();
		
	}
	
}
?>