<?php 
class Country_model extends CI_Model{
	function __construct() {
        parent::__construct();
   }
   
function select_country(){
		$this->db->select('*');
		$this->db->from('countries');
		$this->db->order_by('count_name', 'ASC');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
function select_state($id)
     {
        $this->db ->select('*');
        $this->db->where('country_id',$id);
        $query=$this->db->get('states');
        $result = $query->result();
		return $result;
     }
function select_city($id)
     {
        $this->db->select('*');
        $this->db->where('state_id', $id);
        $query=$this->db->get('cities');
        $result = $query->result();
		return $result;
     }
	 
function show_country_view($country){
		$this->db->select('*');
		$this->db->from('countries');
		$this->db->where('count_id', $country);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
function show_state_view($state)
     {
        $this->db->select('*');
		$this->db->from('states');
		$this->db->where('state_id', $state);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
     }
function show_city_view($city)
     {
        $this->db->select('*');
		$this->db->from('cities');
		$this->db->where('city_id', $city);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
     }	 	 
	 

}

?>