<?php //include "header.php";?>
<!-- Banner Start -->
<div class="row">
<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
<div class="banner_wrapper revolution_slider" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="100">
	<div class="banner_overlay"></div>
    <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
    	<div class="carousel-inner">

    		<style>
    			
			.first_revoslide{
			    background-image: url(http://www.spacesharings.com/wp-content/uploads/2017/09/bg_slide_1.jpg);
			    background-size: cover;
			    background-position: center center;
			    
			}

			.third_revoslide

			{

background-image: url(http://www.spacesharings.com/wp-content/uploads/2017/09/bg_slide_1.jpg);
			    background-size: cover;
			    background-position: center center;
			    


			}
			.second_revoslide

			{

background-image: url(http://www.spacesharings.com/wp-content/uploads/2017/09/bg_slide_1.jpg);
			    background-size: cover;
			    background-position: center center;
			    


			}

    		</style>
    		<div class="item active first_revoslide">
    			<div class="container" style="height:424px;">
            	<div class="slide-description1  animated fadeInLeft col-sm-5">
		            <h3 class="first_revoslide_h animated fadeIn" style="text-align: left;">Lonely Empty Nester with rooms to spare?</h3>
		            <p class="first_revoslide_p" style="text-align: left;"> 
Find your ideal renter while earning extra cash and enjoying new company</p>
		            <span>$3,850 Per Month</span>                                    
		            <div class="btn_wrapper1"><a href="" class="btn rs_btn">read more</a></div>
		        </div>
		        <div class="col-sm-3 revo_img animated bounceInDown">
		        	<img src="<?php echo base_url('assets/images/revoslider_5.jpg'); ?>" alt="" data-no-retina="" style="">	
		        </div>
		        </div>
    		</div>
    		<div class="item third_revoslide">

      			<div class="container" style="height:424px;">
	            	<div class="slide-description3 animated fadeInRight col-sm-5">
               
			            <h3 class="third_revoslide_h" style="text-align: left;">Lonely Empty Nester with rooms to spare?</h3>
			            <p class="third_revoslide_p" style="text-align: left;">
Find your ideal renter while earning extra cash and enjoying new company</p>
			            <span>$3,850 Per Month</span>                                    
			            <div class="btn_wrapper1"><a href="" class="btn rs_btn">read more</a></div>
			        </div>
                    <div class="col-sm-3 revo_img1 animated bounceInDown">
			        	<img src="<?php echo base_url('assets/images/sofa3.png'); ?>" alt="" data-no-retina="" style="">	
			        </div>
		           
                </div>
    		</div>
  		</div>

		<a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> 
		<a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a> 
	</div>
</div>

</div>

<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 " style="background: #2b2525;
    margin-left: -15px;
    padding-top: 52px;">
				
						
					<!--<div class="testimonial_wrapper padding_right50">-->
			<div class="col-lg-12 col-md-12"> 
			<div class="testimonial_slider">
				<div class="owl-carousel owl-theme">
				<div class="item">
					<div class="icon_wrapper1">
						<img class="img-circle" src="<?php echo base_url('assets/images/testimonial_4.png');?>" alt="">
					</div>
					<div class="testimonial_content">

						<i class="flaticon-right176"></i>
						<p>Praesent mauris fusce nec tellus sed augue semper porta.Mauris massa. Vestibulum lacinia arcu eget nulla quis sem at nibhus element ntum sed nisi imperdiet.</p>
						<span>Lohn Anderson</span>
					</div>
				</div>
				<div class="item">

					<div class="icon_wrapper1" style=".icon_wrapper1 {
    float: left;
    display: inline-block;
    margin: 0 0 0 30%;
    text-align: center;
    border-radius: 60px;
    position: relative;
}">
						
					
						<img class="img-circle" src="<?php echo base_url('assets/images/testimonial_4.png');?>" alt="">
					</div>
					<div class="testimonial_content">
						<i class="flaticon-right176"></i>
						<p>Praesent mauris fusce nec tellus sed augue semper porta.Mauris massa. Vestibulum lacinia arcu eget nulla quis sem at nibhus element ntum sed nisi imperdiet.</p>
						<span>Lohn Anderson</span>
					</div>
				</div>
				<div class="item">
					<div class="icon_wrapper1">
						<img class="img-circle" src="<?php echo base_url('assets/images/testimonial_2.png');?>" alt="">
					</div>
					<div class="testimonial_content">
						<i class="flaticon-right176"></i>
						<p>Praesent mauris fusce nec tellus sed augue semper porta.Mauris massa. Vestibulum lacinia arcu eget nulla quis sem at nibhus element ntum sed nisi imperdiet.</p>
						<span>Lohn Anderson</span>
					</div>
				</div>
				<div class="item">
					<div class="icon_wrapper1">
						<img class="img-circle" src="<?php echo base_url('assets/images/testimonial_4.png');?>" alt="">
					</div>
					<div class="testimonial_content">
						<i class="flaticon-right176"></i>
						<p>Praesent mauris fusce nec tellus sed augue semper porta.Mauris massa. Vestibulum lacinia arcu eget nulla quis sem at nibhus element ntum sed nisi imperdiet.</p>
						<span>Lohn Anderson</span>
					</div>
				</div>
			</div>
			<!--</div>--> 
		</div>
	</div>
						
							
					
				
			</div>
			







</div>
<!-- Banner End -->

<!-- Top Rated Wrapper Start -->
<div class="top_rated_wraper">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="rs_heading_wrapper">
					<div class="rs_heading">
						<h3>Welcome to Space Share - Share Space, Enrich Lives, Earn Money!</h3>
						
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 rpw1">
				
						
					
						
							<h5>About Us - We are the "UBER" of Space-Share service. What Uber did in ride-share space, Space Share does the same in property sharing space.</h5>
							<p>Aim is to bring the right match between the property owner or host and the renter or paying guest. Not all the properties shown here are available for rental at this time. Please go to the Rental section to search for available properties.</p>
						
						
							
							<div class="btn_wrapper"><a href="property_detail1.html" class="btn rs_btn">read more</a></div>
					
				
			</div>
			
			
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 rpw1">
				
						
							<div class="img_wrapper">
								<img src="<?php echo base_url('assets/images/blog/blog.png');?>" alt="">
							</div>
						
					
				
			</div>
       </div>
	</div>
</div>
			
<?php //include "footer.php"?>
