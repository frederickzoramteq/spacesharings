
<?php //include "header.php";?>


<div class="property_view_wrapper">
  <div class="container">
    <div class="row">
      <div class="col-lg-9 col-md-8">
        <div class="row">
          <div class="col-lg-12">
            <div class="rs_heading_wrapper">
              <div class="rs_heading">
                <div class="col-lg-10 col-md-10 col-sm-10 padding">
                  <h3 class="property_view_h">Property Listings - Grid View</h3>
                  <p class="property_view_p">Nulla metus metus, ullamcorper vel, tincidunt sed euismod.</p>
                </div>
                <div class="col-lg-2 col-md-2s col-sm-2 padding">
				  <ul class="view_button">
					<li><a href="#" id="grid" class="active"><i class="flaticon-keyboard50"></i></a></li>
					<li><a href="#" id="list"><i class="flaticon-list95"></i></a></li>
				  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
        <div class="grid_view animated fadeIn">
          <div class="col-lg-4 col-md-6 col-sm-4 col-xs-6">
            <div class="property_wrapper">
              <div class="property_img_wrapper"> <div class="property1"><img  src="<?php echo base_url('assets/images/01.jpg');?>" alt=""></div>
                <div class="tag_sale">sale</div>
                <div class="tag_price">$24000</div>
              </div>
              <div class="property_detail">
                <div class="property_content">
                  <h5><a href="property_detail1.html">Property Title Here</a></h5>
                  <p>Phasellus in egestas libero, congue lacus. Cras vel lacus nisiduis.</p>
                </div>
                <ul>
                  <li><i class="flaticon-drawing18"></i>750 SQFT</li>
                  <li><i class="flaticon-bathtub3"></i>2 bath</li>
                  <li><i class="flaticon-bed40"></i>2 beds</li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-4 col-xs-6">
            <div class="property_wrapper">
              <div class="property_img_wrapper "> <div class="property1"><img  src="<?php echo base_url('assets/images/02.jpg');?>" alt=""></div>
                <div class="tag_sale">sale</div>
                <div class="tag_price">$24000</div>
              </div>
              <div class="property_detail">
                <div class="property_content">
                  <h5><a href="property_detail1.html">Property Title Here</a></h5>
                  <p>Phasellus in egestas libero, congue lacus. Cras vel lacus nisiduis.</p>
                </div>
                <ul>
                  <li><i class="flaticon-drawing18"></i>750 SQFT</li>
                  <li><i class="flaticon-bathtub3"></i>2 bath</li>
                  <li><i class="flaticon-bed40"></i>2 beds</li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-4 col-xs-6">
            <div class="property_wrapper">
              <div class="property_img_wrapper "> <div class="property1"><img  src="<?php echo base_url('assets/images/03.jpg');?>" alt=""></div>
                <div class="tag_sale">sale</div>
                <div class="tag_price">$24000</div>
              </div>
              <div class="property_detail">
                <div class="property_content">
                  <h5><a href="property_detail1.html">Property Title Here</a></h5>
                  <p>Phasellus in egestas libero, congue lacus. Cras vel lacus nisiduis.</p>
                </div>
                <ul>
                  <li><i class="flaticon-drawing18"></i>750 SQFT</li>
                  <li><i class="flaticon-bathtub3"></i>2 bath</li>
                  <li><i class="flaticon-bed40"></i>2 beds</li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-4 col-xs-6">
            <div class="property_wrapper">
              <div class="property_img_wrapper "> <div class="property1"><img  src="<?php echo base_url('assets/images/04.jpg');?>" alt=""></div>
                <div class="tag_sale">sale</div>
                <div class="tag_price">$24000</div>
              </div>
              <div class="property_detail">
                <div class="property_content">
                  <h5><a href="property_detail1.html">Property Title Here</a></h5>
                  <p>Phasellus in egestas libero, congue lacus. Cras vel lacus nisiduis.</p>
                </div>
                <ul>
                  <li><i class="flaticon-drawing18"></i>750 SQFT</li>
                  <li><i class="flaticon-bathtub3"></i>2 bath</li>
                  <li><i class="flaticon-bed40"></i>2 beds</li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-4 col-xs-6">
            <div class="property_wrapper">
              <div class="property_img_wrapper "> <div class="property1"><img  src="<?php echo base_url('assets/images/05.jpg');?>" alt=""></div>
                <div class="tag_sale">sale</div>
                <div class="tag_price">$24000</div>
              </div>
              <div class="property_detail">
                <div class="property_content">
                  <h5><a href="property_detail1.html">Property Title Here</a></h5>
                  <p>Phasellus in egestas libero, congue lacus. Cras vel lacus nisiduis.</p>
                </div>
                <ul>
                  <li><i class="flaticon-drawing18"></i>750 SQFT</li>
                  <li><i class="flaticon-bathtub3"></i>2 bath</li>
                  <li><i class="flaticon-bed40"></i>2 beds</li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-4 col-xs-6">
            <div class="property_wrapper">
              <div class="property_img_wrapper "> <div class="property1"><img  src="<?php echo base_url('assets/images/06.jpg');?>" alt=""></div>
                <div class="tag_sale">sale</div>
                <div class="tag_price">$24000</div>
              </div>
              <div class="property_detail">
                <div class="property_content">
                  <h5><a href="property_detail1.html">Property Title Here</a></h5>
                  <p>Phasellus in egestas libero, congue lacus. Cras vel lacus nisiduis.</p>
                </div>
                <ul>
                  <li><i class="flaticon-drawing18"></i>750 SQFT</li>
                  <li><i class="flaticon-bathtub3"></i>2 bath</li>
                  <li><i class="flaticon-bed40"></i>2 beds</li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-4 col-xs-6">
            <div class="property_wrapper">
              <div class="property_img_wrapper "> <div class="property1"><img  src="<?php echo base_url('assets/images/07.jpg');?>" alt=""></div>
                <div class="tag_sale">sale</div>
                <div class="tag_price">$24000</div>
              </div>
              <div class="property_detail">
                <div class="property_content">
                  <h5><a href="property_detail1.html">Property Title Here</a></h5>
                  <p>Phasellus in egestas libero, congue lacus. Cras vel lacus nisiduis.</p>
                </div>
                <ul>
                  <li><i class="flaticon-drawing18"></i>750 SQFT</li>
                  <li><i class="flaticon-bathtub3"></i>2 bath</li>
                  <li><i class="flaticon-bed40"></i>2 beds</li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-4 col-xs-6">
            <div class="property_wrapper">
              <div class="property_img_wrapper "> <div class="property1"><img  src="<?php echo base_url('assets/images/08.jpg');?>" alt=""></div>
                <div class="tag_sale">sale</div>
                <div class="tag_price">$24000</div>
              </div>
              <div class="property_detail">
                <div class="property_content">
                  <h5><a href="property_detail1.html">Property Title Here</a></h5>
                  <p>Phasellus in egestas libero, congue lacus. Cras vel lacus nisiduis.</p>
                </div>
                <ul>
                  <li><i class="flaticon-drawing18"></i>750 SQFT</li>
                  <li><i class="flaticon-bathtub3"></i>2 bath</li>
                  <li><i class="flaticon-bed40"></i>2 beds</li>
                </ul>
              </div>
            </div>
          </div>
		  <div class="col-lg-4 col-md-6 col-sm-4 col-xs-6">
            <div class="property_wrapper">
              <div class="property_img_wrapper "> <div class="property1"><img  src="<?php echo base_url('assets/images/07.jpg');?>" alt=""></div>
                <div class="tag_sale">sale</div>
                <div class="tag_price">$24000</div>
              </div>
              <div class="property_detail">
                <div class="property_content">
                  <h5><a href="property_detail1.html">Property Title Here</a></h5>
                  <p>Phasellus in egestas libero, congue lacus. Cras vel lacus nisiduis.</p>
                </div>
                <ul>
                  <li><i class="flaticon-drawing18"></i>750 SQFT</li>
                  <li><i class="flaticon-bathtub3"></i>2 bath</li>
                  <li><i class="flaticon-bed40"></i>2 beds</li>
                </ul>
              </div>
            </div>
          </div>
          </div>
		  <div class="list_view animated fadeIn">
			<div class="property_wrapper">
				  <div class="col-lg-5 col-md-5 col-sm-5"> 
					  <div class="property_img_wrapper "> <div class="property1"><img  src="<img  src="<?php echo base_url('assets/images/02.jpg');?>" alt=""></div>
						<div class="tag_sale_1">rent</div>
						<div class="tag_price">appartment</div>
					  </div>
				  </div>
				  <div class="col-lg-7 col-md-7 col-sm-7">
					  <div class="property_detail">
							<div class="property_content">
							  <h5><a href="property_detail2.html">789 park avenue</a></h5><span>$850 / Mo</span>
							  <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected umour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum,</p>
							</div>
							<ul>
							  <li><i class="flaticon-drawing18"></i>750 SQFT</li>
							  <li><i class="flaticon-bathtub3"></i>2 bath</li>
							  <li><i class="flaticon-bed40"></i>2 beds</li>
							  <li><i class="flaticon-car211"></i>2 garrage</li>
							</ul>
					  </div>
				  </div>
			 </div>
			 <div class="property_wrapper">
				  <div class="col-lg-5 col-md-5 col-sm-5"> 
					  <div class="property_img_wrapper "> <div class="property1"><img  src="<img  src="<?php echo base_url('assets/images/02.jpg');?>" alt=""></div>
						<div class="tag_sale_1">rent</div>
						<div class="tag_price">appartment</div>
					  </div>
				  </div>
				  <div class="col-lg-7 col-md-7 col-sm-7">
					  <div class="property_detail">
							<div class="property_content">
							  <h5><a href="property_detail2.html">789 park avenue</a></h5><span>$850 / Mo</span>
							  <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected umour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum,</p>
							</div>
							<ul>
							  <li><i class="flaticon-drawing18"></i>750 SQFT</li>
							  <li><i class="flaticon-bathtub3"></i>2 bath</li>
							  <li><i class="flaticon-bed40"></i>2 beds</li>
							  <li><i class="flaticon-car211"></i>2 garrage</li>
							</ul>
					  </div>
				  </div>
			 </div>
			 <div class="property_wrapper">
				  <div class="col-lg-5 col-md-5 col-sm-5"> 
					  <div class="property_img_wrapper "> <div class="property1"><img  src="<img  src="<?php echo base_url('assets/images/02.jpg');?>" alt=""></div>
						<div class="tag_sale_1">rent</div>
						<div class="tag_price">appartment</div>
					  </div>
				  </div>
				  <div class="col-lg-7 col-md-7 col-sm-7">
					  <div class="property_detail">
							<div class="property_content">
							  <h5><a href="property_detail2.html">789 park avenue</a></h5><span>$850 / Mo</span>
							  <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected umour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum,</p>
							</div>
							<ul>
							  <li><i class="flaticon-drawing18"></i>750 SQFT</li>
							  <li><i class="flaticon-bathtub3"></i>2 bath</li>
							  <li><i class="flaticon-bed40"></i>2 beds</li>
							  <li><i class="flaticon-car211"></i>2 garrage</li>
							</ul>
					  </div>
				  </div>
			 </div>
			 <div class="property_wrapper">
				  <div class="col-lg-5 col-md-5 col-sm-5"> 
					  <div class="property_img_wrapper "> <div class="property1"><img  src="<img  src="<?php echo base_url('assets/images/02.jpg');?>" alt=""></div>
						<div class="tag_sale_1">rent</div>
						<div class="tag_price">appartment</div>
					  </div>
				  </div>
				  <div class="col-lg-7 col-md-7 col-sm-7">
					  <div class="property_detail">
							<div class="property_content">
							  <h5><a href="property_detail2.html">789 park avenue</a></h5><span>$850 / Mo</span>
							  <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected umour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum,</p>
							</div>
							<ul>
							  <li><i class="flaticon-drawing18"></i>750 SQFT</li>
							  <li><i class="flaticon-bathtub3"></i>2 bath</li>
							  <li><i class="flaticon-bed40"></i>2 beds</li>
							  <li><i class="flaticon-car211"></i>2 garrage</li>
							</ul>
					  </div>
				  </div>
			 </div>
			 <div class="property_wrapper">
				  <div class="col-lg-5 col-md-5 col-sm-5"> 
					  <div class="property_img_wrapper "> <div class="property1"><img  src="<img  src="<?php echo base_url('assets/images/02.jpg');?>" alt=""></div>
						<div class="tag_sale_1">rent</div>
						<div class="tag_price">appartment</div>
					  </div>
				  </div>
				  <div class="col-lg-7 col-md-7 col-sm-7">
					  <div class="property_detail">
							<div class="property_content">
							  <h5><a href="property_detail2.html">789 park avenue</a></h5><span>$850 / Mo</span>
							  <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected umour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum,</p>
							</div>
							<ul>
							  <li><i class="flaticon-drawing18"></i>750 SQFT</li>
							  <li><i class="flaticon-bathtub3"></i>2 bath</li>
							  <li><i class="flaticon-bed40"></i>2 beds</li>
							  <li><i class="flaticon-car211"></i>2 garrage</li>
							</ul>
					  </div>
				  </div>
			 </div>
			 <div class="property_wrapper">
				  <div class="col-lg-5 col-md-5 col-sm-5"> 
					  <div class="property_img_wrapper "> <div class="property1"><img  src="<img  src="<?php echo base_url('assets/images/02.jpg');?>" alt=""></div>
						<div class="tag_sale_1">rent</div>
						<div class="tag_price">appartment</div>
					  </div>
				  </div>
				  <div class="col-lg-7 col-md-7 col-sm-7">
					  <div class="property_detail">
							<div class="property_content">
							  <h5><a href="property_detail2.html">789 park avenue</a></h5><span>$850 / Mo</span>
							  <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected umour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum,</p>
							</div>
							<ul>
							  <li><i class="flaticon-drawing18"></i>750 SQFT</li>
							  <li><i class="flaticon-bathtub3"></i>2 bath</li>
							  <li><i class="flaticon-bed40"></i>2 beds</li>
							  <li><i class="flaticon-car211"></i>2 garrage</li>
							</ul>
					  </div>
				  </div>
			 </div>
			 <div class="property_wrapper">
				  <div class="col-lg-5 col-md-5 col-sm-5"> 
					  <div class="property_img_wrapper "> <div class="property1"><img  src="<img  src="<?php echo base_url('assets/images/02.jpg');?>" alt=""></div>
						<div class="tag_sale_1">rent</div>
						<div class="tag_price">appartment</div>
					  </div>
				  </div>
				  <div class="col-lg-7 col-md-7 col-sm-7">
					  <div class="property_detail">
							<div class="property_content">
							  <h5><a href="property_detail2.html">789 park avenue</a></h5><span>$850 / Mo</span>
							  <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected umour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum,</p>
							</div>
							<ul>
							  <li><i class="flaticon-drawing18"></i>750 SQFT</li>
							  <li><i class="flaticon-bathtub3"></i>2 bath</li>
							  <li><i class="flaticon-bed40"></i>2 beds</li>
							  <li><i class="flaticon-car211"></i>2 garrage</li>
							</ul>
					  </div>
				  </div>
			 </div>
			 <div class="property_wrapper">
				  <div class="col-lg-5 col-md-5 col-sm-5"> 
					  <div class="property_img_wrapper "> <div class="property1"><img  src="<img  src="<?php echo base_url('assets/images/02.jpg');?>" alt=""></div>
						<div class="tag_sale_1">rent</div>
						<div class="tag_price">appartment</div>
					  </div>
				  </div>
				  <div class="col-lg-7 col-md-7 col-sm-7">
					  <div class="property_detail">
							<div class="property_content">
							  <h5><a href="property_detail2.html">789 park avenue</a></h5><span>$850 / Mo</span>
							  <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected umour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum,</p>
							</div>
							<ul>
							  <li><i class="flaticon-drawing18"></i>750 SQFT</li>
							  <li><i class="flaticon-bathtub3"></i>2 bath</li>
							  <li><i class="flaticon-bed40"></i>2 beds</li>
							  <li><i class="flaticon-car211"></i>2 garrage</li>
							</ul>
					  </div>
				  </div>
			 </div>
		  </div>
          <div class="col-lg-12">
            <nav class="pagger_wrapper">
              <ul class="pagination">
				<li>
				  <a href="#" aria-label="Previous">
					<span aria-hidden="true"><i class="flaticon-direction196"></i></span>
				  </a>
				</li>
				<li><a href="#">1</a></li>
				<li><a href="#">2</a></li>
				<li><a href="#">3</a></li>
				<li><a href="#">4</a></li>
				<li><a href="#">5</a></li>
				<li>
				  <a href="#" aria-label="Next">
					<span aria-hidden="true"><i class="flaticon-direction202"></i></span>
				  </a>
				</li>
			   </ul>
			</nav>
		  </div>
        </div>
      </div>
	  <div class="col-lg-3 col-md-4">
		  <div class="sidebar_wrapper">
			<div class="widget advance_search_wrapper">		
				<div class="widget-title"><h4>Basic search</h4></div>
				<div class="search_form">
					<form>
						<div class="form-group">
							<label>search by keyword</label>
							<input type="text" class="form-control" placeholder="enter your keywords">
						</div>
						<div class="form-group">
							<label>property location</label>
							<select name="orderby" class="orderby form-control select2">
								<option value="menu_order" selected="selected">Any</option>
								<option value="rating">Sort by average rating</option>
								<option value="date">Sort by newness</option>
								<option value="price">Sort by price: low to high</option>
							</select>
						</div>
						<div class="form-group">
							<label>property status</label>
							<select name="orderby" class="orderby form-control select2">
								<option value="menu_order" selected="selected">Any</option>
								<option value="rating">Sort by average rating</option>
								<option value="date">Sort by newness</option>
								<option value="price">Sort by price: low to high</option>
							</select>
						</div>
						<div class="form-group">
							<label>property type</label>
							<select name="orderby" class="orderby form-control select2">
								<option value="menu_order" selected="selected">Any</option>
								<option value="rating">Sort by average rating</option>
								<option value="date">Sort by newness</option>
								<option value="price">Sort by price: low to high</option>
							</select>
						</div>
						<div class="row">
							<div class="col-lg-6 col-md-6 form-group">
								<label>min. beds</label>
								<select name="orderby" class="orderby form-control select2">
									<option value="menu_order" selected="selected">Any</option>
									<option value="rating">1</option>
									<option value="date">2</option>
									<option value="price">3</option>
								</select>
							</div>
							<div class="col-lg-6 col-md-6 form-group">
								<label>min. baths</label>
								<select name="orderby" class="orderby form-control select2">
									<option value="menu_order" selected="selected">Any</option>
									<option value="rating">1</option>
									<option value="date">2</option>
									<option value="price">3</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6 col-md-6 form-group">
								<label>min. area </label>
								<select name="orderby" class="orderby form-control select2">
									<option value="menu_order" selected="selected">Any</option>
									<option value="rating">1</option>
									<option value="date">2</option>
									<option value="price">3</option>
								</select>
							</div>
							<div class="col-lg-6 col-md-6 form-group">
								<label>max.area </label>
								<select name="orderby" class="orderby form-control select2">
									<option value="menu_order" selected="selected">Any</option>
									<option value="rating">1</option>
									<option value="date">2</option>
									<option value="price">3</option>
								</select>
							</div>
						</div>
					
						<div class="btn_wrapper"><a href="#" class="btn rs_btn">search</a></div>
					</form>
				</div>
			</div>
			<div class="widget recent_properties">
				<div class="widget-title"><h4>recent properties</h4></div>
				<ul>
					<li>
						<div class="property_img"><img src="<?php echo base_url('assets/images/recent/3.jpg');?>" alt=""></div>
						<div class="recent_property_detail"><h6><a href="property_detail1.html">beautiful house</a></h6><p>Flishing, NY 112345</p><span>$2,00,000</span></div>
					</li>
					<li>
						<div class="property_img"><img src="<?php echo base_url('assets/images/recent/3.jpg');?>" alt=""></div>
						<div class="recent_property_detail"><h6><a href="property_detail1.html">beautiful house</a></h6><p>Flishing, NY 112345</p><span>$2,00,000</span></div>
					</li>
					<li>
						<div class="property_img"><img src="<?php echo base_url('assets/images/recent/3.jpg');?>" alt=""></div>
						<div class="recent_property_detail"><h6><a href="property_detail1.html">beautiful house</a></h6><p>Flishing, NY 112345</p><span>$2,00,000</span></div>
					</li>
				</ul>
			</div>
		  </div>
	  </div>
    </div><!--row-->
  </div>
</div>
<?php //include "footer.php";?>