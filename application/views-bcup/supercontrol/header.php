<?php
   ob_start();
   $a=$this->session->userdata;
   if($a['is_logged_in'] != 1)
   {
    header("location:".base_url().'supercontrol');
   }
   ?>
<!DOCTYPE html>
<html lang="en">
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   <head>
      <meta charset="utf-8" />
      <title>Spacesharing| Supercontrol</title>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1" name="viewport" />
      <meta content="" name="description" />
      <meta content="" name="author" />
      <!-- BEGIN GLOBAL MANDATORY STYLES -->
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
      <link href="<?php echo base_url(); ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo base_url(); ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo base_url(); ?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
      <!-- END GLOBAL MANDATORY STYLES -->
      <!-- BEGIN PAGE LEVEL PLUGINS -->
      <!-- END PAGE LEVEL PLUGINS -->
      <!-- BEGIN THEME GLOBAL STYLES -->
      <link href="<?php echo base_url(); ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
      <link href="<?php echo base_url(); ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
      <!-- END THEME GLOBAL STYLES -->
      <!-- BEGIN THEME LAYOUT STYLES -->
      <link href="<?php echo base_url(); ?>assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo base_url(); ?>assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
      <link href="<?php echo base_url(); ?>assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo base_url(); ?>css/newstyle.css" rel="stylesheet" type="text/css" />
      <!-- END THEME LAYOUT STYLES -->
      <link rel="shortcut icon" href="favicon.ico" />
   </head>
   <!-- END HEAD -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
   <script src="<?php echo base_url(); ?>assets/js/countries.js"></script>
   <script>
      $(document).ready(function(){
        /// make loader hidden in start
        $('#Loading').hide();
        $('#email').blur(function()
        {
          var a = $("#email").val();
        // alert('hi');
        var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
        // check if email is valid
        if(filter.test(a))
        {
        // show loader
        $('#Loading').show();
        $.post("<?php echo base_url()?>user/ifemailexist",
        {
          email: $('#email').val()
        }
        , function(response)
        {
          $('#Loading').hide();
          setTimeout("finishAjax('Loading', '"+escape(response)+"')", 400);
        });
        return false;
        }
        });
      });
      function finishAjax(id, response)
      {
        $('#'+id).html(unescape(response));
        $('#'+id).fadeIn();
      }
   </script>
   <script>
      $(document).ready(function(){
        /// make loader hidden in start
        $('#Loading1').hide();
        $('#username').blur(function()
        {
        var a = $("#username").val();
        // alert('hi');
        // var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
        // check if email is valid
        $('#Loading1').show();
        $.post("<?php echo base_url()?>user/ifusernamexist",
        {
        username: $('#username').val()
        }
        , function(response)
        {
        $('#Loading1').hide();
        setTimeout("finishAjax('Loading1', '"+escape(response)+"')", 400);
        });
        return false;
        });
      });

      function finishAjax(id, response)
      {
      $('#'+id).html(unescape(response));
      $('#'+id).fadeIn();
      if ( $('#kkkkkkkk').length )
      {
        document.getElementById('email').value = "";
      }

      if ($('#userex').length)
      {
        document.getElementById('username').value = "";
      }
      }

   </script>
   <script type="text/javascript">
      function fun1()
         {
           if ($('#userex').length)
           {
                document.getElementById('username').value = "";
      }


         if ( $('#kkkkkkkk').length )
           {
            document.getElementById('email').value = "";
      }
       }

   </script>
   <script type="text/javascript">
      function chkalpha(a) {
          // var ch = event || evt; // for trans-browser compatibility
          var char = a.which;

          if ((char >= 65 && char <= 90) || (char >= 97 && char <= 122) ||char==32) {
              return true;
          }

          else {
              return false;
          }
      }
          function chknumber(a) {
              // var ch = event || evt; // for trans-browser compatibility
              var char = a.which;

              if (char >= 48 && char <= 57) {
                  return true;
              }

              else {
                  return false;
              }
          }
              function chkaddr(a)
              {
                  // var ch = event || evt; // for trans-browser compatibility
                  var char = a.which;

                  if ((char >= 48 && char<=57)||(char>=65 && char<=90) ||(char>=97 && char<=122)||char==32 ||char==47 ||char==46)
                  {
                      return true;}

                  else {
                      return false;
                  }

      }

   </script>
   <script type="text/javascript">
      function getagent(id)
       {
           //alert('this id value :'+id);
           $.ajax({
               type: "POST",
               url: '<?php echo base_url().'user/getagentlist'.'/';?>'+id,
               data: id='cat_id',
               success: function(data){
                   //alert(data);
                   $('#sub2').html(data);
           },
      });
       }

   </script>
   <script type="text/javascript">
      function getstatedetails(id)
       {
           //alert('this id value :'+id);
           $.ajax({
               type: "POST",
               url: '<?php echo base_url().'user/ajax_state_list'.'/';?>'+id,
               data: id='cat_id',
               success: function(data){
                   //alert(data);
                   $('#sub').html(data);
           },
      });
       }

   </script>
   <script type="text/javascript">
      function getcitydetails(id)
       {
           //alert('this id value :'+id);
           $.ajax({
               type: "POST",
               url: '<?php echo base_url().'user/ajax_city_list'.'/';?>'+id,
               data: id='cat_id',
               success: function(data){
                   //alert(data);
                   $('#sub1').html(data);
           },
      });
       }

   </script>
   <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
      <!-- BEGIN HEADER -->
      <div class="page-header navbar navbar-fixed-top" style="background-color:#f99d2d !important;">
         <!-- BEGIN HEADER INNER -->
         <div class="page-header-inner ">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
               <a style="text-decoration:none;" href="<?php echo base_url(); ?>supercontrol/home">
                  <!-- <b style="font-family:'Times New Roman', Times, serif; color:#fff; font-size:18px;">
                     <?php echo @$title;?>
                     </b> -->
                  <img width="100px" src="<?= base_url('images/yes-logo.png')?>" alt="logo">
               </a>
               <div class="menu-toggler sidebar-toggler"> </div>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
               <ul class="nav navbar-nav pull-right">
                  <!-- BEGIN NOTIFICATION DROPDOWN -->
                  <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                  <li class="dropdown dropdown-user">
                     <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <b style="font-family:'Times New Roman', Times, serif; color:#FFF; font-size:18px;">Super Control Panel</b>
                        <!--<span class="username username-hide-on-mobile"> Nick </span>-->
                        <i class="fa fa-angle-down"></i>
                     </a>
                     <ul class="dropdown-menu dropdown-menu-default">
                        <li> <a href="<?php echo base_url();?>supercontrol/adminprofile/show_adminprofile_id/1"> <i class="icon-user"></i> My Profile </a> </li>
                        <li> <a href="<?php echo base_url()?>supercontrol/main/logout"> <i class="icon-key"></i> Log Out </a> </li>
                     </ul>
                  </li>
                  <!-- END USER LOGIN DROPDOWN -->
                  <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                  <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                  <!--<li class="dropdown dropdown-quick-sidebar-toggler"> <a href="<?php echo base_url(); ?>supercontrol/logout" class="dropdown-toggle"> <i class="icon-logout"></i> </a> </li>-->
                  <!-- END QUICK SIDEBAR TOGGLER -->
               </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
         </div>
         <!-- END HEADER INNER -->
      </div>
      <!-- END HEADER -->
      <!-- BEGIN HEADER & CONTENT DIVIDER -->
      <div class="clearfix"> </div>
      <!-- END HEADER & CONTENT DIVIDER -->