<!--<h2>Welcome <?php //echo $UserName; ?>!</h2>-->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">
        <?php $this->load->view('supercontrol/leftbar'); ?>
        <!-- END SIDEBAR -->
    </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END THEME PANEL -->
            <!-- BEGIN PAGE TITLE-->
            <h3 class="page-title"> <?php echo @$title;?>
                <!--<small>classic page head option</small>-->
            </h3>
            <!-- END PAGE TITLE-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li> <a href="dashboard.php">Home</a> <i class="fa fa-angle-right"></i> </li>
                    <li> <span><?php echo @$title;?></span> </li>
                </ul>
            </div>
            <!-- END PAGE BAR -->
            <!-- END PAGE HEADER-->
            <style type="text/css">
                .tile {
                    padding-top:0px;
                    height: 117px;
                    width: 184px !important;
                    position: relative;
                    overflow: hidden;
                    cursor:pointer;
                    border: 5px solid #ccc;
                }


                .tile .tile-body {
                    height: 100%;
                    overflow: hidden;
                    position: relative;
                }
                .tile:active, .tile.selected {
                    border-color: #ccc;
                }


                .tile.image > .tile-body > img{
                    width: 100%;
                    height: auto;
                    min-height: 100%;
                    max-width: 100%;
                }
            </style>
            <div class="alert alert-success alert-dismissable" style="padding:10px;">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button" style="right:0;"></button>
                <strong>
                    <?php
                    $last = end($this->uri->segments);
                    if($last=="successupdate"){echo "Data Updated Successfully ......";}
                    if($last=="successdelete"){echo "Data Deleted Successfully ......";}
                    ?>
                </strong>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PORTLET-->
                    <div class="portlet box blue-hoki" style="margin-top: 10px">
                        <div class="portlet-title">
                            <div class="caption"> <i class="fa fa-reorder"></i>
                                <?php echo @$title;?>
                            </div>
                            <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
                        </div>
                        <div class="portlet-body">

                            <div class="table-toolbar" style="margin-bottom:10px;">
                                <div class="btn-group">
                                    <a class="btn green" href="<?php echo base_url()?>supercontrol/album_songs/addAlbum" data-toggle="modal">
                                        Add New
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>

                            </div>
                            <!-- BEGIN FORM-->
                            <div class="table-toolbar"> </div>
                            <table class="table table-striped table-bordered table-hover table-checkable order-column dt-responsive" id="sample_1">
                                <thead>
                                <tr>
                                    <th width="2%"> Sl No. </th>
                                    <th width="10%"> Song Name </th>
                                    <th width="25%"> Album Name </th>
                                    <th width="25%"> Song </th>
                                    <th width="10%"> Genere </th>
                                    <th width="10%"> Status </th>
                                    <th width="8%"> Edit </th>
                                    <th width="12%"> Delete </th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php
                                $i=1;
                               foreach ($songs as $song){
                                    $sql="select album_name from album where id= $song->album_id";
                                    $query= $this->db->query($sql);
                                    $result= $query->result();
                                    $album_name= $result[0]->album_name;
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $song->song_name ?></td>
                                        <td><?php echo $album_name ?></td>
                                        <td>
                                            <audio controls autoplay>
                                                <source src="<?php echo base_url() ?>uploads/Songs/<?php echo $song->song_file ?>" type="audio/mp3">
                                                Your browser does not support the audio element.
                                            </audio>
                                        </td>
                                        <td><?php echo $song->genere ?></td>
                                        <td><?php if($song->status){echo 'Published';} else {echo 'Draft';}   ?></td>
                                        <td><a href="<?php echo base_url() ?>supercontrol/Album_songs/editSong/<?php echo $song->id ?>" class="btn btn-primary"> Edit</a></td>
                                        <td><a href="<?php echo base_url() ?>supercontrol/Album_songs/deleteSong/<?php echo $song->id  ?>" onclick="return confirm('Are you sure to delete this Album.')" class="btn btn-danger"> Delete </a></td>
                                    </tr>
                                    <?php $i++; } ?>
                                </tbody>
                            </table>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>

</div>
