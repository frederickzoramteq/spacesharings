<?php //$this->load->view ('header');?>
<!-- BEGIN CONTAINER -->

<div class="page-container">
  <!-- BEGIN SIDEBAR -->
  <div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
      <!-- BEGIN SIDEBAR MENU -->
      <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
      <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
      <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
      <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
      <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
      <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
      <?php $this->load->view ('supercontrol/leftbar');?>
      <!-- END SIDEBAR MENU -->
      <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
  <div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->
      <!-- END THEME PANEL -->
      <!-- BEGIN PAGE BAR -->
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <li> <a href="">Home</a> <i class="fa fa-circle"></i> </li>
          <li> <span>supercontrol Panel</span> </li>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions <i class="fa fa-angle-down"></i> </button>
            <ul class="dropdown-menu pull-right" role="menu">
              <li> <a href="#"> <i class="icon-bell"></i> Action</a> </li>
              <li> <a href="#"> <i class="icon-shield"></i> Another action</a> </li>
              <li> <a href="#"> <i class="icon-user"></i> Something else here</a> </li>
              <li class="divider"> </li>
              <li> <a href="#"> <i class="icon-bag"></i> Separated link</a> </li>
            </ul>
          </div>
        </div>
      </div>
      <?php
	  $last = end($this->uri->segment_array());
	  if($last=="imgup"){
	  ?>
      <div class="alert alert-success alert-dismissable" style="padding:10px;">
        <button class="close" aria-hidden="true" data-dismiss="alert" type="button" style="right:0;"></button>
        <strong>
        <?php 
				
				echo "Image Added Successfully ......";
				
            ?>
        </strong> </div>
      <?php }?>
       <?php
	  $last = end($this->uri->segment_array());
	  if($last=="imgdlt"){
	  ?>
      <div class="alert alert-success alert-dismissable" style="padding:10px;">
        <button class="close" aria-hidden="true" data-dismiss="alert" type="button" style="right:0;"></button>
        <strong>
        <?php 
				
				echo "Image Deleted Successfully ......";
				
            ?>
        </strong> </div>
      <?php }?>
      <!-- END PAGE BAR -->
      <!-- BEGIN PAGE TITLE-->
      <!-- END PAGE TITLE-->
      <!-- END PAGE HEADER-->
      <div class="row">
        <div class="col-md-12">
          <div class="tabbable-line boxless tabbable-reversed">
            <div class="tab-content">
              <div class="tab-pane active" id="tab_0">
                <div class="portlet box blue-hoki">
                  <div class="portlet-title">
                    <div class="caption"> <i class="fa fa-gift"></i>Add Property Images</div>
                    <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
                  </div>
                  <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <?php echo form_open_multipart('supercontrol/imageupload/doupload');?>
                    <div class="form-body" style="padding-top:50px">
                      <div class="form-group">
                        <label class="col-md-3 control-label">Images</label>
                        <div class="col-md-3">
                          <input name="userfile[]" id="userfile" type="file" multiple="multiple" class="form-control"  onchange="checkFile(this)" required/>
                          <input type="hidden" name="property_id" class="form-control" value="<?=$this->uri->segment(4); ?>">
                          <label>Please select jpg, png, jpeg or gif file.</label>
                        </div>
                      </div>
                    </div>
                    <div class="form-actions top" style="padding:35px;margin-left:50px">
                      <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                          <button type="submit" class="btn green">Submit</button>
                          <button type="button" class="btn default" onclick="location.href='<?=base_url(); ?>supercontrol/property/show_property'">Cancel</button>
                        </div>
                      </div>
                    </div>
                   
                    	<div class="form-group">
                      <label class="col-md-3 control-label">Mutiple Images : </label>
                      <?php
					  if($mulimg){
					  ?>
                      <div id="controls">
                        <?php   foreach($mulimg as $i){?>
                        <div style="width:24%;">
                          <?php   if($i->apartment_image == ''){
									$pic = base_url()."property_img/noimage.jpg" ;
								  } else{
								$pic = base_url()."property_img/".$i->apartment_image ; } 
						?>
                          <img src="<?=$pic ?>" alt="Image loading.." style="height:200px;" width="95%"/><a class="btn red btn-sm btn-outline sbold uppercase" href="<?php echo base_url()?>supercontrol/imageupload/delete_property_img/<?php echo $i->id?>/<?=$this->uri->segment(4); ?>" onclick="return confirm('Are you sure you want to delete ?');"><i class="fa fa-trash"></i> Delete</a> </div>
                        <?php }  ?>
                      </div>
                      <?php }?>
                      <style>
#controls {
    display: table;
    width: 100%;
    background: #ccc;
}
#controls > div {
    display: inline-block;
    padding: 1em;
    width: 25%;
	border:1px solid #fff;
}

</style>
                    </div>
                   
                    <?php echo form_close() ?>
                    <!-- end FORM-->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END CONTENT BODY -->
  </div>
  <!-- END CONTENT -->
  <!-- BEGIN QUICK SIDEBAR -->
  <!-- END QUICK SIDEBAR -->
</div>
<script>
                    $('INPUT[type="file"]').change(function () {
                    var ext = this.value.match(/\.(.+)$/)[1];
                    switch (ext) {
                    case 'jpg':
                    case 'jpeg':
                    case 'png':
                    case 'gif':
                    $('#userfile').attr('disabled', false);
                    break;
                    default:
                    alert('This is not an allowed file type.');
                    this.value = '';
                    }
                    });
                    </script>
<!-- END CONTAINER -->
<?php //$this->load->view ('footer');?>
