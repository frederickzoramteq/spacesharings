<style type="text/css">

.form-group::before, .form-group::after {
    content: "";
    display: block;
    clear: both;
}


</style>


<?php //$this->load->view ('header');?>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <?php $this->load->view ('supercontrol/leftbar');?>
                    <!-- END SIDEBAR MENU -->
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN THEME PANEL -->
                    
                    <!-- END THEME PANEL -->
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Admin Panel</span>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="<?php echo base_url()?>supercontrol/yacht/show_Yacht"><span>View Yacht</span></a>
                            </li>
                        </ul>
                        <div class="page-toolbar">
                            <div class="btn-group pull-right">
                                <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li>
                                        <a href="#">
                                            <i class="icon-bell"></i> Action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-shield"></i> Another action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-user"></i> Something else here</a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-bag"></i> Separated link</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_0">
                                        <div class="portlet box blue-hoki">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>View yacht</div>
                                                    
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"> </a>
                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                    <a href="javascript:;" class="reload"> </a>
                                                    <a href="javascript:;" class="remove"> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                            
                                                <!-- BEGIN FORM-->
											<?php
					  if($mulimg){
					  ?>
                      <label class="col-md-3 control-label">Yacht Images :</label><br>
                      <div id="controls" style="padding:8px;">
                    
                        <?php   foreach($mulimg as $j){?>
						
                        <div style="display:inline; padding:5px;"> 
                          <?php   if($j->apartment_image == ''){
									$pic = base_url()."img/nopro.jpg" ;
								  } else{
								$pic = base_url()."uploads/yacht_img/".$j->apartment_image ; } 
						?>
						
                          <img src="<?=$pic ?>" alt="Image loading.." style="height:150px; border:1px solid #000; border:2px solid #3300CC; padding:5px;" width="150px;"/></div>
					
                        <?php }  ?>
                      </div>
                      <?php }?>	 
                                       <?php foreach($viewyacht as $i){?>
                                                  <div class="form-body">
                                                  
                                                  
                                    
                      
                                   
                                   						<div class="form-group">
                                                            <label class="col-md-3 control-label">Yacht Title :</label>
                                                            <div class="col-md-4">
                                                                 <?php echo $i->Yacht_name; ?>
                                           
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Yacht Type :</label>
                                                            <div class="col-md-4">
                                                                 <?php echo $i->yacht_type_title; ?>
                                           
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Yacht Type :</label>
                                                            <div class="col-md-4">
                                                                 <?php echo $i->yacht_type_title; ?>
                                           
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Country :</label>
                                                            <div class="col-md-4">
                                                                 <?php  
																 		$result = $this->yacht_model->show_cntry_id($i->country_id); 
																		
																 		if(!empty($result[0]->country_name)):  echo $result[0]->country_name; else: echo "N/A"; endif;
																		
																 ?>
                                           						 
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">City :</label>
                                                            <div class="col-md-4">
                                                                 <?php echo $i->city_id; ?>
                                           
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Location :</label>
                                                            <div class="col-md-4">
                                                                 <?php echo $i->address; ?>
                                           
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                        
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Monthly Rent (For Rent):</label>
                                                            <div class="col-md-4">
                                                                 <?php if($i->monthly_rent!=''){echo $i->monthly_rent; } else{ echo "N/A";} ?>
                                           
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Maintenance Charge (For Rent):</label>
                                                            <div class="col-md-4">
                                                                 <?php if($i->maintenance_charge!=''){echo $i->maintenance_charge; } else{ echo "N/A";} ?>
                                           
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Available From (For Rent):</label>
                                                            <div class="col-md-4">
                                                                 <?php if($i->available_from!=''){echo $i->available_from; } else{ echo "N/A";} ?>
                                           
                                                            </div>
                                                        </div>
                                                        
                                                       
                                                        
                                                       
                                                       
                                                        
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Yacht Sqft :</label>
                                                            <div class="col-md-4">
                                                                 <?php if($i->yacht_sqft!=''){ echo $i->yacht_sqft; } else{ echo "N/A";}?>
                                           
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Floor :</label>
                                                            <div class="col-md-4">
                                                                 <?php if($i->floor!=''){ echo $i->floor; } else{ echo "N/A";}?>
                                           
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Map :</label>
                                                            <div class="col-md-4">
                                                                 <?php if($i->map!=''){ echo $i->map; } else{ echo "N/A";}?>
                                           
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Description :</label>
                                                            <div class="col-md-9">
                                                                 <?php echo $i->description?>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Status :</label>
                                                            <div class="col-md-4">
                                                                 <?php $st = $i->yacht_status;
														if($st=="1"){ echo "<b style='color:#5A9700'>Active</b>"; }else{
															echo "<b style='color:#DF6C33'>InActive</b>"; }
														?>
                                           
                                                            </div>
                                                        </div>
														
                                              
												<?php    }   ?>
                                                <!-- END FORM-->
                                                    <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            
            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
<?php //$this->load->view ('footer');?>
        