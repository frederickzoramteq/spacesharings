<div class="page-container">
  <div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
      <?php $this->load->view ('supercontrol/leftbar');?>
    </div>
  </div>
  <div class="page-content-wrapper">
    <div class="page-content">
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <li> <a href="<?php echo base_url(); ?>supercontrol/home">Home</a> <i class="fa fa-circle"></i> </li>
          <li> <span>Supercontrol Panel</span> </li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tabbable-line boxless tabbable-reversed">
            <div class="tab-content">
              <div class="tab-pane active" id="tab_0">
                <div class="portlet box blue-hoki">
                  <div class="portlet-title">
                    <div class="caption"> <i class="fa fa-gift"></i>Gallery edit</div>
                  </div>
                  <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <?php foreach($ecms as $i){?>
                    <form method="post" class="form-horizontal form-bordered" action="<?php echo base_url().'supercontrol/gallery/edit_gallery '?>" enctype="multipart/form-data" onsubmit="return Submit()">
                      <div class="form-body">
                        <input type="hidden" name="gallery_id" value="<?=$i->id;?>">
                        <input type="hidden" name="old_file" value="<?=$i->gallery_image;?>">
                        
                        <div class="form-group">
                          <label class="control-label col-md-3"> Gallery Category</label>
                          <div class="col-md-8">
                              <select class="form-control" name="catname" id="catname">
                                <option value="">Select</option>
                              <?php foreach($category as $cat){?> 
                              	<option value="<?php echo $cat->catid?>" <?php if($cat->catid==$i->catname){?> selected="selected" <?php }?>><?php echo $cat->category_name ?></option>
                              <?php }?>
                              </select>
                            <label id="errorBox"></label>
                          </div>
                        </div>
                        
                        <div class="form-group last">
                          <label class="control-label col-md-3">Image</label>
                          <div class="col-md-9">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                              <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                <?php if($i->gallery_image==''){ ?>
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                <?php }else{?>
                                <img src="<?php echo base_url()?>uploads/gallery/<?php echo $i->gallery_image;?>" alt="" />
                                <?php }?>
                              </div>
                              <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                              <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
								<?php
                                $file = array('type' => 'file','name' => 'userfile','id'=>'chkimg','onchange'=>'checkFile(this);');
                                echo form_input($file);
                                ?>
                                </span> <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                            </div>
                            <div class="clearfix margin-top-10"> <span class="label label-danger">Format</span> jpg, jpeg, png, gif </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Gallery Name</label>
                          <div class="col-md-5">
                            <input type="text" class="form-control" id="gallery_name" value="<?php echo $i->gallery_name;?>" name="gallery_name" onkeyup="leftTrim(this)" />
                            <label id="errorBox"></label>
                          </div>
                        </div>
                      </div>
                      <div class="form-actions">
                        <div class="row">
                          <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn red" id="submit" value="Submit" name="update">
                            <a href="<?php echo base_url();?>gallery/show_gallery">
                            <button type="button" class="btn default">Cancel</button>
                            </a> </div>
                        </div>
                      </div>
                    </form>
                    <?php }?>
                    <!-- END FORM-->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
	function Submit(){
	if($("#gallery_name").val() == "" ){
	$("#gallery_name").focus();
	$("#errorBox").html("Please Enter Gallery Name");
	return false;
	}else{
	$("#errorBox").html("");  
	}
	
	}
	
	function leftTrim(element){
	if(element)
	element.value=element.value.replace(/^\s+/,"");
	}
	
	$('INPUT[type="file"]').change(function () {
	// alert("jjjjj");
	var ext = this.value.match(/\.(.+)$/)[1];
	switch (ext) {
	case 'jpg':
	case 'jpeg':
	case 'png':
	case 'gif':
	$('#chkimg').attr('disabled', false);
	break;
	default:
	alert('This is not an allowed file type.');
	this.value = '';
	}
	});
</script>
<style>
#errorBox{
 color:#F00;
 }
 
 #errorBox1{
 color:#F00;
 }
 
 #errorBox2{
 color:#F00;
 }
</style>