<style type="text/css">
.alert {
    padding: 0;
}
	hr, p {
    margin: 12px 0 12px 10px;
}
button.close {
    margin-top: 5px;
    padding: 10px;
}
</style>
<body class="login">
        <div class="menu-toggler sidebar-toggler"></div>
        <!-- END SIDEBAR TOGGLER BUTTON -->
        <!-- BEGIN LOGO -->
        <div class="logo" style="padding-bottom:0;">
        <!--<h3 style="color:#ffffff;text-shadow:2px 2px 3px #000; font-size:23px; font-weight:300;"><?//=strtoupper(PROJECT_NAME)?></h3>-->
        <!--<h3 class="login-heading" style="color:#fff; font-weight:900;"><?php echo $title;?> Admin Panel</h3>-->
        <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
       <style>
@font-face {
  font-family: 'GreatVibes-Regular';
  src: url('<?=base_url();?>userpanel/fonts/GreatVibes-Regular.eot?#iefix') format('embedded-opentype'),
  url('<?=base_url();?>userpanel/fonts/GreatVibes-Regular.otf')  format('opentype'),
	     url('<?=base_url();?>userpanel/fonts/GreatVibes-Regular.woff') format('woff'),
	     url('<?=base_url();?>userpanel/fonts/GreatVibes-Regular.ttf')  format('truetype'),
	     url('<?=base_url();?>userpanel/fonts/GreatVibes-Regular.svg#GreatVibes-Regular') format('svg');
  font-weight: normal;
  font-style: normal;
}	   
	   
	   .logo {
    color: #ebc477 !important;
    font-family: "GreatVibes-Regular" !important;
    font-size: 50px !important;
    text-align: center !important;
    text-transform: capitalize !important;
	padding:0px !important;
}
.logo a {
    color: #f99d2d !important;
    text-decoration: none !important;
}
.login .content {
  margin: 17px auto 10px !important;
}
	   </style>
        <div class="logo"><a href="#">Spacesharing</a></div>
            <a><!--<img src="assets/pages/img/logo-big.png" alt="" />--> <?php //echo $title;?></a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content" style="background-color: #f6dab8 !important;">
            <!-- BEGIN LOGIN FORM -->
            
   	
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
         <?php echo form_open('supercontrol/main/login_validation'); ?>
                <h3 class="form-title font-green" style="color:#000 !important;"><i class="fa fa-user"></i> Login to your account</h3>
                <!--<div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter any username and password. </span>
                </div>-->
                 <?php if(validation_errors()){?>
                <div class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                      <span> <?php echo validation_errors(); ?>  </span>
                </div>
                <?php }?>
                <?php 
				if(@$error!=''){
				?>
                <div class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span> <?php echo @$error; ?>  </span>
                </div>
                <?php }?>
                
               <?php if($this->session->flashdata('loginerror')!=''){?>
                        <div class="alert alert-error" style="background-color:#F0959B; padding:10px;">
                        <button type="button" class="close" data-dismiss="alert">&#10006;</button>
                        <strong style="color:#900;"><?php echo @$this->session->flashdata('loginerror');?></strong> 
                        </div>
               <?php }?>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" /> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> </div>
                     <div>&nbsp;</div>
                
                <div class="create-account">
                	<div class="form-group2" style="text-align:left; margin-left:12%">
                        <input type="submit" class="btn green uppercase" style="background-color:#f6dab8 !important; color:#000 !important; border-color:#000;" value="Login" id="submitButton">
                    </div>
                </div>
          <?php echo form_close(); ?>
            
            <!-- END REGISTRATION FORM -->
        </div>
      
