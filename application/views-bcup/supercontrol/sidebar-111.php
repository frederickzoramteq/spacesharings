<?php 
$nl=$this->uri->segment(1); 
 $nl2=$this->uri->segment(2); 
?>

<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
  <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
  <li class="sidebar-toggler-wrapper hide"> 
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    <div class="sidebar-toggler"> </div>
    <!-- END SIDEBAR TOGGLER BUTTON --> 
  </li>
  <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
  <li class="sidebar-search-wrapper"> 
    <!-- BEGIN RESPONSIVE QUICK SEARCH FORM --> 
    <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box --> 
    <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box --> 
    <!--        <form class="sidebar-search  " action="http://www.keenthemes.com/preview/metronic/theme/supercontrol_1/page_general_search_3.html" method="POST">
                                <a href="javascript:;" class="remove">
                                    <i class="icon-close"></i>
                                </a>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <a href="javascript:;" class="btn submit">
                                            <i class="icon-magnifier"></i>
                                        </a>
                                    </span>
                                </div>
                            </form>--> 
    <!-- END RESPONSIVE QUICK SEARCH FORM --> 
  </li>
  
  <li class="nav-item start "> <a href="<?php echo base_url()?>user/dashboard" class="nav-link "> <span class="title">View Dashboard</span> </a>
    <ul class="sub-menu">
      <li class="nav-item start "> <a href="index.html" class="nav-link "> <i class="icon-bar-chart"></i> <span class="title">View Dashboard</span> </a> </li>
    </ul>
  </li>
  <li class="heading">
    <h3 class="uppercase">Features</h3>
  </li>
  
  <!--Supratim Sen (CMS Section Start- 20/04/2016)-->
  <li class="nav-item <?php if($nl=="cms"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">CMS Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item  <?php if($nl=="cms" && $nl2!="view_addcms" ){?>active open<?php }?>"> <a href="<?php echo base_url()?>cms/show_cms" class="nav-link "> <span class="title">Manage CMS Page</span> </a> </li>
      <!--<li class="nav-item  <?php if($nl2=="view_addcms"){?>active open<?php }?>"> <a href="<?php echo base_url()?>cms/view_addcms" class="nav-link "> <span class="title">Add Page</span> </a> </li>-->
    </ul>
  </li>
  
  <!--Supratim Sen (CMS Section End- 20/04/2016)-->
  <li class="nav-item  <?php if($nl=="banner"){?>active open<?php  }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-diamond"></i> <span class="title">Banner Management</span> <span class="arrow"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item <?php if($nl2 == "Add Banner"){?>active open<?php }?>"> <a href="<?php echo base_url(); ?>banner/addbanner" class="nav-link "> <span class="title">Add Banner</span> </a> </li>
      <li class="nav-item <?php if($nl2 == "Show Banner"){?>active open<?php }?>"> <a href="<?php echo base_url(); ?>banner/showbanner" class="nav-link "> <span class="title">Show Banner</span> </a> </li>
    </ul>
  </li>
  
  <li class="nav-item <?php if($nl=="gallery"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">Gallery Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item  <?php if($nl=="show_gallery"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>gallery/show_gallery" class="nav-link "> <span class="title">Manage Gallery</span> </a> </li>
      <li class="nav-item  <?php if($nl2=="addform"){?>active open<?php }?>"> <a href="<?php echo base_url();?>gallery/addform" class="nav-link "> <span class="title">Add Gallery</span> </a> </li>
    </ul>
  </li>
  
  <!--<li class="nav-item <?php if($nl=="blog"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">Blog Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item  <?php if($nl=="show_blog"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>blog/show_blog" class="nav-link "> <span class="title">Manage Blog</span> </a> </li>
      <li class="nav-item  <?php if($nl2=="blog_add_form"){?>active open<?php }?>"> <a href="<?php echo base_url();?>blog/blog_add_form" class="nav-link "> <span class="title">Add Blog</span> </a> </li>
    </ul>
  </li>-->
  
  <li class="nav-item <?php if($nl=="news"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">News Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">
    
    <li class="nav-item  <?php if($nl2=="news_add_form"){?>active open<?php }?>"> <a href="<?php echo base_url();?>news/news_add_form" class="nav-link "> <span class="title">Add News</span> </a> </li>
    
      <li class="nav-item  <?php if($nl=="show_news"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>news/show_news" class="nav-link "> <span class="title">Manage News</span> </a> </li>
      
    </ul>
  </li>
  
    <!--<li class="nav-item <?php if($nl=="testimonial"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">Testimonial Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item  <?php if($nl=="show_testimonial"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>testimonial/show_testimonial" class="nav-link "> <span class="title">Manage Testimonial</span> </a> </li>
      <li class="nav-item  <?php if($nl2=="testmonial_add_form"){?>active open<?php }?>"> <a href="<?php echo base_url();?>testimonial/testmonial_add_form" class="nav-link "> <span class="title">Add Testimonial</span> </a> </li>
    </ul>
  </li>-->
  
  
    <!--<li class="nav-item <?php if($nl=="team"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">Team Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item  <?php if($nl=="show_team"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>team/show_team" class="nav-link "> <span class="title">Manage Team Member</span> </a> </li>
      <li class="nav-item  <?php if($nl2=="team_add_form"){?>active open<?php }?>"> <a href="<?php echo base_url();?>team/team_add_form" class="nav-link "> <span class="title">Add Team Member</span> </a> </li>
    </ul>
  </li>-->  
   <li class="nav-item <?php if($nl=="contact"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">Contact Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item  <?php if($nl=="show_contact"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>contact/show_contact" class="nav-link "> <span class="title">Manage Contact</span> </a> </li>
      
    </ul>
  </li> 
  
 <!--<li class="nav-item <?php if($nl=="newsletter"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">News Letter Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item  <?php if($nl=="show_newsletter"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>newsletter/show_newsletter" class="nav-link "> <span class="title">Manage News Letter</span> </a> </li>
      
    </ul>
  </li>--> 
  
   <li class="nav-item <?php if($nl=="supercontrolprofile" ||$nl=="settings" ||$nl=="user" ){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">Tools</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item  <?php if($nl=="show_supercontrolprofile_id"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>supercontrolprofile/show_supercontrolprofile_id/1" class="nav-link "> <span class="title">supercontrol profile</span> </a> </li>
      <li class="nav-item  <?php if($nl=="show_settings_id"){?>active open<?php }?>"> <a href="<?php echo base_url();?>settings/show_settings_id/1" class="nav-link "> <span class="title">Settings</span> </a> </li>
      
       <li class="nav-item  <?php if($nl=="show_reset_pass"){?>active open<?php }?>"> <a href="<?php echo base_url();?>user/show_reset_pass/1" class="nav-link "> <span class="title">Change Password</span> </a> </li>
       
       <li class="nav-item  <?php if($nl=="logout"){?>active open<?php }?>"> <a href="<?php echo base_url(); ?>user/logout" class="nav-link "> <span class="title">Log out</span> </a> </li>  
</ul>
  </li> 
  
  
</ul>
