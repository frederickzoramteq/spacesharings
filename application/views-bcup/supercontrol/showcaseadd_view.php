<?php //$this->load->view ('header');?>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.8.2.js"></script>
<script type="text/javascript">
$(function() {
setTimeout(function() { $("#testdiv").fadeOut(1500); }, 5000)
$('#btnclick').click(function() {
$('#testdiv').show();
setTimeout(function() { $("#testdiv").fadeOut(1500); }, 5000)
})
})
</script>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <?php $this->load->view ('supercontrol/leftbar');?>
                    <!-- END SIDEBAR MENU -->
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN THEME PANEL -->
                    
                    <!-- END THEME PANEL -->
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>supercontrol panel</span>
                            </li>
                        </ul>
                        <div class="page-toolbar">
                            <div class="btn-group pull-right">
                                <!--<button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>-->
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li>
                                        <a href="#">
                                            <i class="icon-bell"></i> Action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-shield"></i> Another action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-user"></i> Something else here</a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-bag"></i> Separated link</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                    
					 <?php if (isset($success_msg)) { echo $success_msg; } ?>
                    <div class="row">
                   
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_0">
                                        <div class="portlet box blue-hoki">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Add showcase</div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"> </a>
                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                    <a href="javascript:;" class="reload"> </a>
                                                    <a href="javascript:;" class="remove"> </a>
                                                </div>
                                            </div>
            <div class="portlet-body form">
          	<form action="<?php echo base_url().'supercontrol/showcase/add_showcase' ?>" class="form-horizontal form-bordered" method="post" enctype="multipart/form-data">
            <div class="form-body">
            
            
            <!--<div class="form-group">
            <label class="control-label col-md-3">Showcase Image</label>
            <div class="col-md-8">
            <?php
            $file = array('id' => 'showcase', 'type' => 'file','name' => 'userfile', 'onchange' => 'readURL(this);' );
            echo form_input($file);
            ?>
           <span class="text-danger"><?php if (isset($error)) { echo $error; } ?></span>
            </div>
            </div>-->
            
            <div class="form-group last">
                                                <label class="control-label col-md-3">Image</label>
                                                <div class="col-md-9">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                   
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                          
                                                            </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <!--<input type="file" name="...">-->
																	<?php
                                                                    $file = array('id' => 'showcase', 'type' => 'file','name' => 'userfile', 'onchange' => 'readURL(this);' );
                                                                    echo form_input($file);
                                                                    ?> 
                                                            </span>
                                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix margin-top-10">
                                                        <span class="label label-danger">Format</span> jpg, jpeg, png, gif </div>
                                                </div>
                                            </div>
            
            
            <div class="form-group">
            <label class="control-label col-md-3"> Heading</label>
            <div class="col-md-8">
            <?php echo form_input(array('id' => 'showcase_heading', 'name' => 'showcase_heading','class'=>'form-control' )); ?>
            <?php echo form_error('showcase_heading'); ?>
            </div>
            </div>
            
            <div class="form-group">
            <label class="control-label col-md-3">Sub Heading</label>
            <div class="col-md-8">
            <?php echo form_input(array('id' => 'showcase_sub_heading', 'name' => 'showcase_sub_heading','class'=>'form-control' )); ?>
            <?php echo form_error('showcase_sub_heading'); ?>
            </div>
            </div>
            
            <div class="form-group">
            <label class="control-label col-md-3">Description</label>
            <div class="col-md-8">
            <?php echo form_textarea(array('id' => 'pagedes', 'name' => 'showcase_description','class'=>'form-control ' )); ?>
            <?php echo form_error('showcase_description'); ?>
            </div>
            </div>
            
            
            </div>
            <div class="form-actions">
            <div class="row">
            <div class="col-md-offset-3 col-md-9">
            <?php echo form_submit(array('id' => 'submit', 'name' => 'Submit' , 'value' => 'Submit' ,'class'=>'btn red')); ?>
            <button type="button" class="btn default">Cancel</button>
            </div>
             
            </div>
            </div>
            </form>
			<script>
            $('INPUT[type="file"]').change(function () {
            var ext = this.value.match(/\.(.+)$/)[1];
            switch (ext) {
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'gif':
            $('#showcase').attr('disabled', false);
            break;
            default:
            alert('This is not an allowed file type.');
            this.value = '';
            }
            });
            </script>
            <!-- END FORM-->
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <!-- END QUICK SIDEBAR -->
            </div>
        <!-- END CONTAINER -->
<?php //$this->load->view ('footer');?>
        