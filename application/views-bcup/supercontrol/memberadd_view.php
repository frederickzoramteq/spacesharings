<?php $this->load->view ('header');?>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.8.2.js"></script>
<script type="text/javascript">
  $(function() {
    setTimeout(function() { $("#testdiv").fadeOut(1500); }, 5000)
    $('#btnclick').click(function() {
      $('#testdiv').show();
      setTimeout(function() { $("#testdiv").fadeOut(1500); }, 5000)
    })
  })
</script>
<!-- BEGIN CONTAINER -->

<div class="page-container"> 
  <!-- BEGIN SIDEBAR -->
  <div class="page-sidebar-wrapper"> 
    <!-- BEGIN SIDEBAR --> 
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse"> 
      <!-- BEGIN SIDEBAR MENU --> 
      <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
      <?php $this->load->view ('supercontrol/leftbar');?>
      <!-- END SIDEBAR MENU --> 
      <!-- END SIDEBAR MENU --> 
    </div>
    <!-- END SIDEBAR --> 
  </div>
  <!-- END SIDEBAR --> 
  <!-- BEGIN CONTENT -->
  <div class="page-content-wrapper"> 
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content"> 
      <!-- BEGIN PAGE BAR -->
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <li> <a href="">Home</a> <i class="fa fa-circle"></i> </li>
          <li> <span>supercontrol panel</span> </li>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <!--<button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions <i class="fa fa-angle-down"></i> </button>-->
            <ul class="dropdown-menu pull-right" role="menu">
              <li> <a href="#"> <i class="icon-bell"></i> Action</a> </li>
              <li> <a href="#"> <i class="icon-shield"></i> Another action</a> </li>
              <li> <a href="#"> <i class="icon-user"></i> Something else here</a> </li>
              <li class="divider"> </li>
              <li> <a href="#"> <i class="icon-bag"></i> Separated link</a> </li>
            </ul>
          </div>
        </div>
      </div>
      <!-- END PAGE BAR --> 
      <!-- END PAGE HEADER-->
      <?php if (isset($success_msg)) { echo $success_msg; } ?>
      <div class="row">
        <div class="col-md-12">
          <div class="tabbable-line boxless tabbable-reversed">
            <div class="tab-content">
              <div class="tab-pane active" id="tab_0">
                <div class="portlet box blue-hoki">
                  <div class="portlet-title">
                    <div class="caption">
                      <i class="fa fa-gift"></i>Add Member
                    </div>
                    <div class="tools">
                      <a href="javascript:;" class="collapse"> </a>
                      <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                      <a href="javascript:;" class="reload"> </a>
                      <a href="javascript:;" class="remove"> </a>
                    </div>
                  </div>
                  <div class="portlet-body form">
                      <?php
                      echo form_open_multipart('supercontrol/Member/add_member','class="form-horizontal form-bordered"');
                      ?>
                      <div class="form-body">
                        <div class="form-group last">
                          <label class="control-label col-md-3">Profile Image</label>
                          <div class="col-md-9">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                              <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> 
                                <img src="https://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                              </div>
                              <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                              </div>
                              <div>
                                <span class="btn default btn-file">
                                  <span class="fileinput-new"> Select image </span>
                                  <span class="fileinput-exists"> Change </span>
                                  <?php
                                  $file = array('id' => 'banner', 'type' => 'file','name' => 'userfile', 'onchange' => 'readURL(this);' );
                                  echo form_input($file);
                                  ?>
                                </span>
                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                              </div>
                            </div>
                            <div class="clearfix margin-top-10"> <span class="label label-danger">Format</span> jpg, jpeg, png, gif </div>
                          </div>
                          <div class="col-md-3">
                            <?php if(isset($upload_error)) echo $upload_error ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">First Name</label>
                          <div class="col-md-5"> 
                            <?php echo form_input(array('id' => 'first_name', 'name' => 'first_name','class'=>'form-control', 'value' => set_value('first_name') )); ?>
                          </div>
                          <div class="col-md-4">
                            <?php echo form_error('first_name'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Last Name</label>
                          <div class="col-md-5"> 
                            <?php echo form_input(array('id' => 'last_name', 'name' => 'last_name','class'=>'form-control', 'value' => set_value('last_name') )); ?>
                          </div>
                          <div class="col-md-4">
                            <?php echo form_error('last_name'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Email</label>
                          <div class="col-md-5">
                            <?php echo form_input(array('id' => 'email', 'name' => 'email','class'=>'form-control', 'value' => set_value('email') )); ?>
                          </div>
                          <div class="col-md-4">
                            <?php echo form_error('email'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Phone</label>
                          <div class="col-md-5">
                            <?php echo form_input(array('id' => 'phone', 'name' => 'phone','class'=>'form-control', 'value' => set_value('phone') )); ?>
                          </div>
                          <div class="col-md-4">
                            <?php echo form_error('phone'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Password</label>
                          <div class="col-md-5">
                            <input type="password" name="password" class="form-control" id="password" />
                          </div>
                          <div class="col-md-4">
                            <?php echo form_error('password'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Gender</label>
                          <div class="col-md-5">
                            <select name="gender" id="gender" class="form-control">
                              <option value="male">male</option>
                              <option value="female">female</option>
                            </select>
                          </div>
                          <div class="col-md-4">
                            <?php echo form_error('gender'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Country</label>
                          <div class="col-md-5">
                            <select class="form-control"  name="country" id="country" onChange="getstatedetails(this.value)">
                              <option value="">Select Country</option>
                              <?php foreach($country as $c){?>
                              <option value="<?=$c->count_id;?>"><?=$c->count_name;?></option>
                              <?php }?>
                            </select>
                          </div>
                          <!-- <div class="col-md-4">
                            <?php echo form_error('country'); ?>
                          </div> -->
                        </div>
                        <div id="sub"></div>
                        <div id="sub1"></div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Postcode</label>
                          <div class="col-md-5">
                            <?php echo form_input(array('id' => 'postcode', 'name' => 'postcode','class'=>'form-control', 'value' => set_value('postcode') )); ?>
                          </div>
                          <div class="col-md-4">
                            <?php echo form_error('postcode'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Date of Birth</label>
                          <div class="col-md-5" style="margin-left:-15px;">
                            <div class="col-md-4">
                              <select class="form-control" id="day" name="day" onchange="date_check()" required>
                                <option>Date</option>
                                <?php for($j=1; $j<=31; $j++){ ?>
                                <option value="<?php echo $j  ?>"><?php echo $j   ?></option>
                                <?php } ?>
                              </select>
                            </div>
                            <div class="col-md-4">
                              <?php
                              $options = array(
                                ''    =>'Month',
                                '01'         => 'January',
                                '02'           => 'February',
                                '03'           => 'March',
                                '04'           => 'April',
                                '05'           => 'May',
                                '06'           => 'June',
                                '07'           => 'July',
                                '08'           => 'August',
                                '09'           => 'September',
                                '10'           => 'October',
                                '11'           => 'November',
                                '12'           => 'December',
                              );
                              echo form_dropdown('month', $options,'', 'class="form-control" id="month" name="month" onchange="date_check()" ');
                              ?>
                            </div>
                            <div class="col-md-4">
                              <select class="form-control" id="yeara" name="year" onchange="date_check()">
                                <option value="">Year</option>
                                <?php for ($i=1920; $i<2018; $i++){  ?>
                                <option value="<?php echo $i  ?>"><?php echo $i ?></option>
                                <?php } ?>
                              </select>
                            </div>
                            <p id="date_error" style="color: red;" ></p>
                            <?php echo form_error('day'); ?><?php echo form_error('month'); ?><?php echo form_error('year'); ?>
                          </div>
                        </div>
                      </div>
                      <div class="form-actions">
                        <div class="row">
                          <div class="col-md-offset-3 col-md-9"> 
                            <?php echo form_submit(array('id' => 'submit', 'name' => 'Submit' , 'value' => 'Submit' ,'class'=>'btn red')); ?>
                            <button type="button" class="btn default">Cancel</button>
                          </div>
                        </div>
                      </div>
                      <?= form_close(); ?>
                      <script>
                        function date_check() {
                          var month= $('#month').val();
                          var date= $('#day').val();
                          var yeara= $('#yeara').val();
                          var html='';
                          $('#date_error').empty();
                          if((month==2)|(month==4)|(month==6)|(month==9)|(month==11)&& (date==31)  )
                          {
                            $('#date_error').empty();
                            html='';
                            html="Error: Selected Month Does not have this Date. Please Correct it.";
                            $('#date_error').append(html);
                          }
                          if(month==2 && (date == 30 | date==31) ){

                            $('#date_error').empty();
                            html='';
                            html="Error: February month do not have 30th and 31st date. Please Correct it."
                            $('#date_error').append(html);
                          }
                          if(yeara !=null) {
                            if ((month == 2 & date > 28) && yeara % 4 != 0) {
                              $('#date_error').empty();
                              html='';
                              html="Error: 29th day of February is only available in Leap years. Please Correct it."
                              $('#date_error').append(html);
                            }
                          }
                        }
                      </script>
                      <script>
                        $('INPUT[type="file"]').change(function () {
                          var ext = this.value.match(/\.(.+)$/)[1];
                          switch (ext) {
                            case 'jpg':
                            case 'jpeg':
                            case 'JPG':
                            case 'png':
                            case 'gif':
                            $('#banner').attr('disabled', false);
                            break;
                            default:
                            alert('This is not an allowed file type.');
                            this.value = '';
                          }
                        });
                      </script>
                      <script>
                        function getstatedetails(id)
                        {
                          $.ajax({
                            type: "POST",
                            url: '<?php echo base_url().'supercontrol/Member/ajax_state_list'.'/';?>'+id,
                            data: id='cat_id',
                            success: function(data){
                              $('#sub').html(data);
                            },
                          });
                        }

                        function getcitydetails(id)
                        {
                          $.ajax({
                            type: "POST",
                            url: '<?php echo base_url().'supercontrol/Member/ajax_city_list'.'/';?>'+id,
                            data: id='cat_id',
                            success: function(data){
                              $('#sub1').html(data);
                            },
                          });
                        }
                      </script>
                      <!-- END FORM-->
                  </div>
 </div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- END CONTENT BODY --> 
</div>
<!-- END CONTENT --> 
<!-- BEGIN QUICK SIDEBAR --> 
<!-- END QUICK SIDEBAR --> 
</div>
<!-- END CONTAINER -->
<?php //$this->load->view ('footer');?>
