
<?php //include "header.php";?>
<div class="property_view_wrapper">
  <div class="container">
  
  
  
          <div class="row">
          <div class="col-lg-12">
            <div class="rs_heading_wrapper">
              <div class="rs_heading">
                <div class="search_form">
                  <div class="col-md-6 col-md-offset-3">
                    <div class="row">
                      <div class="col-md-12">
                         <div>
                          <h3>Browse Rental Requests</h3>
                        </div>
                    
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
		  </div>
        <div class="row">
          <form method="POST" action="">
          <div class="col-md-10 col-md-offset-1">
                             
            <div class="col-md-3">
            <div class="form-group">
            <select id="countryId" name="country" class="orderby form-control select2 countries select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                          <option value="">Select Country</option>
             
              <option value="38">Canada</option><option value="44">China</option><option value="101">India</option><option value="109">Japan</option><option value="230">United Kingdom</option><option value="231">United States</option></select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 200px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-countryId-container"><span class="select2-selection__rendered" id="select2-countryId-container" title="Select Country">Select Country</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
            </div>
            </div>

         
            <div class="col-md-3">
            <div class="form-group">
           
            <select name="state" class="orderby form-control select2 required states select2-hidden-accessible" id="stateId" tabindex="-1" aria-hidden="true">
                                <option value="">Select State</option>
                </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 200px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-stateId-container"><span class="select2-selection__rendered" id="select2-stateId-container" title="Select State">Select State</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
            </div>
            </div>
            
            <!-- --------------------------city---------------------------- -->      
               <div class="col-md-3">
            <div class="form-group">
             
              <select name="city" class="orderby form-control select2 required cities select2-hidden-accessible" id="cityId" tabindex="-1" aria-hidden="true">
                                <option value="">Select City</option>
              </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 200px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-cityId-container"><span class="select2-selection__rendered" id="select2-cityId-container" title="Select City">Select City</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
            </div>
            </div>
            <!-- --------------------------/city---------------------------- -->     <div class="col-md-3">
            <div class="form-group">
              <div class="btn_wrapper">
           <input name="reqs_btn_list" class="btn rs_btn" id="reqs_btn_list" value="Search" type="submit">
            </div>
            </div>
            </div>
            <!-- --------------------------/city---------------------------- -->
        </div>
        </form>
  </div>

 <div class="row">
   <div class="col-md-10 col-md-offset-1">
      <div class="grid_view1 animated fadeIn" style="display: block;">
                           <div class="property_wrapper">
            <div class="property_detail">
                  <ul style="background:#0363a7;">
                <li>Posted By – Lester Wolf</li>

              <!--   <li><i class="fa fa-envelope"></i>pyhades@hotmail.com</li> -->

                <li><i class="fa fa-calendar"></i>January 15, 2018</li>

                 <!--                                      -->
                                  <li <b=""> Country :   United Kingdom</li>
                
                  
                 
              </ul>
              <div class="property_content">
                <a href="javascript:void(0);"><h5>Obcaecati autem officiis et fuga Corporis tenetur ut delectus eu nostrud deserunt quam est</h5></a>
                <p> 
                  </p><p>Deserunt quasi rerum impedit dolorem</p>
              <p></p>
              </div>
            </div>
			<div class="property_detail">
                  <ul style="background:#0363a7;">
                <li>Posted By – Lester Wolf</li>

              <!--   <li><i class="fa fa-envelope"></i>pyhades@hotmail.com</li> -->

                <li><i class="fa fa-calendar"></i>January 15, 2018</li>

                 <!--                                      -->
                                  <li <b=""> Country :   United Kingdom</li>
                
                  
                 
              </ul>
              <div class="property_content">
                <a href="javascript:void(0);"><h5>Obcaecati autem officiis et fuga Corporis tenetur ut delectus eu nostrud deserunt quam est</h5></a>
                <p> 
                  </p><p>Deserunt quasi rerum impedit dolorem</p>
              <p></p>
              </div>
            </div>
			<div class="property_detail">
                  <ul style="background:#0363a7;">
                <li>Posted By – Lester Wolf</li>

              <!--   <li><i class="fa fa-envelope"></i>pyhades@hotmail.com</li> -->

                <li><i class="fa fa-calendar"></i>January 15, 2018</li>

                 <!--                                      -->
                                  <li <b=""> Country :   United Kingdom</li>
                
                  
                 
              </ul>
              <div class="property_content">
                <a href="javascript:void(0);" style="color:white;"><h5>Obcaecati autem officiis et fuga Corporis tenetur ut delectus eu nostrud deserunt quam est</h5></a>
                <p> 
                  </p><p>Deserunt quasi rerum impedit dolorem</p>
              <p></p>
              </div>
            </div>
			<div class="property_detail">
                  <ul style="background:#0363a7;">
                <li>Posted By – Lester Wolf</li>

              <!--   <li><i class="fa fa-envelope"></i>pyhades@hotmail.com</li> -->

                <li><i class="fa fa-calendar"></i>January 15, 2018</li>

                 <!--                                      -->
                                  <li <b=""> Country :   United Kingdom</li>
                
                  
                 
              </ul>
              <div class="property_content">
                <a href="javascript:void(0);" style="color:white;"><h5>Obcaecati autem officiis et fuga Corporis tenetur ut delectus eu nostrud deserunt quam est</h5></a>
                <p> 
                  </p><p>Deserunt quasi rerum impedit dolorem</p>
              <p></p>
              </div>
            </div>
			<div class="property_detail">
                  <ul style="background:#0363a7;">
                <li>Posted By – Lester Wolf</li>

              <!--   <li><i class="fa fa-envelope"></i>pyhades@hotmail.com</li> -->

                <li><i class="fa fa-calendar"></i>January 15, 2018</li>

                 <!--                                      -->
                                  <li <b=""> Country :   United Kingdom</li>
                
                  
                 
              </ul>
              <div class="property_content">
                <a href="javascript:void(0);" style="color:white;"><h5>Obcaecati autem officiis et fuga Corporis tenetur ut delectus eu nostrud deserunt quam est</h5></a>
                <p> 
                  </p><p>Deserunt quasi rerum impedit dolorem</p>
              <p></p>
              </div>
            </div>
             <hr>
       </div>  

        <!-- Modal -->
         
   
        
        <!-- Modal -->



        <!-- Modal -->
        
        <!-- Modal -->


             

        <!-- Modal -->
        
        <!-- Modal -->


             

        <!-- Modal -->
        
        <!-- Modal -->


                 

        <!-- Modal -->
       
        <!-- Modal -->


               
        <!-- Modal -->
         
        <!-- Modal -->


                     </div>
    </div>
  </div>
</div>
</div>
<?php // include "footer.php";?>