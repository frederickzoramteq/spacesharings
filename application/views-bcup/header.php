<!DOCTYPE html>

<html lang="en">
<?php 
if($this->session->userdata('is_user_id')!='') 
{
//$this->session->set_userdata('is_user_id',$uid);
	$userid=$this->session->userdata('is_user_id');
    // echo $userid;
}
?>
<head>
	<meta charset="utf-8" />
	<title>space share</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta name="description"  content="space share"/>
	<meta name="keywords" content="space share">
	<meta name="author"  content=""/>
	<meta name="MobileOptimized" content="320">
	<link href="https://fonts.googleapis.com/css?family=Montserrat%7cRaleway:400,500%7cPoppins%7cSource+Sans+Pro%7cPoppins%7cSource+Sans+Pro" rel="stylesheet" />

	<!-- Font Awesome -->
	<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<!--CSS Start-->
	<link href="<?php echo base_url('assets/css/main.css'); ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url('assets/css/style5.css');?>" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="plugins/select2/select2.min.css" />
	<!--CSS End-->
	<!-- favicon links-->
	<link rel="icon" type="image/icon" href="favicon.png" />
</head>
<!--Head End-->
<!--Body Start-->
<body>


	<!-- Main menu Start -->
	<div class="menu_wrapper">
		<div class="container">
			<div class="">
				<div class="navbar_header">
					<div class="logo"><a class="navbar_brand" href=""><img src="<?php echo base_url('assets/images/Space-Sharings-01.png');?>" alt="">
					</a></div>
				</div>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="">
				<div class="menu_toggle"><span></span><span></span><span></span></div>
				<div class="real_menu">
					<div class="close_btn"></div>
					<div class="menu_overlay"></div>
					<ul class="nav navbar-nav navbar-right">
						<li><a href="<?php echo base_url('Home');?>">home</a>

						</li>
						<li><a href="<?=base_url('Page/register_page');?>">Register</a></li>
						<li><i class="flaticon-arrow483"></i><a href="#">Host & Earn</a>
							<ul class="sub-menu">
								<li><a href="<?php echo base_url('Page/basic_hosting');?>"> Basic Hosting</a></li>
								<li>
									<?php 
									if($this->session->userdata('is_user_id')!='') 
									{

										$userid=$this->session->userdata('is_user_id');

										?>
										<!-- <a href="<?php echo base_url('Page/AdvanceSearch');?>">Advanced Hosting</a> -->
										<?= anchor('Page/AdvanceSearch', 'Advanced Hosting'); ?>
									</li>

										<?php
									}

									else
									{

										?>

										<a href="<?php echo base_url('Page/user_login');?>">Advanced Hosting</a></li>
										<?php
									}
									?>			
								</ul>
							</li>
							<li><i class="flaticon-arrow483"></i><a href="#">Search Rental</a>
								<ul class="sub-menu">
									<li><a href="<?php echo base_url('Page/basic_search');?>">Basic Search</a></li>
									<li><a href="<?php echo base_url('Page/adv_search');?>">Advanced Search</a></li>

								</ul>
							</li>
							<li><i class="flaticon-arrow483"></i><a href="#">Rental Requests</a>
								<ul class="sub-menu">
									<li><a href="<?php echo base_url('Page/post_request');?>">Post Your Requests</a></li>
									<li><a href="<?php echo base_url('Page/browse_request');?>">Browse Requests</a></li>

								</ul>
							</li>


							<li><a href="<?php echo base_url('Page/contact');?>">contact us</a></li>
                        
						<li>

						<?php 
									if($this->session->userdata('is_user_id')!='') 
									{

										$userid=$this->session->userdata('is_user_id');

										?>
										
										<?= anchor('Auth/logout', 'Logout'); ?>
									
</li>
										<?php
									}

									else
									{

										?>

										<?= anchor('Page/user_login', 'Login'); ?>
											
										</li>
										<?php
									}
									?>	
						</ul>
					</div>
				</div>
			</div>
		</div>
<!-- Main menu End -->