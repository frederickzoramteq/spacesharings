<!-- Footer Wrapper Start -->

<div class="suscribe_wrapper" data-stellar-background-ratio=".5">
	<div class="banner_overlay">
		<div class="container">
			<div class="row">
				<div class="suscribe_content">
					<div class="col-lg-8 col-md-8">
						<div class="suscribe_content_wrapper">
							<div class="news_detail"><h4>Subscribe Newsletter</h4><p>Get weekly updates of hot properties</p></div>
							<div class="news_search_wrapper"><input type="text" class="form-control search_input" placeholder="Enter your email address"><a href="#" class="btn search_btn">subscribe</a></div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4">
						<div class="social_wrapper">
							<ul>
								<li><a href="#"><i class="flaticon-facebook55"></i></a></li>
								<li><a href="#"><i class="flaticon-twitter1"></i></a></li>
								<li><a href="#"><i class="flaticon-google116"></i></a></li>
								<li><a href="#"><i class="flaticon-pinterest34"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="footer_wrapper">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-3">
				<div class="widget widget_text">
					<h4 class="widget-title">about us</h4>
					<p>We are a group seeking to bring people together who wants to host paying guests and those looking for a homely touch away from home. It also about creating bonds and friendships while making good use of already existing resources and earning extra income . </p>
					<!--<ul>
						<li><i class="flaticon-phone41"></i><span>+1 (234) 567 890</span></li>
						<li><i class="flaticon-mail80"></i><span>mail@popothemes.com</span></li>
						<li><i class="flaticon-pin56"></i><span>829/B, Pranik Chambers, Saki Vihar Road, 400072</span></li>
					</ul>-->
				</div>
			</div>
			<div class="col-lg-3 col-md-3">
				<div class="widget widget_twitter_feed">
					<h4 class="widget-title">Contact Us</h4>
					<ul>
						<li><i class="flaticon-phone41"></i><span>+1 (720) 982 9212</span></li>
						<li><i class="flaticon-mail80"></i><span>sharespaceinfo@gmail.com
</span></li>
						<li><i class="flaticon-pin56"></i><span>
Highlands Ranch, CO, USA</span></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-3 col-md-3">
				<div class="widget widget_flickr">
					<h4 class="widget-title">recent properties</h4>
					<ul>
						<li><a href="property_detail1.html"><img src="<?php echo base_url('assets/images/rp1.jpg');?>" alt=""></a></li>
						<li><a href="property_detail1.html"><img src="<?php echo base_url('assets/images/rp2.jpg');?>" alt=""></a></li>
						<li><a href="property_detail1.html"><img src="<?php echo base_url('assets/images/rp3.jpg');?>" alt=""></a></li>
						<li><a href="property_detail1.html"><img src="<?php echo base_url('assets/images/rp4.jpg');?>" alt=""></a></li>
						<li><a href="property_detail1.html"><img src="<?php echo base_url('assets/images/rp5.jpg');?>" alt=""></a></li>
						<li><a href="property_detail1.html"><img src="<?php echo base_url('assets/images/rp6.jpg');?>" alt=""></a></li>
						<li><a href="property_detail1.html"><img src="<?php echo base_url('assets/images/rp7.jpg');?>" alt=""></a></li>
						<li><a href="property_detail1.html"><img src="<?php echo base_url('assets/images/rp8.jpg');?>" alt=""></a></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-3 col-md-3">
				<div class="widget widget_recent_property">
					<h4 class="widget-title">recent properties</h4>
					<div class="tagcloud"><a href="#">Popo Homes</a><a href="#">interior</a><a href="#">property</a><a href="#">hot properties</a><a href="#">rental homes</a><a href="#">office for sale</a><a href="#">properties in uk</a><a href="#">buy home</a><a href="#">private villa with garden</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Footer Wrapper End -->

<!-- BottomFooter Wrapper Start -->
<footer class="bottom_footer_wrapper">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="bottom_footer">
					<ul>
						<li><a href="#">© Copyright 2016 spacesharings.com</a></li>
						<li><a href="#">Terms &amp; Conditions</a></li>
						<li><a href="#">Privacy Policy</a></li>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- BottomFooter Wrapper End -->

<!--main js file start--> 
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-2.2.4.min.js');?>"></script>
<script>
    $(document).ready(function() {
        $("#country").on('change', function() {
                $.ajax({
                url: '<?= base_url("AjaxController/fetch_state")?>',
                type: 'POST',
                data: {data: $(this).val()},
                success: function(data){
                  // alert(data);
                  console.log(data);
                  $("#state").html(data);
              }
          })
            
        });
    });
</script>
<script>
    $(document).ready(function() {
        $("#state").on('change',function(){
        $.ajax({
           url:'<?= base_url("AjaxController/fetch_city")?>',
           type:'POST',
           data:{data: $(this).val() },
           success: function(data){
              //alert(data);
              console.log(data);
              $("#city").html(data);
          }

      })

    });



  });
</script>>



<!--Plugin-->
<script type="text/javascript" src="<?php echo base_url('assets/js/modernizr.custom.js');?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/plugin/smoothscroll/smoothscroll.js');?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/plugin/animate/wow.js');?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/plugin/parallax/stellar.js');?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/plugin/owl/owl.carousel.js');?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/plugin/priceslider/bootstrap-slider.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/custom.js');?>"></script> 
<script src="<?php echo base_url('plugins/select2/select2.full.min.js');?>"></script>

</body>

</html>