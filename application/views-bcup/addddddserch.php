<div class="col-lg-3 col-md-4">
      <div class="sidebar_wrapper">
      <div class="widget advance_search_wrapper">   
        <div class="widget-title"><h4>advance search</h4></div>
        <div class="search_form">
          <form method="POST" action="">
            <div class="form-group">
              <label>search by keyword</label>
              <!--  -->
              <input type="text" name="keyword" class="" placeholder="enter your keywords">
            </div>
            <!--<div class="form-group">
              <label>property location</label>
              <select name="city" class="orderby form-control select2">
                <option value="" selected="selected">City</option>
                                 <option value="1">1</option>
                                <option value="12">12</option>
                                <option value="25175">25175</option>
                                <option value="2707">2707</option>
                                <option value="301">301</option>
                                <option value="305">305</option>
                                <option value="41">41</option>
                                <option value="42702">42702</option>
                                <option value="43383">43383</option>
                                <option value="43386">43386</option>
                                <option value="43397">43397</option>
                                <option value="43408">43408</option>
                                <option value="43425">43425</option>
                                <option value="45">45</option>
                                <option value="46267">46267</option>
                                <option value="46505">46505</option>
                                <option value="46519">46519</option>
                                <option value="5318">5318</option>
                                <option value="5370">5370</option>
                                <option value="5465">5465</option>
                                <option value="5583">5583</option>
                                <option value="5909">5909</option>
                                <option value="5910">5910</option>
                                <option value="5916">5916</option>
                                <option value="596">596</option>
                                <option value="6">6</option>
                                <option value="6026">6026</option>
                                <option value="6087">6087</option>
                                <option value="6414">6414</option>
                                <option value="7">7</option>
                                <option value="707">707</option>
                                <option value="8083">8083</option>
                                <option value="91">91</option>
                                <option value="Ahmedabad">Ahmedabad</option>
                                <option value="Asansol">Asansol</option>
                                <option value="delhi">delhi</option>
                                <option value="DGP">DGP</option>
                                <option value="Durgapur">Durgapur</option>
                                <option value="Patna">Patna</option>
                                <option value="washington">washington</option>
                                <option value="Wasingtone">Wasingtone</option>
                              </select>
            </div>-->
            
            
            <!-- -------------------country---------------------- -->
            <div class="form-group">
              <label>Country</label>

            <select id="countryId" name="country" class="orderby form-control select2 countries select2-hidden-accessible" tabindex="-1" aria-hidden="true">
              
                          
            <option value="231">Select Country</option>
                          <!-- <option value="231">United States</option>-->
              <option value="38">Canada</option><option value="44">China</option><option value="101">India</option><option value="109">Japan</option><option value="230">United Kingdom</option><option value="231">United States</option></select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 221px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-countryId-container"><span class="select2-selection__rendered" id="select2-countryId-container" title="United States">United States</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
            </div>
            <!-- -------------------state---------------------- -->
            <div class="form-group">
              <label>State</label>
            <select name="state" class="orderby form-control select2 required states select2-hidden-accessible" id="stateId" tabindex="-1" aria-hidden="true">
                                <option value="">Select State</option>
                </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 221px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-stateId-container"><span class="select2-selection__rendered" id="select2-stateId-container" title="Select State">Select State</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
            </div>
            <!-- --------------------------city---------------------------- -->      
               
            <div class="form-group">
              <label>City</label>
              <select name="city" class="orderby form-control select2 required cities select2-hidden-accessible" id="cityId" tabindex="-1" aria-hidden="true">
                                <option value="">Select City</option>
              </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 221px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-cityId-container"><span class="select2-selection__rendered" id="select2-cityId-container" title="Select City">Select City</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
            </div>
            <!-- --------------------------/city---------------------------- -->
            
            
            
            <div class="form-group">
              <label>Zip Code</label>
              <select name="zip" class="orderby form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                <option value="" selected="selected">Any</option>
                                <option value="1">1</option>
                              <option value="10101">10101</option>
                              <option value="10728">10728</option>
                              <option value="12345">12345</option>
                              <option value="123456">123456</option>
                              <option value="12345674">12345674</option>
                              <option value="123456788">123456788</option>
                              <option value="12365">12365</option>
                              <option value="1236548">1236548</option>
                              <option value="124321">124321</option>
                              <option value="12517">12517</option>
                              <option value="1253">1253</option>
                              <option value="13004">13004</option>
                              <option value="15641">15641</option>
                              <option value="2">2</option>
                              <option value="20951">20951</option>
                              <option value="2345678">2345678</option>
                              <option value="26185">26185</option>
                              <option value="27492">27492</option>
                              <option value="3">3</option>
                              <option value="34324">34324</option>
                              <option value="3434">3434</option>
                              <option value="3455">3455</option>
                              <option value="400072">400072</option>
                              <option value="41021">41021</option>
                              <option value="43973">43973</option>
                              <option value="452011">452011</option>
                              <option value="54345">54345</option>
                              <option value="546567">546567</option>
                              <option value="61252">61252</option>
                              <option value="6768">6768</option>
                              <option value="712584">712584</option>
                              <option value="713045">713045</option>
                              <option value="713148">713148</option>
                              <option value="713169">713169</option>
                              <option value="713207">713207</option>
                              <option value="713213">713213</option>
                              <option value="713245">713245</option>
                              <option value="713256">713256</option>
                              <option value="713269">713269</option>
                              <option value="714651">714651</option>
                              <option value="72140">72140</option>
                              <option value="77375">77375</option>
                              <option value="77379">77379</option>
                              <option value="77388">77388</option>
                              <option value="77453">77453</option>
                              <option value="77865">77865</option>
                              <option value="78945">78945</option>
                              <option value="789456">789456</option>
                              <option value="80124">80124</option>
                              <option value="80126">80126</option>
                              <option value="80161">80161</option>
                              <option value="80211">80211</option>
                              <option value="80226">80226</option>
                              <option value="80840">80840</option>
                              <option value="90165">90165</option>
                              <option value="96651">96651</option>
                              <option value="98001">98001</option>
                              <option value="98002">98002</option>
                              <option value="98008">98008</option>
                              <option value="98009">98009</option>
                                                <!-- <option value="rating">Sort by average rating</option>
                <option value="date">Sort by newness</option>
                <option value="price">Sort by price: low to high</option> -->
              </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 221px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-zip-mq-container"><span class="select2-selection__rendered" id="select2-zip-mq-container" title="Any">Any</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
            </div>
               
            <!-- <div class="form-group">
              <label>Country</label>
              <select name="country" class="orderby form-control select2">
                <option value="" selected="selected">Any</option>
                                <option value="1">1</option>
                                  <option value="101">101</option>
                                  <option value="110">110</option>
                                  <option value="111">111</option>
                                  <option value="122">122</option>
                                  <option value="139">139</option>
                                  <option value="2">2</option>
                                  <option value="216">216</option>
                                  <option value="222">222</option>
                                  <option value="23">23</option>
                                  <option value="231">231</option>
                                  <option value="3">3</option>
                                  <option value="31">31</option>
                                  <option value="4">4</option>
                                  <option value="45">45</option>
                                  <option value="56">56</option>
                                  <option value="78">78</option>
                                  <option value="81">81</option>
                                  <option value="90">90</option>
                                  <option value="91">91</option>
                                  <option value="94">94</option>
                                  <option value="India">India</option>
                                  <option value="inf">inf</option>
                                  <option value="United Kingdom">United Kingdom</option>
                                  <option value="United States">United States</option>
                                </select>
            </div> -->

            
            <p style="font-size: 17px;color: #435061;margin-top: 20px;margin-bottom: 10px;
              border-bottom: 1px solid;">Preferred Criteria of Host</p>
            <div class="row">
              <div class="col-lg-12 col-md-12 form-group">
                <label>Gender</label>
                <select name="gender" class="orderby form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                     <option value="">Any</option>
                  <option value="Male">Male</option>
                  <option value="Female">Female</option>
                  <option value="Others">Others</option>
                </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 221px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-gender-yl-container"><span class="select2-selection__rendered" id="select2-gender-yl-container" title="Any">Any</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
              </div>
              <div class="col-lg-12 col-md-12 form-group">
                <label>Age Group</label>
                <select name="adgegroup" class="orderby form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                  <option value="">Any</option>
                  <option value="16–20">16–20</option>

                        <option value="21-30">21-30</option>

                        <option value="31-40">31-40</option>
                        <option value="41–60">41–60</option>
                        <option value="60+">60+</option>
                        <option value="Any of the above">Any of the above</option>
                </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 221px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-adgegroup-0l-container"><span class="select2-selection__rendered" id="select2-adgegroup-0l-container" title="Any">Any</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12 form-group">
                <label>Nationality </label>
                <select name="nationality" class="orderby form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                    <option value="">Any</option>
                  <option value="El Salvador">El Salvador </option>
                  <option value="Honduras">Honduras</option>
                  <option value="Guatemala">Guatemala</option>
                  <option value="India">India</option>
                  <option value="Ecuador">Ecuador</option>
                </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 221px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-nationality-7x-container"><span class="select2-selection__rendered" id="select2-nationality-7x-container" title="Any">Any</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
              </div>
              <div class="col-lg-12 col-md-12 form-group">
                <label>Ethnicity</label>
                <select name="ethnicity" class="orderby form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                    <option value="">Any</option>
                  <option value="Caucasian or White">Caucasian or White</option>
                  <option value="African or Black">African or Black</option>
                  <option value="East Asian">East Asian</option>
                  <option value="Middle Eastern">Middle Eastern</option>
                  <option value="Latino">Latino</option>
                  <option value="Chinese">Chinese</option>
                  <option value="Spanish ">Spanish </option>
                  <option value="Indian ">Indian </option>
                </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 221px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-ethnicity-ae-container"><span class="select2-selection__rendered" id="select2-ethnicity-ae-container" title="Any">Any</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
              </div>
            </div>
                        
                        <div class="row">
              <div class="col-lg-12 col-md-12 form-group">
                <label>Speaks Languages </label>
                <select name="hosts_languages" class="orderby form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                  <option value="English">English</option>

                <option value="Spanish">Spanish</option>

                <option value="Portuguese">Portuguese</option>

                <option value="French">French</option>

                <option value="Chinese(Mandarin)">Chinese(Mandarin)</option>

                <option value="Chinese(Traditional)">Chinese(Traditional)</option>

                <option value="Japanese">Japanese</option>

                <option value="Hindi">Hindi</option>

                <option value="Bengali">Bengali</option>

                <option value="Telegu">Telegu</option>

                <option value="Tamil">Tamil</option>

                <option value="Marathi">Marathi</option>

                <option value="Punjabi">Punjabi</option>

                <option value="Russian">Russian</option>

                <option value="Arabic">Arabic</option>

                <option value="German">German</option>
                </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 221px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-hosts_languages-oy-container"><span class="select2-selection__rendered" id="select2-hosts_languages-oy-container" title="English">English</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
              </div>
              <div class="col-lg-12 col-md-12 form-group">
                <label>Status or category</label>
                <select name="renter_category" class="orderby form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                    <option value="">Any</option>
                  <option value="Single">Single</option>
                  <option value="Couple">Couple</option>
                  <option value="Family">Family</option>
                  <option value="Single Parent">Single Parent</option>
                  <option value="Others">Others</option>
                </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 221px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-renter_category-kn-container"><span class="select2-selection__rendered" id="select2-renter_category-kn-container" title="Any">Any</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
              </div>
            </div>
          <!--  <div class="price_slider">
              <span id="ex6CurrentSliderValLabel">price: <span id="priceSliderVal">$10,000</span></span>
              <input id="price" type="text" data-slider-min="1000" data-slider-max="50000" data-slider-step="1000" data-slider-value="10000"/>
            </div> -->
            <div class="btn_wrapper">
           <input type="submit" name="btn_submit" class="btn rs_btn" id="btn_submit1" value="Search">
            </div>
          </form>
        </div>
      </div>

      </div>
    </div>