<?php
$config = [
	'registerr'		=>	[
		[
			'field'	=>	'first_name',
			'label'	=>	'First Name',
			'rules'	=>	'required|alpha_dash|trim'
		],
		[
			'field'	=>	'last_name',
			'label'	=>	'Last Name',
			'rules'	=>	'required',
		],
	/*[
	'field'	=>	'address_1',
	'label'	=>	'Address ',
	'rules'	=>	'required',
	],
	[
	'field'	=>	'address_2',
	'label'	=>	'Address',
	'rules'	=>	'required',
	],
	[
	'field'	=>	'country',
	'label'	=>	'country',
	'rules'	=>	'required'
	],
	[
	'field'	=>	'state',
	'label'	=>	'State',
	'rules'	=>	'required',
	],
	[
	'field'	=>	'city',
	'label'	=>	'City',
	'rules'	=>	'required',
	],*/
	[
		'field'	=>	'postcode',
		'label'	=>	'Postcode',
		'rules'	=>	'required|integer|trim',
	],
	/*[
	'field'	=>	'phone',
	'label'	=>	'Phone',
	'rules'	=>	'required|alpha_dash|trim'
	],*/
	[
		'field'	=>	'email',
		'label'	=>	'Email',
		'rules'	=>	'trim|required|is_unique[users.email]',
	],
	[
		'field'	=>	'con_email',
		'label'	=>	'Re Email',
		'rules'	=>	'trim|required|matches[email]',
	],
	[
		'field'	=>	'password',
		'label'	=>	'Password',
		'rules'	=>	'required',
	],
	[
		'field'	=>	'con_pass',
		'label'	=>	'Re Password',
		'rules'	=>	'required|matches[password]',
	]
],
'basic_hostings'=>	[
	[
		'field'	=>	'fname',
		'label'	=>	'First Name',
		'rules'	=>	'required|alpha_dash|trim',
	],
	[
		'field'	=>	'lname',
		'label'	=>	'Last Name',
		'rules'	=>	'required',
	],
	[
		'field'	=>	'phone',
		'label'	=>	'Phone',
		'rules'	=>	'required'
	],
	[
		'field'	=>	'email',
		'label'	=>	'Email',
		'rules'	=>	'required'
	],
	[
		'field'	=>	'country',
		'label'	=>	'country',
		'rules'	=>	'required'
	],
	[
		'field'	=>	'state',
		'label'	=>	'State',
		'rules'	=>	'required',
	],
	[
		'field'	=>	'city',
		'label'	=>	'City',
		'rules'	=>	'required',
	],
	[
		'field'	=>	'postcode',
		'label'	=>	'Postcode',
		'rules'	=>	'required|integer|trim',
	],
	[
		'field'	=>	'title',
		'label'	=>	'title',
		'rules'	=>	'required',
	],
	
	[
		'field'	=>	'desc',
		'label'	=>	'description',
		'rules'	=>	'required',
	]
],

'advance_hostings'=>[
	[
		'field'	=>	'fname',
		'label'	=>	'First Name',
		'rules'	=>	'required'
	],
	[
		'field'	=>	'lname',
		'label'	=>	'Last Name',
		'rules'	=>	'required'
	],
	[
		'field'	=>	'phone',
		'label'	=>	'Phone',
		'rules'	=>	'required'
	],
	[
		'field'	=>	'email',
		'label'	=>	'Email',
		'rules'	=>	'required'
	],
	[
		'field'	=>	'title',
		'label'	=>	'Rental Title',
		'rules'	=>	'required'
	],
	[
		'field'	=>	'description',
		'label'	=>	'Rental Description',
		'rules'	=>	'required',
	],
	[
		'field'	=>	'address',
		'label'	=>	'Address ',
		'rules'	=>	'required',
	],
	[
		'field'	=>	'postcode',
		'label'	=>	'Postcode',
		'rules'	=>	'required|integer|trim',
	]
	
	
],



'post_request'	=>	[
	[
		'field'	=>	'name',
		'label'	=>	'Name',
		'rules'	=>	'required|alpha_dash|trim'
	],
	[
		'field'	=>	'email',
		'label'	=>	'Email',
		'rules'	=>	'required',
	],
	[
		'field'	=>	'subject',
		'label'	=>	'Subject',
		'rules'	=>	'required',
	],
	[
		'field'	=>	'message',
		'label'	=>	'Message',
		'rules'	=>	'required',
	]
],
'contact_rules' =>[
	[
		'field' => 'name',
		'label' => 'Your Name',
		'rules' => 'required'
	],
	[
		'field'	=>	'email',
		'label'	=>	'Email',
		'rules'	=>	'required',
	],
	[
		'field'	=>	'msg',
		'label'	=>	'Your Message',
		'rules'	=>	'required',
	]
],
'create_bots_rules' => [
	[
		'field' => 'bot_name',
		'label' => 'Bot Name',
		'rules' => 'required'
	],
	[
		'field' => 'bot_price',
		'label' => 'Bot Price',
		'rules' => 'required'
	],
	[
		'field' => 'author_name',
		'label' => 'Author Name',
		'rules' => 'required'
	],
	[
		'field' => 'bot_desc',
		'label' => 'Description',
		'rules' => 'required'
	],
],
'entertainer_rules' => [
	[
		'field' => 'user_name',
		'label' => 'User Name',
		'rules' => 'required'
	],
	[
		'field' => 'display_name',
		'label' => 'Display Name',
		'rules' => 'required'
	],
	[
		'field' => 'password',
		'label' => 'Password',
		'rules' => 'required'
	],
	[
		'field' => 'email_address',
		'label' => 'Email Address',
		'rules' => 'required|is_unique[users.email]'
	],
	[
		'field' => 'gender',
		'label' => 'Gender',
		'rules' => 'required'
	],
],
'change_pass' => [
	[
		'field'	=>	'new_pass',
		'label'	=>	'Password',
		'rules'	=>	'required',
	],
	[
		'field'	=>	'con_pass',
		'label'	=>	'Re Password',
		'rules'	=>	'required|matches[new_pass]',
	]
]
];